<?php


include( dirname( __FILE__ ) . '/wp-load.php' );

function Mail_creer_tables() {
   global $wpdb;
    $nom_table = $wpdb->prefix . 'devis_mails';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $nom_table (
      id int(20) NOT NULL auto_increment,
      reference_user varchar(50) ,
      reference_forwarder varchar(50) ,
      id_user int(11) ,
      id_forwarder int(11),
      mail_content text,
      status_mail INT(10) NOT NULL DEFAULT 0,
      mail_type varchar(50),
      
      PRIMARY KEY  (id)

   ) $charset_collate ;";
   require_once('wp-admin/includes/upgrade.php' );
   dbDelta( $sql );
   
    
}

function alter_tablem (){
    global $wpdb;
    $nom_table = $wpdb->prefix . 'devis_mails';
    $wpdb->query("ALTER TABLE $nom_table ADD lecture_message varchar(20) NOT NULL DEFAULT 'non_lu' after status_mail ");
    
}



alter_tablem();
//alter_table();


