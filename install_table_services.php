<?php


include( dirname( __FILE__ ) . '/wp-load.php' );

function Service_creer_tables() {
   global $wpdb;
    $nom_table = $wpdb->prefix . 'services';
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $nom_table (
      id bigint(20) unsigned NOT NULL auto_increment,
      reference varchar(50) NOT NULL,
      categories varchar(50) NOT NULL,
      date_creation  datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      express varchar(10) NOT NULL,
      pays_chargement varchar(200) NOT NULL,
      ville_chargement varchar(200) NOT NULL,
      adresse_chargement varchar(200) NOT NULL,
      num_tel_chargement varchar(200) NOT NULL,
      date_chargement datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      contrainte_chargement varchar(200) NOT NULL,
      pays_dechargement varchar(200) NOT NULL,
      ville_dechargement varchar(200) NOT NULL,
      adresse_dechargement varchar(200) NOT NULL,
      num_tel_dechargement varchar(200) NOT NULL,
      date_dechargement datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      contrainte_dechargement varchar(200) NOT NULL,
      accepter_sous_traitant varchar(10) NOT NULL,
      emballage varchar(50) NOT NULL,
      hauteur varchar(50) NOT NULL,
      poids varchar(50) NOT NULL,
      largeur varchar(50) NOT NULL,
      type_materiaux varchar(100) NOT NULL,
      matiere_dangereux varchar(10) NOT NULL,
      fragile varchar(10) NOT NULL,
      
      PRIMARY KEY  (id)

   ) $charset_collate ;";
   require_once('wp-admin/includes/upgrade.php' );
   dbDelta( $sql );
   
    
}

function alter_table (){
    global $wpdb;
    $nom_table = $wpdb->prefix . 'services';
    //$wpdb->query("ALTER TABLE $nom_table ADD id_client INT(10) NOT NULL after reference");
    // $wpdb->query("ALTER TABLE $nom_table CHANGE COLUMN `catégories` `categories` VARCHAR(250) NOT NULL");
    // $wpdb->query("ALTER TABLE $nom_table ADD status INT(10) NOT NULL  DEFAULT 0 after reference");
    // $wpdb->query("ALTER TABLE $nom_table ADD forwarder_attrib INT(10) NOT NULL  DEFAULT 0 after reference");
     //$wpdb->query("ALTER TABLE $nom_table ADD heure_desirer_chargement varchar(200) after date_chargement");
    // $wpdb->query("ALTER TABLE $nom_table ADD heure_desirer_dechargement varchar(200)  after date_dechargement");
     //$wpdb->query("ALTER TABLE $nom_table ADD longueur varchar(10)  after largeur");
     $wpdb->query("ALTER TABLE $nom_table ADD type_societe varchar(20)  after reference");
    //var_dump("ALTER TABLE $nom_table CHANGE COLUMN `catégories` `categories` VARCHAR(250) NOT NULL");
}



//Service_creer_tables();
alter_table();


