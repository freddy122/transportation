<?php
/*
Plugin Name: Catégorie service
Description: Liste des catégories des service
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/


global $db_version;
$db_version = '0.1'; 


function categ_install()
{
    global $wpdb;
    global $db_version;

    $table_name = $wpdb->prefix . 'category_service'; // do not forget about tables prefix
    
    $sql = "CREATE TABLE " . $table_name . " (
      id int(11) NOT NULL AUTO_INCREMENT,
      libelle_categ VARCHAR(200) NOT NULL,
      description_categ text NOT NULL,
      PRIMARY KEY  (id)
    );";
    
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

    //add_option('db_version', $db_version);
    
    $installed_ver = get_option('db_version');
    if ($installed_ver != $db_version) {
        $sql = "CREATE TABLE " . $table_name . " (
            id int(11) NOT NULL AUTO_INCREMENT,
            libelle_categ VARCHAR(200) NOT NULL,
            description_categ text NOT NULL,
            PRIMARY KEY  (id)
        );";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        //update_option('db_version', $db_version);
    }
}

register_activation_hook(__FILE__, 'categ_install');


function categ_install_data()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'category_service'; 
    $wpdb->insert($table_name, array(
        'libelle_categ' => 'plomberie',
        'description_categ' => 'plomberie'
    ));
    $wpdb->insert($table_name, array(
        'libelle_categ' => 'service de demenagement',
        'description_categ' => 'service de demenagement'
    ));
}
register_activation_hook(__FILE__, 'categ_install_data');


function update_db_check()
{
    global $db_version;
    if (get_site_option('db_version') != $db_version) {
        categ_install();
    }
}
add_action('plugins_loaded', 'update_db_check');


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Categories_Table extends WP_List_Table
{
    
    function __construct()
    {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'person',
            'plural' => 'persons',
        ));
    }
    
    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    function column_libelle_categ($item)
    {
        return '<em>' . $item['libelle_categ'] . '</em>';
    }
    
    function column_description_categ($item)
    {
      
        $actions = array(
            'edit' => sprintf('<a href="?page=categ_service&id=%s">%s</a>', $item['id'], __('Modifier', 'cltd_example')),
            'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('Supprimer', 'cltd_example')),
        );

        return sprintf('%s %s',
            $item['description_categ'],
            $this->row_actions($actions)
        );
    }

    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }

    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />', 
            'libelle_categ' => __('libelle_categ', 'cltd_example'),
            'description_categ' => __('description_categ', 'cltd_example')
        );
        return $columns;
    }
    
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'libelle_categ' => array('libelle_categ', true),
            'description_categ' => array('description_categ', false),
        );
        return $sortable_columns;
    }

    
    function get_bulk_actions()
    {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    
    function process_bulk_action()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'category_service'; 

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

   
    function prepare_items()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'category_service';
        $per_page = 5;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'libelle_categ';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';

        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);

        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}


function categ_admin_menu()
{
    add_menu_page(__('Categories Service', 'cltd_example'), __('Categories Service', 'cltd_example'), 'activate_plugins', 'persons', 'cltd_example_persons_page_handler');
    add_submenu_page('persons', __('Categories Service', 'cltd_example'), __('Categories Service', 'cltd_example'), 'activate_plugins', 'persons', 'cltd_example_persons_page_handler');
    add_submenu_page('persons', __('Ajouter', 'cltd_example'), __('Ajouter', 'cltd_example'), 'activate_plugins', 'categ_service', 'cltd_example_persons_form_page_handler');
}

add_action('admin_menu', 'categ_admin_menu');


function cltd_example_persons_page_handler()
{
    global $wpdb;

    $table = new Categories_Table();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'cltd_example'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Categorie service', 'cltd_example')?> 
            <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=categ_service');?>"><?php _e('Ajouter', 'cltd_example')?></a>
        </h2>
        <?php echo $message; ?>
        <form id="persons-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>
    </div>
<?php
}


function cltd_example_persons_form_page_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'category_service'; 
    $message = '';
    $notice = '';
   
    $default = array(
        'id' => 0,
        'libelle_categ' => '',
        'description_categ' => '',
    );

    if ( isset($_REQUEST['nonce']) && wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        $item = shortcode_atts($default, $_REQUEST);
        
        $item_valid = cltd_example_validate_person($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Enregistrement effectué', 'cltd_example');
                } else {
                    $notice = __('Erreur', 'cltd_example');
                }
            } else {
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                if ($result) {
                    $message = __('Modification effectué', 'cltd_example');
                } else {
                    $notice = __('Erreur', 'cltd_example');
                }
            }
        } else {
            $notice = $item_valid;
        }
    }
    else {
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Non vue', 'cltd_example');
            }
        }
    }
    
    add_meta_box('persons_form_meta_box', 'Person data', 'cltd_example_persons_form_meta_box_handler', 'person', 'normal', 'default');

    ?>
        <div class="wrap">
            <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
            <h2><?php _e('Categorie service', 'cltd_example')?> <a class="add-new-h2"
                                        href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=persons');?>"><?php _e('Retour', 'cltd_example')?></a>
            </h2>
            
            <?php if (!empty($notice)): ?>
            <div id="notice" class="error"><p><?php echo $notice ?></p></div>
            <?php endif;?>
            <?php if (!empty($message)): ?>
            <div id="message" class="updated"><p><?php echo $message ?></p></div>
            <?php endif;?>

            <form id="form" method="POST">
                <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
                <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
                <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

                <div class="metabox-holder" id="poststuff">
                    <div id="post-body">
                        <div id="post-body-content">
                            <?php /* And here we call our custom meta box */ ?>
                            <?php do_meta_boxes('person', 'normal', $item); ?>
                            <input type="submit" value="<?php _e('Enregistrer', 'cltd_example')?>" id="submit" class="button-primary" name="submit">
                        </div>
                    </div>
                </div>
            </form>
        </div>
<?php
}


function cltd_example_persons_form_meta_box_handler($item)
{
    ?>
        <table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
            <tbody>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="libelle_categ"><?php _e('libelle_categ', 'cltd_example')?></label>
                </th>
                <td>
                    <input id="name" name="libelle_categ" type="text" style="width: 95%" value="<?php echo esc_attr($item['libelle_categ'])?>"
                           size="50" class="code" placeholder="<?php _e('libelle_categ', 'cltd_example')?>" required>
                </td>
            </tr>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="description_categ"><?php _e('description_categ', 'cltd_example')?></label>
                </th>
                <td>
                    <input id="email" name="description_categ" type="text" style="width: 95%" value="<?php echo esc_attr($item['description_categ'])?>"
                           size="50" class="code" placeholder="<?php _e('description_categ', 'cltd_example')?>" required>
                </td>
            </tr>

            </tbody>
        </table>
<?php
}


function cltd_example_validate_person($item)
{
    $messages = array();

    if (empty($item['libelle_categ'])) $messages[] = __('libelle_categ is required', 'cltd_example');
   // if (!empty($item['email']) && !is_email($item['email'])) $messages[] = __('E-Mail is in wrong format', 'cltd_example');
    //if (!ctype_digit($item['age'])) $messages[] = __('Age in wrong format', 'cltd_example');
    
    if (empty($messages)) return true;
    return implode('<br />', $messages);
}


function cltd_example_languages()
{
    load_plugin_textdomain('cltd_example', false, dirname(plugin_basename(__FILE__)));
}

add_action('init', 'cltd_example_languages');

