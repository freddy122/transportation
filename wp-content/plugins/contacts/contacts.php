<?php
/*
Plugin Name: Page contacts
Description: Controle page contacts
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/

add_shortcode( 'contacts_page', 'afficher_contacts_page');

function afficher_contacts_page(){
    if(!is_admin()){
        ob_start();
        require dirname( __FILE__ ) . '/template/index.php';
        return ob_get_clean();
    }
}