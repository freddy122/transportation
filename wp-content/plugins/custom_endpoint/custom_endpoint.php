<?php
/**
 * Plugin Name: WC Custom Endpoint
 * Plugin URI:  http://stackoverflow.com/questions/38784599/woocommerce-custom-end-points
 * Description: A custom endpoint
 * Version:     0.1.0
 * Author:      Kathy Darling
 * Author URI:  http://kathyisawesome.com
 * Text Domain: wc_custom_endpoint
 * Domain Path: /languages
 * Requires at least: 4.6.0
 * Tested up to: 4.6.0  
 *
 * Copyright: © 2016 Kathy Darling.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */


/**
 * The Main WC_Custom_Endpoint class
 **/
if ( ! class_exists( 'WC_Custom_Endpoint' ) ) :

class WC_Custom_Endpoint {

    const VERSION = '0.1.0';

    /**
     * Custom endpoint name.
     */
    public static $endpoint = 'add_students_details';

    /**
     * @var WC_Custom_Endpoint - the single instance of the class
     * @since 0.1.0
     */
    protected static $instance = null;            

    /**
     * Plugin Directory
     *
     * @since 0.1.0
     * @var string $dir
     */
    public $dir = '';

    /**
     * Plugin URL
     *
     * @since 0.1.0
     * @var string $url
     */
    public $url = '';


    /**
     * Main WC_Custom_Endpoint Instance
     *
     * Ensures only one instance of WC_Custom_Endpoint is loaded or can be loaded.
     *
     * @static
     * @see WC_Custom_Endpoint()
     * @return WC_Custom_Endpoint - Main instance
     * @since 0.1.0
     */
    public static function instance() {
        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WC_Custom_Endpoint ) ) {
            self::$instance = new WC_Custom_Endpoint();
        }
        return self::$instance;
    }


    public function __construct(){

        $this->dir = plugin_dir_path(__FILE__);

        $this->url = plugin_dir_url(__FILE__);

        // Load translation files
        add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );

        // Actions used to insert a new endpoint in the WordPress.
        add_action( 'init', array( $this, 'add_endpoints' ) );
        add_filter( 'query_vars', array( $this, 'add_query_vars' ), 0 );

        // Insering your new tab/page into the My Account page.
        add_filter( 'woocommerce_account_menu_items', array( $this, 'new_menu_items' ) );
        add_action( 'woocommerce_endpoint_' . self::$endpoint .  '_title', array( $this, 'endpoint_title' ) );    
        add_action( 'woocommerce_account_' . self::$endpoint .  '_endpoint', array( $this, 'endpoint_content' ) );

    }


    /*-----------------------------------------------------------------------------------*/
    /* Localization */
    /*-----------------------------------------------------------------------------------*/


    /**
     * Make the plugin translation ready
     *
     * @return void
     * @since  1.0
     */
    public function load_plugin_textdomain() {
        load_plugin_textdomain( 'wc-custom-endpoint' , false , dirname( plugin_basename( __FILE__ ) ) .  '/languages/' );
    }

    /*-----------------------------------------------------------------------------------*/
    /* Endpoint */
    /*-----------------------------------------------------------------------------------*/

    /**
     * Register new endpoint to use inside My Account page.
     *
     * @see https://developer.wordpress.org/reference/functions/add_rewrite_endpoint/
     */

    public function add_endpoints() {
        add_rewrite_endpoint( self::$endpoint, EP_ROOT | EP_PAGES );
    }

    /**
     * Add new query var.
     *
     * @param array $vars
     * @return array
     */
    public function add_query_vars( $vars ) {
        $vars[] = self::$endpoint;

        return $vars;
    }


    /*-----------------------------------------------------------------------------------*/
    /* Display */
    /*-----------------------------------------------------------------------------------*/


    /**
     * Set endpoint title.
     *
     * @return string
     */
    public function endpoint_title() {

        return __( 'My Stuff', 'wc_custom_endpoint' );

    }

    /**
     * Insert the new endpoint into the My Account menu.
     *
     * @param array $items
     * @return array
     */
    public function new_menu_items( $items ) {
        // Remove the logout menu item.
        $logout = $items['customer-logout'];
        unset( $items['customer-logout'] );
        // Insert your custom endpoint.
        $items[ self::$endpoint ] = __( 'My Stuff', 'wc_custom_endpoint' );

        // Insert back the logout item.
        $items['customer-logout'] = $logout;

        return $items;
    }

    /**
     * Endpoint HTML content.
     */
    public function endpoint_content() { ?>
        <p>Hello World! - custom wc_get_template() can go here</p>

        <?php
    }

    /*-----------------------------------------------------------------------------------*/
    /* Activation */
    /*-----------------------------------------------------------------------------------*/

    /**
     * Plugin install action.
     * Flush rewrite rules to make our custom endpoint available.
     */
    public static function install() {
        WC_Custom_Endpoint()->add_endpoints();
        flush_rewrite_rules();
    }

    /**
     * Plugin install action.
     * Flush rewrite rules to make our custom endpoint available.
     */
    public static function uninstall() {
        flush_rewrite_rules();
    }

} //end class: do not remove or there will be no more guacamole for you

endif; // end class_exists check


/**
 * Returns the main instance of WC_Custom_Endpoint to prevent the need to use globals.
 *
 * @since  1.0
 * @return WC_Custom_Endpoint
 */
function WC_Custom_Endpoint() {
    return WC_Custom_Endpoint::instance();
}

// Launch the whole plugin
add_action( 'woocommerce_loaded', 'WC_Custom_Endpoint' );

// register activation hook
register_activation_hook( __FILE__, array( 'WC_Custom_Endpoint', 'install' ) );
register_deactivation_hook( __FILE__, array( 'WC_Custom_Endpoint', 'uninstall' ) );