<?php
/*
Plugin Name: Liste Services
Description: liste services 
Version: 1.0
Author: Orimbatosoa fréderic	
*/

define('KV_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define('KV jQuerydatatables', '1.0');

function kv_datatables_script_js() {
	wp_register_style('fred_de_style' , KV_PLUGIN_URL. 'css/jquery.dataTables.min.css');
	wp_enqueue_style('fred_de_style');	
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-datatable' ,  KV_PLUGIN_URL. 'js/jquery.dataTables.min.js',  array('jquery' ));	
?>
<script>  
	jQuery(document).ready(function() {	
		jQuery('#liste_service_table').DataTable();
	});
        function valider_demande_client(id_devis,status){
            var msg = "";
            if(status === 0){
                msg = "Aucun forwarder n'a pas encore effectuer une offre sur ce devis, voulez vous continuez";
            }else if(status === 1){
                msg = "Un forwarder a effectuer une offre sur ce devis mais le client n'a pas encore valider, voulez vous continuez";
            }else{
                msg = "Voulez vous confirmer ce dévis";
            }
            if(confirm(msg)){
                jQuery.ajax({
                    type: "POST",
                    url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
                    data: {action : 'envoie_confirmation_client',id_devis : id_devis},
                    success: function (res) {
                        if (res) {
                            //window.location.href = "<?php echo get_site_url();?>/wp-admin/admin.php?page=services";
                        }
                    }
                });
            }
            
        }
</script> 	
<?php		
	}
        
add_action('admin_head', 'kv_datatables_script_js');

add_action('wp_ajax_envoie_confirmation_client', 'envoie_confirmation_client');
add_action('wp_ajax_nopriv_envoie_confirmation_client', 'envoie_confirmation_client');
function envoie_confirmation_client() {
    var_dump($_POST['id_devis']);die();
}
add_action( 'admin_menu', 'wpse_91693_registers' );
function wpse_91693_registers()
{
    add_menu_page(
        'Services',     // page title
        'Services',     // menu title
        'manage_options',   // capability
        'services',     // menu slug
        'liste_services' // callback function
    );
}

function liste_services() { 
   global $wpdb;
   $querys45 = "SELECT * from ".$wpdb->prefix."services order by id desc";
   $alldatas45 = $wpdb->get_results($querys45, OBJECT);
	 ?>
    <h2>Gestion services</h2>
    <div style="width:98%">		
        <table cellpadding="0" cellspacing="0" border="0" class="display" id="liste_service_table" width="100%">
                <thead>
                        <tr>
                                <th>Réferences</th>
                                <th>Date création</th>
                                <th>Utilisateur</th>
                                <th>Status</th>
                                <th>Action</th>
                        </tr>
                </thead>
                <tbody>

                        <?php foreach($alldatas45 as $dats){ ?>
                        <tr class="gradeU">
                                <td><?php echo $dats->reference; ?></td>
                                <td><?php echo $dats->date_creation; ?></td>
                                <td><?php echo $dats->id_client; ?></td>
                                <td>
                                    <?php   if($dats->status == 0){
                                                echo "Dévis non traiter";
                                            }elseif($dats->status == 1){
                                                echo "Dévis en cours";
                                            }else{
                                                echo "Dévis valider";
                                            }
                                    
                                    ?>
                                </td>
                                <td class="center">
                                    <?php if($dats->status == 2){ ?>
                                    <button onclick="valider_demande_client(<?php echo $dats->id; ?>,<?php echo $dats->status; ?>)">
                                        Valider
                                    </button>
                                    <?php } ?>
                                </td>
                        </tr>
                        <?php } ?>
                </tbody>
        </table>
    </div>
<?php
}
function dt_plugin_settings_link($links) { 
    $settings_link = '<a href="admin.php?page=kv_jquery_datatables.php">Settings</a>'; 
    array_unshift($links, $settings_link); 
    return $links; 
    }   

$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'dt_plugin_settings_link');
?>