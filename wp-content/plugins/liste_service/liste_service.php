<?php

/*
Plugin Name: Liste services Ancien
Description: Liste des services Ancien
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Liste_service extends WP_List_Table
{
    
    function __construct()
    {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'service',
            'plural' => 'services',
        ));
    }
    
    function column_default($item, $column_name)
    {
        return $item[$column_name];
       
    }

    function column_reference($item)
    {
        return '<em>' . $item['reference'] . '</em>';
    }
    
    function column_date_creation($item)
    {
       
        $actions = array(
            'edit' => sprintf('<a href="?page=categ_services&id=%s">%s</a>', $item['id'], __('Modifier', 'cltd_example')),
            'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('Supprimer', 'cltd_example')),
        );

        /*return sprintf('%s %s',
            $item['date_creation'],
            $this->row_actions($actions,true)
        );*/
        return sprintf('%s ',
            $item['date_creation'] 
        );
    }

    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }

    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />', 
            'reference' => __('reference', 'cltd_example'),
            'categories' => __('catégorie', 'cltd_example'),
            'date_creation' => __('Date création', 'cltd_example')
        );
        return $columns;
    }
    
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'date_creation' => array('date_creation', true),
            'categories' => array('categories', false),
        );
        return $sortable_columns;
    }

    
    function get_bulk_actions()
    {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    
    function process_bulk_action()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'services'; 

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
    }

   
    function prepare_items()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'services';
        $per_page = 5;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';

        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);

        $this->set_pagination_args(array(
            'total_items' => $total_items, 
            'per_page' => $per_page, 
            'total_pages' => ceil($total_items / $per_page) 
        ));
    }
}


function categories_admin_menu()
{
    add_menu_page(__('Services', 'cltd_example'), __('Services', 'cltd_example'), 'activate_plugins', 'services', 'cltd_example_services_page_handler');
    add_submenu_page('services', __('Services', 'cltd_example'), __('Services', 'cltd_example'), 'activate_plugins', 'services', 'cltd_example_services_page_handler');
    add_submenu_page('services', __('Ajouter', 'cltd_example'), __('Ajouter', 'cltd_example'), 'activate_plugins', 'categ_services', 'services_form_page_handler');
}

add_action('admin_menu', 'categories_admin_menu');


function cltd_example_services_page_handler()
{
    global $wpdb;

    $table = new Liste_service();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Suppression effectué : %d', 'cltd_example'), count($_REQUEST['id'])) . '</p></div>';
    }
    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Services', 'cltd_example')?> 
            <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=categ_services');?>"><?php _e('Ajouter', 'cltd_example')?></a>
        </h2>
        <?php echo $message; ?>
        <form id="services-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>
    </div>
<?php
}


function services_form_page_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'services'; 
    $message = '';
    $notice = '';
    
    $default = array(
        'id' => 0,
        'reference' => '',
        'categories' => '',
        'date_creation' => '',
    );

    if ( isset($_REQUEST['nonce']) && wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        $item = shortcode_atts($default, $_REQUEST);
        
        $item_valid = validate_categories($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Enregistrement effectué', 'cltd_example');
                } else {
                    $notice = __('Erreur', 'cltd_example');
                }
            } else {
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                if ($result) {
                    $message = __('Modification effectué', 'cltd_example');
                } else {
                    $notice = __('Erreur', 'cltd_example');
                }
            }
        } else {
            $notice = $item_valid;
        }
    }
    else {
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Non vue', 'cltd_example');
            }
        }
    }
    
    add_meta_box('services_form_meta_box', 'Services', 'services_form_meta_box_handler', 'person', 'normal', 'default');

    ?>
        <div class="wrap">
            <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
            <h2><?php _e('Services', 'cltd_example')?> <a class="add-new-h2"
                                        href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=services');?>"><?php _e('Retour', 'cltd_example')?></a>
            </h2>
            
            <?php if (!empty($notice)): ?>
            <div id="notice" class="error"><p><?php echo $notice ?></p></div>
            <?php endif;?>
            <?php if (!empty($message)): ?>
            <div id="message" class="updated"><p><?php echo $message ?></p></div>
            <?php endif;?>

            <form id="form" method="POST">
                <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
                <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
                <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

                <div class="metabox-holder" id="poststuff">
                    <div id="post-body">
                        <div id="post-body-content">
                            <?php /* And here we call our custom meta box */ ?>
                            <?php do_meta_boxes('person', 'normal', $item); ?>
                            <input type="submit" value="<?php _e('Enregistrer', 'cltd_example')?>" id="submit" class="button-primary" name="submit">
                        </div>
                    </div>
                </div>
            </form>
        </div>
<?php
}


function services_form_meta_box_handler($item)
{
    ?>

        <table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
            <tbody>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="reference"><?php _e('reference', 'cltd_example')?></label>
                </th>
                <td>
                    <input id="name" name="reference" type="text" style="width: 95%" value="<?php echo esc_attr($item['reference'])?>"
                           size="50" class="code" placeholder="<?php _e('reference', 'cltd_example')?>" required readonly="readonly">
                </td>
            </tr>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="categories"><?php _e('categories', 'cltd_example')?></label>
                </th>
                <td>
                    <input id="name" name="categories" type="text" style="width: 95%" value="<?php echo esc_attr($item['categories'])?>"
                           size="50" class="code" placeholder="<?php _e('categories', 'cltd_example')?>" required>
                </td>
            </tr>
            <tr class="form-field">
                <th valign="top" scope="row">
                    <label for="date_creation"><?php _e('date_creation', 'cltd_example')?></label>
                </th>
                <td>
                    <input id="email" name="date_creation" type="text" style="width: 95%" value="<?php echo esc_attr($item['date_creation'])?>"
                           size="50" class="code" placeholder="<?php _e('date_creation', 'cltd_example')?>" required>
                </td>
            </tr>

            </tbody>
        </table>
<?php
}


function validate_categories($item)
{
    $messages = array();

    if (empty($item['categories'])) $messages[] = __('categories is required', 'cltd_example');
   // if (!empty($item['email']) && !is_email($item['email'])) $messages[] = __('E-Mail is in wrong format', 'cltd_example');
    //if (!ctype_digit($item['age'])) $messages[] = __('Age in wrong format', 'cltd_example');
    
    if (empty($messages)) return true;
    return implode('<br />', $messages);
}


function services_languages()
{
    load_plugin_textdomain('cltd_example', false, dirname(plugin_basename(__FILE__)));
}

add_action('init', 'services_languages');

