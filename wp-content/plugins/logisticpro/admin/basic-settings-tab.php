<?php 
	if( isset( $_POST['submit-basic-settings'] ) )
	{
		$basic_settings_data = ( json_encode( $_POST ) );   
		update_option("logisticpro-plugin-basic-settings", $basic_settings_data);			
	}
	
	
	$setting = json_decode(  get_option("logisticpro-plugin-basic-settings" ),  true );
?>

<div class="wrap">
  <?php screen_icon('themes'); ?>
  <h2>Front page elements</h2>
  <?php $list_of_pages = get_pages(); ?>
  <form method="POST">
    <table class="form-table">
      <tr valign="top">
        <th scope="row"> <label for="element-login-page"><?php echo __("Login Page: ", "logistic-pro");?></label>
        </th>
        <td><select class="element-login-page" name="logistic-pro-login-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-login-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select>
          </td>
          
      </tr>
     
      <tr valign="top">
        <th scope="row"> <label for="element-signup-page"><?php echo __("Signup Page: ", "logistic-pro");?></label>
        </th>
        <td><select class="element-signup-page" name="logistic-pro-signup-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-signup-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr valign="top">
        <th scope="row"> <label for="element-profile-page"><?php echo __("Profile Page: ", "logistic-pro");?></label>
        </th>
        <td><select class="element-profile-page" name="logistic-pro-profile-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-profile-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr valign="top">
        <th scope="row"> <label for="element-forgot-page"><?php echo __("Forgot Password", "logistic-pro");?></label>
        </th>
        <td><select class="element-forgot-page" name="logistic-pro-forgot-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-forgot-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr valign="top">
        <th scope="row"> <label for="element-resetpass-page"><?php echo __("Reset Password", "logistic-pro");?></label>
        </th>
        <td><select class="element-resetpass-page" name="logistic-pro-resetpass-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-resetpass-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr valign="top">
        <th scope="row"> <label for="element-listing-page"><?php echo __("Listing Page", "logistic-pro");?></label>
        </th>
        <td><select class="element-listing-page" name="logistic-pro-listing-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-listing-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr valign="top">
        <th scope="row"> <label for="element-order-page"><?php echo __("Order Page", "logistic-pro");?></label>
        </th>
        <td><select class="element-order-page" name="logistic-pro-order-page">
        	<option><?php echo __("Select Page", "logistic-pro");?></option>
            <?php foreach ($list_of_pages as $pages){ ?>
            <?php  $select = ''; if($setting['logistic-pro-order-page'] == $pages->ID ) {  $select = 'selected="selected"';  } ?>
            <option value="<?php echo esc_attr( $pages->ID ); ?>" <?php echo $select; ?>><?php echo esc_html( $pages->post_title ); ?></option>
            <?php } ?>
          </select></td>
      </tr>
      <tr valign="top">
        <td colspan="2" class="text-center"><input type="submit" name="submit-basic-settings" value="<?php echo __("Save/Update Settings", "logistic-pro");?>" class="button button-primary button-large"/></td>
      </tr>
    </table>
  </form>
</div>
