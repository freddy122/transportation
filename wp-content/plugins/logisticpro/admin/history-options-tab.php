<div class="wrap">
  <?php screen_icon('themes'); ?>
  </div>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#start_date").datepicker({
      		showButtonPanel: true
    	});
		$("#end_date").datepicker({
      		showButtonPanel: true
    	});
    });
	jQuery(window).on("load resize ", function() {
	  var scrollWidth = jQuery('.tbl-content').width() - jQuery('.tbl-content table').width();
	  jQuery('.tbl-header').css({'padding-right':scrollWidth});
	}).resize();	
</script>
<section class="cargo-history">
   <form method="POST" id="logisticpro-order-tab-form">
  <div  class="tbl-header">
 
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
      	
          <tr class="header-area">
          <th colspan="2"><h1><?php echo esc_html__("Order History", "logistic-pro");?></h1></th>
          <th>
          		<strong><?php echo esc_html__("Start Date", "logistic-pro");?></strong>
                <input type="text" id="start_date" name="start_date" class="form-control value input-size-50"  placeholder="<?php echo esc_attr__("Start Date", "logistic-pro");?>" value="" /></th>
          <th>
          		<strong><?php echo esc_html__("End Date", "logistic-pro");?></strong>
            	<input type="text" id="end_date" name="end_date" class="form-control value input-size-50" placeholder="<?php echo esc_html__("End Date", "logistic-pro");?>" value="" /></th>
          <th>
          		<br />
          		<input type="button" id="submit-for-order" name="submit_range" value="<?php echo esc_html__("Submit", "logistic-pro");?>" class="button button-primary button-large foodpro-loadmore"/>
          </th>
        </tr>
            
        <tr>
          <th><?php echo esc_html__("Cargo Name", "logistic-pro");?></th>
          <th><?php echo esc_html__("Sent From", "logistic-pro");?></th>
          <th><?php echo esc_html__("Sent To", "logistic-pro");?></th>
          <th><?php echo esc_html__("Status", "logistic-pro");?></th>
          <th><?php echo esc_html__("Actions", "logistic-pro");?></th>
        </tr>
      </thead>
    </table>
     
  </div>
  <div  class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody id="history-content-area">
        <tr>
          <td colspan="5" align="center"><?php echo esc_html__("Please select date range.", "logistic-pro");?></td>
        </tr>
      

      </tbody>
    </table>
  </div>
  </form>
</section>
