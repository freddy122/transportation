<?php //
if ( ! function_exists('logisticpro_shipments_func') ) {

// Register Custom Shipment
function logisticpro_shipments_func() {

	$labels = array(
		'name'                  => _x( 'Shipments', 'Shipment General Name', 'logisticpro' ),
		'singular_name'         => _x( 'Shipment', 'Shipment Singular Name', 'logisticpro' ),
		'menu_name'             => __( 'Shipments', 'logisticpro' ),
		'name_admin_bar'        => __( 'Shipment', 'logisticpro' ),
		'archives'              => __( 'Item Archives', 'logisticpro' ),
		'parent_item_colon'     => __( 'Parent Item:', 'logisticpro' ),
		'all_items'             => __( 'All Shipments', 'logisticpro' ),
		'add_new_item'          => __( 'Add New Shipment', 'logisticpro' ),
		'add_new'               => __( 'Add New Shipment', 'logisticpro' ),
		'new_item'              => __( 'New Shipment', 'logisticpro' ),
		'edit_item'             => __( 'Edit Shipment', 'logisticpro' ),
		'update_item'           => __( 'Update Shipment', 'logisticpro' ),
		'view_item'             => __( 'View Shipment', 'logisticpro' ),
		'search_items'          => __( 'Search Shipment', 'logisticpro' ),
		'not_found'             => __( 'Not found', 'logisticpro' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'logisticpro' ),
		'featured_image'        => __( 'Featured Image', 'logisticpro' ),
		'set_featured_image'    => __( 'Set featured image', 'logisticpro' ),
		'remove_featured_image' => __( 'Remove featured image', 'logisticpro' ),
		'use_featured_image'    => __( 'Use as featured image', 'logisticpro' ),
		'insert_into_item'      => __( 'Insert into item', 'logisticpro' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'logisticpro' ),
		'items_list'            => __( 'Shipments list', 'logisticpro' ),
		'items_list_navigation' => __( 'Shipments list navigation', 'logisticpro' ),
		'filter_items_list'     => __( 'Filter Shipments list', 'logisticpro' ),
	);

	
	$args = array(
		'label'                 => __( 'Shipment', 'logisticpro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'author', 'comments'),
		'taxonomies'            => array( 'shipment_type' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'shipment',
		'capability_type'       => 'page',
	);
	register_post_type( 'shipment', $args );

}


function logisticpro_cargo_price_func()
{
		global $post;
		wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );			
		$logisticpro_details = json_decode(  get_post_meta( get_the_ID(), '_logisticpro_all_fields', true), true );
	?>
<?php echo logisticproForm::fields('logisticpro_order_status', 'admin', '12', 'select', 'Order Status:', 'logistic-order-status'); ?>

<div class="logisticpro-div">
  <div class="form-group">
    <label><?php echo __("Pick Up", "logistic-pro");?></label>
    <br />
    <select class="country form-control" name="logisticpro_is_pickup" required>
      <option value=""><?php echo __("Select Option", "logistic-pro");?></option>
      <option value="1" <?php if( isset( $logisticpro_details['logisticpro_is_pickup'] ) && $logisticpro_details['logisticpro_is_pickup'] == 1){ echo esc_attr('selected="selected"'); }?>> <?php echo __("Yes", "logistic-pro");?></option>
      <option value="0" <?php if( isset( $logisticpro_details['logisticpro_is_pickup'] ) && $logisticpro_details['logisticpro_is_pickup'] == 0){ echo esc_attr('selected="selected"'); }?>><?php echo __("No", "logistic-pro");?></option>
    </select>
  </div>
</div>
<div class="logisticpro-div">
  <div class="form-group">
    <label><?php echo __("Price", "logistic-pro");?></label>
    <br />
    <input type="text" class="form-control"  name="logisticpro_cargo_price" id="logisticpro_cargo_price" value="<?php echo esc_attr( isset( $logisticpro_details['logisticpro_cargo_price'] ) ) ? esc_attr( $logisticpro_details['logisticpro_cargo_price'] ) : ''; ?>">
  </div>
</div>
<?php
	
}
function logisticpro_cargo_info_func()
{

?>
<div class="logisticpro-div clearfix">
  <div class="form-group ">
    <div class="pull-right">
      <label><strong><?php echo __("Tracking ID:", "logistic-pro");?></strong></label>
      <?php 
					if( get_post_meta( get_the_ID(), '_logisticpro_tracking_id', true) )
					{
						echo esc_html( get_post_meta( get_the_ID(), '_logisticpro_tracking_id', true) );
					}
				 ?>
    </div>
  </div>
</div>
<br />
<div class="clearfix"></div>
<?php echo logisticproForm::fields('logisticpro_shiping_mode', 'admin', '12', 'select', 'Shipping Mode:','logistic-shipments-type'); ?> <?php echo logisticproForm::fields('logisticpro_cargo_name', 'admin', '12', 'input', 'Commodity/Cargo Name:'); ?> <?php echo logisticproForm::fields('logisticpro_weight_value', 'admin', '7', 'input', 'Total Estimated Weight:'); ?> <?php echo logisticproForm::fields('logisticpro_weight_unit', 'admin', '5', 'select', 'Select Weight Unit', 'logistic-weight-type'); ?> <?php echo logisticproForm::fields('logisticpro_packing_type', 'admin', '12', 'select', 'Packing Type:', 'logistic-packings-type'); ?> <?php echo logisticproForm::fields('logisticpro_quantity', 'admin', '12', 'input', 'Quantity:'); ?> <?php echo logisticproForm::fields('logisticpro_cargo_type', 'admin', '12', 'select', 'Type of Cargo:', 'logistic-cargo-type'); ?> <?php echo logisticproForm::fields('logisticpro_is_insurance', 'admin', '12', 'select', 'Insurance Required?', 'insurance'); ?>
<?php	
}
function logisticpro_cargo_sender_func()
{
?>
<?php echo logisticproForm::fields('logisticpro_sender_country', 'admin', '6', 'select', 'Country:', 'logistic-location-type'); ?> <?php echo logisticproForm::fields('logisticpro_sender_state', 'admin', '6', 'input', 'State / County:'); ?> <?php echo logisticproForm::fields('logisticpro_sender_city', 'admin', '6', 'input', 'Town / City:'); ?> <?php echo logisticproForm::fields('logisticpro_sender_postcode', 'admin', '6', 'input', 'Postcode / ZIP:'); ?> <?php echo logisticproForm::fields('logisticpro_sender_street', 'admin', '12', 'input', 'Street Street Address:'); ?> <?php echo logisticproForm::fields('logisticpro_sender_appart', 'admin', '12', 'input', 'Apartment, suite, unit:'); ?> <?php echo logisticproForm::fields('logisticpro_sender_name', 'admin', '12', 'input', 'Company/Person Name:'); ?>
<?php
}
function logisticpro_cargo_receiver_func()
{
?>
<?php echo logisticproForm::fields('logisticpro_receiver_country', 'admin', '6', 'select', 'Country:', 'logistic-location-type'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_state', 'admin', '6', 'input', 'State / County:'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_city', 'admin', '6', 'input', 'Town / City:'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_postcode', 'admin', '6', 'input', 'Postcode / ZIP:'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_street', 'admin', '12', 'input', 'Street Street Address:'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_appart', 'admin', '12', 'input', 'Apartment, suite, unit:'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_name', 'admin', '12', 'input', 'Company/Person Name:'); ?>
<?php	
}
function logisticpro_cargo_pickup_func()
{
?>
<?php echo logisticproForm::fields('logisticpro_pickup_name', 'admin', '12', 'input', 'Contact Name:'); ?> <?php echo logisticproForm::fields('logisticpro_pickup_phone', 'admin', '12', 'input', 'Phone Number:'); ?> <?php echo logisticproForm::fields('logisticpro_pickup_datetime', 'admin', '12', 'input', 'Pick up Date:'); ?> <?php echo logisticproForm::fields('logisticpro_pickup_aditional_info', 'admin', '12', 'textarea', 'Additional Details:'); ?>
<?php	
}

function logisticpro_metabox_add() 
{
add_meta_box( 'logisticpro-price-info', __('Cargo Status (For Company only)', 'logisticpro'),'logisticpro_cargo_price_func','shipment', 'normal','high' );
add_meta_box( 'logisticpro-cargo-info', __('Cargo Info', 'logisticpro'),'logisticpro_cargo_info_func','shipment', 'normal','high' );
add_meta_box( 'logisticpro-sender-info', __('Sender Info', 'logisticpro'),'logisticpro_cargo_sender_func','shipment', 'normal','high' );
add_meta_box( 'logisticpro-receiver-info', __('Receiver Info', 'logisticpro'),'logisticpro_cargo_receiver_func','shipment', 'normal','high' );
add_meta_box( 'logisticpro-pickup-info', __('Pick up Info', 'logisticpro'),'logisticpro_cargo_pickup_func','shipment', 'normal','high' );
}
add_action('add_meta_boxes', 'logisticpro_metabox_add');


add_action( 'init', 'logisticpro_shipments_func', 0 );

}



if ( ! function_exists( 'shipments_taxonomy_func' ) ) {

// Register Custom Taxonomy
function shipments_taxonomy_func() {

	$labels = array(
		'name'                       => _x( 'Shipment Types', 'Taxonomy General Name', 'logisticpro' ),
		'singular_name'              => _x( 'Shipment Type', 'Taxonomy Singular Name', 'logisticpro' ),
		'menu_name'                  => __( 'Shipment Type', 'logisticpro' ),
		'all_items'                  => __( 'All Items', 'logisticpro' ),
		'parent_item'                => __( 'Parent Item', 'logisticpro' ),
		'parent_item_colon'          => __( 'Parent Item:', 'logisticpro' ),
		'new_item_name'              => __( 'New Item Name', 'logisticpro' ),
		'add_new_item'               => __( 'Add New Item', 'logisticpro' ),
		'edit_item'                  => __( 'Edit Item', 'logisticpro' ),
		'update_item'                => __( 'Update Item', 'logisticpro' ),
		'view_item'                  => __( 'View Item', 'logisticpro' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'logisticpro' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'logisticpro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'logisticpro' ),
		'popular_items'              => __( 'Popular Items', 'logisticpro' ),
		'search_items'               => __( 'Search Items', 'logisticpro' ),
		'not_found'                  => __( 'Not Found', 'logisticpro' ),
		'no_terms'                   => __( 'No items', 'logisticpro' ),
		'items_list'                 => __( 'Items list', 'logisticpro' ),
		'items_list_navigation'      => __( 'Items list navigation', 'logisticpro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'logistic-shipments-type', array( 'shipment' ), $args );

}
add_action( 'init', 'shipments_taxonomy_func', 0 );

}

/* -------------- Packing Type -------------- */
if ( ! function_exists( 'packings_taxonomy_func' ) ) {

// Register Custom Taxonomy
function packings_taxonomy_func() {

	$labels = array(
		'name'                       => _x( 'Packings', 'Taxonomy General Name', 'logisticpro' ),
		'singular_name'              => _x( 'Packing', 'Taxonomy Singular Name', 'logisticpro' ),
		'menu_name'                  => __( 'Packing Type', 'logisticpro' ),
		'all_items'                  => __( 'All Items', 'logisticpro' ),
		'parent_item'                => __( 'Parent Item', 'logisticpro' ),
		'parent_item_colon'          => __( 'Parent Item:', 'logisticpro' ),
		'new_item_name'              => __( 'New Item Name', 'logisticpro' ),
		'add_new_item'               => __( 'Add New Item', 'logisticpro' ),
		'edit_item'                  => __( 'Edit Item', 'logisticpro' ),
		'update_item'                => __( 'Update Item', 'logisticpro' ),
		'view_item'                  => __( 'View Item', 'logisticpro' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'logisticpro' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'logisticpro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'logisticpro' ),
		'popular_items'              => __( 'Popular Items', 'logisticpro' ),
		'search_items'               => __( 'Search Items', 'logisticpro' ),
		'not_found'                  => __( 'Not Found', 'logisticpro' ),
		'no_terms'                   => __( 'No items', 'logisticpro' ),
		'items_list'                 => __( 'Items list', 'logisticpro' ),
		'items_list_navigation'      => __( 'Items list navigation', 'logisticpro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'logistic-packings-type', array( 'shipment' ), $args );

}
add_action( 'init', 'packings_taxonomy_func', 0 );

}


/* -------------- Weight Type -------------- */
if ( ! function_exists( 'weight_taxonomy_func' ) ) {

// Register Custom Taxonomy
function weight_taxonomy_func() {

	$labels = array(
		'name'                       => _x( 'Weights', 'Taxonomy General Name', 'logisticpro' ),
		'singular_name'              => _x( 'Weight', 'Taxonomy Singular Name', 'logisticpro' ),
		'menu_name'                  => __( 'Weight Type', 'logisticpro' ),
		'all_items'                  => __( 'All Items', 'logisticpro' ),
		'parent_item'                => __( 'Parent Item', 'logisticpro' ),
		'parent_item_colon'          => __( 'Parent Item:', 'logisticpro' ),
		'new_item_name'              => __( 'New Item Name', 'logisticpro' ),
		'add_new_item'               => __( 'Add New Item', 'logisticpro' ),
		'edit_item'                  => __( 'Edit Item', 'logisticpro' ),
		'update_item'                => __( 'Update Item', 'logisticpro' ),
		'view_item'                  => __( 'View Item', 'logisticpro' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'logisticpro' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'logisticpro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'logisticpro' ),
		'popular_items'              => __( 'Popular Items', 'logisticpro' ),
		'search_items'               => __( 'Search Items', 'logisticpro' ),
		'not_found'                  => __( 'Not Found', 'logisticpro' ),
		'no_terms'                   => __( 'No items', 'logisticpro' ),
		'items_list'                 => __( 'Items list', 'logisticpro' ),
		'items_list_navigation'      => __( 'Items list navigation', 'logisticpro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'logistic-weight-type', array( 'shipment' ), $args );

}
add_action( 'init', 'weight_taxonomy_func', 0 );

}

/* -------------- cargo Type -------------- */
if ( ! function_exists( 'cargo_taxonomy_func' ) ) {

// Register Custom Taxonomy
function cargo_taxonomy_func() {

	$labels = array(
		'name'                       => _x( 'Cargos', 'Taxonomy General Name', 'logisticpro' ),
		'singular_name'              => _x( 'Cargo', 'Taxonomy Singular Name', 'logisticpro' ),
		'menu_name'                  => __( 'Cargo Type', 'logisticpro' ),
		'all_items'                  => __( 'All Items', 'logisticpro' ),
		'parent_item'                => __( 'Parent Item', 'logisticpro' ),
		'parent_item_colon'          => __( 'Parent Item:', 'logisticpro' ),
		'new_item_name'              => __( 'New Item Name', 'logisticpro' ),
		'add_new_item'               => __( 'Add New Item', 'logisticpro' ),
		'edit_item'                  => __( 'Edit Item', 'logisticpro' ),
		'update_item'                => __( 'Update Item', 'logisticpro' ),
		'view_item'                  => __( 'View Item', 'logisticpro' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'logisticpro' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'logisticpro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'logisticpro' ),
		'popular_items'              => __( 'Popular Items', 'logisticpro' ),
		'search_items'               => __( 'Search Items', 'logisticpro' ),
		'not_found'                  => __( 'Not Found', 'logisticpro' ),
		'no_terms'                   => __( 'No items', 'logisticpro' ),
		'items_list'                 => __( 'Items list', 'logisticpro' ),
		'items_list_navigation'      => __( 'Items list navigation', 'logisticpro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'logistic-cargo-type', array( 'shipment' ), $args );

}
add_action( 'init', 'cargo_taxonomy_func', 0 );

}

/* -------------- cargo Type -------------- */
if ( ! function_exists( 'locations_taxonomy_func' ) ) {

// Register Custom Taxonomy
function locations_taxonomy_func() {

	$labels = array(
		'name'                       => _x( 'Locations', 'Taxonomy General Name', 'logisticpro' ),
		'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'logisticpro' ),
		'menu_name'                  => __( 'Add Locations', 'logisticpro' ),
		'all_items'                  => __( 'All Locations', 'logisticpro' ),
		'parent_item'                => __( 'Parent Location', 'logisticpro' ),
		'parent_item_colon'          => __( 'Parent Location:', 'logisticpro' ),
		'new_item_name'              => __( 'New Location Name', 'logisticpro' ),
		'add_new_item'               => __( 'Add New Location', 'logisticpro' ),
		'edit_item'                  => __( 'Edit Locations', 'logisticpro' ),
		'update_item'                => __( 'Update Location', 'logisticpro' ),
		'view_item'                  => __( 'View Locations', 'logisticpro' ),
		'separate_items_with_commas' => __( 'Separate Locations with commas', 'logisticpro' ),
		'add_or_remove_items'        => __( 'Add or remove Locations', 'logisticpro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'logisticpro' ),
		'popular_items'              => __( 'Popular Locations', 'logisticpro' ),
		'search_items'               => __( 'Search Locations', 'logisticpro' ),
		'not_found'                  => __( 'Not Found', 'logisticpro' ),
		'no_terms'                   => __( 'No Locations', 'logisticpro' ),
		'items_list'                 => __( 'Locations list', 'logisticpro' ),
		'items_list_navigation'      => __( 'Locations list navigation', 'logisticpro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'logistic-location-type', array( 'shipment' ), $args );

}
add_action( 'init', 'locations_taxonomy_func', 0 );

}

// Add term page
function logisticpro_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
<div class="form-field">
  <label for="term_meta[logisticpro_location_shortname]"><?php echo __( 'Country Code', 'logisticpro' ); ?></label>
  <input type="text" name="term_meta[logisticpro_location_shortname]" id="term_meta[logisticpro_location_shortname]" value="" required>
  <p class="description"><?php echo __( 'Please enter the short name like US, UK or PK','logisticpro' ); ?></p>
</div>
<?php
}
add_action( 'logistic-location-type_add_form_fields', 'logisticpro_taxonomy_add_new_meta_field', 10, 2 );

function logisticpro_taxonomy_edit_meta_field($term) {
 
	$t_id = $term->term_id;
	$term_meta = get_option( "logisticpro_location_shortname_$t_id" ); 
	?>
<tr class="form-field">
  <th scope="row" valign="top"> <label for="term_meta[logisticpro_location_shortname]"><?php echo __( 'Country Code', 'logisticpro' ); ?></label>
  </th>
  <td><input type="text" name="term_meta[logisticpro_location_shortname]" id="term_meta[logisticpro_location_shortname]" value="<?php echo esc_attr( $term_meta['logisticpro_location_shortname'] ) ? esc_attr( $term_meta['logisticpro_location_shortname'] ) : ''; ?>">
    <p class="description"><?php echo __( 'Please enter the short name like US, UK or PK','logisticpro' ); ?></p></td>
</tr>
<?php
}
add_action( 'logistic-location-type_edit_form_fields', 'logisticpro_taxonomy_edit_meta_field', 10, 2 );

// Save extra taxonomy fields callback function.
function save_edir_logisticpro_taxonomy_location_meta_field( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "logisticpro_location_shortname_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "logisticpro_location_shortname_$t_id", $term_meta );
	}
}  
//
add_action( 'edited_logistic-location-type', 'save_edir_logisticpro_taxonomy_location_meta_field', 10, 2 );  
add_action( 'create_logistic-location-type', 'save_edir_logisticpro_taxonomy_location_meta_field', 10, 2 );

/* -------------- Packing Type -------------- */
if ( ! function_exists( 'order_status_taxonomy_func' ) ) {

// order_status_taxonomy_func
function order_status_taxonomy_func() {

	$labels = array(
		'name'                       => _x( 'Order Status', 'Taxonomy General Name', 'logisticpro' ),
		'singular_name'              => _x( 'Order Status', 'Taxonomy Singular Name', 'logisticpro' ),
		'menu_name'                  => __( 'Add Order Status', 'logisticpro' ),
		'all_items'                  => __( 'All Order Status', 'logisticpro' ),
		'parent_item'                => __( 'Parent Order Status', 'logisticpro' ),
		'parent_item_colon'          => __( 'Parent Order Status:', 'logisticpro' ),
		'new_item_name'              => __( 'New Order Status', 'logisticpro' ),
		'add_new_item'               => __( 'Add New Order Status', 'logisticpro' ),
		'edit_item'                  => __( 'Edit Order Status', 'logisticpro' ),
		'update_item'                => __( 'Update Order Status', 'logisticpro' ),
		'view_item'                  => __( 'View Order Status', 'logisticpro' ),
		'separate_items_with_commas' => __( 'Separate Order Status with commas', 'logisticpro' ),
		'add_or_remove_items'        => __( 'Add or remove Order Status', 'logisticpro' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'logisticpro' ),
		'popular_items'              => __( 'Popular Order Status', 'logisticpro' ),
		'search_items'               => __( 'Search Order Status', 'logisticpro' ),
		'not_found'                  => __( 'Not Found', 'logisticpro' ),
		'no_terms'                   => __( 'No Order Status', 'logisticpro' ),
		'items_list'                 => __( 'Order Status list', 'logisticpro' ),
		'items_list_navigation'      => __( 'Order Status list navigation', 'logisticpro' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'logistic-order-status', array( 'shipment' ), $args );

}
add_action( 'init', 'order_status_taxonomy_func', 0 );

}
