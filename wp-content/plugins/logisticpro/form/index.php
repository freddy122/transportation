<?php 

if ( ! class_exists( 'logisticproForm' ) )
{
	class logisticproForm {
	
		public static function fields($field = '', $showon = 'site', $col = 12, $type = '',  $static_name = '', $taxconomy = '', $showText = '')
		{
			if( logisticpro_field_parms($field."_show", 'show', '') == 1)
			{
				if($showon != 'site') {
					global $post;
					if( $showText != "yes")
					{
						wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );	
					}
					$logisticpro_details = json_decode(  get_post_meta( get_the_ID(), '_logisticpro_all_fields', true), true );
				
				} 		
				include (trailingslashit( plugin_dir_path( __FILE__ ) ). "$type.php");
			}
			
		}
		
		public static function text($post_id = '', $field = '', $type = '',  $taxconomy = '')
		{
			if( logisticpro_field_parms($field."_show", 'show', '') == 1)
			{
				
				global $post;
				
				$logisticpro_details = json_decode(  get_post_meta( $post_id, '_logisticpro_all_fields', true), true );						

					$echo = '';
					if( $type == "select" )
					{
						$echo = '';
						if( 'insurance' == $taxconomy)
						{  
							if( isset($logisticpro_details["$field"]) && $logisticpro_details["$field"] == 1) 
							{  
								$echo =  __("Yes", "logistic-pro"); 
							}
							else
							{ 
								$echo =  __("No", "logistic-pro"); 
							}
						}
						else
						{
								$term_id = @$logisticpro_details["$field"];
								if( isset( $term_id ) )
								{
									$terms =  get_term_by('id', $term_id, $taxconomy);
									$echo = esc_html( $terms->name );		
								}
								else
								{
									$echo = __( "N/A", "foodpro" );	
								}
					
						 } 		
						 
					}
					else if($type == "input" || $type == "textarea")
					{
						$echo =  esc_attr( isset( $logisticpro_details["$field"] ) ) ? esc_attr( $logisticpro_details["$field"] ) : '';
						
					}
					
					return $echo;
	
	
			}
			
		}
	}
}