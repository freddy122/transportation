<?php 
	if($showText == 'yes'){ 
?>
		<tr>
            <th><?php echo logisticpro_field_parms($field.'_text', 'val', $static_name); ?></th>
            <td><?php echo esc_attr( isset( $logisticpro_details["$field"] ) ) ? esc_attr( $logisticpro_details["$field"] ) : ''; ?></td>
        </tr>
        
<?php
	}
	else
	{

		if($showon == 'site')
		{
			echo  '<div class="col-sm-12 col-md-'.$col.' col-xs-12 clearfix">'; 
		} 
		else
		{			
			echo  '<div class="logisticpro-div clearfix">';	
		}		
	
?>
       		 <div class="form-group">
          <label><?php echo logisticpro_field_parms($field.'_text', 'val', $static_name); ?></label>
          <input type="text" class="form-control"  name="<?php echo esc_attr( $field ); ?>" value="<?php echo esc_attr( isset( $logisticpro_details["$field"] ) ) ? esc_attr( $logisticpro_details["$field"] ) : ''; ?>" id="<?php echo esc_attr( $field ); ?>">
        </div>
		</div>
<?php } ?>
