<?php 

	if($showText == 'yes'){ 
?>
	<tr>
   <th><?php echo logisticpro_field_parms($field.'_text', 'val', $static_name); ?></th>

    <?php
		if( 'insurance' == $taxconomy)
		{
			?>
            
        		<td>
				<?php 
					if( isset($logisticpro_details["$field"]) && $logisticpro_details["$field"] == 1)
					{ 
						echo __("Yes", "logistic-pro");
					}
					else
					{
						echo __("No", "logistic-pro");	
					}
				?>
                </td>
            </tr>
          <?php     
					
		}
		else
		{
				$term_id = @$logisticpro_details["$field"];
				if( isset( $term_id )){
					$terms =  get_term_by('id', $term_id, $taxconomy);
					echo  '<td>'.esc_html( $terms->name ).'</td></tr>' ;		
				}
				else{echo  '<td>'. __( "N/A", "foodpro" ). '</td></tr>' ;	}
	
		 } 
		
	}

else
	{


	if($showon == 'site'){
		echo  '<div class="col-sm-12 col-md-'.$col.' col-xs-12 clearfix">'; 
	} 
	else if($showon == 'profile')
	{
		echo '<div class="logisticpro-profile">';
	}
	else
	{
		echo  '<div class="logisticpro-div clearfix">';	
	}		
?>
<div class="form-group">
  <label><?php echo logisticpro_field_parms($field.'_text', 'val', $static_name); ?></label>
  <select class="form-control" name="<?php echo esc_attr( $field ); ?>" <?php echo logisticpro_field_parms('logisticpro_shiping_mode_required', 'req', ''); ?> id="<?php echo esc_attr( $field ); ?>">
    <option value=""><?php echo __("Select Option", "logistic-pro");?></option>
    <?php
		if( 'insurance' == $taxconomy)
		{
			?>
              <option value="1" <?php if( isset($logisticpro_details["$field"]) && $logisticpro_details["$field"] == 1){ echo esc_attr('selected="selected"'); }?>><?php echo __("Yes", "logistic-pro");?></option>
              <option value="0" <?php if( isset($logisticpro_details["$field"]) && $logisticpro_details["$field"]  == 0){ echo esc_attr('selected="selected"'); }?>><?php echo __("No", "logistic-pro");?></option>
            
            <?php
		}
		else
		{
				$terms = get_terms( $taxconomy, 'orderby=count&hide_empty=0' );		
				foreach( $terms  as  $term)
				{ 
	?>    
			<option value="<?php echo esc_attr( $term->term_id ); ?>" <?php if( isset($logisticpro_details["$field"]) && $logisticpro_details["$field"]  == $term->term_id){ echo esc_attr('selected="selected"'); }?>><?php echo esc_html( $term->name ); ?></option>
	<?php 
			}
	 } 
	
	?>
  </select>
</div>
</div>

<?php } ?> 