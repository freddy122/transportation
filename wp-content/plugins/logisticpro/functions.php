<?php
/* ------------------------------------------------ */
/* Enquee Site Scripts */
/* ------------------------------------------------ */
add_action( 'wp_enqueue_scripts', 'logisticpro_enqueue_style' );
function logisticpro_enqueue_style() {
	wp_enqueue_style( 'logistic-plugin-style', trailingslashit( plugin_dir_url( __FILE__ ) ).'css/logistic-style.css', false ); 
	wp_enqueue_style( 'logistic-datetimepicker', trailingslashit( plugin_dir_url( __FILE__ ) ).'css/bootstrap-datetimepicker.min.css', false );
	wp_enqueue_style( 'logistic-select2-style', trailingslashit( plugin_dir_url( __FILE__ ) ).'css/select2.min.css', false );  

 	wp_enqueue_script( 'logistic-select2-js', trailingslashit( plugin_dir_url( __FILE__ ) ).'js/select2.min.js', array(), false, true );	
	
	wp_enqueue_script( 'logistic-moment-js', trailingslashit( plugin_dir_url( __FILE__ ) ).'js/moment.js', array(), false, true );	
 	wp_enqueue_script( 'logistic-datetimepicker-js', trailingslashit( plugin_dir_url( __FILE__ ) ).'js/bootstrap-datetimepicker.min.js', array(), false, true );	
	
	
}

/* ------------------------------------------------ */
/* Enquee admin Scripts */
/* ------------------------------------------------ */
add_action('admin_enqueue_scripts', 'logisticpro_admin_enqueue_style');
function logisticpro_admin_enqueue_style(){
    wp_register_style( 'logistic-admin-style', trailingslashit( plugin_dir_url( __FILE__ ) ) . 'css/logistic-admin.css', false, '1.0.0' );
    wp_enqueue_style( 'logistic-admin-style' );
	wp_enqueue_script( 'jquery-ui-datepicker' ); 
    wp_enqueue_style('jquery-ui-dialog', "//ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css", false, null); 	
}

/* ------------------------------------------------ */
/* Search Shipment  */
/* ------------------------------------------------ */


/*add_action('admin_init', 'logisticpro_no_mo_dashboard');
function logisticpro_no_mo_dashboard() {
  if (!current_user_can('manage_options') && $_SERVER['DOING_AJAX'] != '/wp-admin/admin-ajax.php') {
  	wp_redirect(home_url()); exit;
  }
}*/

/* ------------------------------------------------ */
/* Search Shipment  */
/* ------------------------------------------------ */

add_action( 'admin_post_by_shipment_id', 'logisticpro_search_by_shipment_id' );
function logisticpro_search_by_shipment_id() {

		global $current_user;
		wp_get_current_user();	
		global $post;
	
		if( get_current_user_id() == $post->post_author )
		{
			return true;
		}
		else
		{
			return false;	
		}

}


/* ------------------------------------------------ */
/* Limit Shipment  */
/* ------------------------------------------------ */

function logisticpro_limit_shipment( $pid = '' )
{	

		global $current_user;
		wp_get_current_user();
		global $post;
	
		if( get_current_user_id() == $post->post_author )
		{
			return true;
		}
		else
		{
			return false;	
		}
	
}

/* ------------------------------------------------ */
/* Submit Cargo  */
/* ------------------------------------------------ */
function logisticpro_order_for_cargo_backend( $pid )
{	
	
	$type	=	 get_post_type( $pid );
	
	if ($type != 'shipment')
	return;	

	if ( wp_is_post_revision( $pid ) )
	return;	
		
  	// Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
	// if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;
     
    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;
		
	$data[] 	= $_POST['logisticpro_shiping_mode'];
    $data[]   	= $_POST['logisticpro_cargo_name'];
    $data[] 	= $_POST['logisticpro_weight_value'];
    $data[]  	= $_POST['logisticpro_weight_unit'];
    $data[] 	= $_POST['logisticpro_packing_type'];
    $data[]	  	= $_POST['logisticpro_quantity'];
    $data[]	  	= $_POST['logisticpro_cargo_type'];
    $data[] 	= $_POST['logisticpro_is_insurance'];
    $data[] 	= $_POST['logisticpro_sender_country'];
    $data[]   	= $_POST['logisticpro_sender_state'];
    $data[]    	= $_POST['logisticpro_sender_city'];
    $data[]  	= $_POST['logisticpro_sender_postcode'];
    $data[]    	= $_POST['logisticpro_sender_street'];
    $data[]	  	= $_POST['logisticpro_sender_appart'];
    $data[]	  	= $_POST['logisticpro_sender_name'];
    $data[]  	= $_POST['logisticpro_receiver_country'];
    $data[]	   	= $_POST['logisticpro_receiver_state'];
    $data[]     = $_POST['logisticpro_receiver_city'];
    $data[]   	= $_POST['logisticpro_receiver_postcode'];
    $data[]	 	= $_POST['logisticpro_receiver_street'];
    $data[]	 	= $_POST['logisticpro_receiver_appart'];
    $data[]		= $_POST['logisticpro_receiver_name'];
    $data[]		= $_POST['logisticpro_pickup_name'];
    $data[]		= $_POST['logisticpro_pickup_phone'];
    $data[] 	= $_POST['logisticpro_pickup_datetime'];
    $data[]		= $_POST['logisticpro_pickup_aditional_info'];
	
	$data[]		= $_POST['logisticpro_is_pickup'];
	$data[]		= $_POST['logisticpro_cargo_price'];
	$data[]		= $_POST['logisticpro_order_status'];
	
		

		$tracking_code =    logisticpro_get_tracking_code( $pid );
		
		//Tracking ID
		update_post_meta( $pid, '_logisticpro_tracking_id', $tracking_code);
		
		//Order status
		update_post_meta( $pid, '_logisticpro_order_status', $_POST['logisticpro_order_status'] );
		
		
		$history	=	get_post_meta( $pid, '_logisticpro_order_status_history', true );
		$order_status	=	$_POST['logisticpro_order_status'] . '_' . date('Y-m-d H:i:s');
		if( $history != "" )
		{
			$statues	=	array();
			$s_array	=	explode( '|', $history );
			foreach( $s_array as $arr )
			{
				$id	=	explode( '_', $arr );
				$statues[]	=	$id[0];
			}
				if ( !in_array( $_POST['logisticpro_order_status'], $statues ) )
				{
					$new_status		=	$history . '|' . $order_status;	
				}
				else
				{
					$new_status		=	$history;	
				}
		}
		else
		{
			$new_status		=	$order_status;
		}
		
		update_post_meta( $pid, '_logisticpro_order_status_history', $new_status );
		
		
		//taxconomies 
		 update_post_meta( $pid, '_logisticpro_shiping_mode', $_POST['logisticpro_shiping_mode'] );
		 update_post_meta( $pid, '_logisticpro_packing_type', $_POST['logisticpro_packing_type'] );
		 update_post_meta( $pid, '_logisticpro_cargo_type', $_POST['logisticpro_cargo_type'] );
		
		 //Sender		
		 update_post_meta( $pid, '_logisticpro_sender_country', $_POST['logisticpro_sender_country'] );
		 update_post_meta( $pid, '_logisticpro_sender_state', $_POST['logisticpro_sender_state'] );
		 update_post_meta( $pid, '_logisticpro_sender_city', $_POST['logisticpro_sender_city'] );
		 
		 //Receiver 
		 update_post_meta( $pid, '_logisticpro_receiver_country', $_POST['logisticpro_receiver_country'] );
		 update_post_meta( $pid, '_logisticpro_receiver_state', $_POST['logisticpro_receiver_state'] );
		 update_post_meta( $pid, '_logisticpro_receiver_city', $_POST['logisticpro_receiver_city'] );
		 		
		//saving json
		 $logisticpro_all_fields = json_encode( $_POST );
		 update_post_meta( $pid, '_logisticpro_all_fields', $logisticpro_all_fields );
	
}
add_action( 'save_post', 'logisticpro_order_for_cargo_backend' );

//Front End
function logisticpro_order_for_cargo()
{
    parse_str($_POST['formData'], $data);
    	
	$arr = array();
	$arr_data = array();
	foreach( $data as $d => $v )
	{
		
		if( logisticpro_field_parms($d.'_required', 'req') != "" &&  $v == "")
		{
			$arr[] = $d;
		}
		
		$arr_data[$d] = $v;
			
	}

	if( count( $arr ) > 0 )
	{
		$err = json_encode( $arr );
		echo  'err|'.$err;
		exit;
	}
	
 $post_id = wp_insert_post(array(
            'ping_status'   =>  'closed',
            'post_title'    =>  $arr_data['logisticpro_cargo_name'],
			'post_content' => $arr_data['logisticpro_pickup_aditional_info'],
            'post_status'   =>  'publish',
            'post_type'   =>  'shipment',
            'tax_input' =>   array
			(
				'logistic-shipments-type' => $arr_data['logisticpro_shiping_mode'],
				'logistic-packings-type' => $arr_data['logisticpro_packing_type'],
				'logistic-weight-type' => $arr_data['logisticpro_weight_unit'],
				'logistic-cargo-type' => $arr_data['logisticpro_cargo_type'],
				'logistic-location-type' => $arr_data['logisticpro_sender_country'],
			),
          ));	
	
	
		
		$tracking_code =    logisticpro_get_tracking_code( $post_id );
		//Tracking ID
		update_post_meta( $post_id, '_logisticpro_tracking_id', $tracking_code);
		
		//taxconomies 
		 update_post_meta( $post_id, '_logisticpro_shiping_mode', $arr_data['logisticpro_shiping_mode'] );
		 update_post_meta( $post_id, '_logisticpro_packing_type', $arr_data['logisticpro_packing_type'] );
		 update_post_meta( $post_id, '_logisticpro_cargo_type', $arr_data['logisticpro_cargo_type'] );
		
		 //Sender		
		 update_post_meta( $post_id, '_logisticpro_sender_country', $arr_data['logisticpro_sender_country'] );
		 update_post_meta( $post_id, '_logisticpro_sender_state', $arr_data['logisticpro_sender_state'] );
		 update_post_meta( $post_id, '_logisticpro_sender_city', $arr_data['logisticpro_sender_city'] );
		 
		 //Receiver 
		 update_post_meta( $post_id, '_logisticpro_receiver_country', $arr_data['logisticpro_receiver_country'] );
		 update_post_meta( $post_id, '_logisticpro_receiver_state', $arr_data['logisticpro_receiver_state'] );
		 update_post_meta( $post_id, '_logisticpro_receiver_city', $arr_data['logisticpro_receiver_city'] );
		 
		 //All Fields
		 $logisticpro_all_fields = json_encode( $arr_data );
		 update_post_meta( $post_id, '_logisticpro_all_fields', $logisticpro_all_fields );
		 
		 
		echo 'true|success';
		die();
		
}
add_action('wp_ajax_logisticpro_order_for_cargo', 'logisticpro_order_for_cargo');
add_action('wp_ajax_nopriv_logisticpro_order_for_cargo', 'logisticpro_order_for_cargo');

function logisticpro_apply_for_cargo() { ?>
<script type="text/javascript">
(function($) {
	"use strict";
	$("#order-for-cargo").on("click", function() 
	{
		
			var logisticpro_admin_ajax_url = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
			var formData = $("form#order-cargo").serialize();
			$.post(logisticpro_admin_ajax_url, {
				action: "logisticpro_order_for_cargo",
				formData: formData
			}).done(function(data)
			{
				var res = data.split("|");
				if (res[0].trim() == 'true')
				{
	$("#msg-box").html('<div class="alert alert-success"><strong><?php echo __("Bravo!", "logistic-pro");?></strong> <?php echo __("We have received your order.", "logistic-pro");?></div>');
					$('form').trigger("reset");
				} 
				else
				{
					$("form input,form select,form  textarea,form checkbox").removeClass("logistic-err");
					var obj = jQuery.parseJSON( res[1] );
					$.each( obj, function( key, value ) {
						$("[name="+value+"]").addClass("logistic-err");
					});
	$("#msg-box").html('<div class="alert alert-danger"><strong><?php echo __("Alert!", "logistic-pro");?></strong> <?php echo __("Please fill all required fields.", "logistic-pro");?></div>');
				}
		});
		
	});					
	
$(document).ready(function() {
	   
    $("form#order-cargo select").select2({
	  allowClear: true
	});
	$('#logisticpro_pickup_datetime').datetimepicker();
});

$('form input,form select,form  textarea,form  checkbox').on('blur', function(){
   $(this).removeClass("logistic-err");
}).on('focus', function(){
  $(this).removeClass("logistic-err");
});

	
})( jQuery );




</script>
<script type="text/javascript">
(function($) {
	"use strict";
$(document).ready(function() {
 
    var formfield;
 
    /* user clicks button on custom field, runs below code that opens new window */
    jQuery('.onetarek-upload-button').click(function() {
        formfield = jQuery(this).prev('input'); //The input field that will hold the uploaded file url
        tb_show('','media-upload.php?TB_iframe=true');
 
        return false;
 
    });
    /*
    Please keep these line to use this code snipet in your project
    Developed by oneTarek http://onetarek.com
    */
    //adding my custom function with Thick box close function tb_close() .
    window.old_tb_remove = window.tb_remove;
    window.tb_remove = function() {
        window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
        formfield=null;
    };
 
    // user inserts file into post. only run custom if user started process using the above process
    // window.send_to_editor(html) is how wp would normally handle the received data
 
    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor = function(html){
        if (formfield) {
            fileurl = jQuery('img',html).attr('src');
            jQuery(formfield).val(fileurl);
            tb_remove();
        } else {
            window.original_send_to_editor(html);
        }
    };
 
});
});
</script>

<?php }
add_action( 'wp_footer', 'logisticpro_apply_for_cargo', '100' );



function logisticpro_get_tracking_code($cargoID = '')
{
	
	if( $cargoID == "" ){ return '';}
	
	global $post;
	$post = get_post( $cargoID );
	$timeStamp = ( strtotime ( $post->post_date ) );
	
	return ( $timeStamp );				

}

//From Validation/Show/Hide
function logisticpro_field_parms($parm_name = '', $attr = '', $val = '')
{
	if($parm_name == "") return '';
	
	$data  = '';
	$setting = json_decode(  get_option("_logisticpro_plugin_shipment_tab_settings" ),  true );

	if( isset( $setting["$parm_name"] ) && $attr == 'show' )
	{  
		$data = '1';  
	}
	if( isset( $setting["$parm_name"] ) && $attr == 'req' )
	{  
		$data = 'required="required"';  
	}
	if( isset( $setting["$parm_name"] ) && $attr == 'attr' )
	{  
		$data = 'checked="checked"';  
	}
	if( isset( $setting["$parm_name"] ) && $attr == 'val' )
	{  
		$value = $setting["$parm_name"]; 
		
		if( $value )
		{
			$data = $value;
		}
		else
		{
			$data =  sprintf( esc_html__(  '%1$s', 'logisticpro' ), $val);
			
		}
	}
	
	return $data;	
}



// Reset Password
function logisticpro_changePassword() {
	
  //only process if user is loged in
  if ( is_user_logged_in() )
  {
	parse_str($_POST['formData'], $data);
	
	if( $data['logisticpro_old_password'] == "" )
	{
		echo 'err|'. __("Plese enter old password.","logistic-pro");
	}
	else if( $data['logisticpro_new_password']   == "" )
	{
	  echo 'err|'. __("Please enter new password","logistic-pro");
	}
	else if( strlen( $data['logisticpro_new_password'] )  < 5 )
	{
	  echo 'err|'. __("Password must be 5 characters long.","logistic-pro");
	}	
	else if( $data['logisticpro_new_password'] != $data['logisticpro_re_password'] )
	{
	  echo 'err|'. __("New and Condirm Password Mismatched.","logistic-pro");
	}
	else
	{
			$user = wp_get_current_user();
			if( isset( $user ) )
			{
					if ( $user && wp_check_password( $data['logisticpro_old_password'], $user->data->user_pass, $user->ID) )
					{
					   //wp_set_password( $data['logisticpro_new_password'], $user->ID);
					   echo 'success|'. __("Password Updated Successfully.", "logistic-pro");
					}
					else
					{
					   echo 'err|'. __("Please enter correct old password.", "logistic-pro");
					}		
			}
	}
  }
	die();
	
}

add_action('wp_ajax_changePassword', 'logisticpro_changePassword');
add_action('wp_ajax_nopriv_changePassword', 'logisticpro_changePassword');


function logisticpro_profile_footer()
{
	//only process if user is loged in
	if ( is_user_logged_in() )
	{
?>
<script type="text/javascript">
(function($) {
	"use strict";
	$("#change-password-btn").on("click", function() 
	{
		
		var logisticpro_admin_ajax_url = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
		var formData = $("form#change-password").serialize();
		$.post(logisticpro_admin_ajax_url, {
			action: "changePassword",
			formData: formData
		}).done(function(data)
		{
			var res = data.split("|");
			if (res[0].trim() == 'success')
			{
$("#msg-box").html('<div class="alert alert-success"><strong><?php echo __("Bravo!", "logistic-pro");?></strong> '+ res[1] + '</div>');
				$('form#change-password').trigger("reset");
			} 
			else
			{
			
			$("#msg-box").html('<div class="alert alert-danger"><strong><?php echo __("Alert!", "logistic-pro");?></strong> '+res[1]+'</div>');
			}
		});
		
	});					
})( jQuery );
</script>
<?php 
	} 
}
add_action( 'wp_footer', 'logisticpro_profile_footer', '100' );


/* Filter the single_template with our custom function*/
add_filter('single_template', 'shipment_custom_template');

function shipment_custom_template($single) {
    global $wp_query, $post;

    /* Checks for single template by post type */
    if ($post->post_type == "shipment"){
        if(file_exists(trailingslashit( plugin_dir_path( __FILE__ ) ) . 'single-shipment.php'))
            return trailingslashit( plugin_dir_path( __FILE__ ) ) .  'single-shipment.php';
    }
    return $single;
}

/* ------------------------------------------------ */
/* Order History Process */
/* ------------------------------------------------ */

function foodpro_plugin_pagination($maxPages)
{
	 return get_next_posts_link( '<<', $maxPages ) . ' | ' .  get_previous_posts_link( '>>' );	
}

add_action('wp_ajax_logisticpro_order_history', 'logisticpro_order_history_data');
function logisticpro_order_history_data()
{
	parse_str($_POST['formData'], $data);
	
	
	
	$start_date = ( date("Y-m-d H:i:s", strtotime($data['start_date'])) );
	$end_date   = ( date("Y-m-d H:i:s", strtotime($data['end_date'])) );

	
	$html  = '';
	
	if( isset( $data['new_page'] ) )
	{
		$paged = $data['new_page'];
	}
	else
	{
		$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;	
	}
	$metaKey = 'ID';	
	$args = 
		array( 
			'post_type' => 'shipment', 
			'paged' => $paged,
			'date_query' => array(
				array(
					'after'     => $start_date ,
					'before'    => $end_date,
					'inclusive' => true,
				),
			),						
		);	

	

	
	$the_query = new WP_Query( $args ); 
	

if ( $the_query->have_posts() ) 
{ 
	while ( $the_query->have_posts() ) 
	{ 
	$rt	=	'';
		$the_query->the_post(); 
		
		$shipmentID = get_the_permalink() ;
						
			  $html .= '<tr>
				  <td>
					'. logisticproForm::text( get_the_ID(), "logisticpro_cargo_name", "input", "") .'
					 <br>
						<small> '. __("ORDER DATE: ", "foodpro")  . get_the_date() .'</small>
				  </td>
				  <td>'. logisticproForm::text( get_the_ID(), "logisticpro_sender_country", "select", "logistic-location-type") .'</td>
				  <td>'. logisticproForm::text( get_the_ID(), "logisticpro_receiver_country", "select", "logistic-location-type") .'</td>
				  <td>'. logisticproForm::text( get_the_ID(), "logisticpro_order_status", "select", "logistic-order-status") .'</td>
				  <td>
					<a href="'. get_edit_post_link() .'" target="_blank">'.__("Details: ", "foodpro").'</a>
				  </td>
				</tr>';		
	
	}
	
		if( $the_query->max_num_pages > 1 )
		{
			
			$next = $paged+1;
			$previous = $paged-1;

		 $html .= '<tr><td colspan="5">';
		 
		 if($paged > 1 )
		 {
			 $html .=   '<input id="pre_new_page" type="hidden" value="'.$previous.'" name="pre_new_page">
		 <input type="button" id="foodpro-loadmore-pre" name="submit_range" value="'. esc_html__("Previous Page", "logistic-pro").'" class="button button-primary button-large foodpro-loadmore" />';
		 }
		 
		 if($next <= $the_query->max_num_pages )
		 {
		 	$html .=  '<input id="new_page" type="hidden" value="'.$next.'" name="new_page">
		 <input type="button" id="foodpro-loadmore" name="submit_range" value="'. esc_html__("Next Page", "logistic-pro").'" class="button button-primary button-large foodpro-loadmore" />';
		 }
		 $html .= '</td></tr>';
		 
		}
		wp_reset_postdata(); 
	}
	else
	{
	
	} 
	
	 echo 'true|'.$html;

	exit;
}

function logisticpro_submit_history_data() {
	
	wp_enqueue_script('jQuery');
    ?>
<script type="text/javascript">



jQuery(document).ready(function () {
  
	
			"use strict";
			jQuery("body").on("click", '.foodpro-loadmore', function() {
				
				if( this.id == 'foodpro-loadmore-pre')
				{
					var new_page   = jQuery('#logisticpro-order-tab-form #pre_new_page').val();
					
				}
				if( this.id == 'foodpro-loadmore')
				{
					var new_page   = jQuery('#logisticpro-order-tab-form #new_page').val();
				}
				
				var start_date = jQuery('#logisticpro-order-tab-form #start_date').val();
				var end_date   = jQuery('#logisticpro-order-tab-form #end_date').val();	
				var sb_admin_ajax_url = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
				var formData = jQuery("form#logisticpro-order-tab-form").serialize();
		
			jQuery.post(sb_admin_ajax_url, {
				action: "logisticpro_order_history",
				formData: formData
			}).done(function(data) {
				
				var res = data.split("|");
				if (res[0].trim() == 'true')
				{
					jQuery("#history-content-area").html(res[1]);
					
				} else 
				{
					jQuery("#history-content-area").html('No Record');
				}
		});


		
	});
	
	});


</script>
    <?php

}
add_action( 'admin_footer', 'logisticpro_submit_history_data');


?>