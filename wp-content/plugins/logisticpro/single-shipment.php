<?php /* The template for displaying all single posts. */
get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if($sb_themes['page-settings-breadcrumb'] != 0) { echo logistic_pro_theme_the_breadcrumb();  } 
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <section class="section-padding-70 shipment-details">
    <div class="container"> 
      <!-- Row -->
      <?php if( logisticpro_limit_shipment() ){ ?>
      <div class="row">
        <?php				
            $logisticpro_details = json_decode(  get_post_meta( get_the_ID(), '_logisticpro_all_fields', true), true );			
            $shipmentID = get_the_permalink() ;
        ?>
        <div class="col-md-5">
          <div class="panel panel-default ">
            <div class="panel-body">
              <div class="custom-heading">
                <h2><?php echo __('Pickup Info', 'foodpro'); ?></h2>
              </div>
              <table class="table">
                <?php echo logisticproForm::fields('logisticpro_pickup_name', 'admin', '12', 'input', 'Contact Name:','','yes'); ?> <?php echo logisticproForm::fields('logisticpro_pickup_phone', 'admin', '12', 'input', 'Phone Number:','','yes'); ?> <?php echo logisticproForm::fields('logisticpro_pickup_datetime', 'admin', '12', 'input', 'Pick up Date:','','yes'); ?>
              </table>
            </div>
          </div>
          <div class="panel panel-default ">
            <div class="panel-body">
              <div class="custom-heading">
                <h2><?php echo __('Cargo Info', 'foodpro'); ?></h2>
              </div>
              <table class="table">
                <tr>
                  <th><?php echo __('Shipment ID', 'foodpro'); ?></th>
                  <td><?php echo esc_html( get_post_meta( get_the_ID(), '_logisticpro_tracking_id', true) ); ?></td>
                </tr>
                <?php echo logisticproForm::fields('logisticpro_order_status', 'admin', '12', 'select', 'Order Status:', 'logistic-order-status','yes'); ?> <?php echo logisticproForm::fields('logisticpro_shiping_mode', 'admin', '12', 'select', 'Shipping Mode:', 'logistic-shipments-type', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_cargo_name', 'admin', '12', 'input', 'Commodity/Cargo Name:', '', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_weight_value', 'admin', '7', 'input', 'Total Estimated Weight:', '', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_weight_unit', 'admin', '5', 'select', 'Select Weight Unit', 'logistic-weight-type', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_packing_type', 'admin', '12', 'select', 'Packing Type:', 'logistic-packings-type', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_quantity', 'admin', '12', 'input', 'Quantity:', '', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_cargo_type', 'admin', '12', 'select', 'Type of Cargo:', 'logistic-cargo-type', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_is_insurance', 'admin', '12', 'select', 'Insurance Required?', 'insurance', 'yes'); ?>
              </table>
            </div>
          </div>
          <div class="panel panel-default ">
            <div class="panel-body">
              <div class="custom-heading">
                <h2><?php echo __('Sender Info', 'foodpro'); ?></h2>
              </div>
              <table class="table">
                <?php echo logisticproForm::fields('logisticpro_sender_country', 'admin', '6', 'select', 'Country:', 'logistic-location-type', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_sender_state', 'admin', '6', 'input', 'State / County:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_sender_city', 'admin', '6', 'input', 'Town / City:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_sender_postcode', 'admin', '6', 'input', 'Postcode / ZIP:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_sender_street', 'admin', '12', 'input', 'Street Street Address:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_sender_appart', 'admin', '12', 'input', 'Apartment, suite, unit:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_sender_name', 'admin', '12', 'input', 'Company/Person Name:','', 'yes'); ?>
              </table>
            </div>
          </div>
          <div class="panel panel-default ">
            <div class="panel-body">
              <div class="custom-heading">
                <h2><?php echo __('Receiver Info', 'foodpro'); ?></h2>
              </div>
              <table class="table">
                <?php echo logisticproForm::fields('logisticpro_receiver_country', 'admin', '6', 'select', 'Country:', 'logistic-location-type', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_state', 'admin', '6', 'input', 'State / County:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_city', 'admin', '6', 'input', 'Town / City:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_postcode', 'admin', '6', 'input', 'Postcode / ZIP:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_street', 'admin', '12', 'input', 'Street Street Address:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_appart', 'admin', '12', 'input', 'Apartment, suite, unit:','', 'yes'); ?> <?php echo logisticproForm::fields('logisticpro_receiver_name', 'admin', '12', 'input', 'Company/Person Name:','', 'yes'); ?>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="custom-heading">
          <?php $showTitle = 'false'; if($showTitle == 'true'){?>
          <h2> <?php echo __('Cargo Name:', 'foodpro'); ?> <?php echo esc_html( $logisticpro_details['logisticpro_cargo_name'] ); ?> </h2>
            <?php } ?>
            <div class="panel panel-default ">
              <div class="panel-body">
                <div class="custom-heading">
                  <h2><?php echo __('Order History:', 'foodpro'); ?></h2>
                </div>
                <table class="table">
                  <tbody>
                  <tr>
                      <th><?php echo __('Order Status:', 'foodpro'); ?></th>
                      <th><?php echo __('Date', 'foodpro'); ?></th>
                  </tr>
                  
           	<?php 
				    $shipment_history =  get_post_meta( get_the_ID(), '_logisticpro_order_status_history', true);
					$history = array();
					if (  $shipment_history  != "" ) 
					{
						$history = explode("|", $shipment_history );
						if( count( $history[0] ) > 0 )
						{
							foreach( $history as $his )
							{
									$val = explode("_",  $his );
									$tr = get_term( $val[0] );
									echo '<tr><td>'. esc_html( $tr->name ) .'</td>';	
									echo '<td>'. esc_html( date( 'F j, Y @ g:i a' , strtotime($val[1]) ) ) .'</td></tr>';	
	
							}
						}
						else
						{
							echo '<tr><td colspan="2">'.  __('Request received, we will contact you shortly.', 'foodpro') .'</th></tr>';	
						}
					}
					else
					{
						echo '<tr><td colspan="2">'.  __('Request received, we will contact you shortly.', 'foodpro') .'</th></tr>';	
					}
			?>
                    
                    

                  </tbody>
                </table>
              </div>
            </div>
            <h2> <?php echo __('Cargo Details', 'foodpro'); ?> </h2>
          </div>
          <?php echo logisticproForm::fields('logisticpro_pickup_aditional_info', 'admin', '12', 'textarea', 'Additional Details:','','yes'); ?> 
        <?php comments_template(); ?>
       </div>
      </div>
      <?php }else{ ?>
      <div class="row">
        <div class="col-md-12">
          <div class="custom-heading">
            <h2> <?php echo __('You are not authorized to access this page.', 'foodpro'); ?> </h2>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    </div>
  </section>
</article>
<?php get_footer(); ?>
