<?php if ( true ) { ?>
<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
  <?php if ( $attributes['show_title'] ) : ?>
  <h2>
    <?php echo __( 'Login', 'logisticpro' ); ?>
  </h2>
  <?php endif; ?>
  
  <!-- Show errors if there are any -->
  <?php if ( count( $attributes['errors'] ) > 0 ) : ?>
  <?php foreach ( $attributes['errors'] as $error ) : ?>
    <div class="alert alert-danger">
      <strong>Alert!</strong> <?php echo $error; ?>
    </div>
  <?php endforeach; ?>
  <?php endif; ?>
  
  <!-- Show logged out message if user just logged out -->
  <?php if ( $attributes['logged_out'] ) : ?>
  <p class="login-info">
    <?php echo __( 'You have signed out. Would you like to sign in again?', 'logisticpro' ); ?>
  </p>
  <?php endif; ?>
  <?php if ( $attributes['registered'] ) : ?>
  <p class="login-info">
    <?php
				printf(
					__( 'You have successfully registered to <strong>%s</strong>. We have emailed your password to the email address you entered.', 'logisticpro' ),
					get_bloginfo( 'name' )
				);
			?>
  </p>
  <?php endif; ?>
  <?php if ( $attributes['lost_password_sent'] ) : ?>
  <p class="login-info">
    <?php echo __( 'Check your email for a link to reset your password.', 'logisticpro' ); ?>
  </p>
  <?php endif; ?>
  <?php if ( $attributes['password_updated'] ) : ?>
  <p class="login-info">
    <?php echo __( 'Your password has been changed. You can sign in now.', 'logisticpro' ); ?>
  </p>
  <?php endif; ?>
  <form method="post" action="<?php echo wp_login_url(); ?>" class="registration">
    <div class="box-header">
      <h2><?php echo __( 'Login to your account', 'logisticpro' ); ?></h2>
    </div>
    <label for="user_login"><?php echo __( 'Email', 'logisticpro' ); ?><span class="color-red">*</span></label>
    <input type="text" name="log" id="user_login" class="form-control margin-bottom-20" value="demo@logisticpro.glixentech.com">
    <label for="user_pass"><?php echo __( 'Password', 'logisticpro' ); ?><span class="color-red">*</span></label>
    <input type="password" name="pwd" id="user_pass" class="form-control margin-bottom-20" value="logisticpro321">
    <label>
      <input name="rememberme" type="checkbox" id="rememberme" value="forever"/>
      <?php echo __( 'Remember Me', 'logisticpro' ); ?> </label>
    <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-primary btn-sm pull-right" value="<?php echo __( 'Sign In', 'logisticpro' ); ?>" />    <hr>
    <h4><?php echo __( 'Forget your Password ?', 'logisticpro' ); ?></h4>
    <p><?php echo __( 'no worries', 'logisticpro' ); ?>, 
        <a href="<?php echo esc_url( $this->logisticpro_get_page('logistic-pro-forgot-page', '') ); ?>" class="color-green"><?php echo __( 'click here', 'logisticpro' ); ?></a> <?php echo __( 'to reset your password.', 'logisticpro' ); ?> 
            <?php echo __( 'OR', 'logisticpro' ); ?><br /><br />
        <a href="<?php echo esc_url( $this->logisticpro_get_page('logistic-pro-signup-page', '') ); ?>" class="btn btn-primary btn-sm"><?php echo __( 'Register', 'logisticpro' ); ?></a>    
        </p>    
    
  </form>
</div>
<?php }else{ ?>
<div class="login-form-container">
  <form method="post" action="<?php echo wp_login_url(); ?>" class="registration">
    <div class="box-header">
      <h2><?php echo __( 'Login to your account', 'logisticpro' ); ?></h2>
    </div>
    <label for="user_login"><?php echo __( 'Email', 'logisticpro' ); ?><span class="color-red">*</span></label>
    <input type="text" name="log" id="user_login" class="form-control margin-bottom-20" value="demo@logisticpro.glixentech.com">
    <label for="user_pass"><?php echo __( 'Password', 'logisticpro' ); ?><span class="color-red">*</span></label>
    <input type="password" name="pwd" id="user_pass" class="form-control margin-bottom-20" value="logisticpro321">
    <label>
      <input name="rememberme" type="checkbox" id="rememberme" value="forever"/>
      <?php echo __( 'Remember Me', 'logisticpro' ); ?> </label>
    <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-primary btn-sm pull-right" value="<?php echo __( 'Sign In', 'logisticpro' ); ?>" />
    <hr>
    <h4><?php echo __( 'Forget your Password ?', 'logisticpro' ); ?></h4>
    <p><?php echo __( 'no worries', 'logisticpro' ); ?>, 
    <a href="<?php echo esc_url( $this->logisticpro_get_page('logistic-pro-forgot-page', '') ); ?>" class="color-green"><?php echo __( 'click here', 'logisticpro' ); ?></a> <?php echo __( 'to reset your password.', 'logisticpro' ); ?> 
    	OR<br /><br />
    <a href="<?php echo esc_url( $this->logisticpro_get_page('logistic-pro-signup-page', '') ); ?>" class="btn btn-primary btn-sm"><?php echo __( 'Register', 'logisticpro' ); ?></a>    <hr>
    </p>


  </form>
</div>
<?php } ?>
