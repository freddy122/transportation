<div class="container"> 
  <!-- Row -->
  
  
  <div class="row">
    <div class="col-md-7  ">
      <div class="about-title">
        <h2><?php  echo logisticpro_field_parms('logisticpro_cargo_tagline', 'val'); ?></h2>
        <p><?php  echo logisticpro_field_parms('logisticpro_cargo_description', 'val'); ?></p>
      </div>
      <div id="msg-box"></div>
      <div class="margin-top-50">
        <form action="#" id="order-cargo" method="post">
        <div class="about-title-1">
        <h2><?php echo __("Cargo Info", "logistic-pro");?></h2>
      </div>
          <div class="panel panel-default">
            <div class="panel-body">
			  
	  <?php echo logisticproForm::fields('logisticpro_shiping_mode', 'site', '12', 'select', 'Shipping Mode:', 'logistic-shipments-type'); ?>
	  <?php echo logisticproForm::fields('logisticpro_cargo_name', 'site', '12', 'input', 'Commodity/Cargo Name:'); ?>
      <?php echo logisticproForm::fields('logisticpro_weight_value', 'site', '7', 'input', 'Total Estimated Weight:'); ?>
      <?php echo logisticproForm::fields('logisticpro_weight_unit', 'site', '5', 'select', 'Select Weight Unit', 'logistic-weight-type'); ?>
      <?php echo logisticproForm::fields('logisticpro_packing_type', 'site', '12', 'select', 'Packing Type:', 'logistic-packings-type'); ?>
      <?php echo logisticproForm::fields('logisticpro_quantity', 'site', '12', 'input', 'Quantity:'); ?>
      <?php echo logisticproForm::fields('logisticpro_cargo_type', 'site', '12', 'select', 'Type of Cargo:', 'logistic-cargo-type'); ?>
      <?php echo logisticproForm::fields('logisticpro_is_insurance', 'site', '12', 'select', 'Insurance Required?', 'insurance'); ?>
            </div>
          </div>
          <h2><?php echo __("Sender Info", "logistic-pro");?></h2>
          <div class="panel panel-default">
            <div class="panel-body">
		<?php echo logisticproForm::fields('logisticpro_sender_country', 'site', '6', 'select', 'Country:', 'logistic-location-type'); ?>
        <?php echo logisticproForm::fields('logisticpro_sender_state', 'site', '6', 'input', 'State / County:'); ?>
        <?php echo logisticproForm::fields('logisticpro_sender_city', 'site', '6', 'input', 'Town / City:'); ?>            
        <?php echo logisticproForm::fields('logisticpro_sender_postcode', 'site', '6', 'input', 'Postcode / ZIP:'); ?>
        <?php echo logisticproForm::fields('logisticpro_sender_street', 'site', '12', 'input', 'Street Street Address:'); ?>
        <?php echo logisticproForm::fields('logisticpro_sender_appart', 'site', '12', 'input', 'Apartment, suite, unit:'); ?>
        <?php echo logisticproForm::fields('logisticpro_sender_name', 'site', '12', 'input', 'Company/Person Name:'); ?>            
            </div>
          </div>
          
          <h2><?php echo __("Receiver Info", "logistic-pro");?></h2>
          <div class="panel panel-default">
            <div class="panel-body">
		<?php echo logisticproForm::fields('logisticpro_receiver_country', 'site', '6', 'select', 'Country:', 'logistic-location-type'); ?>
        <?php echo logisticproForm::fields('logisticpro_receiver_state', 'site', '6', 'input', 'State / County:'); ?>
        <?php echo logisticproForm::fields('logisticpro_receiver_city', 'site', '6', 'input', 'Town / City:'); ?>            
        <?php echo logisticproForm::fields('logisticpro_receiver_postcode', 'site', '6', 'input', 'Postcode / ZIP:'); ?>
        <?php echo logisticproForm::fields('logisticpro_receiver_street', 'site', '12', 'input', 'Street Street Address:'); ?>
        <?php echo logisticproForm::fields('logisticpro_receiver_appart', 'site', '12', 'input', 'Apartment, suite, unit:'); ?>
        <?php echo logisticproForm::fields('logisticpro_receiver_name', 'site', '12', 'input', 'Company/Person Name:'); ?>             
            </div>
          </div>
          
          <h2><?php echo __("Pick up Info", "logistic-pro");?></h2>
          <div class="panel panel-default">
            <div class="panel-body">
				<?php echo logisticproForm::fields('logisticpro_pickup_name', 'site', '12', 'input', 'Contact Name:'); ?>
                <?php echo logisticproForm::fields('logisticpro_pickup_phone', 'site', '12', 'input', 'Phone Number:'); ?>
                <?php echo logisticproForm::fields('logisticpro_pickup_datetime', 'site', '12', 'input', 'Pick up Date:'); ?>            
                <?php echo logisticproForm::fields('logisticpro_pickup_aditional_info', 'site', '12', 'textarea', 'Additional Details:'); ?>
 
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-body">
              <div class="col-sm-12 col-md-12 col-xs-12 clearfix">
                <button type="button" id="order-for-cargo" class="btn btn-primary btn-sm"><?php echo __("Book Now", "logistic-pro");?></button>
                <img id="loader" alt="<?php echo __("loader", "logistic-pro");?>" src="<?php echo get_template_directory_uri();?>/images/loader.gif" class="loader"> </div>
              <!-- End col-sm-6 --> 
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- Row End --> 
  </div>