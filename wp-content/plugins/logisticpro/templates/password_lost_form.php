<?php if ( $attributes['show_title'] ) : ?>
	<h3><?php echo __( 'Forgot Your Password?', 'logisticpro' ); ?></h3>
<?php endif; ?>

<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
	<?php foreach ( $attributes['errors'] as $error ) : ?>
		<p>
			<div class="alert alert-danger">
                  <strong>Alert!</strong> <?php echo $error; ?>
                </div>
		</p>
	<?php endforeach; ?>
<?php endif; ?>

<p>
	<?php
		echo __(
			"Enter your email address and we'll send you a link you can use to pick a new password.",
			'logisticpro_login'
		);
	?>
</p>

<form id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post" class="registration">
	<p class="form-row">
		<label for="user_login"><?php echo __( 'Email', 'logisticpro' ); ?></label>
		<input type="text" name="user_login" id="user_login"  class="form-control margin-bottom-20">
	</p>

	<p class="lostpassword-submit">
		<input type="submit" name="submit" class="lostpassword-button btn btn-primary btn-sm" value="<?php echo __( 'Reset Password', 'logisticpro' ); ?>"/>
	</p>
</form>