<?php  $user = wp_get_current_user(); ?>

<div class="container"> 
  <!-- Row -->
    <div class="col-sm-12 col-xs-12 col-md-12">
        <div class="profile-section margin-bottom-20">
          <div class="profile-tabs">
            <ul class="nav nav-justified nav-tabs">
              <li class="active"><a href="#profile" data-toggle="tab"><?php echo __("Profile", "logistic-pro");?></a></li>
              <li><a href="#password" data-toggle="tab"><?php echo __("Change/Update Password", "logistic-pro");?></a></li>
              <li><a href="#cargo" data-toggle="tab"><?php echo __("Recent Cargos", "logistic-pro");?></a></li>
            </ul>
            <div class="tab-content">
              <div class="profile-edit tab-pane fade in active" id="profile"> <br>
                <dl class="dl-horizontal">
                  <dt><strong><?php echo __("Uername", "logistic-pro");?> </strong></dt>
                  <dd> <?php echo esc_html( $user->user_login );?> </dd>
                  <hr>
                  <dt><strong><?php echo __("Your name", "logistic-pro");?> </strong></dt>
                  <dd> <?php echo esc_html( $user->display_name );?> </dd>
                  <hr>
                  <dt><strong><?php echo __("Email Address", "logistic-pro");?> </strong></dt>
                  <dd> <?php echo esc_html( $user->user_email );?> </dd>
                </dl>
              </div>
              <div class="profile-edit tab-pane fade" id="password">
                <h2 class="heading-md"><?php echo __("Change/Update Password", "logistic-pro");?></h2>
                <br>
                <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" id="change-password" class="change-password" novalidate method="post">
                  <!--Checkout-Form-->
                  <div id="msg-box"></div>
                  <div class="logisticpro-div clearfix">
                    <div class="form-group">
                      <label><?php echo __("Old Password", "logistic-pro");?></label>
                      <input type="password" id="logisticpro_old_password" value="" name="logisticpro_old_password" class="form-control">
                    </div>
                  </div>
                  <div class="logisticpro-div clearfix">
                    <div class="form-group">
                      <label><?php echo __("New Password", "logistic-pro");?></label>
                      <input type="password" id="logisticpro_new_password" value="" name="logisticpro_new_password" class="form-control">
                    </div>
                  </div>
                  <div class="logisticpro-div clearfix">
                    <div class="form-group">
                      <label><?php echo __("Re-Type Password", "logistic-pro");?></label>
                      <input type="password" id="logisticpro_re_password" value="" name="logisticpro_re_password" class="form-control">
                    </div>
                  </div>
                  <input type="hidden" name="action" value="changePassword">
                  <button class="btn btn-sm btn-default"type="button">Cancel</button>
                  <button type="button" class="btn btn-sm btn-primary" name="submitPass" id="change-password-btn">Save Changes</button>
                  <!--End Checkout-Form-->
                </form>
              </div>
              <div class="profile-edit tab-pane fade" id="cargo">
                <h2 class="heading-md"><?php echo __("Recent Cargo Info.", "logistic-pro");?></h2>
                <br>
                <div class="table-responsive">
                  <table class="table table-clean-paddings margin-bottom-0">
                    <thead>
                      <tr>
                        <th><?php echo __("Cargo Id", "logistic-pro");?></th>
                        <th><?php echo __("Cargo Name", "logistic-pro");?></th>
                        <th><?php echo __("Shipped From", "logistic-pro");?></th>
                        <th><?php echo __("Destination", "logistic-pro");?></th>
                        <th><?php echo __("Status", "logistic-pro");?></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
						if ( is_user_logged_in() ){
						
							$html = '';
							global $current_user;
							wp_get_current_user();
							$cargo_query = array('post_type' => 'shipment', 'posts_per_page' => '10','author' => $current_user->ID);
							$cargo_posts = new WP_Query( $cargo_query );
							while($cargo_posts->have_posts()) 
							{ 
								$cargo_posts->the_post();
								$logisticpro_details = json_decode(  get_post_meta( get_the_ID(), '_logisticpro_all_fields', true), true );
								
								
							?>
                      <tr>
                        <td><?php echo esc_html( get_post_meta( get_the_ID(), '_logisticpro_tracking_id', true) ); ?></td>
                        <td><div class="contact-container"> <a href="<?php echo esc_url( get_the_permalink() );?>"><?php echo esc_html( $logisticpro_details['logisticpro_cargo_name'] ); ?></a> <span><?php echo __("on", "logistic-pro");?> <?php echo get_the_Date(); ?></span> </div></td>
                        <?php $trm = get_term( $logisticpro_details['logisticpro_sender_country'] ); ?>
                        <td><?php if( isset( $trm->name ) ) { echo esc_html(  $trm->name ); } ?></td>
                        <?php $trm = get_term( $logisticpro_details['logisticpro_receiver_country'] ); ?>
                        <td><?php if( isset( $trm->name ) ) { echo esc_html(  $trm->name ); } ?></td>
                        <?php  $trm_status = @get_term( $logisticpro_details['logisticpro_order_status'] );  ?>
                        <td><span class="label label-transparent">
                          <?php if( isset( $trm_status->name ) ) { echo esc_html(  $trm_status->name ); }else{ echo __("N/A", "logistic-pro"); } ?>
                          </span></td>
                      </tr>
                      <?php           
	}


}
?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
  </div>
  
  <!-- Row End --> 
</div>
