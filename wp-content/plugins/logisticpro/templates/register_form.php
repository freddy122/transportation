<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
	<?php if ( $attributes['show_title'] ) : ?>
		<h3><?php echo __( 'Register', 'logisticpro' ); ?></h3>
	<?php endif; ?>

	<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
		<?php foreach ( $attributes['errors'] as $error ) : ?>
			<p>
				<div class="alert alert-danger">
                  <strong><?php echo __( 'Alert!', 'logisticpro' ); ?></strong> <?php echo $error; ?>
                </div>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>

	<form id="signupform" action="<?php echo wp_registration_url(); ?>" method="post"  class="registration">
    
        <div class="box-header">
            <h2><?php echo __( 'Register a new account', 'logisticpro' ); ?></h2>
            <p><?php echo __( 'Already Signed Up? Click ', 'logisticpro' ); ?> <a href="<?php echo esc_url( $this->logisticpro_get_page('logistic-pro-login-page', '') ); ?>"><?php echo __( 'Sign In', 'logisticpro' ); ?></a> <?php echo __( 'to login your account.', 'logisticpro' ); ?></p>
        </div>    
		<p class="form-row">
			<label for="email"><?php echo __( 'Email', 'logisticpro' ); ?> <strong>*</strong></label>
			<input type="text" name="email" id="email" class="form-control margin-bottom-20" >
		</p>

		<p class="form-row">
			<label for="first_name"><?php echo __( 'First name', 'logisticpro' ); ?></label>
			<input type="text" name="first_name" id="first-name" class="form-control margin-bottom-20">
		</p>

		<p class="form-row">
			<label for="last_name"><?php echo __( 'Last name', 'logisticpro' ); ?></label>
			<input type="text" name="last_name" id="last-name" class="form-control margin-bottom-20">
		</p>

		<p class="form-row">
		<?php echo __( 'Note: Your password will be generated automatically and emailed to the address you specify above.', 'logisticpro' ); ?>
		</p>

		<?php if ( $attributes['recaptcha_site_key'] ) : ?>
			<div class="recaptcha-container">
				<div class="g-recaptcha" data-sitekey="<?php echo $attributes['recaptcha_site_key']; ?>"></div>
			</div>
		<?php endif; ?>

		<p></p>

		<p class="signup-submit">
			<input type="submit" name="submit" class="btn btn-primary btn-sm pull-right" value="<?php echo __( 'Register', 'logisticpro' ); ?>"/>
            <br /><br />
            
		</p>
	</form>

    
</div>