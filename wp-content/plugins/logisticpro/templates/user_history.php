<?php  $user = wp_get_current_user(); ?>

<div class="block-content">
  <div class="table-responsive">
    <table class="table table-clean-paddings margin-bottom-0 no-table-border">
      <thead>
        <tr>
          <th><?php echo __("Cargo Id", "logistic-pro");?></th>
          <th><?php echo __("Cargo Name", "logistic-pro");?></th>
          <th><?php echo __("Shipped From", "logistic-pro");?></th>
          <th><?php echo __("Destination", "logistic-pro");?></th>
          <th><?php echo __("Status", "logistic-pro");?></th>
        </tr>
      </thead>
      <tbody>
        <?php 
if ( is_user_logged_in() ){

		$html = '';
		global $current_user;
		wp_get_current_user();
		
		$arrID = '';
		$posts_per_page = -1;
		if( isset( $_GET['id'] ) )
		{
			$post_id = base64_decode( $_GET['id'] );	
			$arrID = "[p] => $post_id";
			$posts_per_page = 1;
		}
	
		$cargo_query = array('post_type' => 'shipment', 'posts_per_page' => $posts_per_page, 'author' => $current_user->ID, $arrID);
		
		
		$cargo_posts = new WP_Query( $cargo_query );
		while($cargo_posts->have_posts()) 
		{ 
			$cargo_posts->the_post();
			$logisticpro_details = json_decode(  get_post_meta( get_the_ID(), '_logisticpro_all_fields', true), true );
			
			$shipmentID = get_the_permalink() ;			
    	?>
        <tr>
          <td><?php echo esc_html( get_post_meta( get_the_ID(), '_logisticpro_tracking_id', true) ); ?></td>
          <td><div class="contact-container"> <a href="<?php echo esc_attr( $shipmentID ); ?>"><?php echo esc_html( $logisticpro_details['logisticpro_cargo_name'] ); ?></a> <span><?php echo __("on", "logistic-pro");?> <?php echo get_the_Date(); ?></span> </div></td>
          <?php $trm = get_term( $logisticpro_details['logisticpro_sender_country'] ); ?>
          <td><?php if( isset( $trm->name ) ) { echo esc_html(  $trm->name ); } ?></td>
          <?php $trm = get_term( $logisticpro_details['logisticpro_receiver_country'] ); ?>
          <td><?php if( isset( $trm->name ) ) { echo esc_html(  $trm->name ); } ?></td>
          <?php  $trm_status = @get_term( $logisticpro_details['logisticpro_order_status'] );  ?>
          <td><span class="label1 label-transparent1">
            <?php if( isset( $trm_status->name ) ) { echo esc_html(  $trm_status->name ); }else{ echo __("N/A", "logistic-pro"); } ?>
            </span></td>
        </tr>
        <?php           
		}
}
?>
      </tbody>
    </table>
  </div>
</div>
<!-- Tracking History End --> 