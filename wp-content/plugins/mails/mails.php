<?php
/*
Plugin Name: Page mails
Description: Controle page mails
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/

add_shortcode( 'mails_page', 'afficher_mails_page');

function afficher_mails_page(){
    if(!is_admin()){
        ob_start();
        
        global $wpdb;
    
        
        require dirname( __FILE__ ) . '/template/index.php';
        return ob_get_clean();
    }
}

add_action('wp_ajax_get_detail_mails', 'get_detail_mails');
add_action('wp_ajax_nopriv_get_detail_mails', 'get_detail_mails');
function get_detail_mails() {
    global $wpdb;
    $refs      = $_GET['refs'];
    
    $quersRfCl = "SELECT * from ".$wpdb->prefix."services where reference = '".$refs."'";
    $datasRfCl = $wpdb->get_results($quersRfCl, OBJECT);
   
    $quersRfCl2 = "SELECT * from ".$wpdb->prefix."devis_mails where reference_user = '".$refs."' and id_forwarder =". get_current_user_id()." and mail_type = 'new_devis'";
    $datasRfCl2 = $wpdb->get_results($quersRfCl2, OBJECT);
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'lecture_message'  => 'lu',
        ), 
        array( 
            'reference_user' => $refs,
            'id_forwarder' => get_current_user_id(),
            'mail_type' => 'new_devis',
        )
    );
    foreach($datasRfCl2 as $rfcl0){
        
        $details_refs = wpautop($rfcl0->mail_content)."<br> <span id='clique_detail_offre' style='color:#000000; cursor:pointer'><h3><u>Détail de l'offre</u></h3></span>";
        echo $details_refs;
    }
    foreach($datasRfCl as $rfcl){
        $details_refs0 = "<div style='color:#ffffff'>"."<br>";
                         //var_dump($rfcl);       
        //$details_refs0 .= "Nom du client : ".$rfcl->id_client."<br>";
        /*$dtch = DateTime::createFromFormat('d/m/Y', $rfcl->date_creation);
        $dtchf = $dtch->format('Y-m-d H:i:s');*/
        $details_refs0 .= "Type : ".$rfcl->type_societe."<br>";
        $details_refs0 .= "Expresse : ".$rfcl->express."<br>";
        $details_refs0 .= "Date création : ".$rfcl->date_creation."<br>";
        $details_refs0 .= "Pays chargement : ".$rfcl->pays_chargement."<br>";
        $details_refs0 .= "Ville chargement : ".$rfcl->ville_chargement."<br>";
        $details_refs0 .= "Adresse chargement : ".$rfcl->adresse_chargement."<br>";
        $dt = date("d-m-Y", strtotime($rfcl->date_chargement));
        $details_refs0 .= "Date et heure chargement : ".$dt." ( ".$rfcl->heure_desirer_chargement." ) <br>";
        $details_refs0 .= "Contrainte chargement : ".$rfcl->contrainte_chargement."<br>";
        $details_refs0 .= "Pays dechargement : ".$rfcl->pays_dechargement."<br>";
        $details_refs0 .= "Ville dechargement : ".$rfcl->ville_dechargement."<br>";
        $details_refs0 .= "Adresse dechargement : ".$rfcl->adresse_dechargement."<br>";
        $d2t = date("d-m-Y", strtotime($rfcl->date_dechargement));
        $details_refs0 .= "Date et heure de dechargement : ".$d2t." ( ".$rfcl->heure_desirer_dechargement." )<br>";
        $details_refs0 .= "Contrainte de dechargement : ".$rfcl->contrainte_dechargement."<br>";
        $details_refs0 .= "Accepter sous traitant : ".$rfcl->accepter_sous_traitant."<br>";
        $details_refs0 .= "Emballage : ".$rfcl->emballage."<br>";
        $details_refs0 .= "Hauteur x Longueur x Largeur x Poids : ".$rfcl->hauteur." cm x ".$rfcl->longueur." cm x ".$rfcl->largeur." cm x ".$rfcl->poids." kg<br>";
        $details_refs0 .= "Matière dangereux : ".$rfcl->matiere_dangereux."<br>";
        $details_refs0 .= "Fragile : ".$rfcl->fragile."<br>";
        $details_refs0 .= "</div>";
        
        echo $details_refs0;
    }
    die();
}

add_action('wp_ajax_get_detail_mails2', 'get_detail_mails2');
add_action('wp_ajax_nopriv_get_detail_mails2', 'get_detail_mails2');
function get_detail_mails2() {
    global $wpdb;
    $refs      = $_GET['refs'];
    
    $quersRfCl = "SELECT * from ".$wpdb->prefix."services where reference = '".$refs."'";
    $datasRfCl = $wpdb->get_results($quersRfCl, OBJECT);
   
    $quersRfCl2 = "SELECT * from ".$wpdb->prefix."devis_mails where reference_user = '".$refs."' and id_forwarder =". get_current_user_id()." and mail_type = 'confirmation_devis'";
    $datasRfCl2 = $wpdb->get_results($quersRfCl2, OBJECT);
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'lecture_message'  => 'lu',
        ), 
        array( 
            'reference_user' => $refs,
            'id_forwarder' => get_current_user_id(),
            'mail_type' => 'confirmation_devis',
        )
    );
    foreach($datasRfCl2 as $rfcl0){
        
        $details_refs = wpautop($rfcl0->mail_content)."<br> <span id='clique_detail_offre' style='color:#000000; cursor:pointer'><h3><u>Détail de l'offre</u></h3></span>";
        echo $details_refs;
    }
    foreach($datasRfCl as $rfcl){
        $details_refs0 = "<div style='color:#ffffff'>"."<br>";
                         //var_dump($rfcl);       
        //$details_refs0 .= "Nom du client : ".$rfcl->id_client."<br>";
        /*$dtch = DateTime::createFromFormat('d/m/Y', $rfcl->date_creation);
        $dtchf = $dtch->format('Y-m-d H:i:s');*/
        $details_refs0 .= "Type : ".$rfcl->type_societe."<br>";
        $details_refs0 .= "Expresse : ".$rfcl->express."<br>";
        $details_refs0 .= "Date création : ".$rfcl->date_creation."<br>";
        $details_refs0 .= "Pays chargement : ".$rfcl->pays_chargement."<br>";
        $details_refs0 .= "Ville chargement : ".$rfcl->ville_chargement."<br>";
        $details_refs0 .= "Adresse chargement : ".$rfcl->adresse_chargement."<br>";
        $dt = date("d-m-Y", strtotime($rfcl->date_chargement));
        $details_refs0 .= "Date et heure chargement : ".$dt." ( ".$rfcl->heure_desirer_chargement." ) <br>";
        $details_refs0 .= "Contrainte chargement : ".$rfcl->contrainte_chargement."<br>";
        $details_refs0 .= "Pays dechargement : ".$rfcl->pays_dechargement."<br>";
        $details_refs0 .= "Ville dechargement : ".$rfcl->ville_dechargement."<br>";
        $details_refs0 .= "Adresse dechargement : ".$rfcl->adresse_dechargement."<br>";
        $d2t = date("d-m-Y", strtotime($rfcl->date_dechargement));
        $details_refs0 .= "Date et heure de dechargement : ".$d2t." ( ".$rfcl->heure_desirer_dechargement." )<br>";
        $details_refs0 .= "Contrainte de dechargement : ".$rfcl->contrainte_dechargement."<br>";
        $details_refs0 .= "Accepter sous traitant : ".$rfcl->accepter_sous_traitant."<br>";
        $details_refs0 .= "Emballage : ".$rfcl->emballage."<br>";
        $details_refs0 .= "Hauteur x Longueur x Largeur x Poids : ".$rfcl->hauteur." cm x ".$rfcl->longueur." cm x ".$rfcl->largeur." cm x ".$rfcl->poids." kg<br>";
        $details_refs0 .= "Matière dangereux : ".$rfcl->matiere_dangereux."<br>";
        $details_refs0 .= "Fragile : ".$rfcl->fragile."<br>";
        $details_refs0 .= "</div>";
        
        echo $details_refs0;
    }
    die();
}

add_action('wp_ajax_get_detail_mails3', 'get_detail_mails3');
add_action('wp_ajax_nopriv_get_detail_mails3', 'get_detail_mails3');
function get_detail_mails3() {
    global $wpdb;
    $refs      = $_GET['refs'];
    
    $quersRfCl = "SELECT * from ".$wpdb->prefix."services where reference = '".$refs."'";
    $datasRfCl = $wpdb->get_results($quersRfCl, OBJECT);
   
    $quersRfCl2 = "SELECT * from ".$wpdb->prefix."devis_mails where reference_user = '".$refs."' and 	id_user =". get_current_user_id()." and mail_type = 'proposition_devis'";
    
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'lecture_message'  => 'lu',
        ), 
        array( 
            'reference_user' => $refs,
            'id_user' => get_current_user_id(),
            'mail_type' => 'proposition_devis',
        )
    );
    $datasRfCl2 = $wpdb->get_results($quersRfCl2, OBJECT);
    
    foreach($datasRfCl2 as $rfcl0){
        
        $details_refs = wpautop($rfcl0->mail_content)."<br> <span id='clique_detail_offre' style='color:#000000; cursor:pointer'><h3><u>Détail de l'offre</u></h3></span>";
        echo $details_refs;
    }
    foreach($datasRfCl as $rfcl){
        $details_refs0 = "<div style='color:#ffffff'>"."<br>";
                         //var_dump($rfcl);       
        //$details_refs0 .= "Nom du client : ".$rfcl->id_client."<br>";
        /*$dtch = DateTime::createFromFormat('d/m/Y', $rfcl->date_creation);
        $dtchf = $dtch->format('Y-m-d H:i:s');*/
        $details_refs0 .= "Type : ".$rfcl->type_societe."<br>";
        $details_refs0 .= "Expresse : ".$rfcl->express."<br>";
        $details_refs0 .= "Date création : ".$rfcl->date_creation."<br>";
        $details_refs0 .= "Pays chargement : ".$rfcl->pays_chargement."<br>";
        $details_refs0 .= "Ville chargement : ".$rfcl->ville_chargement."<br>";
        $details_refs0 .= "Adresse chargement : ".$rfcl->adresse_chargement."<br>";
        $dt = date("d-m-Y", strtotime($rfcl->date_chargement));
        $details_refs0 .= "Date et heure chargement : ".$dt." ( ".$rfcl->heure_desirer_chargement." ) <br>";
        $details_refs0 .= "Contrainte chargement : ".$rfcl->contrainte_chargement."<br>";
        $details_refs0 .= "Pays dechargement : ".$rfcl->pays_dechargement."<br>";
        $details_refs0 .= "Ville dechargement : ".$rfcl->ville_dechargement."<br>";
        $details_refs0 .= "Adresse dechargement : ".$rfcl->adresse_dechargement."<br>";
        $d2t = date("d-m-Y", strtotime($rfcl->date_dechargement));
        $details_refs0 .= "Date et heure de dechargement : ".$d2t." ( ".$rfcl->heure_desirer_dechargement." )<br>";
        $details_refs0 .= "Contrainte de dechargement : ".$rfcl->contrainte_dechargement."<br>";
        $details_refs0 .= "Accepter sous traitant : ".$rfcl->accepter_sous_traitant."<br>";
        $details_refs0 .= "Emballage : ".$rfcl->emballage."<br>";
        $details_refs0 .= "Hauteur x Longueur x Largeur x Poids : ".$rfcl->hauteur." cm x ".$rfcl->longueur." cm x ".$rfcl->largeur." cm x ".$rfcl->poids." kg<br>";
        $details_refs0 .= "Matière dangereux : ".$rfcl->matiere_dangereux."<br>";
        $details_refs0 .= "Fragile : ".$rfcl->fragile."<br>";
        $details_refs0 .= "</div>";
        
        echo $details_refs0;
    }
    die();
}

add_action('wp_ajax_get_detail_mails4', 'get_detail_mails4');
add_action('wp_ajax_nopriv_get_detail_mails4', 'get_detail_mails4');
function get_detail_mails4() {
    global $wpdb;
    $refs      = $_GET['refs'];
    
    $quersRfCl = "SELECT * from ".$wpdb->prefix."services where reference = '".$refs."'";
    $datasRfCl = $wpdb->get_results($quersRfCl, OBJECT);
   
    $quersRfCl2 = "SELECT * from ".$wpdb->prefix."devis_mails where reference_user = '".$refs."' and 	id_user =". get_current_user_id()." and mail_type = 'confirmation_devis_client'";
    //var_dump($quersRfCl2);
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'lecture_message'  => 'lu',
        ), 
        array( 
            'reference_user' => $refs,
            'id_user' => get_current_user_id(),
            'mail_type' => 'confirmation_devis_client',
        )
    );
    $datasRfCl2 = $wpdb->get_results($quersRfCl2, OBJECT);
    
    foreach($datasRfCl2 as $rfcl0){
        
        $details_refs = wpautop($rfcl0->mail_content)."<br> <span id='clique_detail_offre' style='color:#000000; cursor:pointer'><h3><u>Détail de l'offre</u></h3></span>";
        echo $details_refs;
    }
    foreach($datasRfCl as $rfcl){
        $details_refs0 = "<div style='color:#ffffff'>"."<br>";
                         //var_dump($rfcl);       
        //$details_refs0 .= "Nom du client : ".$rfcl->id_client."<br>";
        /*$dtch = DateTime::createFromFormat('d/m/Y', $rfcl->date_creation);
        $dtchf = $dtch->format('Y-m-d H:i:s');*/
        $details_refs0 .= "Type : ".$rfcl->type_societe."<br>";
        $details_refs0 .= "Expresse : ".$rfcl->express."<br>";
        
        $details_refs0 .= "Date création : ".$rfcl->date_creation."<br>";
        $details_refs0 .= "Pays chargement : ".$rfcl->pays_chargement."<br>";
        $details_refs0 .= "Ville chargement : ".$rfcl->ville_chargement."<br>";
        $details_refs0 .= "Adresse chargement : ".$rfcl->adresse_chargement."<br>";
        $dt = date("d-m-Y", strtotime($rfcl->date_chargement));
        $details_refs0 .= "Date et heure chargement : ".$dt." ( ".$rfcl->heure_desirer_chargement." ) <br>";
        $details_refs0 .= "Contrainte chargement : ".$rfcl->contrainte_chargement."<br>";
        $details_refs0 .= "Pays dechargement : ".$rfcl->pays_dechargement."<br>";
        $details_refs0 .= "Ville dechargement : ".$rfcl->ville_dechargement."<br>";
        $details_refs0 .= "Adresse dechargement : ".$rfcl->adresse_dechargement."<br>";
        $d2t = date("d-m-Y", strtotime($rfcl->date_dechargement));
        $details_refs0 .= "Date et heure de dechargement : ".$d2t." ( ".$rfcl->heure_desirer_dechargement." )<br>";
        $details_refs0 .= "Contrainte de dechargement : ".$rfcl->contrainte_dechargement."<br>";
        $details_refs0 .= "Accepter sous traitant : ".$rfcl->accepter_sous_traitant."<br>";
        $details_refs0 .= "Emballage : ".$rfcl->emballage."<br>";
        $details_refs0 .= "Hauteur x Longueur x Largeur x Poids : ".$rfcl->hauteur." cm x ".$rfcl->longueur." cm x ".$rfcl->largeur." cm x ".$rfcl->poids." kg<br>";
        $details_refs0 .= "Matière dangereux : ".$rfcl->matiere_dangereux."<br>";
        $details_refs0 .= "Fragile : ".$rfcl->fragile."<br>";
        $details_refs0 .= "</div>";
        
        echo $details_refs0;
    }
    die();
}


add_action('wp_ajax_change_detail_mails', 'change_detail_mails');
add_action('wp_ajax_nopriv_change_detail_mails', 'change_detail_mails');
function change_detail_mails() {
    global $wpdb;
    $refs      = $_GET['refs2'];
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'status_mail'  => 1,
        ), 
        array( 
            'reference_user' => $refs,
            'mail_type' => 'new_devis',
            'id_forwarder' => get_current_user_id(),
        )
    );
}

add_action('wp_ajax_change_detail_mails2', 'change_detail_mails2');
add_action('wp_ajax_nopriv_change_detail_mails2', 'change_detail_mails2');
function change_detail_mails2() {
    global $wpdb;
    $refs      = $_GET['refs3'];
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'status_mail'  => 1,
        ), 
        array( 
            'reference_user' => $refs,
            'mail_type' => 'confirmation_devis',
            'id_forwarder' => get_current_user_id(),
        )
    );
}

add_action('wp_ajax_change_detail_mails3', 'change_detail_mails3');
add_action('wp_ajax_nopriv_change_detail_mails3', 'change_detail_mails3');
function change_detail_mails3() {
    global $wpdb;
    $refs      = $_GET['refs4'];
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'status_mail'  => 1,
        ), 
        array( 
            'reference_user' => $refs,
            'mail_type' => 'proposition_devis',
            'id_user' => get_current_user_id(),
        )
    );
}
add_action('wp_ajax_change_detail_mails4', 'change_detail_mails4');
add_action('wp_ajax_nopriv_change_detail_mails4', 'change_detail_mails4');
function change_detail_mails4() {
    global $wpdb;
    $refs      = $_GET['refs4'];
    $wpdb->update( 
        $wpdb->prefix.'devis_mails',
        array( 
            'status_mail'  => 1,
        ), 
        array( 
            'reference_user' => $refs,
            'mail_type' => 'confirmation_devis_client',
            'id_user' => get_current_user_id(),
        )
    );
}
