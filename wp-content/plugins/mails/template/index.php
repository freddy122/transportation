<?php 
$pagenames = get_query_var("pagename");
$type_user = get_user_meta(get_current_user_id(), "type_user", true);
global $wpdb;
$user_id = get_current_user_id();
if($type_user == "client"){
    $quersUserclpp = "SELECT count(lecture_message)  as lst_message from ".$wpdb->prefix."devis_mails where id_user = '".$user_id."' "
            . " and lecture_message ='non_lu' "
            . " and mail_type in ('proposition_devis') ";
    $resUserclpp = $wpdb->get_results($quersUserclpp, OBJECT);
    $quersUserclcc = "SELECT count(lecture_message)  as lst_message from ".$wpdb->prefix."devis_mails where id_user = '".$user_id."' "
            . " and lecture_message ='non_lu' "
            . " and mail_type in ('confirmation_devis_client') ";
    $resUserclcc = $wpdb->get_results($quersUserclcc, OBJECT);
   
}else{
    $quersUserfwdnn = "SELECT count(lecture_message) as lst_message from ".$wpdb->prefix."devis_mails where id_forwarder = '".$user_id."' "
            . " and lecture_message ='non_lu' "
            . " and mail_type in ('new_devis') ";
    $resUserfwdnn = $wpdb->get_results($quersUserfwdnn, OBJECT);

    $quersUserfwdcc = "SELECT count(lecture_message) as lst_message from ".$wpdb->prefix."devis_mails where id_forwarder = '".$user_id."' "
            . " and lecture_message ='non_lu' "
            . " and mail_type in ('confirmation_devis') ";
    $resUserfwdcc = $wpdb->get_results($quersUserfwdcc, OBJECT);
    
}
?>


<?php 
    if($type_user !== "client"){
?>
    <div class="row" >
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-body style_paddin_pannels_custom">
                    <ul class="navs_custom">
                        <li><a  href="<?php echo get_site_url()."/mails"; ?>" class="<?php echo ($pagenames == "mails") ? "active":""; ?>"><?php  echo __("<!--:fr-->Demande du client<!--:--><!--:en-->Customer request<!--:-->");echo "<span class='class_proposition_devis'>(".$resUserfwdnn[0]->lst_message.")</span>"; ?> </a></li>
                        <li><a href="<?php echo get_site_url()."/devis-remporter"; ?>" class="<?php echo ($pagenames == "devis-remporter") ? "active":""; ?>"><?php  echo __("<!--:fr-->Devis remporter<!--:--><!--:en-->Winning quote<!--:-->");echo "<span class='class_accepted_devis'>(".$resUserfwdcc[0]->lst_message.")</span>"; ?></a></li>
                        <li><a href="<?php echo get_site_url()."/information-de-lentreprise"; ?>" class="<?php echo ($pagenames == "information-de-lentreprise") ? "active":""; ?>"><?php  echo __("<!--:fr-->Informations de l'entreprise<!--:--><!--:en-->Company Information<!--:-->"); ?></a></li>
                      </ul>
                </div>
            </div>
        </div>
    <?php 
     if($pagenames == "mails"){
    ?>
        <div class="col-md-9">
            <div class="modal fade " id="detail-mail-forwarder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body" id="modal-body-res-valider">
                      <div class="quotation-box-1">
                        <h2><u><?php  echo __("<!--:fr-->Contenu du mail<!--:--><!--:en-->Mail content<!--:-->");  ?></u></h2><br>
                        <div class="desc-text">

                        </div>
                        <form method="post" id="popup-qoute-form">
                            <div class="row clearfix"> 
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details">

                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details-detail-offre" style="display:none;">

                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                    <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                    <li role="presentation" class="active">
                        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
                            <?php  echo __("<!--:fr-->Boite de reception<!--:--><!--:en-->Mailbox<!--:-->"); ?>
                        </a>
                    </li> 
                    <li role="presentation" class="">
                        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">
                            <?php  echo __("<!--:fr-->Corbeille<!--:--><!--:en-->Trash<!--:-->"); ?>
                        </a>
                    </li> 

                </ul> 
                <div class="tab-content" id="myTabContent"> 
                    <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab"> 
                        <table id="liste-mails-demande-client" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Date création<!--:--><!--:en-->Creation Date<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                //$querys0 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'new_devis' and status_mail = 0 and id_forwarder = ".get_current_user_id();
                                $querys0 = "SELECT dm.*,serv.date_creation from ".$wpdb->prefix."devis_mails as dm
                                                inner join ".$wpdb->prefix."services as serv on dm.reference_user = serv.reference
                                                where mail_type = 'new_devis' and status_mail = 0 and id_forwarder = ".get_current_user_id();
                                $devi0 = $wpdb->get_results($querys0, OBJECT);
                                foreach($devi0 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td><?php echo date('d-m-Y h:i:s', strtotime($ldevs->date_creation)); ?></td>
                                        <td>
                                            <button class="btn btn-success" style="padding: 5px;" onclick="showDetail('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button>
                                            <button class="btn btn-danger" style="padding: 5px;" onclick="trashingMail('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Corbeilles<!--:--><!--:en-->Trash<!--:-->");  ?></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 
                    <div class="tab-pane fade " role="tabpanel" id="profile" aria-labelledby="profile-tab"> 
                         <table id="liste-mails-demande-client-trash" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Date création<!--:--><!--:en-->Creation Date<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                //$querys01 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'new_devis' and status_mail = 1 and id_forwarder = ".get_current_user_id();
                                $querys01 = "SELECT dm.*,serv.date_creation from ".$wpdb->prefix."devis_mails as dm
                                                inner join ".$wpdb->prefix."services as serv on dm.reference_user = serv.reference
                                                where mail_type = 'new_devis' and status_mail = 1 and id_forwarder = ".get_current_user_id();
                                $devi01 = $wpdb->get_results($querys01, OBJECT);
                                foreach($devi01 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td><?php echo date('d-m-Y h:i:s', strtotime($ldevs->date_creation)); ?></td>
                                        <td><button class="btn btn-success" style="padding: 5px;" onclick="showDetail('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 

                </div> 
            </div>
        </div>

    <?php 
     }elseif($pagenames == "devis-remporter"){
    ?>
        <div class="col-md-9">
            <div class="modal fade " id="detail-mail-forwarder-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body" id="modal-body-res-valider">
                      <div class="quotation-box-1">
                        <h2><u><?php  echo __("<!--:fr-->Contenu du mail<!--:--><!--:en-->Mail content<!--:-->");  ?></u></h2><br>
                        <div class="desc-text">

                        </div>
                        <form method="post" id="popup-qoute-form">
                            <div class="row clearfix"> 
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details">

                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details-detail-offre" style="display:none;">

                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                    <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                    <li role="presentation" class="active">
                        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
                            <?php  echo __("<!--:fr-->Boite de reception<!--:--><!--:en-->Mailbox<!--:-->"); ?>
                        </a>
                    </li> 
                    <li role="presentation" class="">
                        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">
                            <?php  echo __("<!--:fr-->Corbeille<!--:--><!--:en-->Trash<!--:-->"); ?>
                        </a>
                    </li> 

                </ul> 
                <div class="tab-content" id="myTabContent"> 
                    <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab"> 
                        <table id="liste-mails-demande-client" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $querys1 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'confirmation_devis' and status_mail = 0 and id_forwarder = ".get_current_user_id();
                                $devi1 = $wpdb->get_results($querys1, OBJECT);
                                foreach($devi1 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td>
                                            <button class="btn btn-success" style="padding: 5px;" onclick="showDetail2('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button>
                                            <button class="btn btn-danger" style="padding: 5px;" onclick="trashingMail2('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Corbeilles<!--:--><!--:en-->Trash<!--:-->");  ?></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 
                    <div class="tab-pane fade " role="tabpanel" id="profile" aria-labelledby="profile-tab"> 
                         <table id="liste-mails-demande-client-trash" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $querys02 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'confirmation_devis' and status_mail = 1 and id_forwarder = ".get_current_user_id();
                                $devi02 = $wpdb->get_results($querys02, OBJECT);
                                foreach($devi02 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td><button class="btn btn-success" style="padding: 5px;" onclick="showDetail2('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 

                </div> 
            </div>
        </div>
    <?php 
     }
    ?>
    </div>
<?php 
    } else{
?>
    <div class="row" >
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-body style_paddin_pannels_custom">
                    <ul class="navs_custom">
                        <li><a  href="<?php echo get_site_url()."/devis-transitaire"; ?>" class="<?php echo ($pagenames == "devis-transitaire") ? "active":""; ?>"><?php  echo __("<!--:fr-->Proposition dévis<!--:--><!--:en-->Proposal quote<!--:-->");echo "<span class='class_proposition_devis'>(".$resUserclpp[0]->lst_message.")</span>"; ?> </a></li>
                        <li><a href="<?php echo get_site_url()."/devis-confirmer"; ?>" class="<?php echo ($pagenames == "devis-confirmer") ? "active":""; ?>"><?php  echo __("<!--:fr-->Devis confirmer<!--:--><!--:en-->quote accepted<!--:-->");echo "<span class='class_accepted_devis'>(".$resUserclcc[0]->lst_message.")</span>"; ?></a></li>
                        <li><a href="<?php echo get_site_url()."/information-de-lentreprise"; ?>" class="<?php echo ($pagenames == "information-de-lentreprise") ? "active":""; ?>"><?php  echo __("<!--:fr-->Informations de l'entreprise<!--:--><!--:en-->Company Information<!--:-->"); ?></a></li>
                      </ul>
                </div>
            </div>
        </div>
    <?php 
     if($pagenames == "devis-transitaire"){
    ?>
        <div class="col-md-9">
            <div class="modal fade " id="detail-mail-transitaire-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body" id="modal-body-res-valider">
                      <div class="quotation-box-1">
                        <h2><u><?php  echo __("<!--:fr-->Contenu du mail<!--:--><!--:en-->Mail content<!--:-->");  ?></u></h2><br>
                        <div class="desc-text">

                        </div>
                        <form method="post" id="popup-qoute-form">
                            <div class="row clearfix"> 
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details">

                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details-detail-offre" style="display:none;">

                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                    <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                    <li role="presentation" class="active">
                        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
                            <?php  echo __("<!--:fr-->Boite de reception<!--:--><!--:en-->Mailbox<!--:-->"); ?>
                        </a>
                    </li> 
                    <li role="presentation" class="">
                        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">
                            <?php  echo __("<!--:fr-->Corbeille<!--:--><!--:en-->Trash<!--:-->"); ?>
                        </a>
                    </li> 

                </ul> 
                <div class="tab-content" id="myTabContent"> 
                    <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab"> 
                        <table id="liste-mails-transitaire" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $querys0t = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'proposition_devis' and status_mail = 0 and id_user = ".get_current_user_id();
                                $devi0t = $wpdb->get_results($querys0t, OBJECT);
                                foreach($devi0t as $ldevs){ ?>
                                    <!--tr style="background-color:#DD4B39!important;color:#ffffff!important;"-->
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td>
                                            <button class="btn btn-success" style="padding: 5px;" onclick="showDetail3('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button>
                                            <button class="btn btn-danger" style="padding: 5px;" onclick="trashingMail3('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Corbeilles<!--:--><!--:en-->Trash<!--:-->");  ?></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 
                    <div class="tab-pane fade " role="tabpanel" id="profile" aria-labelledby="profile-tab"> 
                         <table id="liste-mails-demande-client-trash" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $querys01 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'proposition_devis' and status_mail = 1 and id_user = ".get_current_user_id();
                                $devi01 = $wpdb->get_results($querys01, OBJECT);
                                foreach($devi01 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td><button class="btn btn-success" style="padding: 5px;" onclick="showDetail3('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 

                </div> 
            </div>
        </div>

    <?php 
     }elseif($pagenames == "devis-confirmer"){
    ?>
        <div class="col-md-9">
            <div class="modal fade " id="detail-mail-devis-confirmer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body" id="modal-body-res-valider">
                      <div class="quotation-box-1">
                        <h2><u><?php  echo __("<!--:fr-->Contenu du mail<!--:--><!--:en-->Mail content<!--:-->");  ?></u></h2><br>
                        <div class="desc-text">

                        </div>
                        <form method="post" id="popup-qoute-form">
                            <div class="row clearfix"> 
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details">

                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12" id="content-details-detail-offre" style="display:none;">

                                </div>
                                <!--Form Group-->
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                    <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i></a> 
                                </div>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="bs-example bs-example-tabs" data-example-id="togglable-tabs"> 
                <ul class="nav nav-tabs" id="myTabs" role="tablist"> 
                    <li role="presentation" class="active">
                        <a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">
                            <?php  echo __("<!--:fr-->Boite de reception<!--:--><!--:en-->Mailbox<!--:-->"); ?>
                        </a>
                    </li> 
                    <li role="presentation" class="">
                        <a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">
                            <?php  echo __("<!--:fr-->Corbeille<!--:--><!--:en-->Trash<!--:-->"); ?>
                        </a>
                    </li> 

                </ul> 
                <div class="tab-content" id="myTabContent"> 
                    <div class="tab-pane fade active in" role="tabpanel" id="home" aria-labelledby="home-tab"> 
                        <table id="liste-mails-devis-confirmer" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $querys13 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'confirmation_devis_client' and status_mail = 0 and id_user = ".get_current_user_id();
                                $devi13 = $wpdb->get_results($querys13, OBJECT);
                                foreach($devi13 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td>
                                            <button class="btn btn-success" style="padding: 5px;" onclick="showDetail4('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button>
                                            <button class="btn btn-danger" style="padding: 5px;" onclick="trashingMail4('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Corbeilles<!--:--><!--:en-->Trash<!--:-->");  ?></button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 
                    <div class="tab-pane fade " role="tabpanel" id="profile" aria-labelledby="profile-tab"> 
                         <table id="liste-mails-devis-confirmer-trash" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th><?php  echo __("<!--:fr-->ID transitaire<!--:--><!--:en-->Forwarder ID<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $querys02 = "SELECT * from ".$wpdb->prefix."devis_mails where mail_type = 'confirmation_devis_client' and status_mail = 1 and id_user = ".get_current_user_id();
                                $devi02 = $wpdb->get_results($querys02, OBJECT);
                                foreach($devi02 as $ldevs){ ?>
                                    <tr style="<?php echo $ldevs->lecture_message=="non_lu" ?"background-color:#DD4B39!important;color:#ffffff!important;":""; ?>">
                                        <td><?php echo $ldevs->id_forwarder; ?></td>
                                        <td><?php echo $ldevs->reference_user; ?></td>
                                        <td><button class="btn btn-success" style="padding: 5px;" onclick="showDetail4('<?php echo $ldevs->reference_user; ?>')"><?php  echo __("<!--:fr-->Détails<!--:--><!--:en-->View<!--:-->");  ?></button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div> 

                </div> 
            </div>
        </div>
    <?php 
     }
    ?>
    </div>
<?php 
    } 
?>
<script>
    jQuery(document).ready(function(){
       alert_message();
        jQuery('#myTabs a').click(function(e){
            e.preventDefault();
            jQuery(this).tab('show');
        });
        datatablesfunc('liste-mails-demande-client');
        datatablesfunc('liste-mails-demande-client-trash');
        datatablesfunc('liste-mails-transitaire');
        datatablesfunc('liste-mails-devis-confirmer');
        datatablesfunc('liste-mails-devis-confirmer-trash');
    });
    
    
    function datatablesfunc(id_table){
        jQuery("#"+id_table).DataTable({
            "language" : {
                "lengthMenu": "<?php  echo __("<!--:fr-->Afficher<!--:--><!--:en-->Display<!--:-->");  ?> _MENU_ <?php  echo __("<!--:fr-->enregistrement par page<!--:--><!--:en-->records per page<!--:-->");  ?>",
                "zeroRecords": "<?php  echo __("<!--:fr-->Aucun enregistrement<!--:--><!--:en-->Nothing found - sorry<!--:-->");  ?>",
                "info": "<?php  echo __("<!--:fr-->Voir page<!--:--><!--:en-->Showing page<!--:-->");  ?> _PAGE_ <?php  echo __("<!--:fr-->de<!--:--><!--:en-->of<!--:-->");  ?> _PAGES_",
                "infoEmpty": "<?php  echo __("<!--:fr-->Aucun enregistrement trouvé<!--:--><!--:en-->No records available<!--:-->");  ?>",
                "infoFiltered": "<?php  echo __("<!--:fr-->Filtrer parmis<!--:--><!--:en-->(filtered from<!--:-->");  ?> _MAX_ <?php  echo __("<!--:fr-->total enregistrement<!--:--><!--:en-->total records<!--:-->");  ?>)",
                "search":         "<?php  echo __("<!--:fr-->Rechercher:<!--:--><!--:en-->Search:<!--:-->");  ?>",
                "paginate": {
                    "first":      "<?php  echo __("<!--:fr-->Premier<!--:--><!--:en-->First<!--:-->");  ?>",
                    "last":       "<?php  echo __("<!--:fr-->Dérnier<!--:--><!--:en-->Last<!--:-->");  ?>",
                    "next":       "<?php  echo __("<!--:fr-->Suivant<!--:--><!--:en-->Next<!--:-->");  ?>",
                    "previous":   "<?php  echo __("<!--:fr-->Précedent<!--:--><!--:en-->Previous<!--:-->");  ?>"
                },
            }
        });
    }
    function showDetail(refs){
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'get_detail_mails',refs : refs},
            success: function (res) {
                jQuery('#detail-mail-forwarder').find('#content-details').html(res);
                jQuery('#detail-mail-forwarder').modal('show');
                jQuery("#clique_detail_offre").click(function(){
                    jQuery("#content-details-detail-offre").toggle("slow");
                });
            }
        });
    }
    
    function showDetail2(refs){
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'get_detail_mails2',refs : refs},
            success: function (res) {
                jQuery('#detail-mail-forwarder-2').find('#content-details').html(res);
                jQuery('#detail-mail-forwarder-2').modal('show');
                jQuery("#clique_detail_offre").click(function(){
                    jQuery("#content-details-detail-offre").toggle("slow");
                });
            }
        });
    }
    
    function showDetail3(refs){
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'get_detail_mails3',refs : refs},
            success: function (res) {
                jQuery('#detail-mail-transitaire-1').find('#content-details').html(res);
                jQuery('#detail-mail-transitaire-1').modal('show');
                jQuery("#clique_detail_offre").click(function(){
                    jQuery("#content-details-detail-offre").toggle("slow");
                });
            }
        });
    }
    function showDetail4(refs){
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'get_detail_mails4',refs : refs},
            success: function (res) {
                jQuery('#detail-mail-devis-confirmer').find('#content-details').html(res);
                jQuery('#detail-mail-devis-confirmer').modal('show');
                jQuery("#clique_detail_offre").click(function(){
                    jQuery("#content-details-detail-offre").toggle("slow");
                });
            }
        });
    }
    
    function trashingMail(refs2){
        if(confirm("Voulez vous mettre cette message à la corbeille ")){
            jQuery.ajax({
                type: "GET",
                url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
                data: {action : 'change_detail_mails',refs2 : refs2},
                success: function (res) {
                    location.reload();
                }
            });
        }
    }
    
    function trashingMail2(refs3){
        if(confirm("Voulez vous mettre cette message à la corbeille ")){
            jQuery.ajax({
                type: "GET",
                url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
                data: {action : 'change_detail_mails2',refs3 : refs3},
                success: function (res) {
                    location.reload();
                }
            });
        }
    }
    
    function trashingMail3(refs4){
        if(confirm("Voulez vous mettre cette message à la corbeille ")){
            jQuery.ajax({
                type: "GET",
                url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
                data: {action : 'change_detail_mails3',refs4 : refs4},
                success: function (res) {
                    location.reload();
                }
            });
        }
    }
    function trashingMail4(refs4){
        if(confirm("Voulez vous mettre cette message à la corbeille ")){
            jQuery.ajax({
                type: "GET",
                url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
                data: {action : 'change_detail_mails4',refs4 : refs4},
                success: function (res) {
                    location.reload();
                }
            });
        }
    }
</script>
<?php


