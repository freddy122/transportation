<?php
/* ------------------------------------------------ */
/* Popup Qoute Email */
/* ------------------------------------------------ */
function sb_contact_deliver_mail()
{
    global $sb_themes;    
    parse_str($_POST['formData'], $data);
    
    $name        		= $data['contact_name'];
    $your_subject     	= $data['contact_subject'];
	$email       		= $data['contact_email'];
    $comments     	  	= $data['contact_message'];
    
    // if the submit button is clicked, send the email
    if ($name != "" && $email != "" && $comments != "" && $your_subject != "" ) {
		if (strlen($comments) < 10) {
			echo 'err|'.esc_html__( 'Please enter atleast 10 characters in message' , 'logestic-pro');
			die();
		}        
		$name    			= sanitize_text_field($name);
        $email   			= sanitize_email($email);
		$your_subject   	= sanitize_text_field( $your_subject );
        $message 			= esc_textarea($comments);
        $val     			= filter_var($email, FILTER_VALIDATE_EMAIL);
        
		
        $body = str_replace(array(
            '%name%',
			'%subject%',
            '%email%',
            '%message%'
        ), array(
            $name,
			$your_subject,
            $email,
            $message
        ), $sb_themes['contact-message-template']);
		
        // get the blog administrator's email address
        $to      =  $sb_themes['contact-mail-receive-on'];
        $subject =  $sb_themes['contact-mail-receive-title'];

		$headers = "From: $name <$email>" . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        // If email has been process for sending, display a success message
        if (wp_mail($to, $subject, $body, $headers)) {
            echo 'true|' . esc_html( $sb_themes['contact-success-message'] );
        } else {
            echo 'err|' . esc_html( $sb_themes['contact-error-message'] );
        }
        
    } else {
        if ($name == "") {
            echo 'err|'.esc_html__( 'Please Enter Name' , 'logestic-pro');
            die();
        } else if ($email  == "") {
			 echo 'err|'.esc_html__( 'Please Enter email address' , 'logestic-pro');
            die();			
        } else if ($your_subject == "") {
			 echo 'err|'.esc_html__( 'Please Enter Subject' , 'logestic-pro');
            die();
        } else if (strlen($comments) < 10) {
			echo 'err|'.esc_html__( 'Please enter atleast 10 characters in message' , 'logestic-pro');
            die();
        }
		echo 'err|'.esc_html__( 'Something went wrong' , 'logestic-pro');
    }
    die();
}
add_action('wp_ajax_logisticpro_order_for_cargo', 'logisticpro_order_for_cargo');
add_action('wp_ajax_nopriv_logisticpro_order_for_cargo', 'logisticpro_order_for_cargo');

function sb_contact_us_form() {
    ?>
<script type="text/javascript">
(function($) {
	"use strict";
	$("#submit-for-contact").on("click", function() {
		
		var your_name = $('#contact-name').val();
		var your_subject = $('#contact-subject').val();	
		var your_email = $('#contact-email').val();
		var your_message = $('#contact-message').val();
		if( your_name == "" )
		{
	  		$('#contact-name').addClass('input-red');
			return false;
		}
		else
		{
			$('#contact-name').removeClass('input-red');
		}
		
		if( your_email == "" || !validateEmail_contact( your_email ))
		{
	  		$('#contact-email').addClass('input-red');
			return false;
		}
		else
		{
			$('#contact-email').removeClass('input-red');	
		}
		if( your_subject == "" )
		{
	  		$('#contact-subject').addClass('input-red');
			return false;
		}
		else
		{
			$('#contact-subject').removeClass('input-red');
			
		}

		if( your_message == "" )
		{
	  		$('#contact-message').addClass('input-red');
			return false;
		}
		else
		{
			$('#contact-message').removeClass('input-red');
		}
    var sb_admin_ajax_url = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
    $("form#contactForm #submit-for-contact").attr('disabled','disabled');
	//$("form#contactForm a#requesting").show();
    var formData = $("form#contactForm").serialize();

    $.post(sb_admin_ajax_url, {
        action: "sb_contact_deliver_mail",
        formData: formData
    }).done(function(data) {
		
	var res = data.split("|");
	if (res[0].trim() == 'true') {
		$("#msg-box").html('<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+ res[1] +'</div>');
		$('#contactForm').trigger("reset");
	} else {
	$("#msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+ res[1] +'</div>');
	}
});

    setTimeout(function() {
        //$("form#popup-qoute-form #requesting").hide();
		$("form#contactForm #submit-for-contact").removeAttr('disabled');
		
    }, 3000);
		
	});
	
	
 function validateEmail_contact($email) { var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; return emailReg.test( $email ); }	
})( jQuery );

</script>
    <?php

}
add_action( 'wp_footer', 'sb_contact_us_form' );

?>
