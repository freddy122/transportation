<?php
/* ------------------------------------------------ */
/* Popup Qoute Email */
/* ------------------------------------------------ */
function sb_qoute_deliver_mail()
{
    global $sb_themes;    
    parse_str($_POST['formData'], $data);
    
    $name        		= $data['your_name'];
    $your_subject     	= $data['your_subject'];
	$email       		= $data['your_email'];
	$select_location  	= $data['select_location'];
	$select_cargo_type 	= $data['select_cargo_type'];
    $select_rlocation  	= $data['select_rlocation'];
    $comments     	  	= $data['your_message'];
    
    // if the submit button is clicked, send the email
    if ($name != "" && $email != "" && $comments != "" && $your_subject != "" && $select_location != "") {
		if (strlen($comments) < 10) {
			echo 'err|'.esc_html__( 'Please enter atleast 10 characters in message' , 'logestic-pro');
			die();
		}        
		$name    			= sanitize_text_field($name);
        $email   			= sanitize_email($email);
		$your_subject   	= sanitize_text_field( $your_subject );
		$select_location    = sanitize_text_field( $select_location );
		$select_rlocation    = sanitize_text_field( $select_rlocation );
		$select_cargo_type    = sanitize_text_field( $select_cargo_type );
        $message 			= esc_textarea($comments);
        $val     			= filter_var($email, FILTER_VALIDATE_EMAIL);
        
		
        $body = str_replace(array(
            '%name%',
			'%subject%',
            '%email%',
            '%location%',
			'%rlocation%',
			'%cargo_type%',
            '%message%'
        ), array(
            $name,
			$your_subject,
            $email,
            $select_location,
			$select_rlocation,
			$select_cargo_type,
            $message
        ), $sb_themes['qoute-message-template']);
		
        // get the blog administrator's email address
        $to      =  $sb_themes['qoute-mail-receive-on'];
        $subject =  $sb_themes['qoute-mail-receive-title'];

		$headers = "From: $name <$email>" . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        // If email has been process for sending, display a success message
        if (wp_mail($to, $subject, $body, $headers)) {
            echo 'true|' . esc_html( $sb_themes['qoute-success-message'] );
        } else {
            echo 'err|' . esc_html( $sb_themes['qoute-error-message'] );
        }
        
    } else {
        if ($name == "") {
            echo 'err|'.esc_html__( 'Please Enter Name' , 'logestic-pro');
            die();
        } else if ($your_subject == "") {
			 echo 'err|'.esc_html__( 'Please Enter Subject' , 'logestic-pro');
            die();
        } else if ($email  == "") {
			 echo 'err|'.esc_html__( 'Please Enter email address' , 'logestic-pro');
            die();			
        } else if ($select_location  == "") {
			 echo 'err|'.esc_html__( 'Please Select Location' , 'logestic-pro');
            die();			

        } else if (strlen($comments) < 10) {
			echo 'err|'.esc_html__( 'Please enter atleast 10 characters in message' , 'logestic-pro');
            die();
        }
		echo 'err|'.esc_html__( 'Something went wrong' , 'logestic-pro');
    }
    die();
}
add_action('wp_ajax_sb_qoute_deliver_mail', 'sb_qoute_deliver_mail');
add_action('wp_ajax_nopriv_sb_qoute_deliver_mail', 'sb_qoute_deliver_mail');

function sb_contact_us() {
    ?>
<script type="text/javascript">
(function($) {
	"use strict";
	$("#sb_submit_qoute, #section-qoute-button").on("click", function() {
		
		var form_button_id = (this.id);
		
		var form_id = '#section-qoute';
		var is_qoute = '';
		if( 'sb_submit_qoute' == form_button_id )
		{
			var form_id = '#popup-qoute-form';
			var is_qoute = '-qoute';
		}
		else
		{
			var form_id = '#section-qoute';
		}
		
		
		var your_name = $('#your-name'+is_qoute).val();
		var your_subject = $('#your-subject'+is_qoute).val();	
		var your_email = $('#your-email'+is_qoute).val();
		var select_location = $('#select-location'+is_qoute).val();	
		var your_message = $('#your-message'+is_qoute).val();
		var cargo_type = $('#select-cargo-type'+is_qoute).val();	
		var select_rlocation = $('#select-rlocation'+is_qoute).val();	
			

		if( your_name == "" )
		{
	  		$('#your-name'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please enter your name", "sb_framework");?></div>');
			
			return false;
		}
		else
		{
			$('#your-name'+is_qoute).removeClass('input-red');
		}
		
		if( your_email == "" || !validateEmail( your_email ))
		{
	  		$('#your-email'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please enter valid email address", "sb_framework");?></div>');
			return false;
		}
		else
		{
			$('#your-email'+is_qoute).removeClass('input-red');	
		}
		if( your_subject == "" )
		{
	  		$('#your-subject'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please enter your subject", "sb_framework");?></div>');
			
			return false;
		}
		else
		{
			$('#your-subject'+is_qoute).removeClass('input-red');
			
		}
		
		if( cargo_type == "" )
		{
	  		$('#select-cargo-type'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please select cargo type", "sb_framework");?></div>');
			
			return false;
		}
		else
		{
			$('#select-cargo-type'+is_qoute).removeClass('input-red');	
		}
				
		if( select_location == "" )
		{
	  		$('#select-location'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please select your country", "sb_framework");?></div>');
			
			return false;
		}
		else
		{
			$('#select-location'+is_qoute).removeClass('input-red');	
		}
		
		if( select_rlocation == "" )
		{
	  		$('#select-rlocation'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please select destination country", "sb_framework");?></div>');
			
			return false;
		}
		else
		{
			$('#select-rlocation'+is_qoute).removeClass('input-red');	
		}		
		
		if( your_message == "" )
		{
	  		$('#your-message'+is_qoute).addClass('input-red');
			$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo __("Please enter some description", "sb_framework");?></div>');
			
			return false;
		}
		else
		{
			$('#your-message'+is_qoute).removeClass('input-red');
		}
		
		
		
    var sb_admin_ajax_url = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
    $(form_id+" #"+form_button_id).hide();
	$(form_id+" a.requesting").show();
    var formData = $("form"+form_id).serialize();

    $.post(sb_admin_ajax_url, {
        action: "sb_qoute_deliver_mail",
        formData: formData
    }).done(function(data) {
		
	var res = data.split("|");
	if (res[0].trim() == 'true') {
		$(form_id+" .popup-msg-box").html('<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+ res[1] +'</div>');
		$(form_id).trigger("reset");
	} else {
	$(form_id+" .popup-msg-box").html('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'+ res[1] +'</div>');
	}
});

    setTimeout(function() {
        $(form_id+" .requesting").hide();
		$(form_id+" #"+form_button_id).show();
		
    }, 5000);
		
		
		
		
		
		
		
	});
	
	
 function validateEmail($email) { var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; return emailReg.test( $email ); }	
})( jQuery );

</script>
    <?php

}
add_action( 'wp_footer', 'sb_contact_us' );

?>
