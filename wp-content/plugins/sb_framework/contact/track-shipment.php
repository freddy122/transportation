<?php
/* ------------------------------------------------ */
/* Popup Qoute Email */
/* ------------------------------------------------ */
function logistic_sb_track_shipment()
{
    $user = wp_get_current_user();    
    parse_str($_POST['formData'], $data);

    $shipment_id        		= $data['shipment_id'];
    // if the submit button is clicked, send the email
    if ($shipment_id != "") {
       
		$shipment_id    			= sanitize_text_field($shipment_id);
		
			global $wpdb;
			$results = $wpdb->get_results( "SELECT post_id, meta_key FROM $wpdb->postmeta where meta_value = '$shipment_id' AND meta_key = '_logisticpro_tracking_id'", ARRAY_A );

				if( $results[0]['post_id'] != "" )
				{

					$html = '';
					global $current_user;
					

					$cargo_query = array('post_type' => 'shipment', 'posts_per_page' => '1', 'p' => $results[0]['post_id']);
					
					$cargo_posts = get_post( $results[0]['post_id'] );
						
						$logisticpro_details = json_decode(  get_post_meta( $cargo_posts->ID , '_logisticpro_all_fields', true), true );
						
						$shipmentID = get_the_permalink( $cargo_posts->ID ) ;
					
						$trm_sender   = get_term( $logisticpro_details['logisticpro_sender_country'] );
						if( isset( $trm_sender->name ) ) 
						{ $sender = esc_html(  $trm_sender->name ); }else{ $sender =  __("N/A", "sb_framework"); }
						
						$trm_receiver = get_term( $logisticpro_details['logisticpro_receiver_country'] );
						if( isset( $trm_receiver->name ) ) 
						{ $receiver =  esc_html(  $trm_receiver->name );}else{ $receiver = __("N/A", "sb_framework"); }

						$trm_status   = get_term( $logisticpro_details['logisticpro_order_status'] );
						if( isset( $trm_status->name ) ) 
						{ $status = esc_html(  $trm_status->name ); }else{ $status = __("N/A", "sb_framework"); }
						
						$tracking_id = esc_html( get_post_meta( $cargo_posts->ID, '_logisticpro_tracking_id', true) );
						$cargo_name  = esc_html( $logisticpro_details['logisticpro_cargo_name'] ); 
						$date =  '<strong>'. __("Order Date: ", "sb_framework") . '</strong>'. get_the_date( get_option( 'date_format' ), $cargo_posts->ID );
						$btn = '';
						//$btn =  get_the_permalink( get_the_ID() );
						if ( is_user_logged_in() )
						{
							$btn = '<a class="btn btn-default btn-sm" href="'.esc_url( get_the_permalink( $cargo_posts->ID ) ).'">'.__("Details", "sb_framework").'</a>';						
						}
							
                    $html = '<div class="table-responsive">
<table class="table table-clean-paddings margin-bottom-0 no-table-border">
				<thead>
					<tr>
					  <th><p>'. __("Name", "sb_framework") .'</p></th>					 
					  <th><p>'. __("Sender", "sb_framework") .'</p></th>
					  <th><p>'. __("Receiver", "sb_framework") .'</p></th>
					  <th><p>'. __("Status", "sb_framework") .'</p></th>
					</tr>
					</thead>
					<tbody>
					<tr>
					  <td><p>'.$cargo_name.'</p></td>					 
					  <td><p>'. $sender .'</p></td>
					  <td><p>'. $receiver .'</p></td>
					  <td><p>'. $status .'</p></td>
					</tr>
					<tbody>
					<tfoot>
					<tr>
						<td colspan="4">'.$btn.'<p class="pull-right"> '.$date.'</p></td>
					</tr>
					</tfoot>
                   </table> </div>';
					   				
					
				echo 'true|<div class="modal fade " id="tracking-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-body">
					<div class="quotation-box-1">
					  <h2>'. __("Tracking Info", "sb_framework").' <span class="pull-right">'. $tracking_id .'</span></h2>
					  <div class="desc-text">
						'. $html .'
					  </div>
					</div>
				  </div>
				</div>
				</div>
				</div>';
	
				}
				else
				{
					echo 'err|'.esc_html__( 'Invalid Tracking ID' , 'logestic-pro');
					die();
				}
        
    } else {
        if ($shipment_id == "") {
            echo 'err|'.esc_html__( 'Please Tracking Code' , 'logestic-pro');
            die();
        } 
    }
    die();
}
add_action('wp_ajax_track_shipment_now', 'logistic_sb_track_shipment');
add_action('wp_ajax_nopriv_track_shipment_now', 'logistic_sb_track_shipment');

function sb_track_shipment_js() {
    ?>
<script type="text/javascript">


(function($) {
	
	"use strict";
	$("#track-shipment-button").on("click", function() {
		
		var shipment_id = $('#shipment_id').val();

		if( shipment_id == "" )
		{
	  		$('#shipment_id').addClass('input-red');			
			return false;
		}
		else
		{
			
	  		$('#shipment_id').removeClass('input-red');			
			
		}
		
			

    var sb_admin_ajax_url = '<?php echo esc_url( admin_url('admin-ajax.php') ); ?>';
    var formData = $("form#trackingForm").serialize();

    $.post(sb_admin_ajax_url, {
        action: "track_shipment_now",
        formData: formData
    }).done(function(data) {
		
		'';
	var res = data.split("|");
	if (res[0].trim() == 'true') {
		//window.location = ( res[1] );
		$('#popup-content').html(res[1]);
		$('#tracking-popup').modal('toggle');
		$('#shipment_id').removeClass('input-red');		
		$('#id-error').html('');
	} else {
		
		
		
		$('#id-error').html('<div class="alert alert-warning col-md-7"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong><?php echo __("Warning! ", "sb_framework"); ?></strong> ' + res[1] +  '</div>');
		$('#shipment_id').val("");
		$('#shipment_id').attr("placeholder", res[1]);
	    $('#shipment_id').addClass('input-red');	
	}
});	
});	
		//
		
	})( jQuery );
	
</script>
<?php

}
add_action( 'wp_footer', 'sb_track_shipment_js' );

?>
