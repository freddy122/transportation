<?php if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
// Register Custom Post Type
function create_services_cpt() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'sb_framework' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'sb_framework' ),
		'menu_name'             => __( 'Services', 'sb_framework' ),
		'name_admin_bar'        => __( 'Service', 'sb_framework' ),
		'archives'              => __( 'Service Archives', 'sb_framework' ),
		'parent_item_colon'     => __( 'Parent Service:', 'sb_framework' ),
		'all_items'             => __( 'All Services', 'sb_framework' ),
		'add_new_item'          => __( 'Add New Service', 'sb_framework' ),
		'add_new'               => __( 'Add New', 'sb_framework' ),
		'new_item'              => __( 'New Service', 'sb_framework' ),
		'edit_item'             => __( 'Edit Service', 'sb_framework' ),
		'update_item'           => __( 'Update Service', 'sb_framework' ),
		'view_item'             => __( 'View Service', 'sb_framework' ),
		'search_items'          => __( 'Search Service', 'sb_framework' ),
		'not_found'             => __( 'Not found', 'sb_framework' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sb_framework' ),
		'featured_image'        => __( 'Featured Image', 'sb_framework' ),
		'set_featured_image'    => __( 'Set featured image', 'sb_framework' ),
		'remove_featured_image' => __( 'Remove featured image', 'sb_framework' ),
		'use_featured_image'    => __( 'Use as featured image', 'sb_framework' ),
		'insert_into_item'      => __( 'Insert into service', 'sb_framework' ),
		'uploaded_to_this_item' => __( 'Uploaded to this service', 'sb_framework' ),
		'items_list'            => __( 'Items list', 'sb_framework' ),
		'items_list_navigation' => __( 'Items list navigation', 'sb_framework' ),
		'filter_items_list'     => __( 'Filter items list', 'sb_framework' ),
	);
	$rewrite = array(
		'slug'                  => 'service',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Service', 'sb_framework' ),
		'description'           => __( 'Services offered by ServiceSource', 'sb_framework' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail'),
		'taxonomies'            => array( 'type' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'services',
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'services ', $args );

}
add_action( 'init', 'create_services_cpt', 0 );

// Register Custom Taxonomy
function sb_theme_type_custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Types', 'Type General Name', 'sb_framework' ),
		'singular_name'              => _x( 'Type', 'Type Singular Name', 'sb_framework' ),
		'menu_name'                  => __( 'Type', 'sb_framework' ),
		'all_items'                  => __( 'All Items', 'sb_framework' ),
		'parent_item'                => __( 'Parent Item', 'sb_framework' ),
		'parent_item_colon'          => __( 'Parent Item:', 'sb_framework' ),
		'new_item_name'              => __( 'New Item Name', 'sb_framework' ),
		'add_new_item'               => __( 'Add New Item', 'sb_framework' ),
		'edit_item'                  => __( 'Edit Item', 'sb_framework' ),
		'update_item'                => __( 'Update Item', 'sb_framework' ),
		'view_item'                  => __( 'View Item', 'sb_framework' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'sb_framework' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'sb_framework' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'sb_framework' ),
		'popular_items'              => __( 'Popular Items', 'sb_framework' ),
		'search_items'               => __( 'Search Items', 'sb_framework' ),
		'not_found'                  => __( 'Not Found', 'sb_framework' ),
		'no_terms'                   => __( 'No items', 'sb_framework' ),
		'items_list'                 => __( 'Items list', 'sb_framework' ),
		'items_list_navigation'      => __( 'Items list navigation', 'sb_framework' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'type', array( 'services' ), $args );

}
add_action( 'init', 'sb_theme_type_custom_taxonomy', 0 );