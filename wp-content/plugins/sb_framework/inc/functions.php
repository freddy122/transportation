<?php
/* ------------------------------------------------ */
/* Popup Qoute Email */
/* ------------------------------------------------ */
function sb_qoute_deliver_mail()
{
	echo 'true|ok';
    global $sb_themes;
    
    parse_str($_POST['formData'], $data);
    
    $name        		= $data['your_name'];
    $your_subject     	= $data['your_subject'];
	$email       		= $data['your_email'];
    $select_location  	= $data['select_location'];
    $comments     	  	= $data['your_message'];
    
    // if the submit button is clicked, send the email
    if ($name != "" && $email != "" && $comments != "" && $your_subject != "" && $select_location != "") {
		if (strlen($comments) < 10) {
			echo 'err|'.esc_html__( 'Please enter atleast 10 characters in message' , 'logestic-pro');
			die();
		}        
		$name    			= sanitize_text_field($name);
        $email   			= sanitize_email($email);
		$your_subject   	= sanitize_text_field( $your_subject );
		$select_location    = sanitize_text_field( $select_location );
        $message 			= esc_textarea($comments);
        $val     			= filter_var($email, FILTER_VALIDATE_EMAIL);
        
		
        $body = str_replace(array(
            '%name%',
			'%subject%',
            '%email%',
            '%location%',
            '%message%'
        ), array(
            $name,
			$your_subject,
            $email,
            $select_location,
            $message
        ), $sb_themes['qoute-message-template']);
		
        // get the blog administrator's email address
        $to      =  $sb_themes['qoute-mail-receive-on'];
        $subject =  $sb_themes['qoute-mail-receive-title'];

		$headers = "From: $name <$email>" . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        // If email has been process for sending, display a success message
        if (wp_mail($to, $subject, $body, $headers)) {
            echo 'true|<div>' . esc_html( $sb_themes['qoute-success-message'] ). '</div>';
        } else {
            echo 'err|' . esc_html( $sb_themes['qoute-error-message'] );
        }
        
    } else {
        if ($name == "") {
            echo 'err|'.esc_html__( 'Please Enter Name' , 'logestic-pro');
            die();
        } else if ($your_subject == "") {
			 echo 'err|'.esc_html__( 'Please Enter Subjest' , 'logestic-pro');
            die();
        } else if ($email  == "") {
			 echo 'err|'.esc_html__( 'Please Enter email address' , 'logestic-pro');
            die();			
        } else if ($select_location  == "") {
			 echo 'err|'.esc_html__( 'Please Select Location' , 'logestic-pro');
            die();			

        } else if (strlen($comments) < 10) {
			echo 'err|'.esc_html__( 'Please enter atleast 10 characters in message' , 'logestic-pro');
            die();
        }
		echo 'err|'.esc_html__( 'Something went wrong' , 'logestic-pro');
    }
    die();
}
add_action('wp_ajax_sb_qoute_deliver_mail', 'sb_qoute_deliver_mail');
add_action('wp_ajax_nopriv_sb_qoute_deliver_mail', 'sb_qoute_deliver_mail');







add_filter( 'vc_iconpicker-type-tameer', 'vc_iconpicker_type_tameer' );
function vc_iconpicker_type_tameer( $icons ) {
	$flat_icons	=	array(
		'Tameer Icons' => array(
array('flaticon-american-football-cup' => 'american-football-cup' ),
array('flaticon-art' => 'art' ),
array('flaticon-attention-signal-and-construction-worker' => 'attention-signal-and-construction-worker' ),
array('flaticon-avatar' => 'avatar' ),
array('flaticon-avatar-1' => 'avatar-1' ),
array('flaticon-badge-with-ribbons' => 'badge-with-ribbons' ),
array('flaticon-bath' => 'bath' ),
array('flaticon-bathtub' => 'bathtub' ),
array('flaticon-behance' => 'behance' ),
array('flaticon-behance-1' => 'behance-1' ),
array('flaticon-big-pliers' => 'big-pliers' ),
array('flaticon-briefcase' => 'briefcase' ),
array('flaticon-briefcase-1' => 'briefcase-1' ),
array('flaticon-bug' => 'bug' ),
array('flaticon-bug-fixing' => 'bug-fixing' ),
array('flaticon-bugs-search' => 'bugs-search' ),
array('flaticon-building' => 'building' ),
array('flaticon-building-crane' => 'building-crane' ),
array('flaticon-buildings' => 'buildings' ),
array('flaticon-business' => 'business' ),
array('flaticon-cap-outline-hand-drawn-construction-tool' => 'cap-outline-hand-drawn-construction-tool' ),
array('flaticon-car' => 'car' ),
array('flaticon-car-1' => 'car-1' ),
array('flaticon-car-2' => 'car-2' ),
array('flaticon-car-3' => 'car-3' ),
array('flaticon-car-parking' => 'car-parking' ),
array('flaticon-car-wash' => 'car-wash' ),
array('flaticon-city' => 'city' ),
array('flaticon-cleaner' => 'cleaner' ),
array('flaticon-cleaning' => 'cleaning' ),
array('flaticon-clock-1' => 'clock-1' ),
array('flaticon-clock-2' => 'clock-2' ),
array('flaticon-clock-3' => 'clock-3' ),
array('flaticon-cogwheel' => 'cogwheel' ),
array('flaticon-construction' => 'construction' ),
array('flaticon-construction-1' => 'construction-1' ),
array('flaticon-construction-10' => 'construction-10' ),
array('flaticon-construction-11' => 'construction-11' ),
array('flaticon-construction-12' => 'construction-12' ),
array('flaticon-construction-13' => 'construction-13' ),
array('flaticon-construction-14' => 'construction-14' ),
array('flaticon-construction-15' => 'construction-15' ),
array('flaticon-construction-16' => 'construction-16' ),
array('flaticon-construction-17' => 'construction-17' ),
array('flaticon-construction-18' => 'construction-18' ),
array('flaticon-construction-19' => 'construction-19' ),
array('flaticon-construction-20' => 'construction-20' ),
array('flaticon-construction-2' => 'construction-2' ),
array('flaticon-construction-20' => 'construction-20' ),
array('flaticon-construction-21' => 'construction-21' ),
array('flaticon-construction-22' => 'construction-22' ),
array('flaticon-construction-23' => 'construction-23' ),
array('flaticon-construction-24' => 'construction-24' ),
array('flaticon-construction-25' => 'construction-25' ),
array('flaticon-construction-26' => 'construction-26' ),
array('flaticon-construction-27' => 'construction-27' ),
array('flaticon-construction-28' => 'construction-28' ),
array('flaticon-construction-29' => 'construction-29' ),
array('flaticon-construction-30' => 'construction-30' ),
array('flaticon-construction-5' => 'construction-5' ),
array('flaticon-construction-6' => 'construction-6' ),
array('flaticon-construction-7' => 'construction-7' ),
array('flaticon-construction-8' => 'construction-8' ),
array('flaticon-construction-9' => 'construction-9' ),
array('flaticon-construction-crane-machine' => 'construction-crane-machine' ),
array('flaticon-construction-transport-tool-with-a-crane' => 'construction-transport-tool-with-a-crane' ),
array('flaticon-cup' => 'cup' ),
array('flaticon-customer-service' => 'customer-service' ),
array('flaticon-cut' => 'cut' ),
array('flaticon-cut-1' => 'cut-1' ),
array('flaticon-detergent' => 'detergent' ),
array('flaticon-dish' => 'dish' ),
array('flaticon-drill' => 'drill' ),
array('flaticon-drill-1' => 'drill-1' ),
array('flaticon-drill-2' => 'drill-2' ),
array('flaticon-drill-3' => 'drill-3' ),
array('flaticon-drill-4' => 'drill-4' ),
array('flaticon-drill-hand-drawn-contruction-tool' => 'drill-hand-drawn-contruction-tool' ),
array('flaticon-drill-outline' => 'drill-outline' ),
array('flaticon-drill-outline-1' => 'drill-outline-1' ),
array('flaticon-driller' => 'driller' ),
array('flaticon-driller-1' => 'driller-1' ),
array('flaticon-dustpan' => 'dustpan' ),
array('flaticon-e-mail-envelope' => 'e-mail-envelope' ),
array('flaticon-earth-globe' => 'earth-globe' ),
array('flaticon-earth-globe-1' => 'earth-globe-1' ),
array('flaticon-earth-globe-2' => 'earth-globe-2' ),
array('flaticon-education' => 'education' ),
array('flaticon-education-1' => 'education-1' ),
array('flaticon-electric-car' => 'electric-car' ),
array('flaticon-excavator' => 'excavator' ),
array('flaticon-excavator-construction-machine-side-view' => 'excavator-construction-machine-side-view' ),
array('flaticon-facebook' => 'facebook' ),
array('flaticon-facebook-social-badge' => 'facebook-social-badge' ),
array('flaticon-follow-me-on-linkedin-social-badge' => 'follow-me-on-linkedin-social-badge' ),
array('flaticon-forklift-tool' => 'forklift-tool' ),
array('flaticon-graduation' => 'graduation' ),
array('flaticon-hammer-in-a-hand-inside-a-house' => 'hammer-in-a-hand-inside-a-house' ),
array('flaticon-hammer-in-a-hand-inside-a-house-1' => 'hammer-in-a-hand-inside-a-house-1' ),
array('flaticon-hand-with-triangular-shovel-inside-a-home-outline' => 'hand-with-triangular-shovel-inside-a-home-outline' ),
array('flaticon-hard-hat' => 'hard-hat' ),
array('flaticon-holidays' => 'holidays' ),
array('flaticon-home' => 'home' ),
array('flaticon-home-1' => 'home-1' ),
array('flaticon-home-2' => 'home-2' ),
array('flaticon-home-3' => 'home-3' ),
array('flaticon-home-4' => 'home-4' ),
array('flaticon-home-5' => 'home-5' ),
array('flaticon-home-repair-symbol' => 'home-repair-symbol' ),
array('flaticon-hot-air-balloon-and-paper-airplane' => 'hot-air-balloon-and-paper-airplane' ),
array('flaticon-hot-air-balloon-flying' => 'hot-air-balloon-flying' ),
array('flaticon-hot-air-baloon-flying' => 'hot-air-baloon-flying' ),
array('flaticon-hotel' => 'hotel' ),
array('flaticon-hotel-building' => 'hotel-building' ),
array('flaticon-house' => 'house' ),
array('flaticon-house-1' => 'house-1' ),
array('flaticon-house-2' => 'house-2' ),
array('flaticon-idea' => 'idea' ),
array('flaticon-insignia-and-ribbon' => 'insignia-and-ribbon' ),
array('flaticon-instagram' => 'instagram' ),
array('flaticon-instagram-1' => 'instagram-1' ),
array('flaticon-instagram-2' => 'instagram-2' ),
array('flaticon-instagram-social-badge' => 'instagram-social-badge' ),
array('flaticon-internet' => 'internet' ),
array('flaticon-key' => 'key' ),
array('flaticon-key-1' => 'key-1' ),
array('flaticon-key-2' => 'key-2' ),
array('flaticon-key-3' => 'key-3' ),
array('flaticon-key-4' => 'key-4' ),
array('flaticon-keys' => 'keys' ),
array('flaticon-letter' => 'letter' ),
array('flaticon-light-bulb' => 'light-bulb' ),
array('flaticon-light-bulb-1' => 'light-bulb-1' ),
array('flaticon-light-bulb-2' => 'light-bulb-2' ),
array('flaticon-light-bulb-on' => 'light-bulb-on' ),
array('flaticon-light-bulb-outline' => 'light-bulb-outline' ),
array('flaticon-linkedin' => 'linkedin' ),
array('flaticon-linkedin-logo' => 'linkedin-logo' ),
array('flaticon-liquid-soap' => 'liquid-soap' ),
array('flaticon-list' => 'list' ),
array('flaticon-list-1' => 'list-1' ),
array('flaticon-lock' => 'lock' ),
array('flaticon-logo' => 'logo' ),
array('flaticon-love' => 'love' ),
array('flaticon-medal' => 'medal' ),
array('flaticon-nature' => 'nature' ),
array('flaticon-nature-1' => 'nature-1' ),
array('flaticon-navigation' => 'navigation' ),
array('flaticon-nippers' => 'nippers' ),
array('flaticon-office-briefcase' => 'office-briefcase' ),
array('flaticon-open-parachute' => 'open-parachute' ),
array('flaticon-outdoor-worker' => 'outdoor-worker' ),
array('flaticon-paint' => 'paint' ),
array('flaticon-paint-1' => 'paint-1' ),
array('flaticon-paint-2' => 'paint-2' ),
array('flaticon-parachute' => 'parachute' ),
array('flaticon-paratrooper' => 'paratrooper' ),
array('flaticon-people' => 'people' ),
array('flaticon-people-1' => 'people-1' ),
array('flaticon-phone-call' => 'phone-call' ),
array('aticon-pinterest' => '' ),
array('flaticon-pinterest-social-badge' => 'pinterest-social-badge' ),
array('flaticon-pipeline-with-wheel' => 'pipeline-with-wheel' ),
array('flaticon-pipes' => 'pipes' ),
array('flaticon-plier-outline' => 'plier-outline' ),
array('flaticon-plier-outline-1' => 'plier-outline-1' ),
array('flaticon-pliers' => 'pliers' ),
array('flaticon-pliers-1' => 'pliers-1' ),
array('flaticon-plumber' => 'plumber' ),
array('flaticon-plumber-working' => 'plumber-working' ),
array('flaticon-police-car' => 'police-car' ),
array('flaticon-portfolio' => 'portfolio' ),
array('flaticon-puzzle' => 'puzzle' ),
array('flaticon-puzzle-1' => 'puzzle-1' ),
array('flaticon-puzzle-2' => 'puzzle-2' ),
array('flaticon-puzzle-3' => 'puzzle-3' ),
array('flaticon-puzzle-4' => 'puzzle-4' ),
array('flaticon-repairman-inside-a-home' => 'repairman-inside-a-home' ),
array('flaticon-ribbon' => 'ribbon' ),
array('flaticon-search' => 'search' ),
array('flaticon-sedan-car-model' => 'sedan-car-model' ),
array('flaticon-sending-pipe' => 'sending-pipe' ),
array('flaticon-shop' => 'shop' ),
array('flaticon-shopping-cart' => 'shopping-cart' ),
array('flaticon-shovel' => 'shovel' ),
array('flaticon-shovel-1' => 'shovel-1' ),
array('flaticon-smartphone' => 'smartphone' ),
array('flaticon-social' => 'social' ),
array('flaticon-social-youtube-circular-button' => 'social-youtube-circular-button' ),
array('flaticon-sport-car' => 'sport-car' ),
array('flaticon-suitcase' => 'suitcase' ),
array('flaticon-suspension' => 'suspension' ),
array('flaticon-tap' => 'tap' ),
array('flaticon-technology' => 'technology' ),
array('flaticon-technology-1' => 'technology-1' ),
array('flaticon-technology-2' => 'technology-2' ),
array('flaticon-technology-3' => 'technology-3' ),
array('flaticon-teeth-cleaning' => 'teeth-cleaning' ),
array('flaticon-telemarketer' => 'telemarketer' ),
array('flaticon-thinking' => 'thinking' ),
array('flaticon-tool' => 'tool' ),
array('flaticon-tool-1' => 'tool-1' ),
array('flaticon-tool-10' => 'tool-10' ),
array('flaticon-tool-11' => 'tool-11' ),
array('flaticon-tool-12' => 'tool-12' ),
array('flaticon-tool-13' => 'tool-13' ),
array('flaticon-tool-14' => 'tool-14' ),
array('flaticon-tool-15' => 'tool-15' ),
array('flaticon-tool-2' => 'tool-2' ),
array('flaticon-tool-3' => 'tool-3' ),
array('flaticon-tool-4' => 'tool-4' ),
array('flaticon-tool-5' => 'tool-5' ),
array('flaticon-tool-6' => 'tool-6' ),
array('flaticon-tool-7' => 'tool-7' ),
array('flaticon-tool-8' => 'tool-8' ),
array('flaticon-tool-9' => 'tool-9' ),
array('flaticon-transport' => 'transport' ),
array('flaticon-transport-2' => 'transport-2' ),
array('flaticon-transport-3' => 'transport-3' ),
array('flaticon-transport-4' => 'transport-4' ),
array('flaticon-transport-5' => 'transport-5' ),
array('flaticon-transport-6' => 'transport-6' ),
array('flaticon-transport-7' => 'transport-7' ),
array('flaticon-transport-8' => 'transport-8' ),
array('flaticon-travel' => 'travel' ),
array('flaticon-trophies' => 'trophies' ),
array('flaticon-trophy' => 'trophy' ),
array('flaticon-trophy-1' => 'trophy-1' ),
array('flaticon-trophy-2' => 'trophy-2' ),
array('flaticon-twitter' => 'twitter' ),
array('flaticon-twitter-logo-on-badge' => 'twitter-logo-on-badge' ),
array('flaticon-unlink' => 'unlink' ),
array('flaticon-unlink-1' => 'unlink-1' ),
array('flaticon-unlink-2' => 'unlink-2' ),
array('flaticon-unlink-3' => 'unlink-3' ),
array('flaticon-vacuum-cleaner' => 'vacuum-cleaner' ),
array('flaticon-verification-of-delivery-list-clipboard-symbol' => 'verification-of-delivery-list-clipboard-symbol' ),
array('flaticon-washing-machine' => 'washing-machine' ),
array('flaticon-water-tower' => 'water-tower' ),
array('flaticon-water-valve' => 'water-valve' ),
array('flaticon-window' => 'window' ),
array('flaticon-window-1' => 'window-1' ),
array('flaticon-worker' => 'worker' ),
array('flaticon-worldwide' => 'worldwide' ),
array('flaticon-wrench' => 'wrench' ),
array('flaticon-wrench-tool-in-a-hand-inside-a-house-shape' => 'wrench-tool-in-a-hand-inside-a-house-shape' ),
array('flaticon-wrench-tool-in-a-hand-inside-a-house-shape-1' => 'wrench-tool-in-a-hand-inside-a-house-shape-1' ),
array('flaticon-youtube' => 'youtube' ),
array('flaticon-youtube-social-badge' => 'youtube-social-badge' ),
array('flaticon-zoom' => 'zoom' ),
array('flaticon-zoom-out' => 'zoom-out' ),
array('flaticon-zoom-out-1' => 'zoom-out-1' ),
			),
	'Tameer Et Line Icons' => array(
array('icon-mobile' => 'mobile' ),
array('icon-laptop' => 'laptop' ),
array('icon-desktop' => 'desktop' ),
array('icon-tablet' => 'tablet' ),
array('icon-phone' => 'phone' ),
array('icon-document' => 'document' ),
array('icon-documents' => 'documents' ),
array('icon-search' => 'search' ),
array('icon-clipboard' => 'clipboard' ),
array('icon-newspaper' => 'newspaper' ),
array('icon-notebook' => 'notebook' ),
array('icon-book-open' => 'book-open' ),
array('icon-browser' => 'browser' ),
array('icon-calendar' => 'calendar' ),
array('icon-presentation' => 'presentation' ),
array('icon-picture' => 'picture' ),
array('icon-pictures' => 'pictures' ),
array('icon-video' => 'video' ),
array('icon-camera' => 'camera' ),
array('icon-printer' => 'printer' ),
array('icon-toolbox' => 'toolbox' ),
array('icon-briefcase' => 'briefcase' ),
array('icon-wallet' => 'wallet' ),
array('icon-gift' => 'gift' ),
array('icon-bargraph' => 'bargraph' ),
array('icon-grid' => 'grid' ),
array('icon-expand' => 'expand' ),
array('icon-focus' => 'focus' ),
array('icon-edit' => 'edit' ),
array('icon-adjustments' => 'adjustments' ),
array('icon-ribbon' => 'ribbon' ),
array('icon-hourglass' => 'hourglass' ),
array('icon-lock' => 'lock' ),
array('icon-megaphone' => 'megaphone' ),
array('icon-shield' => 'shield' ),
array('icon-trophy' => 'trophy' ),
array('icon-flag' => 'flag' ),
array('icon-map' => 'map' ),
array('icon-puzzle' => 'puzzle' ),
array('icon-basket' => 'basket' ),
array('icon-envelope' => 'envelope' ),
array('icon-streetsign' => 'streetsign' ),
array('icon-telescope' => 'telescope' ),
array('icon-gears' => 'gears' ),
array('icon-key' => 'key' ),
array('icon-paperclip' => 'paperclip' ),
array('icon-attachment' => 'attachment' ),
array('icon-pricetags' => 'pricetags' ),
array('icon-lightbulb' => 'lightbulb' ),
array('icon-layers' => 'layers' ),
array('icon-pencil' => 'pencil' ),
array('icon-tools' => 'tools' ),
array('icon-tools-2' => 'tools-2' ),
array('icon-scissors' => 'scissors' ),
array('icon-paintbrush' => 'paintbrush' ),
array('icon-magnifying-glass' => 'magnifying-glass' ),
array('icon-circle-compass' => 'circle-compass' ),
array('icon-linegraph' => 'linegraph' ),
array('icon-mic' => 'mic' ),
array('icon-strategy' => 'strategy' ),
array('icon-beaker' => 'beaker' ),
array('icon-caution' => 'caution' ),
array('icon-recycle' => 'recycle' ),
array('icon-anchor' => 'anchor' ),
array('icon-profile-male' => 'profile-male' ),
array('icon-profile-female' => 'profile-female' ),
array('icon-bike' => 'bike' ),
array('icon-wine' => 'wine' ),
array('icon-hotairballoon' => 'hotairballoon' ),
array('icon-glob' => 'glob' ),
array('icon-genius' => 'genius' ),
array('icon-map-pin' => 'map-pin' ),
array('icon-dial' => 'dial' ),
array('icon-chat' => 'chat' ),
array('icon-heart' => 'heart' ),
array('icon-cloud' => 'cloud' ),
array('icon-upload' => 'upload' ),
array('icon-download' => 'download' ),
array('icon-traget' => 'traget' ),
array('icon-hazardous' => 'hazardous' ),
array('icon-piechart' => 'piechart' ),
array('icon-speedometer' => 'speedometer' ),
array('icon-global' => 'global' ),
array('icon-compass' => 'compass' ),
array('icon-lifesaver' => 'lifesaver' ),
array('icon-clock' => 'clock' ),
array('icon-aperture' => 'aperture' ),
array('icon-quote' => 'quote' ),
array('icon-scope' => 'scope' ),
array('icon-alarmclock' => 'alarmclock' ),
array('icon-refresh' => 'refresh' ),
array('icon-happy' => 'happy' ),
array('icon-sad' => 'sad' ),
array('icon-facebook' => 'facebook' ),
array('icon-twitter' => 'twitter' ),
array('icon-googleplus' => 'googleplus' ),
array('icon-rss' => 'rss' ),
array('icon-tumblr' => 'tumblr' ),
array('icon-linkedin' => 'linkedin' ),
array('icon-dribbble' => 'dribbble' ),
)
	);

	  return array_merge( $icons, $flat_icons );
}
