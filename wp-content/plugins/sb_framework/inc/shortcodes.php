<?php
/* ------------------------------------------------ */
/* VC Shortcode */
/* ------------------------------------------------ */
/* Include Icons */
require SB_PLUGIN_PATH . 'inc/vc_functions/icons/index.php';	

/* Include Shortcodes*/
require SB_PLUGIN_PATH . 'inc/shortcodes/revolution-slider/index.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/services.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/services-block.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/partners.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/our-gallery.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/testimonial.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/section-quote.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/posts.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/call-to-action.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/about-us.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/about-us2.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/counter.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/our-team.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/page-header.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/hero.php';
require SB_PLUGIN_PATH . 'inc/shortcodes/contact.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/process.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/our-app.php';	
require SB_PLUGIN_PATH . 'inc/shortcodes/track-your-shipment.php';