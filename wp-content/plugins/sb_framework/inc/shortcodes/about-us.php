<?php

/* ------------------------------------------------ */
/* about_us Shortcode */
/* ------------------------------------------------ */



function sb_theme_about_us_func() {
	
 	
vc_map( array(
    "name" => __("About Us", 'sb_framework'),
    "base" => "select_about_us",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(

			array(
                "type" => "dropdown",
                "heading" => esc_html__("Select Layout", 'sb_framework'),
                "param_name" => "about_us_layout",
                "admin_label" => true,
                "value" => 
				array(
					'Select Option' => '',
					'Image Right Side' => '0',
					'Image Left Side' => '1',
				),
            ),				
	
			array(
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "about_us_title",
                "admin_label" => true,
                "value" => '',
                "std" => '',
                "description" => esc_html__("Enter about us title here", 'sb_framework'),
            ),
			array(
                "type" => "textarea",
                "heading" => esc_html__("Description", 'sb_framework'),
                "param_name" => "about_us_desc",
                "admin_label" => true,
                "value" => '',
                "std" => '',
                "description" => esc_html__("Enter about us description here", 'sb_framework'),
            ),


			array(
				"group" => "Images",
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Image 1", 'sb_framework' ),
                "param_name" => "about_us_image",
                "description" => __( "Upload image.", 'sb_framework' ),
				
            ),					

			array(
				"group" => "Images",
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Image 2", 'sb_framework' ),
                "param_name" => "about_us_image2",
                "description" => __( "Upload image 2nd image", 'sb_framework' ),
				
            ),					

			array(
				"group" => "Button",
                "type" => "dropdown",
                "heading" => esc_html__("Open Link In", 'sb_framework'),
                "param_name" => "about_us_button_is_show",
                "admin_label" => true,
                "value" => 
				array(
					'Select Option' => '',
					'Show' => '1',
					'Hide' => '0',
				),
                "description" => esc_html__("Show or hide button.", 'sb_framework')
            ),				

			array(
				"group" => "Button",
                "type" => "textfield",
                "heading" => esc_html__("Button Text", 'sb_framework'),
                "param_name" => "about_us_button_text",
                "admin_label" => true,
                "value" => '',
                "std" => 'Get A Quote',
                "description" => esc_html__("Enter button text here", 'sb_framework'),
				'dependency' => array(
					'element' => 'about_us_button_is_show',
					'value' => '1',
				),				
				
            ),
			array(
				"group" => "Button",
                "type" => "textfield",
                "heading" => esc_html__("Button URL", 'sb_framework'),
                "param_name" => "about_us_button_url",
                "admin_label" => true,
                "value" => '',
                "std" => '#',
				'dependency' => array(
					'element' => 'about_us_button_is_show',
					'value' => '1',
				),				
				
            ),
			array(
				"group" => "Button",
                "type" => "dropdown",
                "heading" => esc_html__("Open Link In", 'sb_framework'),
                "param_name" => "about_us_button_target",
                "admin_label" => true,
                "value" => 
				array(
					'Select Target Window' => '',
					'Current Windo' => '_self',
					'New Window' => '_blank',
				),
                "description" => esc_html__("Select the target window when click on button.", 'sb_framework'),
				'dependency' => array(
					'element' => 'about_us_button_is_show',
					'value' => '1',
				),				
				
            ),				
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_about_us_func' );

function about_us_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'about_us_layout' => '0',
		  'about_us_title' => '',
		  'about_us_desc' => '',
		  'about_us_button_target' => '#',
		  'about_us_image' => '',	
		  'about_us_image2' => '',	
		  'about_us_button_is_show' => '1',
		  'about_us_button_text' => __('Contact Us', 'sb_framework'),	
		  'about_us_button_url' => '#',	
		  'about_us_button_target' => '_self',				   
	   ), $atts));
		
		
		
		
		//Show, Hide Button
		$is_show_button = '';
		if($about_us_button_is_show == 1)
		{
			$is_show_button = '<div class="more-about margin-top-30"> <a class="btn btn-primary btn-lg" href="'.esc_url($about_us_button_url).'" target="'.esc_attr($about_us_button_target).'">'.esc_html($about_us_button_text).'<i class="fa fa-chevron-circle-right"></i></a> </div><br /><br />';
		}
		
		//Description
		$content_html = '<div class="col-md-6 col-sm-12 col-xs-12 "><div class="about-title"><h2>'.esc_html($about_us_title).'</h2><p>'.($about_us_desc).'</p>'.$is_show_button.'</div></div>';
		
		// Image
		$image_html = '';
		if($about_us_image != "")
		{
			$image = wp_get_attachment_image_src( $about_us_image, 'full' );
			
			$image_html = '<a class="tt-lightbox" href="'.esc_url($image[0]).'">
							<img class="img-responsive" alt="Image" src="'.esc_url($image[0]).'">
						  </a>';
		}
		
		$image2_html = '';
		if($about_us_image2 != "")
		{
			$image2 = wp_get_attachment_image_src( $about_us_image2, 'full' );
			$image2_html = '<a class="tt-lightbox" href="'.esc_url($image2[0]).'">
							 <img class="img-responsive margin-bottom-30" alt="Image" src="'.esc_url($image2[0]).'">
							</a>';
		}
		
		$img_html = '<div class="col-md-6 col-sm-12 col-xs-12 our-gallery">'.$image_html.''.$image2_html.'</div>';
		
		
		
		// Manage Layout Images Left/Right Side
		$img_html_r = '';
		$img_html_l = $img_html;
		if($about_us_layout == 0)
		{
			$img_html_r = $img_html;
			$img_html_l = '';
		}
	  
		return '<section class="padding-top-70" id="about">
				  <div class="container">
					<div class="row clearfix"> 
						'.$img_html_l.'
						'.$content_html .'
						'.$img_html_r.'
					</div>
				  </div>
				</section>';
	
						
}
add_shortcode('select_about_us', 'about_us_shortcode');

?>