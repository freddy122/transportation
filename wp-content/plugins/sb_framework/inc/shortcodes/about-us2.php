<?php
/* ------------------------------------------------ */
/* Section whychoose Shortcode */
/* ------------------------------------------------ */

function sb_theme_why_choose_us_func() {
 	
vc_map( array(
    "name" => __("Why Choose Us", 'sb_framework'),
    "base" => "why_choose_us",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
			
		//  Background Starts

			array(
               "group" => "Background/Layout",			
                "type" => "dropdown",
                "heading" => esc_html__("Show BG Color", 'sb_framework'),
                "param_name" => "whychoose_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Background' => '',
					'White' => 'white',
					'Gray' => 'gray',
				),
                "std" => '',
                "description" => esc_html__("Select background color.", 'sb_framework')
            ),			
			array(
               "group" => "Background/Layout",			
                "type" => "dropdown",
                "heading" => esc_html__("Select Layout", 'sb_framework'),
                "param_name" => "need_us_position",
                "admin_label" => true,
                "value" => 
				array(
					'Select Layout Side' => '',
					'Layout 1' => 'right',
					'Layout 2' => 'left',
				),
                "std" => '',
                "description" => esc_html__("Select where you want to show from on left or right side.", 'sb_framework')
            ),			
		
		// Background Ends
		//More About Us
		array(
				"group" => "More About Us",
                "type" => "textfield",
                "heading" => esc_html__("Tagline", 'sb_framework'),
                "param_name" => "whychoose_main_tagline",
                "admin_label" => true,
                "description" => esc_html__("Enter tagline here.", 'sb_framework'),
            ),


			array(
				"group" => "More About Us",
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "whychoose_main_title",
                "admin_label" => true,
                "description" => esc_html__("Enter title here.", 'sb_framework'),
            ),

			array(
				"group" => "More About Us",
                "type" => "textarea",
                "heading" => esc_html__("Short Description", 'sb_framework'),
                "param_name" => "whychoose_main_desc",
                "admin_label" => true,
                "description" => esc_html__("Enter short description here.", 'sb_framework'),
	            ),	
			//Group Starts	
			array(
				'group' => __( 'More About Us', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Pricing Items', 'sb_framework' ),
				'param_name' => 'about_us_items',
				'value' => '',
				'params' => array
				(				
					 array(
					 	"group" => "Add New",
						 'type' => 'iconpicker',
						 'heading' => __( 'Services Icons', 'js_composer' ),
						 'param_name' => 'whychoose_item_icons',
						 'settings' => array(
						 	 'value' => 'all',
							 'emptyIcon' => false,
							 'type' => 'sb_theme_flaticon',
							 'iconsPerPage' => 100, // default 100, how many icons per/page to display
						 ),
					 ),
					
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Title", 'sb_framework'),
						"param_name" => "whychoose_item_title",
						"admin_label" => true,
						"value" => '',
						"std" => '',
						"description" => esc_html__("Enter title here", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textarea",
						"heading" => esc_html__("Description", 'sb_framework'),
						"param_name" => "whychoose_item_desc",
						"admin_label" => true,
						"value" => '',
						"std" => '#',
						"description" => esc_html__("Enter some description here.", 'sb_framework'),
					),		
				)
			),
		//  More About Us Ends	
		//  Need Help 
		
		array(
				"group" => "Need Help",
                "type" => "textfield",
                "heading" => esc_html__("Tagline", 'sb_framework'),
                "param_name" => "need_help_tagline",
                "admin_label" => true,
                "value" => '',
                "std" => '',
                "description" => esc_html__("Enter tagline here.", 'sb_framework'),
            ),


			array(
				"group" => "Need Help",
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "need_help_title",
                "admin_label" => true,
                "value" => '',
                "std" => '',
                "description" => esc_html__("Enter title here.", 'sb_framework'),
            ),

			array(
				"group" => "Need Help",
                "type" => "textarea",
                "heading" => esc_html__("Short Description", 'sb_framework'),
                "param_name" => "need_help_desc",
                "admin_label" => true,
                "value" => '',
                "std" => '',
                "description" => esc_html__("Enter short description here.", 'sb_framework'),
            ),
		//Group Starts			
		array(
			'group' => __( 'Need Help', 'sb_framework' ),
			'type' => 'param_group',
			'heading' => __( 'Pricing Items', 'sb_framework' ),
			'param_name' => 'need_help_items',
			'value' => '',
			'params' => array
			(
				array(
					"group" => "Add New",
					"type" => "textfield",
					"heading" => esc_html__("Title", 'sb_framework'),
					"param_name" => "need_us_item_title",
					"admin_label" => true,
					"value" => '',
					"std" => '',
					"description" => esc_html__("Enter title here", 'sb_framework'),
				),
				array(
					"group" => "Add New",
					"type" => "textarea",
					"heading" => esc_html__("Description", 'sb_framework'),
					"param_name" => "need_us_item_desc",
					"admin_label" => true,
					"value" => '',
					"std" => '#',
					"description" => esc_html__("Enter some description here.", 'sb_framework'),
				),		
			)
		)
		//  Need Help Ends							
								
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_why_choose_us_func' );


function why_choose_us_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		'whychoose_main_tagline' => '',
		'whychoose_main_title' => '',
		'whychoose_main_desc' => '',
		'whychoose_bg_color' => 'gray',	
		'need_us_position' => 'right',
		  	 
		'about_us_items' => '',
		
		'need_help_tagline' => '',
		'need_help_title' => '',
		'need_help_desc' => '',
		'need_help_items' => '',
	
		
	   ), $atts));

		// about_us_items Half		
		$first_half = vc_param_group_parse_atts( $atts['about_us_items'] );
		$first_half_list = '';
		if( count( $first_half ) > 0 )
		{
		
			foreach($first_half as $why)
			{
				$chose_icon  = '';
				$chose_name  = '';
				$chose_desc  = '';

				if( isset( $why['whychoose_item_icons'] ) ){ $chose_icon  =  $why['whychoose_item_icons'];}
				if( isset( $why['whychoose_item_title'] ) ){ $chose_name  =  $why['whychoose_item_title'];}
				if( isset( $why['whychoose_item_desc'] ) ){ $chose_desc  =  $why['whychoose_item_desc'];}

				$first_half_list .= '<li><div class="choose-box"><span class="iconbox"><i class="'.esc_attr( $chose_icon ).'"></i></span><div class="choose-box-content"><h4>'.esc_html( $chose_name ).'</h4><p>'.esc_html( $chose_desc ).'</p></div></div></li>';
			}
		}
		
		// need help Half		
		$other_half = vc_param_group_parse_atts( $atts['need_help_items'] );
		$need_half_list = '';
		if( count( $other_half ) > 0 )
		{
			$cc = 1;
			foreach($other_half as $need)
			{
				$title  = '';
				$desc = '';
				if( isset( $need['need_us_item_title'] ) ){ $title  =  $need['need_us_item_title'];}
				if( isset( $need['need_us_item_desc'] ) ){ $desc  =  $need['need_us_item_desc'];}
				
				$need_half_list .= '<div class="accordion accordion-block"><div class="accord-btn"><h4>'.esc_html( $title ).'</h4></div><div class="accord-content"><p>'.esc_html( $desc ).'</p></div></div>';
			
				$cc++;
			}
		}
	$need_half_html = '<div class="col-md-5 col-sm-12 col-xs-12">
        <div class="accordion-box style-one">
          <div class="faqs-title">
            <h3>'.esc_html( $need_help_tagline ) .'</h3>
            <h2>'.esc_html( $need_help_title ) .'</h2>
            <p>'.esc_html( $need_help_desc ) .'</p>
          </div>
          <!-- Accordion -->
          '.$need_half_list.'
        </div>
      </div>';
		
	$need_html_left = '';
	$need_html_right = $need_half_html;
	if($need_us_position == 'left' )
	{
		$need_html_left = $need_half_html;
		$need_html_right = '';
	}


	return '<section class="section-padding-70 '.$whychoose_bg_color.'" id="why-choose">
	  <div class="container">
		<div class="row clearfix"> 
		
		 <!-- Extra Here OR Below -->
		  '.$need_html_left.'
		  <div class="col-md-7 col-sm-12 col-xs-12 ">
			<div class="about-title">
			  <h3>'.esc_html($whychoose_main_tagline).'</h3>
			  <h2>'.esc_html($whychoose_main_title).'</h2>
			  <p>'.esc_html($whychoose_main_desc).'</p>
			</div>
			<div class="choose-services">
			  <ul class="choose-list">
			  		'. $first_half_list .'
			</ul>
		      <!-- end choose-list --> 
		  </div>
		</div>
			'.$need_html_right.'
			<!-- Extra Here OR ABOVE -->
			
		</div>
	  </div>
	</section>';		
		
		// need help Half	
	
						
}
add_shortcode('why_choose_us', 'why_choose_us_shortcode');
