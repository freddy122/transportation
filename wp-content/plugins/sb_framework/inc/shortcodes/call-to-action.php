<?php

/* ------------------------------------------------ */
/* call_to_action Shortcode */
/* ------------------------------------------------ */



function sb_theme_call_to_action_func() {
	
 	
vc_map( array(
    "name" => __("Call Action Button", 'sb_framework'),
    "base" => "select_call_to_action",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
	
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Tagline", 'sb_framework'),
                "param_name" => "call_to_action_tagline",
                "admin_label" => true,
                "value" => '',
                "std" => 'Not sure which solution fits you business needs?',
                "description" => esc_html__("Enter some tagline here", 'sb_framework'),
            ),
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Button Text", 'sb_framework'),
                "param_name" => "call_to_action_button_text",
                "admin_label" => true,
                "value" => '',
                "std" => 'Get A Quote',
                "description" => esc_html__("Enter button text here", 'sb_framework'),
            ),
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Button URL", 'sb_framework'),
                "param_name" => "call_to_action_button_url",
                "admin_label" => true,
                "value" => '',
                "std" => '#',
            ),
			array(
               "group" => "Header",			
                "type" => "dropdown",
                "heading" => esc_html__("Open Link In", 'sb_framework'),
                "param_name" => "call_to_action_button_target",
                "admin_label" => true,
                "value" => 
				array(
					'Select Target Window' => '',
					'Current Windo' => '_self',
					'New Window' => '_blank',
				),
                "std" => '',
                "description" => esc_html__("Select the target window when click on button.", 'sb_framework')
            ),				
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_call_to_action_func' );

function call_to_action_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'call_to_action_tagline' => 'Not sure which solution fits you business needs?',
		  'call_to_action_button_text' => 'Get A Quote',
		  'call_to_action_button_url' => '#',
		  'call_to_action_button_target' => '#',			   
	   ), $atts));
		
		
		return '<div class="parallex-small" id="call-to-action">
        <div class="container">
            <div class="row custom-padding-20 ">
                <div class="col-md-8 col-sm-8">
                    <div class="parallex-text">
                        <h4>'.esc_html( $call_to_action_tagline ).'</h4>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="parallex-button"> 
						<a class="page-scroll btn btn-lg btn-clean" href="'.esc_url( $call_to_action_button_url ).'" target="'.esc_attr($call_to_action_button_target).'">
						'.esc_html( $call_to_action_button_text ).'
						<i class="fa fa-angle-double-right "></i></a> 
					</div>
                </div>
            </div>
        </div>
    </div>';
	
						
}
add_shortcode('select_call_to_action', 'call_to_action_shortcode');

?>