<?php

/* ------------------------------------------------ */
/* Contact US */
/* ------------------------------------------------ */


function sb_contact_us_short() {
vc_map( array(
    "name" => __("Contact US", 'sb_framework'),
    "base" => "sb_contact",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
    "params" => array(
			 array(
                "type" => "dropdown",
                "heading" => esc_html__("Select Format", 'sb_framework'),
                "param_name" => "sb_section_alignment",
                "admin_label" => true,
                "value" => array(
                    'Select Format' => '',
                    'Only Map' => 'map',
                    'Only Contact Form' => 'form',
                    'Contact Form Right' => 'right',
                    'Contact Form Left' => 'left'
                ),
                "std" => '',
            	),
			 array(
                "type" => "dropdown",
                "heading" => esc_html__("Background Color", 'sb_framework'),
                "param_name" => "sb_bg_color",
                "admin_label" => true,
                "value" => array(
                    'Select Background Color' => '',
                    'White' => 'white',
                    'Gray' => 'gray'
                ),
                "std" => '',
                "description" => esc_html__("Select Background Color", 'sb_framework')
            	),
				
			array(
			  "type" => "checkbox",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Show Section Header", 'sb_framework' ),
			  "param_name" => "sb_show_header",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Title", 'sb_framework' ),
			  "param_name" => "sb_section_title",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
			array(
			  "type" => "textarea",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Description", 'sb_framework' ),
			  "param_name" => "sb_section_description",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
			array(
			  "group" => 'Map',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Longitude", 'sb_framework' ),
			  "param_name" => "sb_long",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
			array(
			  "group" => 'Map',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Latitude", 'sb_framework' ),
			  "param_name" => "sb_lat",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),	
			
			//Contact Info Tab
			
			array(
				"group" => "Contact Info",
                "type" => "dropdown",
                "heading" => esc_html__("Show Contact Info", 'sb_framework'),
                "param_name" => "show_contact_info_area",
                "admin_label" => true,
                "value" => 
				array(
					'Select Option' => '',
					'Show' => '1',
					'Hide' => '0',
				),
				"std" => "1",
                "description" => esc_html__("Show or hide contact info.", 'sb_framework')
            ),				
			array(
				"group" => 'Contact Info',
				"type" => "heading",
				"heading" => esc_html__("Enter Location Info Here:", 'sb_framework'),
				"param_name" => "heading_1",
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
								
			),			
						
			 array(
				"group" => 'Contact Info',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'sb_contact_icon_1',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
				 
			 ),
							
			array(
			  "group" => 'Contact Info',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Heading Title", 'sb_framework' ),
			  "param_name" => "sb_contact_title1",
			  "value" => __( "", 'sb_framework' ),
			  'edit_field_class' => 'vc_col-sm-6 vc_column',
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
			  
			),	
			array(
			  "group" => 'Contact Info',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Heading Value", 'sb_framework' ),
			  "param_name" => "sb_contact_val1",
			  "value" => __( "", 'sb_framework' ),
			  'edit_field_class' => 'vc_col-sm-6 vc_column',
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
			  
			),	

			array(
				"group" => 'Contact Info',
				"type" => "heading",
				"heading" => esc_html__("Enter Call Us Info Here:", 'sb_framework'),
				"param_name" => "heading_2",
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
				
			),
						

			 array(
				"group" => 'Contact Info',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'sb_contact_icon_2',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
				 
			 ),
			
			array(
			  "group" => 'Contact Info',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Heading Title", 'sb_framework' ),
			  "param_name" => "sb_contact_title2",
			  "value" => __( "", 'sb_framework' ),
			  'edit_field_class' => 'vc_col-sm-6 vc_column',
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
			  
			),	
			array(
			  "group" => 'Contact Info',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Heading Value", 'sb_framework' ),
			  "param_name" => "sb_contact_val2",
			  "value" => __( "", 'sb_framework' ),
			  'edit_field_class' => 'vc_col-sm-6 vc_column',
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
			  
			),	

			array(
				"group" => 'Contact Info',
				"type" => "heading",
				"heading" => esc_html__("Enter Mail Us Info Here:", 'sb_framework'),
				"param_name" => "heading_3",
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
				
			),			
			 array(
				"group" => 'Contact Info',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'sb_contact_icon_3',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
				 
			 ),			
			array(
			  "group" => 'Contact Info',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Heading Title", 'sb_framework' ),
			  "param_name" => "sb_contact_title3",
			  "value" => __( "", 'sb_framework' ),
			  'edit_field_class' => 'vc_col-sm-6 vc_column',
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'show_contact_info_area',
					'value' => '1',
				),				
			  
			),	
			array(
			  "group" => 'Contact Info',
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Heading Value", 'sb_framework' ),
			  "param_name" => "sb_contact_val3",
			  "value" => __( "", 'sb_framework' ),
			  'edit_field_class' => 'vc_col-sm-6 vc_column',
			  "description" => __( "", 'sb_framework' ),
			  'dependency' => array(
				'element' => 'show_contact_info_area',
				'value' => '1',
			  ),				

			),	
			

				
    ),
) );

}
add_action( 'vc_before_init', 'sb_contact_us_short' );

function sb_contact_func($atts, $content = '') {
	
	
   extract(shortcode_atts(array(
	  'sb_bg_color' => '',
	  'sb_show_header' => '',
      'sb_section_title' => '',
	  'sb_section_description' => '',
	  
	  'sb_long' => '',
	  'sb_lat' => '',
	  
	  'sb_contact_title1' => '',
	  'sb_contact_val1' => '',
	  'sb_contact_icon_1' => '',
	  
	  'sb_contact_title2' => '',
	  'sb_contact_val2' => '',
	  'sb_contact_icon_2' => '',
	  
	  'sb_contact_title3' => '',
	  'sb_contact_val3' => '',
	  'sb_contact_icon_3' => '',
	  'sb_section_alignment' => 'right',
	  'show_contact_info_area' => '1',
   ), $atts));
	
global $sb_themes;	
$google_api_key = $sb_themes['header-map-api-key'];
	
wp_register_script( 'google-map-js', '//maps.googleapis.com/maps/api/js?key='. $google_api_key, false, false, true );
wp_register_script( 'google-map-settings-js', trailingslashit( get_template_directory_uri () ) . 'js/map-settings.js', false, false, true );
wp_enqueue_script( 'google-map-js');
wp_enqueue_script( 'google-map-settings-js');	
	
	
	$s_head = 	'';
	if( $sb_show_header )
	{
		$s_head = '<div class="main-heading text-center padding-top-100">
          <h2>'.esc_html( $sb_section_title ).'</h2>
          <p>'.esc_html( $sb_section_description ).'</p>
        </div>';
	}
	else
	{
		$s_head = '<div class="padding-top-60"></div>';
	}
	
	//show_contact_info_area
	$show_contact_info_html = '';
	if($show_contact_info_area == 1)
	{
		$show_contact_info_html =  '<div class="row">
				<div class="col-sm-4 col-md-4 col-xs-12 '.$sb_bg_color.'">
				  <div class="location-item text-center">
					<div class="icon"> <i class="'.esc_attr( $sb_contact_icon_1 ).'"></i> </div>
					<h4 class="text-uppercase">'.esc_html( $sb_contact_title1 ).'</h4>
					<p>'.esc_html( $sb_contact_val1 ).'</p>
				  </div>
				</div>
				<div class="col-sm-4 col-md-4 col-xs-12">
				  <div class="location-item text-center">
					<div class="icon"> <i class="'.esc_attr( $sb_contact_icon_2 ).'"></i> </div>
					<h4 class="text-uppercase">'.esc_html( $sb_contact_title2 ).'</h4>
					<p>'.esc_html( $sb_contact_val2 ).'</p>
				  </div>
				</div>
				<div class="col-sm-4 col-md-4 col-xs-12">
				  <div class="location-item text-center">
					<div class="icon"> <i class="'.esc_attr( $sb_contact_icon_3 ).'"></i> </div>
					<h4 class="text-uppercase">'.esc_html( $sb_contact_title3 ).'</h4>
					 <p>'.esc_html( $sb_contact_val3 ).'</p>
				  </div>
				</div>
			  </div>';
	}

	if( $sb_section_alignment == 'right' )
	{

		return '
		<section id="one-page-contact" class="'.$sb_bg_color.'">
		<div class="container-fluid '.$sb_bg_color.'">
		  <div class="row">
			<div class="col-md-6 no-padding">
			  <div id="map"></div>
			</div>
			<!-- /.col-md-6 -->
			<div class="col-md-6 '.$sb_bg_color.'">
			<!-- title-section -->
			'. $s_head .'
			<!-- End title-section -->
			  
			  '.$show_contact_info_html.'
			  
			  <form id="contactForm" method="post"  >
			  <div id="msg-box"></div>
			  <div class="row ">
				<div class="col-sm-6 col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="text" placeholder="'.esc_attr__( 'Name', 'sb_framework').'" id="contact-name" name="contact_name" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-6 col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="email" placeholder="'.esc_attr__( 'Email', 'sb_framework').'" id="contact-email" name="contact_email" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-6 col-md-12 col-xs-12">
				  <div class="form-group">
					<input type="text" placeholder="'.esc_attr__( 'Subject', 'sb_framework').'" id="contact-subject" name="contact_subject" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-12 col-md-12 col-xs-12">
				  <div class="form-group">
					<textarea cols="12" rows="6" placeholder="'.esc_attr__( 'Message...', 'sb_framework').'" id="contact-message" name="contact_message" class="form-control" required></textarea>
				  </div>
				</div>
				<div class="col-sm-12 col-md-12 col-xs-12">
				  <div class="form-group">
       <button type="button" class="btn btn-primary pull-right" id="submit-for-contact">'.esc_html__( 'Send Message', 'sb_framework' ).'</button><img id="loader" alt="loader" src="'.trailingslashit( get_template_directory_uri() ).'images/loader.gif" class="loader">
						<input type="hidden" id="sb_lat" value="'.$sb_long.'" />
						<input type="hidden" id="sb_long" value="'.$sb_lat.'" />
						<input type="hidden" id="sb_marker" value="'.trailingslashit( get_template_directory_uri() ).'images/marker.png" />
	
				  </div>
				</div>
				</form>
			  </div>
			</div>
		  </div>
		</div>
		<!-- /.container --> 
	  </section>
		';
	}
	else if( $sb_section_alignment == 'left' )
	{
		return '
		<section id="one-page-contact" class="'.$sb_bg_color.'">
		<div class="container-fluid '.$sb_bg_color.'">
		  <div class="row">
			<!-- /.col-md-6 -->
			<div class="col-md-6 '.$sb_bg_color.'">
			<!-- title-section -->
			'. $s_head .'
			<!-- End title-section -->
			  '.$show_contact_info_html.'
			  <form id="contactForm" method="post"  >
			  <div id="msg-box"></div>
			  <div class="row ">
				<div class="col-sm-6 col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="text" placeholder="'.esc_attr__( 'Name', 'sb_framework').'" id="contact-name" name="contact_name" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-6 col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="email" placeholder="'.esc_attr__( 'Email', 'sb_framework').'" id="contact-email" name="contact_email" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-6 col-md-12 col-xs-12">
				  <div class="form-group">
					<input type="text" placeholder="'.esc_attr__( 'Subject', 'sb_framework').'" id="contact-subject" name="contact_subject" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-12 col-md-12 col-xs-12">
				  <div class="form-group">
					<textarea cols="12" rows="6" placeholder="'.esc_attr__( 'Message...', 'sb_framework').'" id="contact-message" name="contact_message" class="form-control" required></textarea>
				  </div>
				</div>
				<div class="col-sm-12 col-md-12 col-xs-12">
				  <div class="form-group">
       <button type="button" class="btn btn-primary pull-right" id="submit-for-contact">'.esc_html__( 'Send Message', 'sb_framework' ).'</button><img id="loader" alt="loader" src="'.trailingslashit( get_template_directory_uri() ).'images/loader.gif" class="loader">
						<input type="hidden" id="sb_lat" value="'.$sb_long.'" />
						<input type="hidden" id="sb_long" value="'.$sb_lat.'" />
												<input type="hidden" id="sb_marker" value="'.trailingslashit( get_template_directory_uri() ).'images/marker.png" />

	
				  </div>
				</div>
				</form>
			  </div>
			</div>
						<div class="col-md-6 no-padding">
			  <div id="map"></div>
			</div>
		  </div>
		</div>
		<!-- /.container --> 
	  </section>
		';
	}
	else if( $sb_section_alignment == 'map' )
	{
		return '<section id="google-map" class="'.$sb_bg_color.'">
		<div class="container-fluid '.$sb_bg_color.'">
		  <div class="row">
			<div class="col-md-12 no-padding '.$sb_bg_color.'">
			  <div id="map"></div>
			</div>
			<input type="hidden" id="sb_lat" value="'.$sb_long.'" />
			<input type="hidden" id="sb_long" value="'.$sb_lat.'" />
									<input type="hidden" id="sb_marker" value="'.trailingslashit( get_template_directory_uri() ).'images/marker.png" />

		  </div>
		  
		</div>
		<!-- /.container --> 
	  </section>';
	}
	else if( $sb_section_alignment == 'form' )
	{
		return '<section id="one-page-contact" class="'.$sb_bg_color.'">
		<div class="container-fluid '.$sb_bg_color.'">
		<div class="container">
		  <div class="row">
			<div class="col-md-12">
			<!-- title-section -->
			'. $s_head .'
			<!-- End title-section -->
			  '.$show_contact_info_html.'
			  <form id="contactForm" method="post"  >
			  <div id="msg-box"></div>
			  <div class="row ">
				<div class="col-sm-6 col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="text" placeholder="'.esc_attr__( 'Name', 'sb_framework').'" id="contact-name" name="contact_name" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-6 col-md-6 col-xs-12">
				  <div class="form-group">
					<input type="email" placeholder="'.esc_attr__( 'Email', 'sb_framework').'" id="contact-email" name="contact_email" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-6 col-md-12 col-xs-12">
				  <div class="form-group">
					<input type="text" placeholder="'.esc_attr__( 'Subject', 'sb_framework').'" id="contact-subject" name="contact_subject" class="form-control" required>
				  </div>
				</div>
				<div class="col-sm-12 col-md-12 col-xs-12">
				  <div class="form-group">
					<textarea cols="12" rows="6" placeholder="'.esc_attr__( 'Message...', 'sb_framework').'" id="contact-message" name="contact_message" class="form-control" required></textarea>
				  </div>
				</div>
				<div class="col-sm-12 col-md-12 col-xs-12">
				  <div class="form-group">
				  
       <button type="button" class="btn btn-primary pull-right" id="submit-for-contact">'.esc_html__( 'Send Message', 'sb_framework' ).'</button><img id="loader" alt="loader" src="'.trailingslashit( get_template_directory_uri() ).'images/loader.gif" class="loader">
				  
						<input type="hidden" id="sb_lat" value="'.$sb_long.'" />
						<input type="hidden" id="sb_long" value="'.$sb_lat.'" />
												<input type="hidden" id="sb_marker" value="'.trailingslashit( get_template_directory_uri() ).'images/marker.png" />

	
				  </div>
				</div>
				</form>
				
			  </div>
			</div>
		  </div>
		  <br /><br />
		</div></div>
		<!-- /.container --> 
	  </section>';
	}
	
	
}

add_shortcode('sb_contact', 'sb_contact_func');



?>