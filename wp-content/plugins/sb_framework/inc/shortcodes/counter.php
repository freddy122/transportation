<?php

/* ------------------------------------------------ */
/* counter Shortcode */
/* ------------------------------------------------ */



function sb_theme_counter_func() {
	
 	
vc_map( array(
    "name" => __("Fun Facts", 'sb_framework'),
    "base" => "select_counter",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(


			array(
               "group" => "Background",
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Background Image", 'sb_framework' ),
                "param_name" => "counter_bg_image",
                "description" => __( "Upload background image.", 'sb_framework' ),
				
            ),					

			 array(
				"group" => 'Block 1',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'counter_icons1',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),					
			array(
				"group" => "Block 1",
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "counter_title1",
                "admin_label" => true,
                "value" => '',
                "std" => 'Happy Clients',
            ),
			array(
				"group" => "Block 1",
                "type" => "textfield",
                "heading" => esc_html__("Numbers", 'sb_framework'),
                "param_name" => "counter_number1",
                "admin_label" => true,
                "value" => '',
                "std" => '356',
            ),

			 array(
				"group" => 'Block 2',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'counter_icons2',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),					
				
			array(
				"group" => "Block 2",
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "counter_title2",
                "admin_label" => true,
                "value" => '',
                "std" => 'Order Received',
            ),
			array(
				"group" => "Block 2",
                "type" => "textfield",
                "heading" => esc_html__("Numbers", 'sb_framework'),
                "param_name" => "counter_number2",
                "admin_label" => true,
                "value" => '',
                "std" => '126',
            ),		
			
			 array(
				"group" => 'Block 3',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'counter_icons3',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),	
			 				
			array(
				"group" => "Block 3",
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "counter_title3",
                "admin_label" => true,
                "value" => '',
                "std" => 'Free Delivery',
            ),
			array(
				"group" => "Block 3",
                "type" => "textfield",
                "heading" => esc_html__("Numbers", 'sb_framework'),
                "param_name" => "counter_number3",
                "admin_label" => true,
                "value" => '',
                "std" => '274',
            ),	

			 array(
				"group" => 'Block 4',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'counter_icons4',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),					
			array(
				"group" => "Block 4",
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "counter_title4",
                "admin_label" => true,
                "value" => '',
                "std" => 'Completed Projects',
            ),
			array(
				"group" => "Block 4",
                "type" => "textfield",
                "heading" => esc_html__("Numbers", 'sb_framework'),
                "param_name" => "counter_number4",
                "admin_label" => true,
                "value" => '',
                "std" => '434',
            ),
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_counter_func' );

function counter_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'counter_bg_image' => '',
		  
		  'counter_icons1' => 'flaticon-woman-with-headset',
		  'counter_title1' => 'Happy Clients',
		  'counter_number1' => '356',

		  'counter_icons2' => 'flaticon-commercial-delivery-symbol-of-a-list-on-clipboard-on-a-box-package',
		  'counter_title2' => 'Order Received',
		  'counter_number2' => '126',

		  'counter_icons3' => 'flaticon-free-delivery-truck',
		  'counter_title3' => 'Free Delivery',
		  'counter_number3' => '274',

		  'counter_icons4' => 'flaticon-ocean-transportation',
		  'counter_title4' => 'Completed Projects',
		  'counter_number4' => '434',
		  				   
	   ), $atts));
		
		$image = wp_get_attachment_image_src( $counter_bg_image, 'full' );
		$style = '';
		if($image[0] != "")
		{
			$style = 'style="rgba(0, 0, 0, 0) url('.esc_url($image[0]).') repeat scroll center top / cover;"';
		}

	return	'<div class="parallex section-padding-70 fun-facts-bg text-center" data-stellar-background-ratio="0" '.$style.' id="counter">
					<div class="container">
						<div class="row">
						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="statistic-percent" data-perc="'.esc_attr($counter_number1).'">
								<div class="facts-icons"> <span class="'.esc_attr($counter_icons1).'"></span> </div>
								<div class="fact"> <span class="percentfactor">'.esc_html($counter_number1).'</span>
									<p>'.esc_html($counter_title1).'</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="statistic-percent" data-perc="'.esc_attr($counter_number2).'">
								<div class="facts-icons"> <span class="'.esc_attr($counter_icons2).'"></span> </div>
								<div class="fact"> <span class="percentfactor">'.esc_html($counter_number2).'</span>
									<p>'.esc_html($counter_title2).'</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="statistic-percent" data-perc="'.esc_attr($counter_number3).'">
								<div class="facts-icons"> <span class="'.esc_attr($counter_icons3).'"></span> </div>
								<div class="fact"> <span class="percentfactor">'.esc_html($counter_number3).'</span>
									<p>'.esc_html($counter_title3).'</p>
								</div>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3 col-md-3">
							<div class="statistic-percent" data-perc="'.esc_attr($counter_number4).'">
								<div class="facts-icons"> <span class="'.esc_attr($counter_icons4).'"></span> </div>
								<div class="fact"> <span class="percentfactor">'.esc_html($counter_number4).'</span>
									<p>'.esc_html($counter_title4).'</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>';
	
						
}
add_shortcode('select_counter', 'counter_shortcode');

?>