<?php
/* ------------------------------------------------ */
/* Hero Section */
/* ------------------------------------------------ */


function sb_hero_admin() {
vc_map( array(
    "name" => __("Hero Section", 'sb_framework'),
    "base" => "sb_hero",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
    "params" => array(
			array(
				"type" => "attach_image",
				"holder" => "img",
				"class" => "",
				"heading" => __( "Background Image", 'sb_framework' ),
				"param_name" => "sb_bg",
				"value" => __( '', 'sb_framework' ),
				"description" => __( "1200 X 800", 'sb_framework' )
			),
			 array(
                "type" => "dropdown",
                "heading" => esc_html__("Select Type", 'sb_framework'),
                "param_name" => "sb_bg_style",
                "admin_label" => true,
                "value" => array(
                    'Select Style' => '',
                    'Parallax BG' => 'parallax-home',
                    'Static BG' => 'static-bg'
                ),
                "std" => '',
                "description" => esc_html__("", 'sb_framework')
            	),
				
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Title", 'sb_framework' ),
			  "param_name" => "sb_section_title",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "Please enter title here", 'sb_framework' )
			),			
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Sub-Title", 'sb_framework' ),
			  "param_name" => "sb_section_sub_title",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Button Title", 'sb_framework' ),
			  "param_name" => "sb_section_btn_title",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Link", 'sb_framework' ),
			  "param_name" => "sb_section_btn_link",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
	
				
    ),
) );

}
add_action( 'vc_before_init', 'sb_hero_admin' );

function sb_hero_func($atts, $content = '') {
	
	
   extract(shortcode_atts(array(
   	  'sb_bg' => '',
	  'sb_bg_style' => 'parallax-home',
      'sb_section_title' => '',
	  'sb_section_sub_title' => '',
	  'sb_section_btn_title' => '',
	  'sb_section_btn_link' => '',
   ), $atts));
	
	$img	=	'';
	if( $sb_bg != "" )
	{
		$img_array = wp_get_attachment_image_src( $sb_bg, 'full' );
		$img	=	$img_array[0];
	}
	$btn	=	'';
	if( $sb_section_btn_title != "" )
	{
		$btn	=	   '<a class="btn page-scroll" href="'.esc_url( $sb_section_btn_link ).'">'.esc_html( $sb_section_btn_title ).'</a>';

	}
	
	$style	=	'<style>
		#home {
    background: transparent url("'.$img.'") no-repeat scroll 0 0 / cover;
    background-size: cover;
    -moz-background-size: cover;
    -ms-background-size: cover;
    -o-background-size: cover;
	height: 690px;
}
	</style>';
	if( $sb_bg_style == 'rotate' )
	{
	
	}
	else
	{
		$data=	'';
		if( $sb_bg_style == 'parallax-home' )
			$data = 'data-stellar-background-ratio="0.5"';
	return
	$style . '
	<div id="home" class="full-section '.$sb_bg_style.'" '.$data.'>
  <div class="slider-caption">
    <h1>'.esc_html( $sb_section_title ).'</h1>
    <p>'.esc_html( $sb_section_sub_title ).'</p>
    '.$btn.'
  </div>
</div>
	';
	}
	
}

add_shortcode('sb_hero', 'sb_hero_func');



?>