<?php
/* ------------------------------------------------ */
/* Our App Shortcode */
/* ------------------------------------------------ */
function logisticpro_our_app_func() {
vc_map( array(
    "name" => __("Our App", 'sb_framework'),
    "base" => "select_our_app_block",
	"as_parent" => array('only' => 'select_our_app_block_area'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
			
			array(
                "type" => "textfield",
                "heading" => esc_html__("Our App Title", 'sb_framework'),
                "param_name" => "our_app_title",
                "admin_label" => true,
                "description" => esc_html__("Download Our Tracking App", 'sb_framework'),
            ),
			array(
                "type" => "textarea",
                "heading" => esc_html__("Our App Description", 'sb_framework'),
                "param_name" => "our_app_desc",
                "admin_label" => true,
                "value" => '',
                "description" => esc_html__("Enter main description here.", 'sb_framework'),
            ),	

			array(
                "type" => "dropdown",
                "heading" => esc_html__("Image Position", 'sb_framework'),
                "param_name" => "process_image_position",
                "admin_label" => true,
                "value" => 
				array(
				    'Select Option' => '',
					'Left' => 'left',
					'Right' => 'right',
				),
                "std" => '',
                "description" => esc_html__("Select the option you want to show image on right or left side.", 'sb_framework')
            ),				
			array(
				"holder" => "img",
				"class" => "",
				"type" => "attach_image",
				"heading" => __( "Our App Image", 'sb_framework' ),
				"param_name" => "our_app_item_image",
				"description" => __( "Upload gallery image.", 'sb_framework' ),
			),	
			array(
				"holder" => "img",
				"class" => "",
				"type" => "attach_image",
				"heading" => __( "Backgroung Image", 'sb_framework' ),
				"param_name" => "our_app_item_image_bg",
				"description" => __( "Upload Backgroung Image", 'sb_framework' ),
			),			
			
			/* Android */
			array(
				"group" => "Android App",
				"type" => "lebel",
				"param_name" => "our_app_android_lebel",
				"admin_label" => true,
				
			),			
			array(
				"group" => "Android App",
				"type" => "textfield",
				"heading" => esc_html__("Android App Title", 'sb_framework'),
				"param_name" => "our_app_android_title",
				"admin_label" => true,
				"std" => "Abdroid",
				
			),
			array(
				"group" => "Android App",
				"type" => "textfield",
				"heading" => esc_html__("Android App Link", 'sb_framework'),
				"param_name" => "our_app_android_link",
				"admin_label" => true,
				"std" => "#",
				
			),
		
			
			/* iPhone */

			array(
				"group" => "iPhone App",
				"type" => "textfield",
				"heading" => esc_html__("iPhone App Title", 'sb_framework'),
				"param_name" => "our_app_iphone_title",
				"admin_label" => true,
				"std" => "iPhone",
				
			),
			array(
				"group" => "iPhone App",
				"type" => "textfield",
				"heading" => esc_html__("iPhone App Link", 'sb_framework'),
				"param_name" => "our_app_iphone_link",
				"admin_label" => true,
				"std" => "#",
				
			),	
				
			/* Windows */

			array(
				"group" => "Windows App",
				"type" => "textfield",
				"heading" => esc_html__("Windows App Title", 'sb_framework'),
				"param_name" => "our_app_windows_title",
				"admin_label" => true,
				"std" => "Windows",
				
			),
			array(
				"group" => "Windows App",
				"type" => "textfield",
				"heading" => esc_html__("Windows App Link", 'sb_framework'),
				"param_name" => "our_app_windows_link",
				"admin_label" => true,
				"std" => "#",
				
			),	
															
																							
	)
) );

}
add_action( 'vc_before_init', 'logisticpro_our_app_func' );


function our_appblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(

		  'our_app_title' => '',
		  'our_app_desc' => '',
		  'our_app_item_image' => '',
		  'our_app_item_image_bg' => '',
		  'process_image_position' => 'left',
		  
		  'our_app_android_title' => 'Android',
		  'our_app_android_link' => '#',
		  'our_app_android_sort' => '1',
		  'our_app_android_icon' => 'fa fa-android',
		  
		  'our_app_iphone_title' => 'iPhone',
		  'our_app_iphone_link' => '#',
		  'our_app_iphone_sort' => '2',
		  'our_app_iphone_icon' => 'fa fa-apple',

		  'our_app_windows_title' => 'Windows',
		  'our_app_windows_link' => '#',
		  'our_app_windows_sort' => '3',
		  'our_app_windows_icon' => 'fa fa-windows',
		  
		  
		  
	   ), $atts));
		
		
		//our_app_item_image_bg
		$style = '';
		$image_bg = wp_get_attachment_image_src( $our_app_item_image_bg, 'full' );
		if( $image_bg[0] != "")
		{
			$style = 'style="background-image: url('. esc_attr( $image_bg[0] ).');"';
		}
				
		
		$image = wp_get_attachment_image_src( $our_app_item_image, 'full' );
		
		$image_html = '';
		if($image[0] != "")
		{
			$image_html = '<img src="'.esc_url($image[0]).'" alt="'. esc_attr( $our_app_title ).'" class="img-absolute" >';
		}
		
		$img_html_area = '<div class="col-md-5 col-sm-4">'.$image_html.'</div>';
		$img_html_left = '';
		$img_html_right = '';
				
		if( $process_image_position == 'left' )
		{
			$img_html_left = $img_html_area;
		}
		else
		{
			$img_html_right = $img_html_area;
		}

		return '<section id="our-app" class="parallex our-app section-padding-100" '.$style.'>
        <div class="container">
          <div class="row"> 
            <!-- Section Content -->
            <div class="section-content">
			  '.$img_html_left.'
              <div class="col-md-7 col-sm-8 text-left">
                <h3>'.$our_app_title.'</h3>
                <p>'.$our_app_desc.'</p>
                <ul class="list-inline">
                  <li>
				  	<a href="'. esc_url($our_app_android_link) .'" class="btn btn-bordered">
				  		<i class="'. esc_attr($our_app_android_icon) .'"></i>
						'. esc_html($our_app_android_title) .'
					</a>
				  </li>
                  <li>
				  	<a href="'. esc_url($our_app_iphone_link) .'" class="btn btn-bordered">
				  		<i class="'. esc_attr($our_app_iphone_icon) .'"></i>
						'. esc_html($our_app_iphone_title) .'
					</a>
				  </li>
                  <li>
				  	<a href="'. esc_url($our_app_windows_link) .'" class="btn btn-bordered">
				  		<i class="'. esc_attr($our_app_windows_icon) .'"></i>
						'. esc_html( $our_app_windows_title ) .'
					</a>
				  </li>				  				  
                </ul>
              </div>
			  '.$img_html_right.'
            </div>
          </div>
        </div>
</section>';
			
						
}
add_shortcode('select_our_app_block', 'our_appblock_shortcode');

?>