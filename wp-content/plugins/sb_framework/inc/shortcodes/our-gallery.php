<?php
/* ------------------------------------------------ */
/* Gallery Shortcode */
/* ------------------------------------------------ */
function sb_theme_gallery_block_func() {
vc_map( array(
    "name" => __("Our Gallery", 'sb_framework'),
    "base" => "select_gallery_block",
	"as_parent" => array('only' => 'select_gallery_block_area'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
			array(
				"group" => "Header",
                "type" => "dropdown",
                "heading" => esc_html__("Select Header", 'sb_framework'),
                "param_name" => "gallery_header",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select the option you want to show the header or not.", 'sb_framework')
            ),			
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Gallery Title", 'sb_framework'),
                "param_name" => "gallery_title",
                "admin_label" => true,
                "description" => esc_html__("Enter main title here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'gallery_header',
					'value' => '1',
				),				
            ),
			array(
				"group" => "Header",
                "type" => "textarea",
                "heading" => esc_html__("Gallery Description", 'sb_framework'),
                "param_name" => "gallery_desc",
                "admin_label" => true,
                "value" => '',
                "description" => esc_html__("Enter main description here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'gallery_header',
					'value' => '1',
				),				
            ),	
			
						
			array(
				"group" => "Background",
                "type" => "dropdown",
                "heading" => esc_html__("Background Color", 'sb_framework'),
                "param_name" => "gallery_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Gray' => 'gray',
					'White' => 'white',
				),
                "std" => '',
                "description" => esc_html__("Select background color", 'sb_framework')
            ),	
			
			//LOOP STARTS
			//Group Starts	for adding Testimonials
			array
			(
				'group' => __( 'Add/Edit Gallery', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Add/Edit Gallery', 'sb_framework' ),
				'param_name' => 'gallery_items',
				'value' => '',
				'params' => array
				(
					array(
					   "group" => "Add New",
						"holder" => "img",
						"class" => "",
						"type" => "attach_image",
						"heading" => __( "Gallery Image", 'sb_framework' ),
						"param_name" => "gallery_item_image",
						"description" => __( "Upload gallery image.", 'sb_framework' ),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Gallery Name", 'sb_framework'),
						"param_name" => "gallery_item_name",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						"description" => esc_html__("Enter gallery item name here.", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Gallery Type", 'sb_framework'),
						"param_name" => "gallery_type_name",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						"description" => esc_html__("Enter gallery type name here.", 'sb_framework'),
					),
					
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Link", 'sb_framework'),
						"param_name" => "gallery_external_link",
						"admin_label" => true,
						"description" => esc_html__("Enter the link related with image.", 'sb_framework'),
					),

				)
			)	
			//LOOP ENDS				
														
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_gallery_block_func' );


function galleryblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'gallery_header' => '1',
		  'gallery_title' => '',
		  'gallery_desc' => '',
		   'gallery_bg_color' => '',
		   'gallery_items' => '',   
	   ), $atts));
		
		$galleryItems = vc_param_group_parse_atts( $atts['gallery_items'] );	
		//LOOP STARTS
		$gallery_innet_html = '';
		if( count( $galleryItems ) > 0 )
		{
		
			foreach($galleryItems as $galleryItem)
			{
				
			////////////////////////////////////////
				$project_name = '';
				$project_type = '';
				$project_url  = '';
				
				if( isset( $galleryItem['gallery_item_name'] ) ){$project_name = $galleryItem['gallery_item_name'];}
				if( isset( $galleryItem['gallery_type_name'] ) ){$project_type = $galleryItem['gallery_type_name'];}
				if( isset( $galleryItem['gallery_external_link'] ) ){$project_url = $galleryItem['gallery_external_link'];}
				
				$imgThumbHTML = '';
				$image_full = '';
				if( isset( $galleryItem['gallery_item_image'] ) )
				{
					$imgID = $galleryItem['gallery_item_image'];
					
					$image = wp_get_attachment_image_src( $imgID, 'logistic-pro-team-thumb' );
					//$image = wp_get_attachment_image_src( $imgID, 'full');
					
					if($image[0] != "")
					{
						$imgThumbHTML = '<img src="'.esc_url($image[0]).'" alt="'. esc_attr( $project_name ).'">';
					}
					
					$image_full = wp_get_attachment_image_src( $imgID, 'full' );
				}
		
				$gallery_innet_html .= '<li class="portfolio-item gutter">
						<div class="portfolio">
							<div class="tt-overlay"></div>
							'. $imgThumbHTML .'
							<div class="portfolio-info">
								<h3 class="project-title">'.esc_html( $project_name ).'</h3>
								<a href="#" class="links">'.esc_html( $project_type ).'</a>
							</div>
							<ul class="portfolio-details">
								<li><a class="tt-lightbox" href="'.esc_url($image_full[0]).'"><i class="fa fa-search"></i></a></li>
								<li><a href="'.esc_url( $project_url ).'"><i class="fa fa-external-link"></i></a></li>
							</ul>
						</div>
					</li>';			
			
			///////////////////////////////////////////
			
			}	
		
		}
		//LOOP ENDS
		// Header Settings 
		$header = '';
		if($gallery_header == 1)
		{
			$header = '<div class="main-heading text-center"><h2>'.$gallery_title.'</h2><p>'.$gallery_desc.'</p></div>';
		}

		return ' <section id="gallery" class="custom-padding '.$gallery_bg_color.'" >
        <div class="container">
			'. $header .'
            <div class="portfolio-container text-center">
                <ul id="portfolio-grid" class="three-column hover-two">
                    '. $gallery_innet_html .'
				</ul>	
            </div>
            <!-- Row End -->
        </div>
        <!-- end container -->
    </section>';
			
						
}
add_shortcode('select_gallery_block', 'galleryblock_shortcode');

?>