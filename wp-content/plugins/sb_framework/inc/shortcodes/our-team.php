<?php
/* ------------------------------------------------ */
/* Our Team Shortcode */
/* ------------------------------------------------ */
function sb_theme_our_team_func() {
	  	
vc_map( array(
    "name" => __("Our Team", 'sb_framework'),
    "base" => "select_team_block",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
			array(
				"group" => "Header",
                "type" => "dropdown",
                "heading" => esc_html__("Select Header", 'sb_framework'),
                "param_name" => "team_header",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select the option you want to show the header or not.", 'sb_framework')
            ),			
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Team Title", 'sb_framework'),
                "param_name" => "team_title",
                "admin_label" => true,
                "description" => esc_html__("Enter main title here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'team_header',
					'value' => '1',
				),				
            ),
			array(
				"group" => "Header",
                "type" => "exploded_textarea",
                "heading" => esc_html__("Team Description", 'sb_framework'),
                "param_name" => "team_desc",
                "admin_label" => true,
                "description" => esc_html__("Enter main description here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'team_header',
					'value' => '1',
				),				
            ),	
			
						
			array(
				"group" => "Background/Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Background Color", 'sb_framework'),
                "param_name" => "team_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Background' => '',
					'Gray' => 'gray',
					'White' => 'white',
				),
                "std" => '',
                "description" => esc_html__("Select background color", 'sb_framework')
            ),	
			array(
				"group" => "Background/Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Team Layout", 'sb_framework'),
                "param_name" => "team_columns",
                "admin_label" => true,
                "value" => 
				array(
					'Select Layout' => '',
					'3 Column' => '4',
					'4 Column' => '3',
				),
                "description" => esc_html__("Select team layout", 'sb_framework')
            ),	
			//
			array(
				"group" => "Background/Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Text Position", 'sb_framework'),
                "param_name" => "team_text_position",
                "admin_label" => true,
                "value" => 
				array(
					'Select Text Position' => '',
					'Left' => 'text-left',
					'Center' => 'text-center',
					'Right' => 'text-right',
				),
                "description" => esc_html__("Select team layout", 'sb_framework')
            ),				
			//Group Starts	
			array(
				'group' => __( 'Add Team Members', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Add Team Members', 'sb_framework' ),
				'param_name' => 'team_member_items',
				'value' => '',
				'params' => array
				(
					array(
						"group" => "Add New",
						"holder" => "img",
						"class" => "",
						"type" => "attach_image",
						"heading" => esc_html__("Upload Image", 'sb_framework'),
						"param_name" => "member_image",
						"admin_label" => true,
						"description" => esc_html__("Upload image  Size: ( 480 X 480 )", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Name", 'sb_framework'),
						"param_name" => "member_name",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						'std' => '',
						"description" => esc_html__("Enter name here", 'sb_framework'),
					), 
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Position", 'sb_framework'),
						"param_name" => "member_position",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						'std' => '',
						"description" => esc_html__("Enter position here i.e. CEO", 'sb_framework'),
					),
					
					//profile
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Facebook", 'sb_framework'),
						"param_name" => "member_facebook",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						'std' => '',
						"description" => esc_html__("Enter facebook profile link", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Twitter", 'sb_framework'),
						"param_name" => "member_twitter",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						'std' => '',
						"description" => esc_html__("Enter twitter profile link", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Google+", 'sb_framework'),
						"param_name" => "member_gplus",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						'std' => '',
						"description" => esc_html__("Enter Google+ profile link", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Linkedin", 'sb_framework'),
						"param_name" => "member_linkedin",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						'std' => '',
						"description" => esc_html__("Enter Linkedin profile link", 'sb_framework'),
					),
					//profile
						
				)
			),					
														
	)
) );



}
add_action( 'vc_before_init', 'sb_theme_our_team_func' );



function teamblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'team_header' => '1',
		  'team_title' => '',
		  'team_desc' => '',
		  'team_bg_color' => '',
		  'team_columns' => '4',
		  'team_text_position' => 'text-center',
		  'team_member_items' => '',	   
	   ), $atts));
		
		$team_members = vc_param_group_parse_atts( $atts['team_member_items'] );
		// Header Settings 
		$header = '';
		
		if($team_header == 1)
		{
			$header = '<div class="main-heading text-center"><h2>'.esc_html( $team_title ).'</h2><p>'.esc_html( $team_desc ).'</p></div>';
		}
		$team_list = '';
		if( count( $team_members ) > 0 )
		{
			foreach($team_members as $team_member)
			{
			$image_html = '';
			$name  = '';
			$position  = '';
			
			
			if( isset( $team_member["member_name"] )){ $name = $team_member["member_name"]; }
			if( isset( $team_member["member_position"] )){ $position = $team_member["member_position"]; }
				
			$social_icons = '';
			if( isset( $team_member["member_facebook"] ))
			{
				$social_icons .= '<a class="facebook" href="'.esc_url($team_member["member_facebook"]).'"><i class="fa fa-facebook"></i></a>'; 
			}
			if( isset( $team_member["member_twitter"] ))
			{
				$social_icons .= '<a class="twitter" href="'.esc_url($team_member["member_twitter"]).'"><i class="fa fa-twitter"></i></a>'; 
			}
			if( isset( $team_member["member_gplus"] ))
			{
				$social_icons .= '<a class="google" href="'.esc_url($team_member["member_gplus"]).'"><i class="fa fa-google-plus"></i></a>'; 
			}
			if( isset( $team_member["member_linkedin"] ))
			{
				$social_icons .= '<a class="linkedin" href="'.esc_url($team_member["member_linkedin"]).'"><i class="fa fa-linkedin"></i></a>'; 
			}
			$member_profile = '';		
			if($social_icons != "") { $member_profile = '<div class="social-media">'. $social_icons .'</div>'; }	
			if( isset( $team_member["member_image"] ))
			{		 
				$image = wp_get_attachment_image_src( $team_member["member_image"], 'logistic-pro-team-thumb' );
				if($image[0] != "")
				{
					$image_html = '<img alt="'.esc_attr($name).'" class="img-responsive" src="'.esc_url($image[0]).'">';
				}
			}
			
				$team_list .= '<div class="col-md-'.$team_columns.' col-sm-6 col-xs-12">
					<div class="team-grid">
					  <div class="team-image"> '.$image_html .'
						<div class="team-grid-overlay">
							'.$member_profile.'
						</div>
					  </div>
					  <div class="team-content '.$team_text_position.'">
						<h2>'. esc_html( $name  ) .'</h2>
						<p>'. esc_html( $position ) .'</p>
					  </div>
					</div>
				  </div>';
			}			
		}
		
		return '<section id="team" class="custom-padding '.$team_bg_color.'">
		  <div class="container"> 
			'.$header.'
			<div class="row">'.$team_list.'</div>
		  </div>
		</section>';

			
						
}
add_shortcode('select_team_block', 'teamblock_shortcode');

?>