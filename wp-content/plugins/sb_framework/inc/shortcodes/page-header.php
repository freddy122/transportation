<?php

/* ------------------------------------------------ */
/* page_header Shortcode */
/* ------------------------------------------------ */



function sb_theme_page_header_func() {
	
 	
vc_map( array(
    "name" => __("Page Header", 'sb_framework'),
    "base" => "select_page_header",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
	
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Tagline", 'sb_framework'),
                "param_name" => "page_header_tagline",
                "admin_label" => true,
                "description" => esc_html__("Enter some tagline here", 'sb_framework'),
            ),
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Page Title", 'sb_framework'),
                "param_name" => "page_header_title",
                "admin_label" => true,
                "description" => esc_html__("Enter page name here.", 'sb_framework'),
            ),				
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_page_header_func' );

function page_header_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'page_header_tagline' => '',
		  'page_header_title' => '',
	   ), $atts));
		
		
		return logistic_pro_theme_the_breadcrumb($page_header_title, $page_header_tagline);
	
						
}
add_shortcode('select_page_header', 'page_header_shortcode');

?>