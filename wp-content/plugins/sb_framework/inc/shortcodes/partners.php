<?php
/* ------------------------------------------------ */
/* Partners Shortcode */
/* ------------------------------------------------ */
function sb_theme_partners_block_func() {	  	
vc_map( array(
    "name" => __("Our Partners", 'sb_framework'),
    "base" => "select_partners_block",
	"as_parent" => array('only' => 'select_partners_block_area, select_partners_block_area2'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(

			array(
				"group" => "Header",
                "type" => "dropdown",
                "heading" => esc_html__("Select Layout", 'sb_framework'),
                "param_name" => "partners_layout",
                "admin_label" => true,
                "value" => 
				array(
					'Select Layout' => '',
					'Layout 1' => '1',
					'Layout 2' => '2',
					'Layout 3' => '3',
				),
            ),			

	
			array(
				"group" => "Header",
                "type" => "dropdown",
                "heading" => esc_html__("Select Header", 'sb_framework'),
                "param_name" => "partners_header",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select the option you want to show the header or not.", 'sb_framework')
            ),			
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Partners Title", 'sb_framework'),
                "param_name" => "partners_title",
                "admin_label" => true,
                "description" => esc_html__("Enter main title here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'partners_header',
					'value' => '1',
				),				
            ),
			array(
				"group" => "Header",
                "type" => "textarea",
                "heading" => esc_html__("Partners Description", 'sb_framework'),
                "param_name" => "partners_desc",
                "admin_label" => true,
                "description" => esc_html__("Enter main description here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'partners_header',
					'value' => '1',
				),				
            ),	
									
			array(
               "group" => "Background",
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Partners Background Image", 'sb_framework' ),
                "param_name" => "partners_block_image",
                "description" => __( "Upload background image or select color below.", 'sb_framework' ),				
            ),
			
			array(
				"group" => "Background",
                "type" => "dropdown",
                "heading" => esc_html__("Select Background Color", 'sb_framework'),
                "param_name" => "partners_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Color' => '',
					'White' => 'white',
					'Gray' => 'gray',
				),
				"description" => esc_html__("Select background color or upload image above", 'sb_framework'),
            ),
			
			//LOOP STARTS
			//Group Starts	for adding Partners
			array
			(
				'group' => __( 'Add/Edit Partners', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Add/Edit Partners', 'sb_framework' ),
				'param_name' => 'partners_items',
				'params' => array
				(

						array(
							"group" => "Add New",
							"type" => "textfield",
							"heading" => esc_html__("Partner Name", 'sb_framework'),
							"param_name" => "partner_name",
							"admin_label" => true,
							"description" => esc_html__("Enter partner name here.", 'sb_framework'),
						),
			
						array(
						   "group" => "Add New",
							"holder" => "img",
							"class" => "",
							"type" => "attach_image",
							"heading" => __( "Partner Image", 'sb_framework' ),
							"param_name" => "partner_image",
							"description" => __( "Upload partner image.", 'sb_framework' ),
							
						),	
				)
			)

						
														
	)
) );


}
add_action( 'vc_before_init', 'sb_theme_partners_block_func' );


function partnersblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'partners_header' => '1',
		  'partners_title' => '',
		  'partners_desc' => '',
		   'partners_block_image' => '',
		   'partners_layout' => '1',
		   'partners_bg_color' => '',
		   'partners_items' => '',
	   ), $atts));
		
		$partners = vc_param_group_parse_atts( $atts['partners_items'] );	
		//LOOP STARTS
		
		$inner_div_starts = '<div class="item"><div class="clients-grid"> ';
		$inner_div_ends   = '</div></div>';
	
		if($partners_layout == 2 )
		{
			$inner_div_starts = '<div class="item">';
			$inner_div_ends   = '</div>';
		}	
		if($partners_layout == 3 )
		{
			$inner_div_starts = '<div class="col-md-3 col-sm-3 col-xs-12"><div class="clients-grid">';
			$inner_div_ends   = '</div></div>';
		}	
		$partners_inner_html = '';		
		if( count( $partners ) > 0 )
		{
			foreach( $partners as $partner )
			{
				$imgHTML = '';
				$name = '';
				
				if( isset( $partner['partner_name']) ){ $name = $partner['partner_name']; }
				
				if( isset( $partner['partner_image'] ))
				{
					$image = wp_get_attachment_image_src(  $partner['partner_image'], 'logistic-pro-partner-thumb' );
					if($image[0] != "")
					{
						$partners_inner_html .= "$inner_div_starts <img class='img-responsive' alt='".esc_attr( $name )."' src='".esc_url( $image[0] )."'> $inner_div_ends";
					}
				}
			}	
		}
		//LOOP ENDS		
		
		
		// Image settings
		$image_bg = wp_get_attachment_image_src( $partners_block_image, 'full' );
		$bgImg = '';
		if( $image_bg[0] != "" ){
			$bgImg = 'style="background: rgba(240, 240, 240, 1) url('.$image_bg[0].') repeat scroll 0 0 / cover ;"';
		}

		// Header Settings 
		$header = '';
		if($partners_header == 1)
		{
			$header = '<div class="main-heading text-center"><h2>'. esc_html( $partners_title ).'</h2><p>'. 
			wp_kses( $partners_desc, logistic_pro_theme_required_tags() ) .'</p></div>';
		}
		
		$section_class = $partners_bg_color;
		$div_starts = '<div id="clients" class="text-center">';
		$div_ends   = '</div>';

		if($partners_layout == 3 )
		{
			$section_class = $partners_bg_color;
			$div_starts = '';
			$div_ends   = '';
			$bgImg = '';
		}
		
		//section-padding-70 clients white
		return '<section class="section-padding-70 '.$section_class.'" '.$bgImg.'>
        <div class="container">
			'. $header .'
            <div class="row">
                	'.$div_starts.'
                    '. $partners_inner_html .'
					'.$div_ends.'
                </div>
        </div>
    </section>';
			
						
}
add_shortcode('select_partners_block', 'partnersblock_shortcode');