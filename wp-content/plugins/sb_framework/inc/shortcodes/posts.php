<?php

/* ------------------------------------------------ */
/* latest Shortcode */
/* ------------------------------------------------ */



function sb_theme_latest_func() {
	
    $ars    = get_categories('category');
    $ar_type   = array(
        "All Categories" => "0"
    );
    $pushed = '';
    foreach ($ars as $ar) {
		$names = '';
		$count = 'count';
		$names =  $ar->name . ' ('. $ar->$count.')';
        $ar_type[$names] = $ar->term_id;
    }
 	
vc_map( array(
    "name" => __("latest Posts", 'sb_framework'),
    "base" => "select_latest",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
			array(
				"group" => "Header",
                "type" => "dropdown",
                "heading" => esc_html__("Select Header", 'sb_framework'),
                "param_name" => "latest_header",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select Yes or No to show or hide header.", 'sb_framework')
            ),	
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Header Title", 'sb_framework'),
                "param_name" => "latest_title",
                "admin_label" => true,
                "value" => '',
                "std" => 'Our latest',
                "description" => esc_html__("Enter header title here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'latest_header',
					'value' => '1',
				),				
            ),
			array(
				"group" => "Header",
                "type" => "textarea",
                "heading" => esc_html__("Header Description", 'sb_framework'),
                "param_name" => "latest_desc",
                "admin_label" => true,
                "value" => '',
                "description" => esc_html__("Enter header description here.", 'sb_framework'),
                "std" => 'Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit',
				'dependency' => array(
					'element' => 'latest_header',
					'value' => '1',
				),				
            ),	
			// layout starts
			array(
				"group" => "Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Show Admin Meta", 'sb_framework'),
                "param_name" => "latest_admin_meta",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '0',
				),
                "std" => '1',
            ),	
			
			array(
				"group" => "Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Blog Type", 'sb_framework'),
                "param_name" => "latest_blog_type",
                "admin_label" => true,
                "value" => 
				array(
					'Select Blog Type' => '',
					'General' => 'general',
					'Masonry' => 'masonry',
				),
            ),	

			array(
				"group" => "Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Blog Sidebar", 'sb_framework'),
                "param_name" => "latest_blog_sidebar",
                "admin_label" => true,
                "value" => 
				array(
					'Select Sidebar' => '',
					'No Sidebar' => '0',
					'Right Sidebar' => '1',
					'Left Sidebar' => '2',
				),
            ),	

			
			array(
				"group" => "Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Show Admin Meta", 'sb_framework'),
                "param_name" => "latest_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Background Color' => '',
					'White' => 'white',
					'Gray' => 'gray',
				),
            ),	

						
			/* Build Query */
			array(
				"group" => "Query",
                "type" => "dropdown",
                "heading" => esc_html__("Sellect No.Of latest", 'sb_framework'),
                "param_name" => "no_of_latest",
                "admin_label" => true,
                "value" => range(-1, 100),
                "std" => '3',
                "description" => esc_html__("Select -1 to show all", 'sb_framework')
            ),	
			array(
				"group" => "Query",
                "type" => "dropdown",
                "heading" => esc_html__("Sellect Order By", 'sb_framework'),
                "param_name" => "orderby_latest",
                "admin_label" => true,
                "value" => 
				array(
					"DESC" => "DESC",
					"ASC" => "ASC",
				),
                "std" => '3',
                "description" => esc_html__("Select -1 to show all", 'sb_framework')
            ),	
			array(
				"group" => "Query",
                "type" => "dropdown",
                "heading" => esc_html__("Sellect Categories", 'sb_framework'),
                "param_name" => "cats_latest",
                "admin_label" => true,
                "value" => $ar_type,
                "std" => '3',
                "description" => esc_html__("Select category for which you want to display latest.", 'sb_framework')
            ),	
								
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_latest_func' );

function latest_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'latest_header' => '1',
		  'latest_title' => 'Our latest',
		  'latest_desc' => 'Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit',
		  
		   'latest_admin_meta' => '1',
		   'latest_bg_color' => 'white',
		   
   		    /* Query Settings */
			'orderby_latest' => 'DESC',
			'cats_latest' => '0',
			'no_of_latest' => '3',
			'latest_blog_type' => 'general',
			'latest_blog_sidebar' => '0',
		   
	   ), $atts));
		
		
		
		$all_posts	= '';
		$catAttr = '';
		if($cats_latest != 0)
		{ 
			$catAttr = "'tax_query' => array(array( 'taxonomy' => 'category', 'field' => 'id', 'terms'    => $cats_latest)),";
		}
		$parameters = array(
		'orderby' => 'date',
		'order' => $orderby_latest,
		'post_type' => 'post',
		'posts_per_page' =>  $no_of_latest,
		$catAttr
		
		);

		$latests	=	get_posts( $parameters );
		
		$loop = '';
		$count = 1;
		foreach( $latests as $service )
		{
			if($latest_blog_type == "general")
			{			
				$is_image	= wp_get_attachment_image_src( get_post_thumbnail_id( $service->ID ), 'sb-logistic-latest-thumb' );
			}
			else
			{
				$is_image	= wp_get_attachment_image_src( get_post_thumbnail_id( $service->ID ), 'large' );
			}
		$image_html = '';
		
		if($is_image[0] != "")
		{ 
			$image_html = '<div class="news-thumb"> 
					<a href="'.esc_html( get_the_permalink( $service->ID ) ).'" title="'.esc_attr( get_the_title( $service->ID ) ).'">
						<img class="img-responsive" alt="'.esc_attr( get_the_title( $service->ID ) ).'" src="'.esc_url( $is_image[0] ).'">
					</a>
				<div class="date"> <strong>'.esc_html( get_the_date('d') ).'</strong> <span>'.esc_html( get_the_date('M') ).'</span> </div>
			</div>';
		}
		$blog_type_div_starts = '';
		$blog_type_div_ends = '';	
		$wordsCount  = '150';
		if($latest_blog_type == "masonry")
		{
			$blog_type_div_starts = '<div id="posts-masonry" class="posts-masonry">';
			$blog_type_div_ends = '</div>';	
			$wordsCount  = '175';
		}	

		//latest_blog_sidebar
		
		$column = 12;
		$inner_columns = '4';
		$sidebar_right = '';
		$sidebar_left = '';
		$clearfix = 3;

		if($latest_blog_sidebar == '0')
		{
			$column = 12;
			$sidebar_right = '';
			$sidebar_left = '';
			$inner_columns = '4';
			$clearfix = 3;
		}
		else if($latest_blog_sidebar == '1')
		{
			$column = 8;
			$inner_columns = '6';
			$clearfix = 2;
			
		}
		else if($latest_blog_sidebar == '2')
		{
			$column = 8;
			$inner_columns = '6';
			$clearfix = 2;
			
		}
		//Sidebar ends		
		$show_meta_html = '';
		if($latest_admin_meta == 1)
		{
			$show_meta_html = '<div class="entry-footer">
			  <div class="post-meta">
				<div class="post-like"> 
					<i class="icon-calendar"></i> '.esc_html( get_the_date( get_option('date_format'), $service->ID ) ).' </div>
				<div class="post-comment"> 
					<i class="icon-chat"></i> 
					<a href="'.esc_html( get_the_permalink( $service->ID ) ).'">'. get_comments_number($service->ID) . '</a> 
				</div>
			  </div>
			</div>';
		}
		
		$loop .= '<div class="col-md-'.$inner_columns.' col-sm-12 col-xs-12">
		  <div class="news-box">
			'.$image_html.'
			<div class="news-detail">
			  <h2>
			  	<a href="'.esc_html( get_the_permalink( $service->ID ) ).'" title="'.esc_attr( get_the_title( $service->ID ) ).'">
			  		'.esc_html( get_the_title( $service->ID ) ).'
				</a>
			  </h2>
			  <p>'. logistic_pro_theme_words_count( ( $service->post_content ), $wordsCount) .'</p>
			 	'.$show_meta_html.'
			</div>
		  </div>
		</div>';

			if($count % $clearfix == 0){ $loop .=	 '<div class="clearfix"></div>';}
			$count++;	
		}
$header = '';			
if($latest_header == 1)
{
	$header = '<div class="main-heading text-center"><h2>'. esc_html( $latest_title ).'</h2><p>'.$latest_desc.'</p></div>';
}			


$return = '';

$return .=  '<section id="blog-post" class="custom-padding '.$latest_bg_color.'">
		  <div class="container"> 
			'.$header.'
			<div class="row">';
		
			if($latest_blog_sidebar == '2')
			{
				ob_start();
				dynamic_sidebar('sidebar-1');
				$return .=	'<div class="col-md-4 col-sm-12 col-xs-12 clearfix"> <div class="side-bar"> '.ob_get_contents().'</div></div>';
				ob_end_clean();
		  
			}
			
$return .=	'<div class="col-sm-12 col-xs-12 col-md-'.$column.' nopadding"> 
						'.$blog_type_div_starts.'
						'. $loop . '
						'.$blog_type_div_ends.'
					<div class="clearfix"></div>
			 </div>';
			if($latest_blog_sidebar == '1')
			{
				ob_start();
				dynamic_sidebar('sidebar-1');
				$return .= '<div class="col-md-4 col-sm-12 col-xs-12 clearfix"> <div class="side-bar"> '.ob_get_contents().'</div></div>';
				ob_end_clean();

			}

$return .=	 '</div>
		  </div>
		 
		</section>';
						
	return $return;

}
add_shortcode('select_latest', 'latest_shortcode');

?>