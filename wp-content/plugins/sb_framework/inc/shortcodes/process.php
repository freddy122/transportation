<?php
/* ------------------------------------------------ */
/* process Shortcode */
/* ------------------------------------------------ */
function sb_theme_process_func() {
	
 	
vc_map( array(
    "name" => __("Our Process", 'sb_framework'),
    "base" => "select_process",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(

			array(
				"group" => "Background/Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Show Header", 'sb_framework'),
                "param_name" => "process_header_option",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select the option you want to show the header or not.", 'sb_framework')
            ),			
			array(
				"group" => "Background/Layout",
                "type" => "textfield",
                "heading" => esc_html__("Header Title", 'sb_framework'),
                "param_name" => "process_header_title",
                "admin_label" => true,
                "value" => '',
                "std" => '',
				'dependency' => array(
					'element' => 'process_header_option',
					'value' => '1',
				),				
				
            ),	
			array(
				"group" => "Background/Layout",
                "type" => "textarea",
                "heading" => esc_html__("Header Description", 'sb_framework'),
                "param_name" => "process_header_desc",
                "admin_label" => true,
                "value" => '',
                "std" => '',
				'dependency' => array(
					'element' => 'process_header_option',
					'value' => '1',
				),				
				
            ),	

			array(
				"group" => "Background/Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Background Color", 'sb_framework'),
                "param_name" => "process_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Background' => '',
					'Gray' => 'gray',
					'White' => 'white',
					'Image' => 'image',
				),
                "std" => '',
                "description" => esc_html__("Select background color", 'sb_framework')
            ),			
			array(
               "group" => "Background/Layout",
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Background Image", 'sb_framework' ),
                "param_name" => "process_bg_image",
                "description" => __( "Upload background image.", 'sb_framework' ),
				'dependency' => array(
					'element' => 'process_bg_color',
					'value' => 'image',
				),				
				
            ),			
											

			 array(
				"group" => 'Block 1',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'process_icons1',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),					
			array(
				"group" => "Block 1",
                "type" => "textfield",
                "heading" => esc_html__("Process Step", 'sb_framework'),
                "param_name" => "process_step1",
                "admin_label" => true,
                "value" => '',
                "std" => 'Step: 1',
            ),
			array(
				"group" => "Block 1",
                "type" => "textfield",
                "heading" => esc_html__("Description", 'sb_framework'),
                "param_name" => "process_desc1",
                "admin_label" => true,
                "value" => '',
                "std" => 'Create Your Account',
            ),

			 array(
				"group" => 'Block 2',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'process_icons2',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),					
				
			array(
				"group" => "Block 2",
                "type" => "textfield",
                "heading" => esc_html__("Process Step", 'sb_framework'),
                "param_name" => "process_step2",
                "admin_label" => true,
                "value" => '',
                "std" => 'Step: 2',
            ),
			array(
				"group" => "Block 2",
                "type" => "textfield",
                "heading" => esc_html__("Description", 'sb_framework'),
                "param_name" => "process_desc2",
                "admin_label" => true,
                "value" => '',
                "std" => 'Place Your Order',
            ),		
			
			 array(
				"group" => 'Block 3',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'process_icons3',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, // default 100, how many icons per/page to display
				 ),
			 ),	
			 				
			array(
				"group" => "Block 3",
                "type" => "textfield",
                "heading" => esc_html__("Process Step", 'sb_framework'),
                "param_name" => "process_step3",
                "admin_label" => true,
                "value" => '',
                "std" => 'Step: 3',
            ),
			array(
				"group" => "Block 3",
                "type" => "textfield",
                "heading" => esc_html__("Description", 'sb_framework'),
                "param_name" => "process_desc3",
                "admin_label" => true,
                "value" => '',
                "std" => 'We Collect It',
            ),	

			 array(
				"group" => 'Block 4',
				 'type' => 'iconpicker',
				 'heading' => __( 'Select Icon', 'js_composer' ),
				 'param_name' => 'process_icons4',
				 'settings' => array(
					 'emptyIcon' => false,
					 'type' => 'sb_theme_flaticon',
					 'iconsPerPage' => 100, 
				 ),
			 ),					
			array(
				"group" => "Block 4",
                "type" => "textfield",
                "heading" => esc_html__("Process Step", 'sb_framework'),
                "param_name" => "process_step4",
                "admin_label" => true,
                "value" => '',
                "std" => 'Step: 4',
            ),
			array(
				"group" => "Block 4",
                "type" => "textfield",
                "heading" => esc_html__("Description", 'sb_framework'),
                "param_name" => "process_desc4",
                "admin_label" => true,
                "value" => '',
                "std" => 'Delivered',
            ),
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_process_func' );

function process_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
	 
	 	  'process_header_option' => '',
	 	  'process_header_title'  => '',
		  'process_header_desc'  => '',
		  'process_bg_color' => '',
		  'process_bg_image' => '',
		  
		  'process_icons1' => 'flaticon-man-standing-with-delivery-box',
		  'process_step1' => 'Step: 1',
		  'process_desc1' => 'Create Your Account',

		  'process_icons2' => 'flaticon-commercial-delivery-symbol-of-a-list-on-clipboard-on-a-box-package',
		  'process_step2' => 'Step: 2',
		  'process_desc2' => 'Place Your Order',

		  'process_icons3' => 'fflaticon-delivery-worker-giving-a-box-to-a-receiver',
		  'process_step3' => 'Step: 3',
		  'process_desc3' => 'We Collect It',

		  'process_icons4' => 'flaticon-logistics-delivery-truck-in-movement',
		  'process_step4' => 'Step: 4',
		  'process_desc4' => 'Delivered',
		  				   
	   ), $atts));
		
		$image = wp_get_attachment_image_src( $process_bg_image, 'full' );
		$style = '';
		$bg_color = '';
		if( $process_bg_color != "image" )
		{
			$bg_color = $process_bg_color;
		}
		else
		{
			if($image[0] != "")
			{
				$style = 'style="background: rgba(0, 0, 0, 0) url('.esc_url($image[0]).') repeat scroll center top / cover;"';
			}
		}

		// Header Settings 
		$header = '';
		
		if($process_header_option == 1)
		{
			$header = '<div class="main-heading text-center"><h2>'.esc_html( $process_header_title ).'</h2><p>'.esc_html( $process_header_desc ).'</p></div>';
		}
		
			return	'<section id="process" class="section-padding-80 '.$bg_color.'" '.$style.'>
					<div class="container">
					'.$header.'
					<div class="row">
						<div class="col-md-12 col-xs-12 com-sm-12 our-process process-steps text-left">
								<ul>
								  <li><span class="process-icon">
								  	<i class="'.esc_attr($process_icons1).'"></i></span>
									<div class="process-detail">
									  <h3>'.esc_html($process_step1).'</h3>
									  <h2>'.esc_html($process_desc1).'</h2>
									  <div class="clearboth"></div>
									</div>
								  </li>
								  <li><span class="process-icon">
								  	<i class="'.esc_attr($process_icons2).'"></i>
									</span>
									<div class="process-detail">
									  <h3>'.esc_html($process_step2).'</h3>
									  <h2>'.esc_html($process_desc2).'</h2>
									  <div class="clearboth"></div>
									</div>
								  </li>
								  <li><span class="process-icon">
								  	<i class="'.esc_attr($process_icons3).'"></i></span>
									<div class="process-detail">
									  <h3>'.esc_html($process_step3).'</h3>
									  <h2>'.esc_html($process_desc3).'</h2>
									  <div class="clearboth"></div>
									</div>
								  </li>
								  <li><span class="process-icon">
								  	<i class="'.esc_attr($process_icons4).'"></i></span>
									<div class="process-detail">
									  <h3>'.esc_html($process_step4).'</h3>
									  <h2>'.esc_html($process_desc4).'</h2>
									  <div class="clearboth"></div>
									</div>
								  </li>
								  <div class="clearfix"></div>
								</ul>
							  </div>	
							 </div>							
						</div>
					</section>';
	
						
}
add_shortcode('select_process', 'process_shortcode');

?>