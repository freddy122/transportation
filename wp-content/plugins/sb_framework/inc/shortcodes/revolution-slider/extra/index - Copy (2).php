<?php if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/* ------------------------------------------------ */
/* Homepage slider shortcode */
/* ------------------------------------------------ */


function revolution_slider_func() {
vc_map( array(
    "name" => __("Slider", 'sb_framework'),
    "base" => "homepage_slider",
    "as_parent" => array('only' => 'homepage_slider_block'),
    "content_element" => true,
    "show_settings_on_create" => true,
    "is_container" => true,
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	 "js_view" => 'VcColumnView',
	 "params" => array(

		array(
			"type"        => "dropdown",
			"heading"     => __("Slider Type", 'sb_framework' ),
			"param_name"  => "slider_type",
			"admin_label" => true,
			"value"       => array(
				'Standard'   => 'standard',
				'Hero'   => 'hero',
				'Carousel'   => 'carousel',
			),
			"description" => __("Select The Slider Type.", 'sb_framework' ),
		),
		
		array(
			"type"        => "dropdown",
			"heading"     => __("Slider Layout", 'sb_framework' ),
			"param_name"  => "slider_layout",
			"admin_label" => true,
			"value"       => array(
				'Auto'   => 'auto',
				'Full Width'   => 'fullwidth',
				'Full Screen'   => 'fullscreen',
			),
			"description" => __("Select The Slider Layout.", 'sb_framework' ),
		), // 
		array(
			"type"        => "dropdown",
			"heading"     => __("Dotted Overlay", 'sb_framework' ),
			"param_name"  => "dotted_overlay",
			"admin_label" => true,
			"value"       => array(
				'None'   => 'none',
				'2 X 2'      => 'twoxtwo',
				'3 X 3'      => 'threexthree',
				'2 X 2 White'      => 'twoxtwowhite',
				'3 X 3 White'		=> 'threexthreewhite',
			),
		),	
		array(
			"type"        => "textfield",
			"heading"     => __("Delay", 'sb_framework' ),
			"param_name"  => "slider_delay",
			"admin_label" => true,
			"value"       => '5000',
			"description" => __("enter delay in ms e.g. 5000 is 5 seconds", 'sb_framework' ),
		),				

		/* Navigation */					
		array(
		    "group" => __( 'Navigation', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Keyboard Navigation", 'sb_framework' ),
			"param_name"  => "keyboard_navigation",
			"admin_label" => true,
			"value"       => array(
				'Off'   => 'off',
				'ON'   => 'on',
			),
		),
		array(
		    "group" => __( 'Navigation', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Keyboard Direction", 'sb_framework' ),
			"param_name"  => "keyboard_direction",
			"admin_label" => true,
			"value"       => array(
				'Horizontal'   => 'horizontal',
			),
		),	
			
		array(
		    "group" => __( 'Navigation', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Mouse Scroll", 'sb_framework' ),
			"param_name"  => "mouse_scroll_navigation",
			"admin_label" => true,
			"value"       => array(
				'Off'   => 'off',
				'ON'   => 'on',
			),
		),	
		array(
		    "group" => __( 'Navigation', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("On Hover Stop", 'sb_framework' ),
			"param_name"  => "on_hover_stop",
			"admin_label" => true,
			"value"       => array(
				'Off'   => 'off',
				'ON'   => 'on',
			),
		),	
		array(
		    "group" => __( 'Navigation', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Touch Enabled", 'sb_framework' ),
			"param_name"  => "touchenabled",
			"admin_label" => true,
			"value"       => array(
				'Off'   => 'off',
				'ON'   => 'on',
			),
		),	
		/* Arrows */
		array(
		    "group" => __( 'Arrows', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Arrows Enabled", 'sb_framework' ),
			"param_name"  => "arrows_enabled",
			"admin_label" => true,
			"value"       => array(
				'True'   => 'true',
				'False'   => 'false',
			),
		),	
		array(
		    "group" => __( 'Arrows', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Arrows Style", 'sb_framework' ),
			"param_name"  => "arrows_style",
			"admin_label" => true,
			"value"       => array(
				'Gyges'   => 'gyges',
				'Hebe'   => 'hebe',
				'Hephaistos'   => 'hephaistos',
				'Hermes'   => 'hermes',
				'Hesperiden'   => 'hesperiden',
				'Metis'   => 'metis',
				'Persephone'   => 'persephone',
				'Uranus'   => 'uranus',
				'Zeus'   => 'zeus',
			),
		),	
		array(
		    "group" => __( 'Arrows', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Hide On Mobile", 'sb_framework' ),
			"param_name"  => "hide_on_mobile",
			"admin_label" => true,
			"value"       => array(
				'False'   => 'false',
				'True'   => 'true',
			),
		),	
		array(
		    "group" => __( 'Arrows', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Hide On Leave", 'sb_framework' ),
			"param_name"  => "hide_on_leave",
			"admin_label" => true,
			"value"       => array(
				'True'   => 'true',
				'False'   => 'false',
			),
		),	
		array(
		    "group" => __( 'Arrows', 'js_composer' ),		
			"type"        => "textfield",
			"heading"     => __("Hide Delay", 'sb_framework' ),
			"param_name"  => "hide_delay",
			"admin_label" => true,
			"value"       => '200',
			"description" => __("enter delay in ms e.g. 5000 is 5 seconds", 'sb_framework' ),
		),
		array(
		    "group" => __( 'Arrows', 'js_composer' ),		
			"type"        => "textfield",
			"heading"     => __("Hide Mobile Delay", 'sb_framework' ),
			"param_name"  => "hide_delay_mobile",
			"admin_label" => true,
			"value"       => '1200',
			"description" => __("enter delay in ms e.g. 5000 is 5 seconds", 'sb_framework' ),
		),				
		/* Bullets */
		array(
		    "group" => __( 'Bullets', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Enable Bullets", 'sb_framework' ),
			"param_name"  => "bullets_enable",
			"admin_label" => true,
			"value"       => array(
				'True'   => 'true',
				'False'   => 'false',
			),
		),	
		array(
		    "group" => __( 'Bullets', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Hide On Mobile", 'sb_framework' ),
			"param_name"  => "bullets_hide_onmobile",
			"admin_label" => true,
			"value"       => array(
				'True'   => 'true',
				'False'   => 'false',
			),
		),	
		array(
			"group" => __( 'Bullets', 'js_composer' ),
			"type"        => "textfield",
			"heading"     => __("Hide Under", 'sb_framework' ),
			"param_name"  => "bullets_hide_under",
			"admin_label" => true,
			"value"       => '800',
			"description" => __("Hide bullets under specific width", 'sb_framework' ),
		),				
		array(
		    "group" => __( 'Bullets', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Bullets Style", 'sb_framework' ),
			"param_name"  => "bullets_style",
			"admin_label" => true,
			"value"       => array(
				'Gyges'   => 'gyges',
				'Hebe'   => 'hebe',
				'Hephaistos'   => 'hephaistos',
				'Hermes'   => 'hermes',
				'Hesperiden'   => 'hesperiden',
				'Metis'   => 'metis',
				'Persephone'   => 'persephone',
				'Uranus'   => 'uranus',
				'Zeus'   => 'zeus',
			),
		),	
		array(
		    "group" => __( 'Bullets', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Hide On Leave", 'sb_framework' ),
			"param_name"  => "bullets_hide_onleave",
			"admin_label" => true,
			"value"       => array(
				'True'   => 'true',
				'False'   => 'false',
			),
		),	
		array(
		    "group" => __( 'Bullets', 'js_composer' ),
			"type"        => "dropdown",
			"heading"     => __("Hide On Leave", 'sb_framework' ),
			"param_name"  => "bullets_direction",
			"admin_label" => true,
			"value"       => array(
				'Horizontal'   => 'horizontal',
			),
		),	

	)
) );
vc_map( array(
    "name" => __("Add Slider Slides", 'sb_framework'),
    "base" => "homepage_slider_block",
    "content_element" => true,
    "as_child" => array('only' => 'homepage_slider'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
    "params" => array(
			array(
			  	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Slide Title", 'sb_framework' ),
			  "param_name" => "slide_title",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "Enter Title Here", 'sb_framework' )
			),
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Slide Subtitle", 'sb_framework' ),
			  "param_name" => "slide_subtitle",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "Enter Title Here", 'sb_framework' )
			),	
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Slide Button Text", 'sb_framework' ),
			  "param_name" => "slide_button_text",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "Enter Title Here", 'sb_framework' )
			),
			array(
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Slide Button Link", 'sb_framework' ),
			  "param_name" => "slide_button_link",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "Enter Title Here", 'sb_framework' )
			),		
			array(
                "type" => "attach_image",
                "holder" => "img",
                "class" => "",
                "heading" => __( "Slide Image", 'sb_framework' ),
                "param_name" => "image_url",
                "value" => __( "", 'sb_framework' ),
                "description" => __( "1349 X 675", 'sb_framework' )
            ),			
						
				
    )
) );

	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_homepage_slider extends WPBakeryShortCodesContainer {
		 }
	}
	
	if ( class_exists( 'WPBakeryShortCode' ) ) {
		class WPBakeryShortCode_homepage_slider_block extends WPBakeryShortCode {
		}
	}



}
add_action( 'vc_before_init', 'revolution_slider_func' );


function sb_themes_homepage_slider_header($atts, $content = '') {
	
	
   extract(shortcode_atts(array(
	'slider_type' => 'standard',
	'slider_layout' => 'auto',
	'dotted_overlay' => 'none',
	'slider_delay' => '5000',
	/* Navigation */
	'keyboard_navigation' => 'off',
	'keyboard_direction' => 'horizontal',
	'mouse_scroll_navigation' => 'off',
	'on_hover_stop' => 'off',
	'touchenabled' => 'off',
	/* Arrows */
	'arrows_style' => 'gyges',
	'arrows_enabled' => 'true',
	'hide_on_mobile' => 'false',
	'hide_on_leave' => 'true',
	'hide_delay' => '200',
	'hide_delay_mobile' => '1200',
	/* bullets */
	'bullets_enable' => 'true',
	'bullets_hide_onmobile' => 'true',
	'bullets_hide_under' => '800',
	'bullets_style' => 'hebe',
	'bullets_hide_onleave' => 'false',
	'bullets_direction' => 'horizontal',
   ), $atts));
/* Add Slider Assests CSS/JS */
$revFilePath = SB_PLUGIN_URL . 'inc/shortcodes/revolution-slider/';
/*  Revolution Slider */
wp_register_script( 'themepunch-tools', $revFilePath . 'js/jquery.themepunch.tools.min.js', false, false, true );
wp_register_script( 'themepunch-revolution', $revFilePath . 'js/jquery.themepunch.revolution.min.js', false, false, true );
wp_register_script( 'revolution-extension-actions', $revFilePath . 'js/extensions/revolution.extension.actions.min.js', false, false, true );
wp_register_script( 'revolution-extension-carousel', $revFilePath . 'js/extensions/revolution.extension.carousel.min.js', false, false, true );
wp_register_script( 'revolution-extension-kenburn', $revFilePath . 'js/extensions/revolution.extension.kenburn.min.js', false, false, true );
wp_register_script( 'revolution-extension-layeranimation', $revFilePath . 'js/extensions/revolution.extension.layeranimation.min.js', false, false, true );
wp_register_script( 'revolution-extension-migration', $revFilePath . 'js/extensions/revolution.extension.migration.min.js', false, false, true );
wp_register_script( 'revolution-extension-navigation', $revFilePath . 'js/extensions/revolution.extension.navigation.min.js', false, false, true );
wp_register_script( 'revolution-extension-parallax', $revFilePath . 'js/extensions/revolution.extension.parallax.min.js', false, false, true );
wp_register_script( 'revolution-extension-slideanims', $revFilePath . 'js/extensions/revolution.extension.slideanims.min.js', false, false, true );
wp_register_script( 'revolution-extension-video', $revFilePath . 'js/extensions/revolution.extension.video.min.js', false, false, true );
wp_enqueue_script( 'themepunch-tools' );
wp_enqueue_script( 'themepunch-revolution' );
wp_enqueue_script( 'revolution-extension-actions' );
wp_enqueue_script( 'revolution-extension-carousel' );
wp_enqueue_script( 'revolution-extension-kenburn' );
wp_enqueue_script( 'revolution-extension-layeranimation' );
wp_enqueue_script( 'revolution-extension-migration' );
wp_enqueue_script( 'revolution-extension-navigation' );
wp_enqueue_script( 'revolution-extension-parallax' );
wp_enqueue_script( 'revolution-extension-slideanims' );
wp_enqueue_script( 'revolution-extension-video' );
wp_enqueue_script( 'bootstrap-dropdownhover' );
wp_enqueue_script( 'sb-logistic-validator' );


wp_enqueue_style( 'revolution-settings', $revFilePath . 'css/settings.css' );
wp_enqueue_style( 'revolution-navigation', $revFilePath . 'css/navigation.css' );

    $return = '';
	$return .= '<script  type="text/javascript">
(function($) {
	  $(document).ready(function(e) {
		var revapi = $(".rev_slider").revolution({
		  sliderType:"'. $slider_type .'",
		  jsFileLocation: "'. SB_PLUGIN_URL . 'inc/shortcodes/revolution-slider/js/'.'",
		  sliderLayout: "'. $slider_layout .'",
		  dottedOverlay: "'. $dotted_overlay .'",
		  delay: '. $slider_delay .',
		  navigation: {
			  keyboardNavigation: "'. $keyboard_navigation .'",
			  keyboard_direction: "'. $keyboard_direction .'",
			  mouseScrollNavigation: "'. $mouse_scroll_navigation .'",
			  onHoverStop: "'. $on_hover_stop .'",
			  touch: {
				  touchenabled: "'. $touchenabled .'",
				  swipe_threshold: 75,
				  swipe_min_touches: 1,
				  swipe_direction: "horizontal",
				  drag_block_vertical: false
			  },
			  arrows: {
				  style: "'. $arrows_style .'",
				  enable: '. $arrows_enabled .',
				  hide_onmobile: '. $hide_on_mobile .',
				  hide_onleave: '. $hide_on_leave .',
				  hide_delay: '. $hide_delay .',
				  hide_delay_mobile: '. $hide_delay_mobile .',
				  tmp: \'\',
				  left: {
					  h_align: "left",
					  v_align: "center",
					  h_offset: 0,
					  v_offset: 0
				  },
				  right: {
					  h_align: "right",
					  v_align: "center",
					  h_offset: 0,
					  v_offset: 0
				  }
			  },
				bullets: {
				enable: '. $bullets_enable .',
				hide_onmobile: '. $bullets_hide_onmobile .',
				hide_under: '. $bullets_hide_under .',
				style: "'. $bullets_style .'",
				hide_onleave: '. $bullets_hide_onleave .',
				direction: "'. $bullets_direction .'",
				h_align: "center",
				v_align: "bottom",
				h_offset: 0,
				v_offset: 30,
				space: 5,
				tmp: \'<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>\'
			}
		  },
		  responsiveLevels: [1240, 1024, 778],
		  visibilityLevels: [1240, 1024, 778],
		  gridwidth: [1170, 1024, 778, 480],
		  gridheight: [620, 768, 960, 720],
		  lazyType: "none",
		  parallax: {
			type: "scroll",
			origo: "slidercenter",
			speed: 1000,
			levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
			type: "scroll",
		  },
		  shadow: 0,
		  spinner: "off",
		  stopLoop: "on",
		  stopAfterLoops: 0,
		  stopAtSlide: -1,
		  shuffle: "off",
		  autoHeight: "off",
		  fullScreenAutoWidth: "off",
		  fullScreenAlignForce: "off",
		  fullScreenOffsetContainer: "",
		  fullScreenOffset: "0",
		  hideThumbsOnMobile: "off",
		  hideSliderAtLimit: 0,
		  hideCaptionAtLimit: 0,
		  hideAllCaptionAtLilmit: 0,
		  debugMode: false,
		  fallbacks: {
			  simplifyAll: "off",
			  nextSlideOnWindowFocus: "off",
			  disableFocusListener: false,
		  }
		});
	  });
 })(jQuery);
     </script>';

	$return .= '<div class="rev_slider_wrapper"><div class="rev_slider" data-version="5.0"><ul>'. do_shortcode($content) .'</ul></div></div>';

	return $return;
}

add_shortcode('homepage_slider', 'sb_themes_homepage_slider_header');

function sb_theme_slider_body($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'slide_title' => '',
		  'slide_subtitle' => '',
		  'slide_is_popup' => '',
		  'slide_button_text' => '',
		  'slide_button_link' => '',
		  'image_url' => '',
	   ), $atts));
		
 			
			//$image_attributes = wp_get_attachment_image_src( $image_url, 'sb-benaam-main-slider' );
			
			
			return '<li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="'.trailingslashit( get_template_directory_uri() ).'images/slider/thumb-4.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description=""> 
			
        <img src="'.trailingslashit( get_template_directory_uri() ).'images/slider/4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina> 
        <div class="tp-caption tp-resizeme text-white text-uppercase"  id="rs-3-layer-5" data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-125\']" data-fontsize="[\'26\']" data-lineheight="[\'64\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:700;">Ground Or Air </div>
				  
		<div class="tp-caption tp-resizeme text-uppercase font-merry"  id="rs-3-layer-8" data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-60\']" data-fontsize="[\'40\']" data-lineheight="[\'84\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:600; "><span class="text-white">Providing Logistics Services</span>
        </div>
        
        <!-- LAYER NR. 3 -->
        <div class="tp-caption tp-resizeme text-white"  id="rs-3-layer-9"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'20\']" data-fontsize="[\'18\']" data-lineheight="[\'34\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-weight:300;">We provides always our best services for our clients and  always<br>
          try to achieve our client\'s trust and satisfaction. </div>
        
        <!-- LAYER NR. 4 -->
        <div class="tp-caption tp-resizeme"  id="rs-3-layer-10"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'100\']"
 data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1600"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg" href="#">View Services</a> </div>
      </li>';
						
}

add_shortcode('homepage_slider_block', 'sb_theme_slider_body');


?>