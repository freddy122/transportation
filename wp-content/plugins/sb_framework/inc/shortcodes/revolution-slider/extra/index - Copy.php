<?php if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }
/* ------------------------------------------------ */
/* Homepage slider shortcode */
/* ------------------------------------------------ */


function revolution_slider_func() {
vc_map( array(
    "name" => __("Slider", 'sb_framework'),
    "base" => "homepage_slider",
    "as_parent" => array('only' => 'homepage_slider_block'),
    "content_element" => true,
    "show_settings_on_create" => true,
    "is_container" => true,
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	 "js_view" => 'VcColumnView',
	 "params" => array(

		array(
			"type"        => "dropdown",
			"heading"     => __("Slider Type", 'sb_framework' ),
			"param_name"  => "sliderType",
			"admin_label" => true,
			"value"       => array(
				'Standered'   => 'standard',
				'Hero'   => 'hero',
				'Carousel'   => 'carousel',
			),
			"description" => __("Select The Slider Type.", 'sb_framework' ),
		),
		
		array(
			"type"        => "dropdown",
			"heading"     => __("Slider Layout", 'sb_framework' ),
			"param_name"  => "sliderLayout",
			"admin_label" => true,
			"value"       => array(
				'Auto'   => 'auto',
				'Full Width'   => 'fullwidth',
				'Full Screen'   => 'fullscreen',
			),
			"description" => __("Select The Slider Layout.", 'sb_framework' ),
		), // 
		array(
			"type"        => "dropdown",
			"heading"     => __("Dotted Overlay", 'sb_framework' ),
			"param_name"  => "dottedOverlay",
			"admin_label" => true,
			"value"       => array(
				'None'   => 'none',
			),
		),	
		array(
			"type"        => "textfield",
			"heading"     => __("Delay", 'sb_framework' ),
			"param_name"  => "sliderDelay",
			"admin_label" => true,
			"value"       => '5000',
			"description" => __("enter delay in ms", 'sb_framework' ),
		),				
				
	)
) );
 	/* 'group' => __( 'Design Options', 'js_composer' ), */
	if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
		class WPBakeryShortCode_homepage_slider extends WPBakeryShortCodesContainer {
		 }
	}

}
add_action( 'vc_before_init', 'revolution_slider_func' );


function sb_themes_homepage_slider_header($atts, $content = '') {
	
	
   extract(shortcode_atts(array(
	'sliderType' => 'standard',
	'sliderLayout' => 'auto',
	'dottedOverlay' => 'none',
	'sliderDelay' => '5000',
	
   ), $atts));

/* Add Slider Assests CSS/JS */
$revFilePath = SB_PLUGIN_URL . 'inc/shortcodes/revolution-slider/';
/*  Revolution Slider */
wp_register_script( 'themepunch-tools', $revFilePath . 'js/jquery.themepunch.tools.min.js', false, false, true );
wp_register_script( 'themepunch-revolution', $revFilePath . 'js/jquery.themepunch.revolution.min.js', false, false, true );
wp_register_script( 'revolution-extension-actions', $revFilePath . 'js/extensions/revolution.extension.actions.min.js', false, false, true );
wp_register_script( 'revolution-extension-carousel', $revFilePath . 'js/extensions/revolution.extension.carousel.min.js', false, false, true );
wp_register_script( 'revolution-extension-kenburn', $revFilePath . 'js/extensions/revolution.extension.kenburn.min.js', false, false, true );
wp_register_script( 'revolution-extension-layeranimation', $revFilePath . 'js/extensions/revolution.extension.layeranimation.min.js', false, false, true );
wp_register_script( 'revolution-extension-migration', $revFilePath . 'js/extensions/revolution.extension.migration.min.js', false, false, true );
wp_register_script( 'revolution-extension-navigation', $revFilePath . 'js/extensions/revolution.extension.navigation.min.js', false, false, true );
wp_register_script( 'revolution-extension-parallax', $revFilePath . 'js/extensions/revolution.extension.parallax.min.js', false, false, true );
wp_register_script( 'revolution-extension-slideanims', $revFilePath . 'js/extensions/revolution.extension.slideanims.min.js', false, false, true );
wp_register_script( 'revolution-extension-video', $revFilePath . 'js/extensions/revolution.extension.video.min.js', false, false, true );
wp_enqueue_script( 'themepunch-tools' );
wp_enqueue_script( 'themepunch-revolution' );
wp_enqueue_script( 'revolution-extension-actions' );
wp_enqueue_script( 'revolution-extension-carousel' );
wp_enqueue_script( 'revolution-extension-kenburn' );
wp_enqueue_script( 'revolution-extension-layeranimation' );
wp_enqueue_script( 'revolution-extension-migration' );
wp_enqueue_script( 'revolution-extension-navigation' );
wp_enqueue_script( 'revolution-extension-parallax' );
wp_enqueue_script( 'revolution-extension-slideanims' );
wp_enqueue_script( 'revolution-extension-video' );
wp_enqueue_script( 'bootstrap-dropdownhover' );
wp_enqueue_script( 'sb-logistic-validator' );


wp_enqueue_style( 'revolution-settings', $revFilePath . 'css/settings.css' );
wp_enqueue_style( 'revolution-navigation', $revFilePath . 'css/navigation.css' );

	function sb_theme_rev_slider_script(){
   extract(shortcode_atts(array(
	'sliderType' => 'standard',
	'sliderLayout' => 'auto',
	'dottedOverlay' => 'none',
	'sliderDelay' => '5000',
	
   ), $atts));
		
	?>
    <script  type="text/javascript">
		(function($) {
              $(document).ready(function(e) {
                var revapi = $(".rev_slider").revolution({
                  sliderType:"<?php echo $sliderType; ?>",
                  jsFileLocation: "<?php echo SB_PLUGIN_URL . 'inc/shortcodes/revolution-slider/js/';?>",
                  sliderLayout: "auto",
                  dottedOverlay: "none",
                  delay: 5000,
                  navigation: {
                      keyboardNavigation: "off",
                      keyboard_direction: "horizontal",
                      mouseScrollNavigation: "off",
                      onHoverStop: "off",
                      touch: {
                          touchenabled: "on",
                          swipe_threshold: 75,
                          swipe_min_touches: 1,
                          swipe_direction: "horizontal",
                          drag_block_vertical: false
                      },
                      arrows: {
                          style: "gyges",
                          enable: true,
                          hide_onmobile: false,
                          hide_onleave: true,
                          hide_delay: 200,
                          hide_delay_mobile: 1200,
                          tmp: '',
                          left: {
                              h_align: "left",
                              v_align: "center",
                              h_offset: 0,
                              v_offset: 0
                          },
                          right: {
                              h_align: "right",
                              v_align: "center",
                              h_offset: 0,
                              v_offset: 0
                          }
                      },
                      
                        bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 800,
                        style: "hebe",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                    }
                  },
                  responsiveLevels: [1240, 1024, 778],
                  visibilityLevels: [1240, 1024, 778],
                  gridwidth: [1170, 1024, 778, 480],
                  gridheight: [620, 768, 960, 720],
                  lazyType: "none",
                  parallax: {
					type: "scroll",
					origo: "slidercenter",
					speed: 1000,
					levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
					type: "scroll",
                  },
                  shadow: 0,
                  spinner: "off",
                  stopLoop: "on",
                  stopAfterLoops: 0,
                  stopAtSlide: -1,
                  shuffle: "off",
                  autoHeight: "off",
                  fullScreenAutoWidth: "off",
                  fullScreenAlignForce: "off",
                  fullScreenOffsetContainer: "",
                  fullScreenOffset: "0",
                  hideThumbsOnMobile: "off",
                  hideSliderAtLimit: 0,
                  hideCaptionAtLimit: 0,
                  hideAllCaptionAtLilmit: 0,
                  debugMode: false,
                  fallbacks: {
                      simplifyAll: "off",
                      nextSlideOnWindowFocus: "off",
                      disableFocusListener: false,
                  }
                });
              });
		 })(jQuery);
     </script>
    <?php	
}
add_action('wp_footer', 'sb_theme_rev_slider_script');



	return '<div class="rev_slider_wrapper"><div class="rev_slider" data-version="5.0"><ul><li data-index="rs-4" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="'.trailingslashit( get_template_directory_uri() ).'images/slider/thumb-1.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description="">
        <img src="'.trailingslashit( get_template_directory_uri() ).'images/slider/1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina> 
        <div class="tp-caption tp-resizeme text-white text-uppercase"  id="rs-3-layer-1"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-125\']" data-fontsize="[\'26\']" data-lineheight="[\'64\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:700;">Ground or Air </div>
        <div class="tp-caption tp-resizeme text-uppercase text-white font-merry"  id="rs-3-layer-2"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-60\']"  data-fontsize="[\'40\']" data-lineheight="[\'84\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:600; "><span class="text-white">Providing Logistic Services </span> </div>
        <div class="tp-caption tp-resizeme text-white"  id="rs-3-layer-3"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'20\']" data-fontsize="[\'18\']" data-lineheight="[\'34\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-weight:300;">We provides always our best services for our clients and  always<br>
          try to achieve our client\'s trust and satisfaction. </div>
        <div class="tp-caption tp-resizeme"  id="rs-3-layer-4"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'100\']"
 data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1600"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; letter-spacing:1px;"> <a class="btn btn-colored btn-lg"  href="#">Contact Now</a> </div>
      </li>
      <!-- SLIDE 2 -->
      <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="'.trailingslashit( get_template_directory_uri() ).'images/slider/thumb-2.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description=""> 
        <!-- MAIN IMAGE --> 
        <img src="'.trailingslashit( get_template_directory_uri() ).'images/slider/2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina> 
        <div class="tp-caption tp-resizeme text-uppercase text-white "  id="rs-1-layer-1"
 data-x="[\'right\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-125\']" data-fontsize="[\'26\']" data-lineheight="[\'64\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:700;">We Provide Solutions </div>
        <div class="tp-caption tp-resizeme text-white font-merry  text-uppercase"  id="rs-1-layer-2"
 data-x="[\'right\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-60\']" data-fontsize="[\'40\']" data-lineheight="[\'84\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:700;"> Logistic Services </div>
        <div class="tp-caption tp-resizeme text-right text-white"  id="rs-1-layer-3"
 data-x="[\'right\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'30\']" data-fontsize="[\'18\']" data-lineheight="[\'34\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-weight:400;">We provides always our best services for our clients and  always<br>
          try to achieve our client\'s trust and satisfaction. </div>        
        <div class="tp-caption tp-resizeme"  id="rs-1-layer-4"
 data-x="[\'right\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'100\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1600"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg" href="#">Request Quote</a> </div>
      </li>
      <!-- SLIDE 3 -->
      <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="'.trailingslashit( get_template_directory_uri() ).'images/slider/thumb-3.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description=""> 
        <!-- MAIN IMAGE --> 
        <img src="'.trailingslashit( get_template_directory_uri() ).'images/slider/3.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina> 
        <div class="tp-caption tp-resizeme text-center text-white"  id="rs-2-layer-2"
 data-x="[\'center\']" data-hoffset="[\'0\']" data-y="[\'middle\']" data-voffset="[\'-60\']" data-fontsize="[\'26\']" data-lineheight="[\'64\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-weight:400;">- Providing Services Since 1970 - </div>
        <div class="tp-caption tp-resizeme text-uppercase text-white font-merry"  id="rs-2-layer-3"
 data-x="[\'center\']" data-hoffset="[\'0\']" data-y="[\'middle\']" data-voffset="[\'0\']" data-fontsize="[\'40\']" data-lineheight="[\'80\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-weight:700;">We Provide Solutions </div>
        <div class="tp-caption tp-resizeme"  id="rs-2-layer-4"
 data-x="[\'center\']" data-hoffset="[\'0\']" data-y="[\'middle\']" data-voffset="[\'80\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored  btn-lg" href="#">Contact Now</a> </div>
      </li>
      <!-- SLIDE 4 -->	  
      <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="'.trailingslashit( get_template_directory_uri() ).'images/slider/thumb-4.jpg" data-rotate="0" data-saveperformance="off" data-title="Web Show" data-description=""> 
        <img src="'.trailingslashit( get_template_directory_uri() ).'images/slider/4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="6" data-no-retina> 
        <div class="tp-caption tp-resizeme text-white text-uppercase"  id="rs-3-layer-5" data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-125\']" data-fontsize="[\'26\']" data-lineheight="[\'64\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:700;">Ground Or Air </div>
				  
		<div class="tp-caption tp-resizeme text-uppercase font-merry"  id="rs-3-layer-8" data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'-60\']" data-fontsize="[\'40\']" data-lineheight="[\'84\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1000"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on"  style="z-index: 5; white-space: nowrap; font-weight:600; "><span class="text-white">Providing Logistics Services</span>
        </div>
        
        <!-- LAYER NR. 3 -->
        <div class="tp-caption tp-resizeme text-white"  id="rs-3-layer-9"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'20\']" data-fontsize="[\'18\']" data-lineheight="[\'34\']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1400"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; font-weight:300;">We provides always our best services for our clients and  always<br>
          try to achieve our client\'s trust and satisfaction. </div>
        
        <!-- LAYER NR. 4 -->
        <div class="tp-caption tp-resizeme"  id="rs-3-layer-10"
 data-x="[\'left\']" data-hoffset="[\'30\']" data-y="[\'middle\']" data-voffset="[\'100\']"
 data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;s:500" data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;" data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-start="1600"  data-splitin="none"  data-splitout="none"  data-responsive_offset="on" style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg" href="#">View Services</a> </div>
      </li>
  
    </ul>
  </div>
  
  <!-- end .rev_slider --> 
</div>';


}

add_shortcode('homepage_slider', 'sb_themes_homepage_slider_header');

function sb_benaam_homepage_slider_body($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'slide_title' => '',
		  'slide_subtitle' => '',
		  'slide_is_popup' => '',
		  'slide_button_text' => '',
		  'slide_button_link' => '',
		  'image_url' => '',
	   ), $atts));
		
 			
			$image_attributes = wp_get_attachment_image_src( $image_url, 'sb-benaam-main-slider' );
			
			$link	=	'javascrip:void(0);';
			if( $slide_button_link != "" )
			{
				$link	=	esc_url( $slide_button_link );
			}
			if( $slide_button_text == "" )
			{
				$slide_button_text	=	esc_html__('Read more', 'sb_framework' );;
			}
			
			return '<div class="item">
    <div class="color-overlay"></div>
    <div class="slider-pic"><img src="'.esc_url($image_attributes[0]).'" alt="'.esc_html( $slide_title ).'"></div>
    <div class="container">
      <div class="slider-caption"><!-- slider caption -->
        <h1 class="slider-title">'.esc_html( $slide_title ).'</h1>
        <p class="slider-content">'.esc_html( $slide_subtitle ).'</p>
        <a href="'. $link .'" class="btn btn-default btn-white btn-lg"> '.esc_html( $slide_button_text ).'</a> </div>
      <!-- /.slider caption --> 
    </div>
  </div>
';
						
}

add_shortcode('homepage_slider_block', 'sb_benaam_homepage_slider_body');


?>