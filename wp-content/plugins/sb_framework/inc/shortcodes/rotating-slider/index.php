<?php
/* ------------------------------------------------ */
/* Hero Section */
/* ------------------------------------------------ */


function sb_hero_section_admin() {
vc_map( array(
    "name" => __("Hero Area", 'sb_framework'),
    "base" => "sb_hero_section",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
    "params" => array(
			array(
				"group" => "Background",
				"type" => "attach_image",
				"holder" => "img",
				"class" => "",
				"heading" => __( "Background Image", 'sb_framework' ),
				"param_name" => "sb_bg",
				"value" => __( '', 'sb_framework' ),
				"description" => __( "Upload background image 1200 X 800", 'sb_framework' )
			),
			 array(
			 	"group" => "Settings",
                "type" => "dropdown",
                "heading" => esc_html__("Select Type", 'sb_framework'),
                "param_name" => "sb_bg_style",
                "admin_label" => true,
                "value" => array(
                    'Select Style' => '',
                    'Parallax BG' => 'parallax-home',
                    'Text Rotator' => 'rotate',
                    'Static BG' => 'static-bg'
                ),
                "std" => '',
                "description" => esc_html__("", 'sb_framework')
            	),
				
			
			//For Text rotator  
			array(
			  "group" => "Settings",	
			  "type" => "dropdown",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Break Line", 'sb_framework' ),
			  "param_name" => "rotating_line_break",
                "value" => array(
                    'Select Option' => '',
                    'Yes' => 'yes',
                    'No' => 'no',
                ),
			  "description" => __( "Shift rotating text to next line", 'sb_framework' ),
				'dependency' => array(
					'element' => 'sb_bg_style',
					'value' => 'rotate',
				),				
			  
			),			
			array(
			  "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Fixed Text", 'sb_framework' ),
			  "param_name" => "fixed_text",
			  'edit_field_class' => 'vc_col-sm-4 vc_column',
			  "description" => __( "Text enter will remain fix.", 'sb_framework' ),
				'dependency' => array(
					'element' => 'sb_bg_style',
					'value' => 'rotate',
				),				
			  
			),	
			//
			array(
			  "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Rotating Text", 'sb_framework' ),
			  "param_name" => "rotating_text",
			  'edit_field_class' => 'vc_col-sm-8 vc_column',
			  "description" => __( "Text enter will rotate. Use , seprated text.", 'sb_framework' ),
				'dependency' => array(
					'element' => 'sb_bg_style',
					'value' => 'rotate',
				),				
			  
			),	
			array(
			  "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Tagline", 'sb_framework' ),
			  "param_name" => "rotating_subtitle",
			  "description" => __( "Enter tag line here", 'sb_framework' ),
				'dependency' => array(
					'element' => 'sb_bg_style',
					'value' => 'rotate',
				),				
			  
			),	
			/* Otther */
			array(
			 "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Title", 'sb_framework' ),
			  "param_name" => "sb_section_title",
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'sb_bg_style',
					'value' => array('parallax-home', 'static-bg'),
				),				
			  
			),				
					
			array(
			 "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Section Sub-Title", 'sb_framework' ),
			  "param_name" => "sb_section_sub_title",
			  "description" => __( "", 'sb_framework' ),
				'dependency' => array(
					'element' => 'sb_bg_style',
					'value' => array('parallax-home', 'static-bg'),
				),				
			  
			),				
			/* Other */

			array(
			  "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Button Title", 'sb_framework' ),
			  "param_name" => "sb_section_btn_title",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
			array(
			 "group" => "Settings",	
			  "type" => "textfield",
			  "holder" => "div",
			  "class" => "",
			  "heading" => __( "Button Link", 'sb_framework' ),
			  "param_name" => "sb_section_btn_link",
			  "value" => __( "", 'sb_framework' ),
			  "description" => __( "", 'sb_framework' )
			),			
		
			
		
		
	
				
    ),
) );

}
add_action( 'vc_before_init', 'sb_hero_section_admin' );

	function logistic_scripts_plugin()
	{
		$filesPath = SB_PLUGIN_URL . 'inc/shortcodes/rotating-slider/';	
		
		//wp_register_script( 'sb-themes-supersized-js', $filesPath . 'js/supersized.3.2.7.min.js', false, false, false );
		//wp_register_script( 'sb-themes-morphext-js', $filesPath . 'js/morphext.min.js', false, false, false );
		//wp_enqueue_script( 'sb-themes-morphext-js' );
		//wp_enqueue_script( 'sb-themes-supersized-js' );
		
		//wp_enqueue_style( 'sb-themes-morphext',$filesPath . 'css/morphext.css' );
		//wp_enqueue_style( 'sb-themes-supersized', $filesPath  . 'css/supersized.css' );
		

		
		
	}
	//add_action( 'wp_enqueue_scripts', 'logistic_scripts_plugin' );

function sb_hero_section_func($atts, $content = '') {
	
	
   extract(shortcode_atts(array(
   	  'sb_bg' => '',
	  'sb_bg_style' => 'parallax-home',
      'sb_section_title' => '',
	  'sb_section_sub_title' => '',
	  
	  'sb_section_btn_title' => '',
	  'sb_section_btn_link' => '',

	  'fixed_text' => '',
	  'rotating_text' => '',
	  'rotating_line_break' => 'yes',
	  'rotating_subtitle' => '',

   ), $atts));
	
	
		
	$img	=	'';
	if( $sb_bg != "" )
	{
		$img_array = wp_get_attachment_image_src( $sb_bg, 'full' );
		$img	=	$img_array[0];		
	}
	
	
	
	

	$btn	=	'';
	if( $sb_section_btn_title != "" )
	{
		$btn	=	   '<a class="btn page-scroll" href="'.esc_url( $sb_section_btn_link ).'">'.esc_html( $sb_section_btn_title ).'</a>';

	}
	
	$style	=	'<style>
		#home {
    background: transparent url("'.$img.'") no-repeat scroll 0 0 / cover;
    background-size: cover;
    -moz-background-size: cover;
    -ms-background-size: cover;
    -o-background-size: cover;
	height: 690px;
}
	</style>';
	if( $sb_bg_style == 'rotate' )
	{
	
	$linebreak ='';
	if($rotating_line_break == "yes"){$linebreak ='<br />';}
	$style .= '<div id="home" class="full-section parallax-home" data-stellar-background-ratio="0.5">
  <div class="slider-caption">
    <h1> '.esc_html( $fixed_text ).$linebreak.' <span id="js-rotating">'.esc_html( $rotating_text ).'</span></h1>
	 <p>'.esc_html( $rotating_subtitle ).'</p>
		'.$btn.'
  </div>
</div>';

return $style .= "<script type='text/javascript'> (function($) {  $('#js-rotating').Morphext({  animation: 'bounceIn',  separator: ',', speed: 4000, complete: function ()  { /* WoW */ } });   }); </script>";		
	}
	else
	{
		$data=	'';
		if( $sb_bg_style == 'parallax-home' )
			$data = 'data-stellar-background-ratio="0.5"';
	return
	$style . '
	<div id="home" class="full-section '.$sb_bg_style.'" '.$data.'>
  <div class="slider-caption">
    <h1>'.esc_html( $sb_section_title ).'</h1>
    <p>'.esc_html( $sb_section_sub_title ).'</p>
    '.$btn.'
  </div>
</div>
	';
	}
	
}

add_shortcode('sb_hero_section', 'sb_hero_section_func');



?>