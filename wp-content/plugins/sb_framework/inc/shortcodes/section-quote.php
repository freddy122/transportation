<?php

/* ------------------------------------------------ */
/* Section Quote Shortcode */
/* ------------------------------------------------ */

function sb_theme_quote_block_func() {
	  	
vc_map( array(
    "name" => __("Quote Section", 'sb_framework'),
    "base" => "select_quote_block",
	"as_parent" => array('only' => 'select_quote_block_area'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(

			array(
                "type" => "textfield",
                "heading" => esc_html__("Tagline", 'sb_framework'),
                "param_name" => "quote_main_tagline",
                "admin_label" => true,
                "value" => '',
                "std" => 'More about us',
                "description" => esc_html__("Enter tagline here.", 'sb_framework'),
            ),


			array(
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "quote_main_title",
                "admin_label" => true,
                "value" => '',
                "std" => 'Why People Choose Us',
                "description" => esc_html__("Enter title here.", 'sb_framework'),
            ),

			array(
                "type" => "textarea",
                "heading" => esc_html__("Short Description", 'sb_framework'),
                "param_name" => "quote_main_desc",
                "admin_label" => true,
                "value" => '',
                "std" => 'Ut consequat velit a metus accumsan, vel tempor nulla blandit. Integer euismod magna vel mi congue suscipit. Praesent quis facilisis neque. Donec scelerisque nibh vitae sapien ornare efficitur.',
                "description" => esc_html__("Enter short description here.", 'sb_framework'),
            ),

			array(
               "group" => "Background",			
                "type" => "dropdown",
                "heading" => esc_html__("Show BG Color", 'sb_framework'),
                "param_name" => "quote_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Background' => '',
					'White' => 'white',
					'Gray' => 'gray',
				),
                "std" => '',
                "description" => esc_html__("Select background color.", 'sb_framework')
            ),			
			array(
               "group" => "Form",			
                "type" => "dropdown",
                "heading" => esc_html__("Select Form Location", 'sb_framework'),
                "param_name" => "quote_form",
                "admin_label" => true,
                "value" => 
				array(
					'Select Form Side' => '',
					'Right' => 'right',
					'Left' => 'left',
				),
                "std" => '',
                "description" => esc_html__("Select where you want to show from on left or right side.", 'sb_framework')
            ),			
			array(
				"group" => "Form",		
                "type" => "textfield",
                "heading" => esc_html__("Form Title", 'sb_framework'),
                "param_name" => "quote_form_title",
                "admin_label" => true,
                "value" => '',
                "std" => 'REQUEST A QUOTE',
                "description" => esc_html__("Enter title here.", 'sb_framework'),
            ),

			array(
				"group" => "Form",		
                "type" => "textarea",
                "heading" => esc_html__("Form Short Description", 'sb_framework'),
                "param_name" => "quote_form_desc",
                "admin_label" => true,
                "value" => '',
                "std" => 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.',
                "description" => esc_html__("Enter form short description here.", 'sb_framework'),
            ),
			array(
				"group" => "Form",		
                "type" => "textfield",
                "heading" => esc_html__("Button Text", 'sb_framework'),
                "param_name" => "quote_form_button",
                "admin_label" => true,
                "value" => '',
                "std" => 'Submit Request',
                "description" => esc_html__("Enter button text here.", 'sb_framework'),
            ),
			//Group Starts	for adding Testimonials
			array
			(
				'group' => __( 'Add/Edit Facts', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Add/Edit Facts', 'sb_framework' ),
				'param_name' => 'qoute_facts_items',
				'value' => '',
				'params' => array
				(
					 array(
						"group" => "Add New",
						 'type' => 'iconpicker',
						 'heading' => __( 'Select Icon', 'js_composer' ),
						 'param_name' => 'quote_item_icons',
						 'settings' => array(
							 'emptyIcon' => false,
							 'type' => 'sb_theme_flaticon',
							 'iconsPerPage' => 100, // default 100, how many icons per/page to display
						 ),
					 ),					
					
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Title", 'sb_framework'),
						"param_name" => "quote_item_title",
						"admin_label" => true,
						"description" => esc_html__("Enter title here", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textarea",
						"heading" => esc_html__("Description", 'sb_framework'),
						"param_name" => "quote_item_desc",
						"admin_label" => true,
						"description" => esc_html__("Enter some description here.", 'sb_framework'),
					),
				)
			)			

	)
) );

}
add_action( 'vc_before_init', 'sb_theme_quote_block_func' );


function quoteblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		   'quote_main_tagline' => 'More about us.',
		   'quote_main_title' => 'Why People Choose Us.',
		   'quote_main_desc' => '',
		   'quote_bg_color' => 'white',	
		   'quote_form' => 'right',  
		   'quote_form_title' => 'REQUEST A QUOTE',
		   'quote_form_desc' => '',
		   'quote_form_button' => 'Submit Request',
		   'qoute_facts_items' => '', 
	   ), $atts));
		
		
		$typecargo = logistic_pro_cargo_types();
		$typrArrs =  wp_kses( $typecargo, logistic_pro_theme_required_tags() );
		$optionsTypes = '';
		if( count( $typrArrs ) > 0 )
		{
			foreach( $typrArrs as $typrArr)
			{
				$optionsTypes .= '<option value="'.$typrArr.'">'.$typrArr.'</option>';	
			}
		}
		//LOOP START
		$facts_items = vc_param_group_parse_atts( $atts['qoute_facts_items'] );	
		$fact_inner_html = '';
		if( count( $facts_items ) > 0 )
		{
			foreach($facts_items as $facts_item)
			{
				$icon = '';
				$title = '';
				$desc = '';
				
				if( isset($facts_item['quote_item_icons']) ){ $icon = $facts_item['quote_item_icons']; }
				if( isset($facts_item['quote_item_title']) ){ $title = $facts_item['quote_item_title']; }
				if( isset($facts_item['quote_item_desc']) ){ $desc = $facts_item['quote_item_desc']; }
				
				$fact_inner_html .= '<li><div class="choose-box"><span class="iconbox"><i class="'.esc_attr( $icon ).'"></i></span><div class="choose-box-content"><h4>'.esc_html( $title ).'</h4><p>'.esc_html( $desc ).'</p></div></div></li>';
				
			}	
		}
		//LOOP ENDS
		
		$options  = '';
		$countries = CountriesArray::get2d(null, array( 'name' ));
		foreach( $countries as $country)
		{		
			$options .= '<option value="'. esc_attr__( $country['name'] ) .'">'. esc_html__( $country['name'] ) .'</option>';				
		}
	
		
	$form_html = '<div class="col-md-5 col-sm-12 no-extra col-xs-12">
		<div class="quotation-box">
			<h2>'.$quote_form_title.'</h2>
			<div class="desc-text">
				<p>'.$quote_form_desc.'</p>
			</div>
			<form method="post" id="section-qoute">
				<div class="popup-msg-box"></div>
				<div class="row clearfix">
	
					<!--Form Group-->
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
						<input class="form-control" type="text" placeholder="'. esc_attr__('Your Name', 'sb_framework').'" value="" id="your-name" name="your_name" required >
					  </div>
					  <!--Form Group-->
					  <div class="form-group col-md-6 col-sm-6 col-xs-12">
						<input class="form-control" type="email" placeholder="'. esc_attr__('Email Address', 'sb_framework') .'" value="" id="your-email" name="your_email" required >
					  </div>
					  <!--Form Group-->
					  <div class="form-group col-md-12 col-sm-12 col-xs-12">
						<input class="form-control" type="text" placeholder="'. esc_attr__('Subject', 'sb_framework'). '" value="" id="your-subject" name="your_subject" required >
					  </div>
					  
					  
					  <div class="form-group col-md-12 col-sm-12 col-xs-12">
					  <select class="form-control" id="select-cargo-type" name="select_cargo_type" required >
							'.$optionsTypes.'
						</select>
						</div>
					  
				
					  <!--Form Group-->
					  <div class="form-group col-md-6 col-sm-12 col-xs-12">
						<select class="form-control" id="select-location" name="select_location" required >
						  <option value="">'. esc_attr__('Select Your Location', 'sb_framework') .'</option>
							'.$options.'
						</select>
					  </div>
					  
					  <!--Form Group-->
					  <div class="form-group col-md-6 col-sm-12 col-xs-12">
						<select class="form-control" id="select-rlocation" name="select_rlocation" required >
						  <option value="">'. esc_attr__('Select Destination', 'sb_framework') .'</option>
							'.$options.'
						</select>
					  </div>
					  					  
					  <!--Form Group-->
					  <div class="form-group col-md-12 col-sm-12 col-xs-12">
						<textarea class="form-control" rows="7" cols="20" placeholder="'. esc_attr__('Your Message', 'sb_framework') .'" id="your-message" name="your_message" required ></textarea>
					  </div>
	
					<!--Form Group-->
					<div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
  <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i>'.esc_html__("Requesting...", "sb_framework").'</a>
 
						<a class="custom-button light" id="section-qoute-button">'.$quote_form_button.'</a> 
					</div>
				</div>
			</form>
		</div>
	</div>';
		
		$form_html_left = '';
		$form_html_right = $form_html;
		if($quote_form == 'left' )
		{
			$form_html_left = $form_html;
			$form_html_right = '';
		}
		
		return '<section class="quote '.$quote_bg_color.'" id="quote">
        <div class="container">
            <div class="row clearfix">
				'.$form_html_left.'
                <div class="col-md-7 col-sm-12 col-xs-12 ">
                    <div class="choose-title">
                        <h3>'.esc_html($quote_main_tagline).'</h3>
                        <h2>'.esc_html($quote_main_title).'</h2>
                        <p>'.esc_html($quote_main_desc).'</p>
                    </div>
                    <div class="choose-services">
                        <ul class="choose-list">
                    '. $fact_inner_html .'
					</ul>
						</div>
					</div>
						'.$form_html_right.'
					</div>
				</div>
			</section>
';
}
add_shortcode('select_quote_block', 'quoteblock_shortcode');