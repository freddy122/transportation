<?php

/* ------------------------------------------------ */
/* Services Shortcode */
/* ------------------------------------------------ */



function sb_theme_services_block_func() {
	  	
vc_map( array(
    "name" => __("Services Blocks", 'sb_framework'),
    "base" => "select_services_block",
	"as_parent" => array('only' => 'select_services_block_area, select_services_cricle_area'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
			array(
                "type" => "dropdown",
                "heading" => esc_html__("Select Layout", 'sb_framework'),
                "param_name" => "services_layout",
                "admin_label" => true,
                "value" => 
				array(
					'Select Layout' => '',
					'Block Layout With Slider' => '1',
					'Block Layout With Out Slider' => '2',
					'Circle Layout' => '3',
				),
                "description" => esc_html__("Select the layout of the section.", 'sb_framework')
            ),				
			
			array(
                "type" => "dropdown",
                "heading" => esc_html__("Select Header", 'sb_framework'),
                "param_name" => "services_header",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select the option you want to show the header or not.", 'sb_framework')
            ),			
			array(
                "type" => "textfield",
                "heading" => esc_html__("Services Title", 'sb_framework'),
                "param_name" => "services_title",
                "admin_label" => true,
                "description" => esc_html__("Enter main title here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'services_header',
					'value' => '1',
				),				
            ),
			array(
                "type" => "textarea",
                "heading" => esc_html__("Services Description", 'sb_framework'),
                "param_name" => "services_desc",
                "admin_label" => true,
                "description" => esc_html__("Enter some description here.", 'sb_framework'),
				'dependency' => array(
					'element' => 'services_header',
					'value' => '1',
				),				
            ),		
			//
			array(
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Services Image", 'sb_framework' ),
                "param_name" => "services_block_image",
                "description" => __( "Upload background image or select color below.", 'sb_framework' ),
				
            ),	

			array(
                "type" => "dropdown",
                "heading" => esc_html__("Select Background Color", 'sb_framework'),
                "param_name" => "services_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Background Color' => '',
					'White' => 'white',
					'Gray' => 'gray',
				),
                "std" => '',
                "description" => esc_html__("Select background color or upload image above", 'sb_framework'),				
            ),	
			
			
			//Group Starts	for adding Testimonials
			array
			(
				'group' => __( 'Add/Edit Services', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Add/Edit Services', 'sb_framework' ),
				'param_name' => 'services_items',
				'value' => '',
				'params' => array
				(
					 array(
					 	"group" => "Add New",
						 'type' => 'iconpicker',
						 'heading' => __( 'Services Icons', 'js_composer' ),
						 'param_name' => 'services_icons',
						 'settings' => array(
						 	 'value' => 'all',
							 'emptyIcon' => false,
							 'type' => 'sb_theme_flaticon',
							 'iconsPerPage' => 100, // default 100, how many icons per/page to display
						 ),
					 ),


					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Service Title", 'sb_framework'),
						"param_name" => "block_service_title",
						"admin_label" => true,
						"description" => esc_html__("Enter service block title here", 'sb_framework'),
					),				
					array(
						"group" => "Add New",
						"type" => "textarea",
						"heading" => esc_html__("Service Description", 'sb_framework'),
						"param_name" => "block_service_desc",
						"admin_label" => true,
						"description" => esc_html__("Enter some service block description here", 'sb_framework'),
					),				
				
				
				)
			)					



														
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_services_block_func' );


function servicesblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		'services_layout' => '1',
		'services_header' => '1',
		'services_title' => '',
		'services_desc' => '',
		'services_block_image' => '',	
		'services_bg_color' => '' ,
		'services_items' => '',  
	   ), $atts));
		

		// Image settings
		$image = wp_get_attachment_image_src( $services_block_image );
		$bgImg = '';
		if($image[0] != ""){
			$bgImg = 'style="background: rgba(240, 240, 240, 1) url('.$image[0].') repeat scroll 0 0 / cover ;"';
		}

		// Header Settings 
		$header = '';
		if($services_header == 1)
		{
			$header = '<div class="main-heading text-center"><h2>'.$services_title.'</h2><p>'.$services_desc.'</p></div>';
		}
		
		// Loop starts
		$service_items = vc_param_group_parse_atts( $atts['services_items'] );	
		$inner_services = '';
		if( count( $service_items > 0 ) )
		{
			foreach($service_items as $service_item)
			{
				$title = '';
				$icons = '';
				$desc = '';

				if( isset( $service_item['block_service_title'] )){ $title = esc_html( $service_item['block_service_title'] ); }
				if( isset( $service_item['services_icons'] )){ $icons = esc_html( $service_item['services_icons'] ); }
				if( isset( $service_item['block_service_desc'] )){ $desc = '<p>'. esc_html( $service_item['block_service_desc'] ).'</p>'; }
								
				  $div_starts = '';
				  $div_ends = '';
				  $class = 'item';
				  if($services_layout == 1)
				  {
					  $div_starts = '<div class="services-grid">';
					  $div_ends = '</div>';
					  $class = 'item';
					  
				  }
				  else if($services_layout == 2)
				  {
					  $div_starts = '<div class="services-grid">';
					  $div_ends = '</div>';
					  $class = 'col-md-4 col-xs-12 col-sm-6';
					  
				  }  
				  elseif($services_layout == 3)
				  {
					$class = 'col-md-4 col-xs-12 col-sm-6  ';
					$div_starts = '<div class="services-box-2 text-center">';
					$div_ends = '</div>';  
				  }
					$inner_services .=  '<div class="'.$class.'">
								'.$div_starts.'
								<div class="icons"> <i class="'. esc_attr( $icons ).'"></i></div>
								<h4>'. esc_html( $title ).'</h4> '. $desc .'
								'.$div_ends.'
							</div>';		
			}
		}
		// Loop ends
		
		
		
		
		//div changes
		$sectionId = 'services';
		$sectionClass = '';
		$mainClass = "custom-padding $services_bg_color";
		if($services_layout == 1)
		{
			$sectionId = 'services';
			$mainClass = "custom-padding $services_bg_color";
		}
		else if($services_layout == 2)
		{
			$sectionId = 'services-2';
			$mainClass = "custom-padding $services_bg_color";
		}
		else if($services_layout == 3)
		{
			$bgImg = '';
			$mainClass = "custom-padding services-2 $services_bg_color";
			$sectionClass = 'class="col-sm-12 col-xs-12 col-md-12"';
			$sectionId    = 'services-block';
		}

		return '<section class="'.$mainClass.'" '.$bgImg.'>
        <div class="container">
			'. $header .'
            <div class="row">
                <div id="'.$sectionId.'" '.$sectionClass.'>
                    '. $inner_services .'
                </div>
            </div>
        </div>
    </section>';
			
						
}
add_shortcode('select_services_block', 'servicesblock_shortcode');
