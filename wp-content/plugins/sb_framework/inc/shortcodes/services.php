<?php

/* ------------------------------------------------ */
/* Services Shortcode */
/* ------------------------------------------------ */



function sb_theme_services_func() {
	
$taxonomies = array('type');
$args = array(
    'orderby'                => 'name',
    'order'                  => 'desc',
    'hide_empty'             => false,
    'pad_counts'             => false,
); 

$terms = get_terms($taxonomies, $args);
$ar_type[] = 'Select All';
foreach( $terms as $term )
{
	$count = 'count';		 
	$ar_type[$term->term_id] = $term->name .' ('.$term->$count.')';
}
  	
vc_map( array(
    "name" => __("Services Posts", 'sb_framework'),
    "base" => "select_services",
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
			array(
				"group" => "Header",
                "type" => "dropdown",
                "heading" => esc_html__("Select Header", 'sb_framework'),
                "param_name" => "services_header",
                "admin_label" => true,
                "value" => 
				array(
					'Yes' => '1',
					'No' => '2',
				),
                "std" => '',
                "description" => esc_html__("Select numbers of posts that you need you show.", 'sb_framework')
            ),	
			array(
				"group" => "Header",
                "type" => "textfield",
                "heading" => esc_html__("Services Title", 'sb_framework'),
                "param_name" => "services_title",
                "admin_label" => true,
                "value" => '',
                "std" => 'Our Services',
                "description" => esc_html__("Select numbers of posts that you need you show.", 'sb_framework'),
				'dependency' => array(
					'element' => 'services_header',
					'value' => '1',
				),				
            ),
			array(
				"group" => "Header",
                "type" => "textarea",
                "heading" => esc_html__("Services Title", 'sb_framework'),
                "param_name" => "services_desc",
                "admin_label" => true,
                "value" => '',
                "description" => esc_html__("Select numbers of posts that you need you show.", 'sb_framework'),
                "std" => 'Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit',
				'dependency' => array(
					'element' => 'services_header',
					'value' => '1',
				),				
            ),	
			// layout starts
			array(
				"group" => "Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Select Services Layout", 'sb_framework'),
                "param_name" => "serviceslayout",
                "admin_label" => true,
                "value" => 
				array(
					'Select Layout' => '1',
					'Homepage' => '1',
					'Full Width' => '2',
					'Right Sidebar' => '3',
					'Left Sidebar' => '4',
				),
                "std" => '1',
                "description" => esc_html__("Select numbers of posts that you need you show.", 'sb_framework')
            ),	
			
			array(
				"group" => "Layout",
                "type" => "dropdown",
                "heading" => esc_html__("Select Background Color", 'sb_framework'),
                "param_name" => "services_bg_color",
                "admin_label" => true,
                "value" => 
				array(
					'Select Backround Color' => '',
					'White' => 'white',
					'Gray' => 'gray',
				),
                "std" => '1',
                "description" => esc_html__("Select background color.", 'sb_framework')
            ),	

			
			array(
				"group" => "Layout",
                "type" => "textfield",
                "heading" => esc_html__("Services Button Text", 'sb_framework'),
                "param_name" => "services_button",
                "admin_label" => true,
                "value" => '',
                "std" => 'Read More',
                "description" => esc_html__("Enter Services Button Text Here", 'sb_framework'),
			
            ),
						
			/* Build Query */
			array(
				"group" => "Query",
                "type" => "dropdown",
                "heading" => esc_html__("Sellect No.Of Services", 'sb_framework'),
                "param_name" => "no_of_services",
                "admin_label" => true,
                "value" => range(-1, 100),
                "std" => '3',
                "description" => esc_html__("Select -1 to show all", 'sb_framework')
            ),	
			array(
				"group" => "Query",
                "type" => "dropdown",
                "heading" => esc_html__("Sellect Order By", 'sb_framework'),
                "param_name" => "orderby_services",
                "admin_label" => true,
                "value" => 
				array(
					"DESC" => "DESC",
					"ASC" => "ASC",
				),
                "std" => '3',
                "description" => esc_html__("Select -1 to show all", 'sb_framework')
            ),	
			array(
				"group" => "Query",
                "type" => "dropdown",
                "heading" => esc_html__("Sellect Categories", 'sb_framework'),
                "param_name" => "cats_services",
                "admin_label" => true,
                "value" => $ar_type,
                "std" => '3',
                "description" => esc_html__("Select category for which you want to display services.", 'sb_framework')
            ),	
								
	)
) );

}
add_action( 'vc_before_init', 'sb_theme_services_func' );

function services_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		  'services_header' => '1',
		  'services_title' => 'Our Services',
		  'services_desc' => 'Cras varius purus in tempus porttitor ut dapibus efficitur sagittis cras vitae lacus metus nunc vulputate facilisis nisi eu lobortis erat consequat ut. Aliquam et justo ante. Nam a cursus velit',
		  
		   'serviceslayout' => '1',
		   'services_bg_color' => 'white',
		   'services_button' => 'Read More',
		   'services_imageurl' => '',
		   'services_get_custom_query' => '',
		   
   		    /* Query Settings */
			'orderby_services' => 'DESC',
			'cats_services' => '0',
			'no_of_services' => '3',

		   
	   ), $atts));
		
		
		
		
		$div_sidebar = '';
		$div_main_starts = '';
		$div_main_ends  = '';
		$col = 4;
		$div_sidebar_l = ''; 
		$div_sidebar_r = '';
		if($serviceslayout == 3 || $serviceslayout == 4)
		{	
		$col = 6;
		
			
			$div_main_starts = '<div class="col-md-9 col-sm-12 col-xs-12">';
			$div_main_ends   = '</div>';
			
			if($serviceslayout == 3 )
			{
				$div_sidebar_r = logistic_pro_theme_services_bar('type');
			}
			else
			{
				$div_sidebar_l = logistic_pro_theme_services_bar('type');
			}
			
			
		}
		
			
		$all_posts	= '';
		$parameters = array(
		'orderby' => 'date',
		'order' => $orderby_services,
		'post_type' => 'services',
		'posts_per_page' =>  $no_of_services,
		);
		$services	=	get_posts( $parameters );
		
		$loop = '';
		foreach( $services as $service )
		{
			 
		$is_image	= wp_get_attachment_image_src( get_post_thumbnail_id( $service->ID ), 'logistic-pro-services-thumb' );
		$image_html = '';
		if($is_image[0] != "")
		{ 
			$image_html = '
				<div class="service-image"> 
					<a href="'. esc_html( get_the_permalink( $service->ID ) ).'">
						<img alt="'. esc_attr( get_the_title( $service->ID ) ).'" src="'.$is_image[0].'">
					</a> 
				</div>';
		}
		$loop .=	'<div class="col-sm-12 col-md-'.$col.' col-xs-12">
				  <div class="services-grid-1">
					'. $image_html .'
					<div class="services-text">
					  <h4><a href="'.esc_url( get_the_permalink( $service->ID ) ).'">'. esc_html( get_the_title( $service->ID ) ).'</a> </h4>
					  <p>'. logistic_pro_theme_words_count(  $service->post_content , 150) .'</p>
				</div>
					<div class="more-about"> 
						<a class="btn btn-primary btn-lg" href="'.esc_url( get_the_permalink( $service->ID ) ).'">
							'. esc_html( $services_button ) .' <i class="fa fa-chevron-circle-right"></i>
						</a> 
					</div>
				  </div>
				  <!-- end services-grid-1 --> 
				</div>';
			}
$header = '';			
if($services_header == 1)
{
	$header = '<div class="main-heading text-center"><h2>'. esc_html( $services_title ).'</h2><p>'.$services_desc.'</p></div>';
}			
	
// enquee css for background
wp_add_inline_style( 'sb-themes-static', '.set-bg-color {  }' );
			
return '<section class="custom-padding set-bg-color '.$services_bg_color.'" id="about-section-3">
		  <div class="container">
			<div class="main-boxes">
			  <div class="row">
					'. $div_sidebar_l.'
					'. $div_main_starts.'
					'. $header .'				
					'. $loop . '
					'. $div_main_ends.'
					'. $div_sidebar_r.'
				</div>
			</div>
		 </div>
		</section>
		';
	
						
}
add_shortcode('select_services', 'services_shortcode');

?>