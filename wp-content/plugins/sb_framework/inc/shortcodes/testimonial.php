<?php

/* ------------------------------------------------ */
/* Testimonial Shortcode */
/* ------------------------------------------------ */



function sb_theme_testimonial_block_func() {
	  	
vc_map( array(
    "name" => __("Our Testimonial", 'sb_framework'),
    "base" => "select_testimonial_block",
	"as_parent" => array('only' => 'select_testimonial_block_area'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
	
			array(
               "group" => "Background",			
                "type" => "dropdown",
                "heading" => esc_html__("Show Overlay", 'sb_framework'),
                "param_name" => "testimonial_overlay",
                "admin_label" => true,
                "value" => 
				array(
					'Select Layout' => '',
					'No' => '0',
					'Yes' => '1',
				),
                "std" => '',
                "description" => esc_html__("Show or hide overlay on image", 'sb_framework')
            ),			
			array(
               "group" => "Background",
                "holder" => "img",
                "class" => "",
				"type" => "attach_image",
                "heading" => __( "Background Image", 'sb_framework' ),
                "param_name" => "testimonial_bg_image",
                "description" => __( "Upload background image. If you don't upload then default image will show.", 'sb_framework' ),
            ),

			//Group Starts	for adding Testimonials
			array
			(
				'group' => __( 'Add Testimonial', 'sb_framework' ),
				'type' => 'param_group',
				'heading' => __( 'Add Testimonial', 'sb_framework' ),
				'param_name' => 'testimonial_items',
				'value' => '',
				'params' => array
				(
					array(
					   "group" => "Add New",
						"holder" => "img",
						"class" => "",
						"type" => "attach_image",
						"heading" => __( "Client Image", 'sb_framework' ),
						"param_name" => "testimonial_item_image",
						"description" => __( "Upload testimonial image.", 'sb_framework' ),
					),						
				
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Client Name", 'sb_framework'),
						"param_name" => "testimonial_item_name",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						"description" => esc_html__("Enter client name here.", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textfield",
						"heading" => esc_html__("Company Name", 'sb_framework'),
						"param_name" => "testimonial_type_name",
						"admin_label" => true,
						'edit_field_class' => 'vc_col-sm-6 vc_column',
						"description" => esc_html__("Enter company name here.", 'sb_framework'),
					),
					array(
						"group" => "Add New",
						"type" => "textarea",
						"heading" => esc_html__("Client Feedback", 'sb_framework'),
						"param_name" => "testimonial_textarea",
						"admin_label" => true,
						"description" => esc_html__("Enter feedback provided by the client.", 'sb_framework'),
					),
					
				)
			)
	)
) );


}
add_action( 'vc_before_init', 'sb_theme_testimonial_block_func' );


function testimonialblock_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(
		   'testimonial_overlay' => '1',
		   'testimonial_bg_image' => '',	
		   'testimonial_items' => '',   	   
	   ), $atts));
		
		$testimonial_items = vc_param_group_parse_atts( $atts['testimonial_items'] );
		
		$overlay = 'parallex';
		if( $testimonial_overlay == 0 ){ $overlay = ''; }
		
		$image_bg = wp_get_attachment_image_src( $testimonial_bg_image, 'full' );
		
		$style = '';
		if( $image_bg[0] != "")
		{
			$style = 'style="background: rgba(255, 255, 255, 0.9) url('.$image_bg[0].') repeat scroll center top / cover "';
		}
		$inner_html = '';
		//Loop Here
		if( count( $testimonial_items ) > 0 )
		{
			foreach($testimonial_items as $testimonial_item)
			{
				$name 		= '';
				$desc 		= '';				
				$profession = '';
				$image_html = '';

				if( isset( $testimonial_item['testimonial_item_name'] )){ $name = $testimonial_item['testimonial_item_name']; }
				if( isset( $testimonial_item['testimonial_type_name'] )){ $profession = $testimonial_item['testimonial_type_name']; }
				if( isset( $testimonial_item['testimonial_textarea'] )){ $desc = $testimonial_item['testimonial_textarea']; }
				if( isset( $testimonial_item['testimonial_item_image'] ))
				{
					$image_id = $testimonial_item['testimonial_item_image'];
					$image = wp_get_attachment_image_src( $image_id, 'logistic-pro-testimonial-thumb' );
					if($image[0] != "")
					{					
						$image_html = '<img src="'.$image[0].'" class="img-responsive" alt="'.esc_attr($name).'">';
					}
				}
					$inner_html .= '<div class="item">
						<div class="col-sm-12 col-md-12 col-xs-12 testimonial-grid text-center"> 
							'.$image_html.'
						  <p>'.esc_html( $desc ).'</p>
						  <div class="name">'.esc_html($name).'</div>
						  <div class="profession">'.esc_html($profession).'</div>
						</div>
					  </div>';
			}
		}
		//Loop Ends
		
		return '<section data-stellar-background-ratio="0" class="testimonial-bg '.$overlay.' section-padding text-center" '.$style.'>
				  <div class="container">
					<div id="testimonials">
                    '. $inner_html .'
					</div>
				  </div>
				</section>';
}
add_shortcode('select_testimonial_block', 'testimonialblock_shortcode');

?>