<?php
/* ------------------------------------------------ */
/* Our App Shortcode */
/* ------------------------------------------------ */
function logisticpro_track_shipment_func() {
vc_map( array(
    "name" => __("Track Shipment", 'sb_framework'),
    "base" => "track_shipment_block",
	"as_parent" => array('only' => 'track_shipment_block_area'),
	"category" => __( "Theme Shortcodes", 'sb_framework'),
	"content_element" => true,
	"show_settings_on_create" => true,
	"is_container" => true,
    "params" => array(
			
			array(
                "type" => "textfield",
                "heading" => esc_html__("Title", 'sb_framework'),
                "param_name" => "track_shipment_title",
                "admin_label" => true,
                "description" => esc_html__("Enter title here.", 'sb_framework'),
            ),
			array(
                "type" => "textarea",
                "heading" => esc_html__("Description", 'sb_framework'),
                "param_name" => "track_shipment_desc",
                "admin_label" => true,
                "value" => '',
                "description" => esc_html__("Enter main description here.", 'sb_framework'),
            ),	
			
			array(
				"holder" => "img",
				"class" => "",
				"type" => "attach_image",
				"heading" => __( "Upload Background Image", 'sb_framework' ),
				"param_name" => "track_shipment_bg_image",
				"description" => __( "Upload background image.", 'sb_framework' ),
			),
			array(
                "type" => "textfield",
                "heading" => esc_html__("Placeholder Text", 'sb_framework'),
                "param_name" => "track_shipment_placeholder",
                "admin_label" => true,
                "description" => esc_html__("Enter placeholder text.", 'sb_framework'),
            ),				
			array(
                "type" => "textfield",
                "heading" => esc_html__("Info Line", 'sb_framework'),
                "param_name" => "track_shipment_info_line",
                "admin_label" => true,
                "description" => esc_html__("Enter some info.", 'sb_framework'),
            ),				
													
																							
	)
) );

}
add_action( 'vc_before_init', 'logisticpro_track_shipment_func' );


function track_shipment_shortcode($atts, $content = '') {
	
	 extract(shortcode_atts(array(

		  'track_shipment_title' => '',
		  'track_shipment_desc' => '',
		  'track_shipment_bg_image' => '',
		  
		  'track_shipment_placeholder' => '',
		  'track_shipment_info_line' => '',
		  		  
		  
	   ), $atts));
		
		
		$image = wp_get_attachment_image_src( $track_shipment_bg_image, 'full' );
		
		$image_html = '';
		$style = '';
		if($image[0] != "")
		{
			$style = "style='background: #333333 url( $image[0] ) no-repeat scroll center center / cover ;'";
		}
		
		$img_html_left = '';
		$img_html_right = '';
			
		return '<div class="hero-3 full-section " id="banner" '. ( $style ).'>
				<div class="container">
				  <div class="row">
					<div class="col-md-7">
					  <h2>'. esc_html( $track_shipment_title ) .'</h2>
					  <p class="lead">'. esc_html( $track_shipment_desc ) .'</p>
					  
					  <div class="form">
						<form id="trackingForm" method="post"  >
							<span id="id-error" ></span>
						  <input class="form-control" name="shipment_id" id="shipment_id" placeholder="'. esc_attr( $track_shipment_placeholder ).'" type="text">
						  <button id="track-shipment-button" type="button">'. __("Track", "sb_framework") .'</button>
						  
						</form>
						<span class="declaration">'. esc_html( $track_shipment_info_line ).'</span>
					  </div>
					</div>
				  </div>
				  
				</div>
			  </div><div id="popup-content"></div>';
			
						
}
add_shortcode('track_shipment_block', 'track_shipment_shortcode');

?>