<?php


//Enquee css

function sb_themes_plugin_scripts()
{
	$cssFilesPath = SB_PLUGIN_URL . 'inc/vc_functions/icons/';
	
	wp_enqueue_style( 'sb-plugin-et-line-fonts', $cssFilesPath  . 'css/et-line-fonts.css' );
	wp_enqueue_style( 'sb-plugin-flaticon', $cssFilesPath  . 'css/flaticon.css' );
}
add_action( 'wp_enqueue_scripts', 'sb_themes_plugin_scripts' );

add_filter( 'vc_iconpicker-type-sb_theme_flaticon', 'vc_iconpicker_type_sb_theme_flaticon' );
function vc_iconpicker_type_sb_theme_flaticon( $icons ) {
	
	$flaticon['Logistic Icons']	=	
	array(
			array("flaticon-24-hours-delivery" => "24 Hours Delivery"),
			array("flaticon-24-hours-phone-service" => "24 Hours Phone Service"),
			array("flaticon-24-hours-symbol" => "24 Hours Symbol"),
			array("flaticon-air-transport" => "Air Transport"),
			array("flaticon-airplane-around-earth" => "Airplane Around Earth"),
			array("flaticon-airplane-frontal-view" => "Airplane Frontal View"),
			array("flaticon-airplane-in-vertical-ascending-position" => "Airplane In Vertical Ascending Position"),
			array("flaticon-barscode-with-a-magnifier-business-symbol" => "Barscode With A Magnifier Business Symbol"),
			array("flaticon-black-delivery-small-truck-side-view" => "Black Delivery Small Truck Side View"),
			array("flaticon-black-opened-umbrella-symbol-with-rain-drops-falling-on-it" => "Black Opened Umbrella Symbol With Rain Drops Falling On It"),
			array("flaticon-boat-from-front-view" => "Boat From Front View"),
			array("flaticon-box-of-packing-for-delivery" => "Box Of Packing For Delivery"),
			array("flaticon-boxes-piles-stored-inside-a-garage-for-delivery" => "Boxes Piles Stored Inside A Garage For Delivery"),
			array("flaticon-boxes-storage-for-delivery-inside-a-truck-box-from-back-view" => "Boxes Storage For Delivery Inside A Truck Box From Back View"),
			array("flaticon-call-center-service-for-information" => "Call Center Service For Information"),
			array("flaticon-call-center-worker-with-headset" => "Call Center Worker With Headset"),
			array("flaticon-chronometer" => "Chronometer"),
			array("flaticon-clipboard-verification-symbol" => "Clipboard Verification Symbol"),
			array("flaticon-commercial-delivery-symbol-of-a-list-on-clipboard-on-a-box-package" => "Commercial Delivery Symbol Of A List On Clipboard On A Box Package"),
			array("flaticon-container-hanging-of-a-crane" => "Container Hanging Of A Crane"),
			array("flaticon-container-on-a-crane" => "Container On A Crane"),
			array("flaticon-containers-on-oceanic-ship" => "Containers On Oceanic Ship"),
			array("flaticon-crane" => "Crane"),
			array("flaticon-crane-truck" => "Crane Truck"),
			array("flaticon-delivered-box-verification-symbol" => "Delivered Box Verification Symbol"),
			array("flaticon-delivery-box" => "Delivery Box"),
			array("flaticon-delivery-box-and-timer" => "Delivery Box And Timer"),
			array("flaticon-delivery-box-on-a-hand" => "Delivery Box On A Hand"),
			array("flaticon-delivery-box-package-opened-with-up-arrow" => "Delivery Box Package Opened With Up Arrow"),
			array("flaticon-delivery-cube-box-package-with-four-arrows-in-different-directions" => "Delivery Cube Box Package With Four Arrows In Different Directions"),
			array("flaticon-delivery-of-a-box" => "Delivery Of A Box"),
			array("flaticon-delivery-pack-security-symbol-with-a-shield" => "Delivery Pack Security Symbol With A Shield"),
			array("flaticon-delivery-package" => "Delivery Package"),
			array("flaticon-delivery-package-box-with-fragile-content-symbol-of-broken-glass" => "Delivery Package Box With Fragile Content Symbol Of Broken Glass"),
			array("flaticon-delivery-package-opened" => "Delivery Package Opened"),
			array("flaticon-delivery-package-with-umbrella-symbol" => "Delivery Package With Umbrella Symbol"),
			array("flaticon-delivery-packages-on-a-trolley" => "Delivery Packages On A Trolley"),
			array("flaticon-delivery-packaging-box" => "Delivery Packaging Box"),
			array("flaticon-delivery-packing" => "Delivery Packing"),
			array("flaticon-delivery-scale-with-a-box" => "Delivery Scale With A Box"),
			array("flaticon-delivery-time-symbol" => "Delivery Time Symbol"),
			array("flaticon-delivery-time-tool" => "Delivery Time Tool"),
			array("flaticon-delivery-transportation-machine" => "Delivery Transportation Machine"),
			array("flaticon-delivery-truck" => "Delivery Truck"),
			array("flaticon-delivery-truck-1" => "Delivery Truck 1"),
			array("flaticon-delivery-truck-2" => "Delivery Truck 2"),
			array("flaticon-delivery-truck-with-circular-clock" => "Delivery Truck With Circular Clock"),
			array("flaticon-delivery-truck-with-packages-behind" => "Delivery Truck With Packages Behind"),
			array("flaticon-delivery-worker-giving-a-box-to-a-receiver" => "Delivery Worker Giving A Box To A Receiver"),
			array("flaticon-fragile-broken-glass-symbol-for-delivery-boxes" => "Fragile Broken Glass Symbol For Delivery Boxes"),
			array("flaticon-free-delivery-truck" => "Free Delivery Truck"),
			array("flaticon-frontal-truck" => "Frontal Truck"),
			array("flaticon-identification-for-delivery-with-bars" => "Identification For Delivery With Bars"),
			array("flaticon-international-calling-service-symbol" => "International Calling Service Symbol"),
			array("flaticon-international-delivery" => "International Delivery"),
			array("flaticon-international-delivery-business-symbol-of-world-grid-with-an-arrow-around" => "International Delivery Business Symbol Of World Grid With An Arrow Around"),
			array("flaticon-international-delivery-symbol" => "International Delivery Symbol"),
			array("flaticon-international-logistics-delivery-truck-symbol-with-world-grid-behind" => "International Logistics Delivery Truck Symbol With World Grid Behind"),
			array("flaticon-localization-orientation-tool-of-compass-with-cardinal-points" => "Localization Orientation Tool Of Compass With Cardinal Points"),
			array("flaticon-locked-package" => "Locked Package"),
			array("flaticon-logistics-delivery-truck-and-clock" => "Logistics Delivery Truck And Clock"),
			array("flaticon-logistics-delivery-truck-in-movement" => "Logistics Delivery Truck In Movement"),
			array("flaticon-logistics-package" => "Logistics Package"),
			array("flaticon-logistics-transport" => "Logistics Transport"),
			array("flaticon-logistics-truck" => "Logistics Truck"),
			array("flaticon-logistics-weight-scale" => "Logistics Weight Scale"),
			array("flaticon-man-standing-with-delivery-box" => "Man Standing With Delivery Box"),
			array("flaticon-ocean-transportation" => "Ocean Transportation"),
			array("flaticon-package-cube-box-for-delivery" => "Package Cube Box For Delivery"),
			array("flaticon-package-delivery" => "Package Delivery"),
			array("flaticon-package-delivery-in-hand" => "Package Delivery In Hand"),
			array("flaticon-package-for-delivery" => "Package For Delivery"),
			array("flaticon-package-on-rolling-transport" => "Package On Rolling Transport"),
			array("flaticon-package-transport-for-delivery" => "Package Transport For Delivery"),
			array("flaticon-package-transportation-on-a-trolley" => "Package Transportation On A Trolley"),
			array("flaticon-packages-storage-for-delivery" => "Packages Storage For Delivery"),
			array("flaticon-packages-transportation-on-a-truck" => "Packages Transportation On A Truck"),
			array("flaticon-person-standing-beside-a-delivery-box" => "Person Standing Beside A Delivery Box"),
			array("flaticon-phone-auricular-and-a-clock" => "Phone Auricular And A Clock"),
			array("flaticon-phone-auricular-and-clock-delivery-symbol" => "Phone Auricular And Clock Delivery Symbol"),
			array("flaticon-placeholder-on-map-paper-in-perspective" => "Placeholder On Map Paper In Perspective"),
			array("flaticon-protection-symbol-of-opened-umbrella-silhouette-under-raindrops" => "Protection Symbol Of Opened Umbrella Silhouette Under Raindrops"),
			array("flaticon-sea-ship" => "Sea Ship"),
			array("flaticon-sea-ship-with-containers" => "Sea Ship With Containers"),
			array("flaticon-search-delivery-service-tool" => "Search Delivery Service Tool"),
			array("flaticon-storage" => "Storage"),
			array("flaticon-talking-by-phone-auricular-symbol-with-speech-bubble" => "Talking By Phone Auricular Symbol With Speech Bubble"),
			array("flaticon-telephone" => "Telephone"),
			array("flaticon-telephone-1" => "Telephone 1"),
			array("flaticon-three-stored-boxes-for-delivery" => "Three Stored Boxes For Delivery"),
			array("flaticon-train-front" => "Train Front"),
			array("flaticon-triangular-arrows-sign-for-recycle" => "Triangular Arrows Sign For Recycle"),
			array("flaticon-up-arrows-couple-sign-for-packaging" => "Up Arrows Couple Sign For Packaging"),
			array("flaticon-verification-of-delivery-list-clipboard-symbol" => "Verification Of Delivery List Clipboard Symbol"),
			array("flaticon-view-symbol-on-delivery-opened-box" => "View Symbol On Delivery Opened Box"),
			array("flaticon-weight-of-delivery-package-on-a-scale" => "Weight Of Delivery Package On A Scale"),
			array("flaticon-weight-tool" => "Weight Tool"),
			array("flaticon-woman-with-headset" => "Woman With Headset"),
			array("flaticon-wood-package-box-of-square-shape-for-delivery" => "Wood Package Box Of Square Shape For Delivery"),
			array("flaticon-world-grid-with-placeholder" => "World Grid With Placeholder"),
	);

	  return array_merge( $icons, $flaticon );
}


add_filter( 'vc_iconpicker-type-sb_theme_icons', 'vc_iconpicker_type_sb_theme_icons' );
function vc_iconpicker_type_sb_theme_icons( $icons ) {
	
	$icons['Logistic Et Line Icons']	=	
	array(
		array('icon-mobile' => 'Mobile' ),
	);
	  return array_merge( $icons, $icons );
}
