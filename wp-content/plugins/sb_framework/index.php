<?php 
 /**
 * Plugin Name: SB Framework
 * Plugin URI: http://scriptsbundle.com
 * Description: This plugin is essential for the proper theme funcationality.
 * Version: 1.1.3
 * Author: ScriptsBundle
 * Author URI: http://themeforest.net/user/scriptsbundle
 * License: GPL2
 * Text Domain: sb_framework 
 */
	define('SB_PLUGIN_FRAMEWORK_PATH', plugin_dir_path(__FILE__));	
		
	
	define('SB_PLUGIN_PATH', plugin_dir_path(__FILE__));	
	define('SB_PLUGIN_URL', plugin_dir_url(__FILE__));
	define( 'SB_THEMEURL_PLUGIN', get_template_directory_uri () . '/' );
	define( 'SB_IMAGES_PLUGIN', SB_THEMEURL_PLUGIN . 'images/');
	define( 'SB_CSS_PLUGIN', SB_THEMEURL_PLUGIN . 'css/');

	
	/* For Redux Framework */
	require SB_PLUGIN_FRAMEWORK_PATH . '/admin-init.php';

	/* For Theme Custom Post Types */
	require SB_PLUGIN_PATH . 'inc/cpt.php';

	/* For Theme Shortcodes */
	require SB_PLUGIN_PATH . 'inc/shortcodes.php';

	/* For Contact us */
	require SB_PLUGIN_PATH . 'contact/qoute-js.php'; /* qoute */
	//Only include if page template is Counact Us
	require SB_PLUGIN_PATH . 'contact/contact-js.php'; /* qoute */
	// track-shipment
	require SB_PLUGIN_PATH . 'contact/track-shipment.php'; /* track-shipment */
	
add_action( 'admin_enqueue_scripts', 'sb_framework_scripts' );
function sb_framework_scripts()
{
	//wp_enqueue_style( 'sb-benaam-plugin-css',  plugin_dir_url( __FILE__ ) . 'css/plugin.css' );
}

if( get_option( 'logistic' ) == "" )
{
$sb_option_name	=	'logistic';

// Basic Settings
Redux::setOption($sb_option_name, 'website-type', '0' );
Redux::setOption($sb_option_name, 'admin_bar', false );

Redux::setOption($sb_option_name, 'webite-loader', '1' );
Redux::setOption($sb_option_name, 'onepage-slides-settings', '0' );
Redux::setOption($sb_option_name, 'onepage-slides-text', array( 'fixed_text' => '', 'rotating_text' => '',  ) );
Redux::setOption($sb_option_name, 'onepage-slides-line-break', '0' );
Redux::setOption($sb_option_name, 'opt-multi-media-slides', '' );
Redux::setOption($sb_option_name, 'header-map-api-key', '' );
Redux::setOption($sb_option_name, 'onepage-move-to', '#one-page' );

Redux::setOption($sb_option_name, 'site-color-settings', '1' );
Redux::setOption($sb_option_name, 'site-color-change', 'defualt' );
Redux::setOption($sb_option_name, 'breadcrumb-bg-img', array( 'url' => SB_IMAGES_PLUGIN . 'Logistics-and-4PL.jpg' ) );


// Top Header
//color-scheme///
Redux::setOption($sb_option_name, 'top-header-color-scheme', '1' );
Redux::setOption($sb_option_name, 'top-header-switch-on', '1' );
Redux::setOption($sb_option_name, 'favicon', array( 'url' => SB_IMAGES_PLUGIN . 'favicon.ico' ) );
Redux::setOption($sb_option_name, 'top-header-text', 'Opening Hours : Monday to Saturday - 8am to 5pm' );
Redux::setOption($sb_option_name, 'top-header-social-media', array( 'facebook' => 'https://www.facebook.com/ScriptsBundle/?fref=ts', 'twitter' => '#',  'rss' => '', 'pinterest' => '', 'youtube' => '#', 'digg' => '', 'stumbleupon' => '', 'linkedin' => '#', 'google' => '', 'google-plus' => '#', ) );


// Header Options
Redux::setOption($sb_option_name, 'site-logo', array( 'url' => SB_IMAGES_PLUGIN . 'logo.png' ) );
Redux::setOption($sb_option_name, 'header-styles', '0' );
Redux::setOption($sb_option_name, 'show-hide-place-order-button-text', 'Booking' );
Redux::setOption($sb_option_name, 'show-hide-place-order-button', '0' );
Redux::setOption($sb_option_name, 'show-hide-qoute-button', '0' );
Redux::setOption($sb_option_name, 'header-switch-on', '1' );
Redux::setOption($sb_option_name, 'header-email-name', 'EMAIL' );
Redux::setOption($sb_option_name, 'header-email-value', 'email@yourdomain.com' );
Redux::setOption($sb_option_name, 'header-phone-name', 'Call Now' );
Redux::setOption($sb_option_name, 'header-phone-value', '(92) 123-456-78' );
Redux::setOption($sb_option_name, 'header-contact-name', 'Find Us' );
Redux::setOption($sb_option_name, 'header-contact-value', 'Model Town, Pakistan' );
Redux::setOption($sb_option_name, 'header-js-css', '' );

//contact/popup
 //popup
 
Redux::setOption($sb_option_name, 'qoute-title-text', 'REQUEST A QUOTE' );
Redux::setOption($sb_option_name, 'qoute-tagline-text', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.' );
Redux::setOption($sb_option_name, 'qoute-button-text', 'Submit Request' );
Redux::setOption($sb_option_name, 'qoute-mail-receive-on', 'youremail@domainname.com' );
Redux::setOption($sb_option_name, 'qoute-mail-receive-title', 'New Quotation Received' );
Redux::setOption($sb_option_name, 'qoute-message-template', 'Hi Admin,<br /><br />you have received a message from quote form, details are below.<br /><br />%name%<br />%subject%<br />%email%<br />%cargo_type%<br />%location%<br />%rlocation%<br />%message%' );
Redux::setOption($sb_option_name, 'qoute-success-message', 'Message sent, we will get back to you shortly.' );
Redux::setOption($sb_option_name, 'qoute-error-message', 'Some error occured while sending email. Please try shortly.' );

	// contact starts
Redux::setOption($sb_option_name, 'contact-breadcrumb-show', '0' );
Redux::setOption($sb_option_name, 'contact-map', '0' );
Redux::setOption($sb_option_name, 'contact-map-title', 'Logistic Pro' );
Redux::setOption($sb_option_name, 'contact-map-latitude', '31.5497' );
Redux::setOption($sb_option_name, 'contact-map-longitude', '74.3812' );
Redux::setOption($sb_option_name, 'contact-address-area', '0' );
Redux::setOption($sb_option_name, 'contact-address-settings', array('location' => 'Model Town Defense Lahore, Pakistan', 'phone' => '+92 - 0123-333-1245 +92 - 0123-333-1245', 'mailus' => 'support@company.com' ) );

Redux::setOption($sb_option_name, 'top-header-show-title', 0 );
	// contact form 
Redux::setOption($sb_option_name, 'contact-button-text', 'Contact Us' );
Redux::setOption($sb_option_name, 'contact-mail-receive-on', 'contact@domainname.com' );
Redux::setOption($sb_option_name, 'contact-mail-receive-title', 'New Contact Email Received' );
Redux::setOption($sb_option_name, 'contact-message-template', 'Hi Admin,<br /><br />you have received a message from contact us page, details are below.<br /><br />%name%<br />%email%<br />%subject%<br />%message%' );
Redux::setOption($sb_option_name, 'contact-success-message', 'Message sent, we will get back to you shortly.' );
Redux::setOption($sb_option_name, 'contact-error-message', 'Some error occured while sending email. Please try shortly.' );

	
// Top Footer Options
Redux::setOption($sb_option_name, 'site-footer-bg-img', array( 'url' => SB_IMAGES_PLUGIN . 'footer-bg.jpg' ) );
Redux::setOption($sb_option_name, 'top-footer-switch-on', '1' );
Redux::setOption($sb_option_name, 'site-footer-logo', array( 'url' => SB_IMAGES_PLUGIN . 'logo-1.png' ) );
Redux::setOption($sb_option_name, 'top-footer-contact', array('address' => '1234, Lorem ipsum dolor eiusmod, adipisicing eiusmod', 'phone' => '+012 345 6789', 'fax' => '+012 6789 345', 'support' => 'support@company.com') );

Redux::setOption($sb_option_name, 'top-footer-social-media', array( 'facebook' => 'https://www.facebook.com/ScriptsBundle/?fref=ts', 'twitter' => '#',  'rss' => '', 'pinterest' => '', 'youtube' => '#', 'digg' => '', 'stumbleupon' => '', 'linkedin' => '#', 'google' => '', 'google-plus' => '#', ) );

Redux::setOption($sb_option_name, 'footer-service-links-title', 'Our Service' );
Redux::setOption($sb_option_name, 'footer-service-links-pages', '' );

Redux::setOption($sb_option_name, 'footer-posts-title', 'Latest Post' );
Redux::setOption($sb_option_name, 'footer-posts-content', '3' );
Redux::setOption($sb_option_name, 'footer-quick-links-title', 'Quick Links' );
Redux::setOption($sb_option_name, 'footer-quick-links-pages-cats', '1' );
Redux::setOption($sb_option_name, 'footer-quick-links-pages', '' );
Redux::setOption($sb_option_name, 'footer-quick-links-cats', '' );

// Blog Settings 
Redux::setOption($sb_option_name, 'blog-settings-type', 'masonry' );
Redux::setOption($sb_option_name, 'blog-settings-look', '2' );
Redux::setOption($sb_option_name, 'blog-settings-sidebar', '1' );
Redux::setOption($sb_option_name, 'blog-settings-admin-meta', 1 );
Redux::setOption($sb_option_name, 'blog-settings-breadcrumb', 1 );

// Blog Single Settings
Redux::setOption($sb_option_name, 'blog-comments-show', array('0' => 'Allow Comments') );
Redux::setOption($sb_option_name, 'blog-single-settings-breadcrumb', 1 );

// Page Settings
Redux::setOption($sb_option_name, 'page-comments-show', array('0' => 'Allow Comments') );
Redux::setOption($sb_option_name, 'page-settings-breadcrumb', 1 );

// Services Settings 
Redux::setOption($sb_option_name, 'services-settings-breadcrumb', 1 );
Redux::setOption($sb_option_name, 'services-sidebar-show', 1 );


// 404 Settings
Redux::setOption($sb_option_name, 'page-404-title', 'Oops! That page can’t be found.' );
Redux::setOption($sb_option_name, 'page-404-desc', 'Fusce pretium nulla et purus malesuada feugiat sed vel mauris tincidunt vehicula lorem vel hendrerit justo praesent aliquam maximus imperdiet integer sagittis leo' );
Redux::setOption($sb_option_name, 'page-404-widget-title', 'Some Other Links' );
Redux::setOption($sb_option_name, 'pages-cats-404', '1' );
Redux::setOption($sb_option_name, 'pages-404', array() );
Redux::setOption($sb_option_name, 'cats-404', array() );
Redux::setOption($sb_option_name, 'site-404-img', array( 'url' => SB_IMAGES_PLUGIN . '404.png' ) );


// Footer Options
Redux::setOption($sb_option_name, 'footer-copy-right-text', '&copy; All Right Reserved 2016' );
Redux::setOption($sb_option_name, 'footer-js-css', '' );

}

add_action('after_setup_theme', 'logistic_pro_remove_admin_bar');

function logistic_pro_remove_admin_bar() {
  global $sb_themes;
 if (!current_user_can('administrator') && !is_admin())
 {
  if( $sb_themes['admin_bar'] == 1 )
  {
    if(is_user_logged_in() )
	{
   		show_admin_bar( true ); 
	}
  }
  else
  {
   show_admin_bar( false ); 
  }
 }
}

// Load text domain
add_action( 'plugins_loaded', 'sb_framework_load_plugin_textdomain' );
function sb_framework_load_plugin_textdomain()
{
    load_plugin_textdomain( 'sb_framework', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}

