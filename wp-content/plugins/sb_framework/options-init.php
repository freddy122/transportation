<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "logistic";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'logistic',
        'dev_mode' => false,
        'display_name' => 'Theme Options',
        'display_version' => '1.0.0',
        'page_title' => 'Theme Options',
        'update_notice' => TRUE,
        'admin_bar' => TRUE,
        'menu_type' => 'submenu',
        'menu_title' => 'Theme Options',
        'allow_sub_menu' => TRUE,
        'page_parent_post_type' => 'your_post_type',
        'customizer' => TRUE,
        'default_show' => TRUE,
        'default_mark' => '*',
        'hints' => array(
            'icon_position' => 'right',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'global_variable' => 'sb_themes',
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );

    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/scriptsbundle',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );

    Redux::setArgs( $opt_name, $args );

$sample_patterns = '';
$sampleHTML  = '';
    /*
     * ---> END ARGUMENTS
     */

    // -> START Basic Fields
	
	

	
		/* ========================= Basic Settings =============================== */
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Site Settings', 'sb_framework' ),
        'id'               => 'blog-settings-0',
        'icon'             => 'el el-book'
    ) );
	/* Blog Are */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Site Settings', 'sb_framework' ),
        'id'         => 'basic-settings-1',
        'desc'       => __( '', 'sb_framework' ),
        'subsection' => true,
        'fields'     => array(

			
			array(
				'id'       => 'admin_bar',
				'type'     => 'switch',
				'title'    => __( 'Admin Bar', 'sb_framework' ),
				'subtitle' => __( 'wordpress', 'sb_framework' ),
			'desc'     => __( 'This setting is not for Administrator', 'sb_framework' ),
				'default'  => false,
			),
			
			array(
                'id'       => 'website-type',
                'type'     => 'button_set',
                'title'    => __( 'Select Website Type', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Please select site multipage or onepage', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Multipage',
                    '1' => 'Onepage',
                ),
                'default'  => '0'
            ),			
            array(
                'id'       => 'webite-loader',
                'type'     => 'switch',
                'title'    => __( 'Site Loader', 'sb_framework' ),
                'desc' => __( 'Show or hide sites loader', 'sb_framework' ),
                'default'  => true,
            ),
		
			//
			//Onepage settings
			array(
                'id'       => 'onepage-slides-settings',
                'type'     => 'button_set',
                'title'    => __( 'Select Website Type', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Please select site multipage or onepage', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Homepage Only',
                    '1' => 'All Pages',
					'2' => 'Hide All',
                ),
                'default'  => '0',
				'required' => array( 'website-type', '=', '1' ),
            ),			
			//////
			
			array(
                'id'       => 'onepage-slides-text',
                'type'     => 'sortable',
                'mode'     => 'textarea', // checkbox or text
                'title'    => __( 'Enter Slides Text', 'sb_framework' ),
                'desc'     => __( 'Enter slides text in rotating field seprate text with , to make it rotating.', 'sb_framework' ),
                'options'  => array(
                    'fixed_text' => __( 'Fixed Text', 'sb_framework' ),
                    'rotating_text' => __( 'Rotating Text', 'sb_framework' ),
                ),
                'default'  => array(
                    'fixed_text' => '',
                    'rotating_text' => '',
                ),
				'required' => array( 'onepage-slides-settings', '!=', '2' ),
            ),			
			array(
                'id'       => 'onepage-slides-line-break',
                'type'     => 'button_set',
                'title'    => __( 'Break Link', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Would you like to break the line.', 'sb_framework' ),
                'options'  => array(
                    '0' => 'One Line',
                    '1' => 'Break Line',
                ),
                'default'  => '0',
				'required' => array( 'onepage-slides-settings', '!=', '2' ),
            ),	
			array(
				'id'        => 'opt-multi-media-slides',
				'type'     => 'gallery',
				'title'     => __('Onepgae Slider Images', 'sb_framework'),
				'title'     => __('You can choose upto 5 images.', 'sb_framework'),
				'labels'    => array(
					'upload_file'       => __('Select File(s)', 'sb_framework'),
					'remove_image'      => __('Remove Image', 'sb_framework'),
					'remove_file'       => __('Remove', 'sb_framework'),
					'file'              => __('File: ', 'sb_framework'),
					'download'          => __('Download', 'sb_framework'),
					'title'             => __('Multi Media Selector', 'sb_framework'),
					'button'            => __('Add or Upload File','sb_framework')
				),
				'library_filter'  => array('gif','jpg','png'),
				'max_file_upload' => 5,
				'required' => array( 'onepage-slides-settings', '!=', '2' ),
				
			),			
			array(
				'id'       => 'onepage-move-to',
				'type'     => 'text',
				'title'    => __( 'Enter Section Id', 'sb_framework' ),
				'desc'    => __( 'Enter Section Id where you want to move it.', 'sb_framework' ),
				'required' => array( 'header-switch-on', '=', true ),
				'default'  => '#one-page',
				'required' => array( 'onepage-slides-settings', '!=', '2' ),
			),
			//Onepage settings ends //
			array(
                'id'       => 'site-color-settings',
                'type'     => 'button_set',
                'title'    => __( 'Color Selector', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Show hide color selector from the site.', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Hide',
                    '1' => 'Show',
                ),
                'default'  => '1'
            ),			
			array(
					'id'       => 'site-color-change',
					'type'     => 'palette',
					'title'    => __( 'Select Site Color', 'sb_framework' ),
					'desc'     => __( 'You can change the color of the site.', 'sb_framework' ),
					'required' => array( 'site-color-settings', '=', false ),
					'default'  => 'defualt',
					'palettes' => array(
						'defualt'  => array(
							'#016db6',
						),
						'green' => array(
							'#5fc87a',
						),
						'orange' => array(
							'#f47334',
						),
						'purple' => array(
							'#a063a9',
						),
						'red' => array(
							'#f43438',
						),
						'yellow' => array(
							'#f4a534',
						),
					)
				),
				
            array(
                'id'       => 'breadcrumb-bg-img',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Background Image', 'sb_framework' ),
                'compiler' => 'true',
                'mode'      => false,
                'desc'     => __( 'Upload breadcrumb background image', 'sb_framework' ),
                'default'  => array( 'url' => SB_IMAGES_PLUGIN . 'Logistics-and-4PL.jpg' ),
            ),
            array(
					'id'       => 'header-map-api-key',
					'type'     => 'text',
					'title'    => __( 'MAP API KEY', 'sb_framework' ),
					'default'  => '',
					'desc'     => __( 'Enter Map api key here', 'sb_framework' ),
                ),						
				

        )
    ) );	
	
	
	
	
	
    // -> START Header Settings
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Header Settings', 'sb_framework' ),
        'id'               => 'header-settings',
        'icon'             => 'el el-home'
    ) );


    Redux::setSection( $opt_name, array(
        'title'            => __( 'Top Header', 'sb_framework' ),
        'id'               => 'top-header',
        'subsection'       => true,
        'fields'           => array(
            array(
                'id'       => 'top-header-switch-on',
                'type'     => 'switch',
                'title'    => __( 'Show Top Header', 'sb_framework' ),
                'desc' => __( 'Show or Hide Top Header By Selecting On/Off Button', 'sb_framework' ),
                'default'  => true,
            ),
            array(
                'id'       => 'top-header-type',
				'required' => array( 'top-header-switch-on', '=', true ),
                'type'     => 'button_set',
                'title'    => __( 'Top Header Tpye', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Social Media Links or User Profile Links', 'sb_framework' ),
                'options'  => array(
                    'profile' => 'Profile',
                    'social' => 'Social',
                ),
                'default'  => 'profile'
            ),			
            array(
                'id'       => 'top-header-color-scheme',
                'type'     => 'switch',
                'title'    => __( 'Header Color', 'sb_framework' ),
                'desc' => __( 'Show Top Header Colored or not.', 'sb_framework' ),
                'default'  => true,
            ),

            array(
                'id'       => 'top-header-text',
                'type'     => 'text',
                'title'    => __( 'Top Header Text', 'sb_framework' ),
                'desc'     => __( 'Text Will Display In Top Header Left Side', 'sb_framework' ),
                'default'  => 'Opening Hours : Monday to Saturday - 8am to 5pm',
				'required' => array( 'top-header-switch-on', '=', true ),
				
            ),
			
            array(
                'id'       => 'top-header-register-page',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => false,
				'sortable' => false,
                'title'    => __( 'Register Page', 'sb_framework' ),
                'default' => array(),
				'required' => array( 'top-header-type', '=', 'profile' ),
            ),
            array(
                'id'       => 'top-header-login-page',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => false,
				'sortable' => false,
                'title'    => __( 'Login Page', 'sb_framework' ),
                'default' => array(),
				'required' => array( 'top-header-type', '=', 'profile' ),
            ),	
            array(
                'id'       => 'top-header-profile-page',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => false,
				'sortable' => false,
                'title'    => __( 'Profile Page', 'sb_framework' ),
                'default' => array(),
				'required' => array( 'top-header-type', '=', 'profile' ),
            ),
            array(
                'id'       => 'top-header-history-page',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => false,
				'sortable' => false,
                'title'    => __( 'History Page', 'sb_framework' ),
                'default' => array(),
				'required' => array( 'top-header-type', '=', 'profile' ),
            ),
            array(
                'id'       => 'top-header-order-page',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => false,
				'sortable' => false,
                'title'    => __( 'Place Order', 'sb_framework' ),
                'default' => array(),
				'required' => array( 'top-header-type', '=', 'profile' ),
            ),														

            array(
                'id'       => 'top-header-social-media',
				'required' => array( 'top-header-type', '=', 'social' ),
                'type'     => 'sortable',
                'mode'     => 'text', // checkbox or text
                'title'    => __( 'Header Social Media', 'sb_framework' ),
                'desc'     => __( 'Enter The Social Media URL. Leave Empty If You Don\' Wnat To Show Any.', 'sb_framework' ),
                'options'  => array(
                    'facebook' => 'Facebook',
                    'twitter' => 'Twitter',
                    'rss' => 'RSS',
					'pinterest' => 'Pinterest',
					'youtube' => 'Youtube',
					'digg' => 'digg',
					'stumbleupon' => 'Stumbleupon',
					'linkedin' => 'Linkedin',
					'google' => 'Google',
					'google-plus' => 'Google+',
                ),
                'default'  => array(
                    'facebook' => 'https://www.facebook.com/ScriptsBundle/?fref=ts',
                    'twitter' => '#',
                    'rss' => '',
					'pinterest' => '',
					'youtube' => '#',
					'digg' => '',
					'stumbleupon' => '',
					'linkedin' => '#',
					'google' => '',
					'google-plus' => '#',
                )
            ),
			
			
			
            array(
                'id'       => 'favicon',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Fav Icon', 'sb_framework' ),
                'compiler' => 'true',
                'mode'      => false,
                'desc'     => __( 'Upload Site Fav Icon Here.', 'sb_framework' ),
                'default'  => array( 'url' => SB_IMAGES_PLUGIN . 'favicon.ico' ),
            ),			
        )
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Main Header', 'sb_framework' ),
        'id'               => 'main-header',
        'subsection'       => true,
        'fields'           => array(


			// Select Header Style
            array(
                'id'       => 'header-styles',
                'type'     => 'button_set',
                'title'    => __( 'Select Header Layout', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Layout 1',
                    '1' => 'Layout 2',
                ),
                'default'  => '0'
            ),	
			           
            array(
                'id'       => 'site-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Site Logo', 'sb_framework' ),
                'compiler' => 'true',
                'mode'      => false,
                'desc'     => __( 'Upload Site Logo Here.', 'sb_framework' ),
                'default'  => array( 'url' => SB_IMAGES_PLUGIN . 'logo.png' ),
            ),

			// Show Hide button

            array(
                'id'       => 'header-switch-on',
                'type'     => 'switch',
				'required' => array( 'header-styles', '=', false ),
                'title'    => __( 'Show Contact Info', 'sb_framework' ),
                'subtitle' => __( 'Show or Hide Header Content By Selecting On/Off Button', 'sb_framework' ),
                'default'  => true
            ),
            array(
					'id'       => 'header-email-name',
					'type'     => 'text',
					'title'    => __( 'Email Title', 'sb_framework' ),
					'required' => array( 'header-switch-on', '=', true, 'header-styles', '=', false ),
					'default'  => 'EMAIL'
                ),
			array(
				'id'       => 'header-email-value',
				'type'     => 'text',
				'title'    => __( 'Email Address', 'sb_framework' ),
				'required' => array( 'header-switch-on', '=', true ),
				'default'  => 'email@yourdomain.com'
			),

            array(
					'id'       => 'header-phone-name',
					'type'     => 'text',
					'title'    => __( 'Call Us Title', 'sb_framework' ),
					'required' => array( 'header-switch-on', '=', true ),
					'default'  => 'Call Now'
                ),
			array(
				'id'       => 'header-phone-value',
				'type'     => 'text',
				'title'    => __( 'Phone Number', 'sb_framework' ),
				'required' => array( 'header-switch-on', '=', true ),
				'default'  => '(92) 123-456-78'
			),
            array(
					'id'       => 'header-contact-name',
					'type'     => 'text',
					'title'    => __( 'Address Title', 'sb_framework' ),
					'required' => array( 'header-switch-on', '=', true ),
					'default'  => 'Find Us'
                ),
			array(
				'id'       => 'header-contact-value',
				'type'     => 'text',
				'title'    => __( 'Address', 'sb_framework' ),
				'required' => array( 'header-switch-on', '=', true ),
				'default'  => 'Model Town, Pakistan'
			),

           

            array(
                'id'   => 'opt-required-divide-1',
                'type' => 'divide'
            ),

            array(
					'id'       => 'show-hide-place-order-button-text',
					'type'     => 'text',
					'title'    => __( 'Book Now Button Text', 'sb_framework' ),
					'default'  => 'Booking',
                ),
            array(
                'id'       => 'show-hide-place-order-button',
                'type'     => 'button_set',
                'title'    => __( 'Book Now Button', 'sb_framework' ),
				'subtitle' => __( 'Show or Hide Qoute Button From The Menu Right Side.', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Show',
                    '1' => 'Hide',
                ),
                'default'  => '0'
            ),
            array(
                'id'       => 'show-hide-qoute-button',
                'type'     => 'button_set',
                'title'    => __( 'Qoute Button', 'sb_framework' ),
				'subtitle' => __( 'Show or Hide Qoute Button From The Menu Right Side.', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Show',
                    '1' => 'Hide',
                ),
                'default'  => '0'
            ),						


            array(
                'id'      => 'header-js-css',
                'type'    => 'textarea',
                'title'   => __( 'Custom JS/CSS Code', 'sb_framework' ),
                'default' => '',
				'desc'	  => __( 'Enter Custom CSS/JS Code like Analytics Code ', 'sb_framework' ),
                'args'    => array(
                    'wpautop'       => false,
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'teeny'         => false,
                    'quicktags'     => false,
                )
            ),
		       
        )
    ) );
	
	
		/* ========================= Blog Settings =============================== */
    // -> START Blog Settings
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Blog/Page Settings', 'sb_framework' ),
        'id'               => 'blog-settings',
        'icon'             => 'el el-book'
    ) );
	/* Blog Are */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Blog Settings', 'sb_framework' ),
        'id'         => 'blog-settings-layout',
        'desc'       => __( '', 'sb_framework' ),
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'blog-settings-type',
                'type'     => 'button_set',
                'title'    => __( 'Blog Tpye', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Select Blog Layout, Simple, Simple With Sidebar, Masonry, Masonry With Sidebar', 'sb_framework' ),
                'options'  => array(
                    'masonry' => 'Masonry',
                    'simple' => 'Simple',
                ),
                'default'  => 'masonry'
            ),
            array(
                'id'       => 'blog-settings-look',
                'type'     => 'button_set',
                'title'    => __( 'Blog Layout', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Select Blog Layout, Simple, Simple With Sidebar, Masonry, Masonry With Sidebar', 'sb_framework' ),
                'options'  => array(
                    '1' => '3 Column',
                    '2' => '2 Column',
                ),
                'default'  => '2'
            ),
            array(
                'id'       => 'blog-settings-sidebar',
                'type'     => 'button_set',
                'title'    => __( 'Blog Sidebar', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
				'required' => array( 'blog-settings-look', '=', '2' ),
                'desc'     => __( 'Select Blog Layout, Simple, Simple With Sidebar, Masonry, Masonry With Sidebar', 'sb_framework' ),
                'options'  => array(
					'2' => 'Left Sidebar',
					'1' => 'Right Sidebar',
                ),
                'default'  => '1'
            ),

            array(
                'id'       => 'blog-settings-admin-meta',
                'type'     => 'button_set',
                'title'    => __( 'Show Meta', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Show hide meta data.', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Hide',
                    '1' => 'Show',
                ),
                'default'  => '1'
            ),
			
			array(
                'id'       => 'blog-settings-breadcrumb',
                'type'     => 'button_set',
                'title'    => __( 'Show Breadcrumb', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Show hide breadcrumb', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Hide',
                    '1' => 'Show',
                ),
                'default'  => '1'
            ),			

        )
    ) );	


	/* Blog single */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Single Blog Settings', 'sb_framework' ),
        'id'         => 'blog-settings-single',
        'desc'       => __( '', 'sb_framework' ),
        'subsection' => true,
        'fields'     => array(

			array(
                'id'       => 'blog-single-settings-breadcrumb',
                'type'     => 'button_set',
                'title'    => __( 'Show Breadcrumb', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Show hide breadcrumb', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Hide',
                    '1' => 'Show',
                ),
                'default'  => '1'
            ),			
            array(
                'id'       => 'blog-comments-show',
                'type'     => 'button_set',
                'title'    => __( 'Manage Comments', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'With above options you can allow/disable comments as well you can hide comment from all the posts.', 'sb_framework' ),
                'options'  => array(
					'0' => 'Allow Comments',
                    '1' => 'Hide All Comments',
                    '2' => 'Disable Comments',
                ),
                'default'  => '0'
            ),

        )
    ) );	

	/* Blog single */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Page Settings', 'sb_framework' ),
        'id'         => 'page-settings-single',
        'desc'       => __( '', 'sb_framework' ),
        'subsection' => true,
        'fields'     => array(

            array(
                'id'       => 'top-header-show-title',
                'type'     => 'switch',
                'title'    => __( 'Page Title', 'sb_framework' ),
                'desc' => __( 'Show or hide page title.', 'sb_framework' ),
                'default'  => true,
            ),
			array(
                'id'       => 'page-settings-breadcrumb',
                'type'     => 'button_set',
                'title'    => __( 'Show Breadcrumb', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Show hide breadcrumb', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Hide',
                    '1' => 'Show',
                ),
                'default'  => '1'
            ),			

           
		    array(
                'id'       => 'page-comments-show',
                'type'     => 'button_set',
                'title'    => __( 'Manage Page Comments', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'With above options you can allow/disable comments as well you can hide comment from all the pages.', 'sb_framework' ),
                'options'  => array(
					'0' => 'Allow Comments',
                    '1' => 'Hide All Comments',
                    '2' => 'Disable Comments',
                ),
                'default'  => '0'
            ),

        )
    ) );	

	/* Services Page */
    Redux::setSection( $opt_name, array(
        'title'      => __( 'Our Services', 'sb_framework' ),
        'id'         => 'services-page',
        'desc'       => __( '', 'sb_framework' ),
        'subsection' => true,
        'fields'     => array(
           
		array(
                'id'       => 'services-settings-breadcrumb',
                'type'     => 'button_set',
                'title'    => __( 'Show Breadcrumb', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'Show hide breadcrumb', 'sb_framework' ),
                'options'  => array(
                    '0' => 'Hide',
                    '1' => 'Show',
                ),
                'default'  => '1'
            ),			


            array(
                'id'       => 'services-sidebar-show',
                'type'     => 'button_set',
                'title'    => __( 'Manage Comments', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'desc'     => __( 'With above options you can allow/disable comments as well you can hide comment from all the posts.', 'sb_framework' ),
                'options'  => array(
					'0' => 'No Sidebar',
                    '1' => 'Right Sidebar',
                    '2' => 'Left Sidebar',
                ),
                'default'  => '1'
            ),



        )
    ) );	








	/* 404 Page */
    Redux::setSection( $opt_name, array(
        'title'      => __( '404 Page', 'sb_framework' ),
        'id'         => 'page-404',
        'desc'       => __( '', 'sb_framework' ),
        'subsection' => true,
        'fields'     => array(
           
		    array(
                'id'       => 'page-404-title',
                'type'     => 'text',
                'title'    => __( 'Page Tiltle', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'default'     => __( 'Oops! That page can’t be found.', 'sb_framework' ),
            ),

		    array(
                'id'       => 'page-404-desc',
                'type'     => 'textarea',
                'title'    => __( 'Page Description', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'default'     => __( 'Fusce pretium nulla et purus malesuada feugiat sed vel mauris tincidunt vehicula lorem vel hendrerit justo praesent aliquam maximus imperdiet integer sagittis leo', 'sb_framework' ),
            ),

		    array(
                'id'       => 'page-404-widget-title',
                'type'     => 'text',
                'title'    => __( 'Page Widget Tiltle', 'sb_framework' ),
                'subtitle' => __( '', 'sb_framework' ),
                'default'     => __( 'Some Other Links', 'sb_framework' ),
            ),


            array(
                'id'       => 'site-404-img',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Background Image', 'sb_framework' ),
                'compiler' => 'true',
                'mode'      => false,
                'desc'     => __( 'Upload Footer background image', 'sb_framework' ),
                'default'  => array( 'url' => SB_IMAGES_PLUGIN . '404.png' ),
            ),
	

        )
    ) );	


    // START Contact and qoute Info
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Contact/Popup Settings', 'sb_framework' ),
        'id'               => 'contact-popup-settings',
        'icon'             => 'el el-bullhorn'
    ) );

	/* qoute Info */
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Qoute Settings', 'sb_framework' ),
        'id'               => 'qoute-settings',
		'icon'             => 'el el-key',
        'subsection'       => true,
        'fields'           => array(

            array(
                'id'      => 'qoute-title-text',
                'type'    => 'text',
                'title'   => __( 'Popup Title', 'sb_framework' ),
                'default'  => __( 'REQUEST A QUOTE', 'sb_framework' ),
            ),

            array(
                'id'      => 'qoute-tagline-text',
                'type'    => 'text',
                'title'   => __( 'Popup Tagline', 'sb_framework' ),
                'default'  => __( 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'sb_framework' ),
            ),

            array(
                'id'      => 'qoute-button-text',
                'type'    => 'text',
                'title'   => __( 'Button Text', 'sb_framework' ),
                'default'  => __( 'Submit Request', 'sb_framework' ),
            ),
			
			
            array(
                'id'      => 'qoute-mail-receive-on',
                'type'    => 'text',
                'title'   => __( 'Email Address', 'sb_framework' ),
				'default'  => __( 'youremail@domainname.com', 'sb_framework' ),
                'desc'  => __( 'Enter email address where you want to receive qoute emails', 'sb_framework' ),
            ),

            array(
                'id'      => 'qoute-mail-receive-title',
                'type'    => 'text',
                'title'   => __( 'Email Title', 'sb_framework' ),
                'default'  => __( 'New Quotation Received', 'sb_framework' ),
            ),

			
        array(
            'id' => 'qoute-message-template',
            'type' => 'editor',
            'title' => __('Email Template', 'foodpro'),
            'desc' => __('', 'sb_framework'),
            'default' => 'Hi Admin,<br /><br />you have received a message from quote form, details are below.<br /><br />%name%<br />%subject%<br />%email%<br />%cargo_type%<br />%location%<br />%rlocation%<br />%message%',
            'args' => array(
                'wpautop' => false,
                'media_buttons' => false,
                'textarea_rows' => 14,
                'teeny' => false,
                'quicktags' => false
            )
        ),
        array(
            'id' => 'qoute-success-message',
            'type' => 'text',
            'title' => __('Message on Success', 'sb_framework'),
            'subtitle' => '',
            'desc' => '',
            'default' => "Message sent, we will get back to you shortly."
        ),
        array(
            'id' => 'qoute-error-message',
            'type' => 'text',
            'title' => __('Message on Error', 'sb_framework'),
            'subtitle' => '',
            'desc' => '',
            'default' => "Some error occured while sending email. Please try shortly."
        ),
        
		
        )
    ) );
	// Qoute Ends

	/* Contact us Info */
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Contact Settings', 'sb_framework' ),
        'id'               => 'contact-settings',
		'icon'             => 'el el-envelope',
        'subsection'       => true,
        'fields'           => array(
	
			/* address area start */

			
			/* address area ends */
			/* Contact mail settings*/

            array(
                'id'    => 'contact-notice-info1',
                'type'  => 'info',
                'style' => 'info',
                'title' => __( 'Contact Form', 'sb_framework' ),
                'desc'  => __( 'Here you can manage the contact form details and as well can make your own contact email template you want to receive in the email.', 'sb_framework' )
            ),			

            array(
                'id'      => 'contact-mail-receive-on',
                'type'    => 'text',
                'title'   => __( 'Email Address', 'sb_framework' ),
				'default'  => __( 'contact@domainname.com', 'sb_framework' ),
                'desc'  => __( 'Enter email address where you want to receive qoute emails', 'sb_framework' ),
            ),

            array(
                'id'      => 'contact-mail-receive-title',
                'type'    => 'text',
                'title'   => __( 'Email Title', 'sb_framework' ),
                'default'  => __( 'New Contact Email Received', 'sb_framework' ),
            ),

			
        array(
            'id' => 'contact-message-template',
            'type' => 'editor',
            'title' => __('Email Template', 'foodpro'),
            'desc' => __('%name%, %email%, %subject%, %message%', 'sb_framework'),
            'default' => 'Hi Admin,<br /><br />you have received a message from contact us page, details are below.<br /><br />%name%<br />%email%<br />%subject%<br />%message%',
            'args' => array(
                'wpautop' => false,
                'media_buttons' => false,
                'textarea_rows' => 14,
                'teeny' => false,
                'quicktags' => false
            )
        ),
        array(
            'id' => 'contact-success-message',
            'type' => 'text',
            'title' => __('Message on Success', 'sb_framework'),
            'subtitle' => '',
            'desc' => '',
            'default' => "Message sent, we will get back to you shortly."
        ),
        array(
            'id' => 'contact-error-message',
            'type' => 'text',
            'title' => __('Message on Error', 'sb_framework'),
            'subtitle' => '',
            'desc' => '',
            'default' => "Some error occured while sending email. Please try shortly."
        ),			
			
			
			
			
			
			
			
			
			/* contact mail ends*/
		
	)) );

		/* ========================= Footer =============================== */
    // -> START Footer Settings
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Footer Settings', 'sb_framework' ),
        'id'               => 'footer-settings',
        'icon'             => 'el el-home'
    ) );
	/* Top Footer */
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Top Footer', 'sb_framework' ),
        'id'               => 'top-footer',
        'subsection'       => true,
        'fields'           => array(
            array(
                'id'       => 'top-footer-switch-on',
                'type'     => 'switch',
                'title'    => __( 'Show Top Footer', 'sb_framework' ),
                'desc' => __( 'Show or Hide Top Header By Selecting On/Off Button', 'sb_framework' ),
                'default'  => true,
            ),

            array(
                'id'       => 'site-footer-bg-img',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Background Image', 'sb_framework' ),
                'compiler' => 'true',
                'mode'      => false,
                'desc'     => __( 'Upload Footer background image', 'sb_framework' ),
                'default'  => array( 'url' => SB_IMAGES_PLUGIN . 'footer-bg.jpg' ),
            ),


			//Footer Logo/Contact Area
            array(
                'id'       => 'site-footer-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Site Logo', 'sb_framework' ),
                'compiler' => 'true',
                'mode'      => false,
                'desc'     => __( 'Upload Site Fav Icon Here.', 'sb_framework' ),
                'default'  => array( 'url' => SB_IMAGES_PLUGIN . 'logo-1.png' ),
            ),

            array(
                'id'       => 'top-footer-contact',
                'type'     => 'sortable',
                'mode'     => 'text', // checkbox or text
                'title'    => __( 'Enter Contact Details', 'sb_framework' ),
                'desc'     => __( '', 'sb_framework' ),
                'options'  => array(
                    'address' => '1234, Lorem ipsum dolor eiusmod, adipisicing eiusmod',
                    'phone' => '+012 345 6789',
                    'fax' => '+012 6789 345',
                    'support' => 'support@company.com',					
                ),
                'default'  => array(
                    'address' => '1234, Lorem ipsum dolor eiusmod, adipisicing eiusmod',
                    'phone' => '+012 345 6789',
                    'fax' => '+012 6789 345',
                    'support' => 'support@company.com',					
                )
            ),
			array(
                'id'       => 'top-footer-social-media',
                'type'     => 'sortable',
                'mode'     => 'text', // checkbox or text
                'title'    => __( 'Header Social Media', 'sb_framework' ),
                'desc'     => __( 'Enter The Social Media URL. Leave Empty If You Don\' Wnat To Show Any.', 'sb_framework' ),
                'options'  => array(
                    'facebook' => 'Facebook',
                    'twitter' => 'Twitter',
                    'rss' => 'RSS',
					'pinterest' => 'Pinterest',
					'youtube' => 'Youtube',
					'digg' => 'digg',
					'stumbleupon' => 'Stumbleupon',
					'linkedin' => 'Linkedin',
					'google' => 'Google',
					'google-plus' => 'Google+',
                ),
                'default'  => array(
                    'facebook' => 'https://www.facebook.com/ScriptsBundle/?fref=ts',
                    'twitter' => '#',
                    'rss' => '',
					'pinterest' => '',
					'youtube' => '#',
					'digg' => '',
					'stumbleupon' => '',
					'linkedin' => '#',
					'google' => '',
					'google-plus' => '#',
                )
            ),
			/* Other Servicses */
			array(
                'id'       => 'footer-service-links-title',
                'type'     => 'text',
                'title'    => __( 'Service Links Title', 'sb_framework' ),
                'desc'     => __( '', 'sb_framework' ),
                'default'  => 'Our Service',
            ),
            array(
                'id'       => 'footer-service-links-pages',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => true,
				'sortable' => true,
                'title'    => __( 'Select Service Links', 'sb_framework' ),
				'desc'     => __( 'Select Services Pages Links.', 'sb_framework' ),
                'default' => array(),
            ),
			/*posts*/
            array(
                'id'       => 'footer-posts-title',
                'type'     => 'text',
                'title'    => __( 'Latest Posts Title', 'sb_framework' ),
                'desc'     => __( '', 'sb_framework' ),
                'default'  => 'Latest Post',
            ),
            array(
                'id'      => 'footer-posts-content',
                'type'    => 'spinner',
                'title'   => __( 'No.Of Posts', 'sb_framework' ),
                'desc'    => __( 'Select Number Of Posts To Show In Footer.', 'sb_framework' ),
                'default' => '2',
                'min'     => '1',
                'step'    => '1',
                'max'     => '10',
            ),
			/*Quick Links*/
            array(
                'id'       => 'footer-quick-links-title',
                'type'     => 'text',
                'title'    => __( 'Quick Links Title', 'sb_framework' ),
                'desc'     => __( '', 'sb_framework' ),
                'default'  => 'Quick Links',
            ),
			
			//// test starts
			array(
                'id'       => 'footer-quick-links-pages-cats',
                'type'     => 'switch',
                'title'    => __( 'Footer Links', 'sb_framework' ),
                'subtitle' => __( 'You can select Pages or Categories', 'sb_framework' ),
                'default'  => 1,
                'on'       => 'Show Pages',
                'off'      => 'Show Categories',
            ),
            array(
                'id'       => 'footer-quick-links-pages',
                'type'     => 'select',
                'data'     => 'pages',
                'multi'    => true,
				'sortable' => true,
                'required' => array( 'footer-quick-links-pages-cats', '=', '1' ),
                'title'    => __( 'Pages', 'sb_framework' ),
                'default' => array(),
            ),
            array(
                'id'       => 'footer-quick-links-cats',
                'type'     => 'select',
                'data'     => 'categories',
                'multi'    => true,
				'sortable' => true,
                'required' => array( 'footer-quick-links-pages-cats', '=', '0' ),
                'title'    => __( 'Categories', 'sb_framework' ),
                'default' => array(),
            ),
			
        )
    ) );
	/* Footer */
	Redux::setSection( $opt_name, array(
        'title'            => __( 'Main Footer', 'sb_framework' ),
        'id'               => 'main-footer',
        'subsection'       => true,
        'fields'           => array(

            array(
                'id'      => 'footer-copy-right-text',
                'type'    => 'editor',
                'title'   => __( 'Copy Right Text', 'sb_framework' ),
                'default'  => '&copy; All Right Reserved 2016',
                'args'    => array(
                    'wpautop'       => false,
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'teeny'         => false,
                    'quicktags'     => false,
                )
            ),

            array(
                'id'      => 'footer-js-css',
                'type'    => 'textarea',
                'title'   => __( 'Custom JS/CSS Code', 'sb_framework' ),
                'default' => '',
				'desc'	  => __( 'Enter Custom CSS/JS Code like Analytics Code ', 'sb_framework' ),
                'args'    => array(
                    'wpautop'       => false,
                    'media_buttons' => false,
                    'textarea_rows' => 3,
                    'teeny'         => false,
                    'quicktags'     => false,
                )
            ),

        )
    ) );
	// Footer Ends
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	