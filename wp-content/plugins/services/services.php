<?php
/*
Plugin Name: Page services
Description: Controle page services
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/

add_shortcode( 'service_page', 'afficher_service_page');

function afficher_service_page(){
    if(!is_admin()){
        ob_start();
        
        $user_id = get_current_user_id();
        $type_user = get_user_meta($user_id, "type_user", true);
        save_data_forwarder();
        $data_client = $_POST;
        save_data_client($data_client);
        require dirname( __FILE__ ) . '/template/index.php';
        return ob_get_clean();
        
    }
}



function save_data_client($data){
    global $wpdb;
    
    $users = get_users( array( 'fields' => array( 'ID' ) ) );
    
    $arr_to_send_mail= array();
    foreach($users as $user_id){
        $test_service = (get_user_meta ($user_id->ID,'categorie_service',true));
        if(!empty($test_service)){
            $arr_to_send_mail[$user_id->ID] = $test_service;
        }
    }
   
    if(
            !empty($data['references'])
          && !empty($data['express'])
    ){
        $querys45 = "SELECT distinct reference from ".$wpdb->prefix."services where reference = '".$data['references']."'";
        $last_refs = $wpdb->get_results($querys45, OBJECT);
        
        if(empty($last_refs)){
            $dateNow = date('Y-m-d H:i:s');
            $dtch = DateTime::createFromFormat('d/m/Y', $data['date_chargement']);
            $dtchf = $dtch->format('Y-m-d H:i:s');
            $dtdech = DateTime::createFromFormat('d/m/Y', $data['date_dechargement']);
            $dtdechf = $dtdech->format('Y-m-d H:i:s');

            $wpdb->insert(
                $wpdb->prefix . 'services',
                array(
                    'reference'                 => $data['references'],
                    'type_societe'              => $data['type_soc'],
                    'id_client'                 => get_current_user_id(),
                    //'categories'                => implode(',',$data['categorie_service']),
                    'date_creation'             => $dateNow,
                    'express'                   => $data['express'],
                    'pays_chargement'           => $data['pays_chargement'],
                    'ville_chargement'          => $data['ville_chargement'],
                    'adresse_chargement'        => $data['adresse_chargement'],
                    'num_tel_chargement'        => $data['num_tel_chargement'],
                    'date_chargement'           => $dtchf,
                    'heure_desirer_chargement'  => $data['heure_chargement'],
                    'contrainte_chargement'     => $data['contrainte_chargement'],
                    'pays_dechargement'         => $data['pays_dechargement'],
                    'ville_dechargement'        => $data['ville_dechargement'],
                    'adresse_dechargement'      => $data['adresse_dechargement'],
                    'num_tel_dechargement'      => $data['num_tel_dechargement'],
                    'date_dechargement'         => $dtdechf,
                    'heure_desirer_dechargement' => $data['heure_dechargement'],
                    'contrainte_dechargement'   => $data['contrainte_dechargement'],
                    'accepter_sous_traitant'    => $data['accepter_sous_traitant'],
                    'emballage'                 => $data['emballage'],
                    'hauteur'                   => $data['hauteur'],
                    'longueur'                   => $data['longueur'],
                    'poids'                     => $data['poids'],
                    'largeur'                   => $data['largeur'],
                    'matiere_dangereux'         => $data['matiere_dangereux'],
                    'fragile'                   => $data['fragile'],
                )
            );
            $lastid = $wpdb->insert_id;
            $querys4 = "SELECT distinct reference from ".$wpdb->prefix."services where id = ".$lastid;
            $last_ref = $wpdb->get_results($querys4, OBJECT);
            
            /* envoie mail */
            $args = array(
                    'meta_key'     => 'type_user',
                    'meta_value'   => 'forwarder',
                    'meta_compare' => '='
            );
            $type_users = new WP_User_Query( $args );
            $allForwarders = $type_users->get_results();
            $arr_email_forwards = array();
            $arr_id_forwards = array();
            foreach($allForwarders as $forward){
                array_push($arr_email_forwards,$forward->data->user_email);
                array_push($arr_id_forwards,$forward->data->ID);
            }
            $title   = 'Nouvelle transaction';
            $headers = array('Content-type: text/html');
            $contents = "Bonjour , <br><br>";
            $contents .= "Un client a démander un dévis  <br><br>";
            $contents .= "Merci de répondre l'offre en utilisant ce réference  : <b> ".$last_ref[0]->reference." </b><br><br>";
            foreach($arr_email_forwards as $fwds){
                wp_mail($fwds, $title, $contents, $headers);
            }
            
            // mail_type value : new_devis, proposition_devis, confirmation_devis
            foreach($arr_id_forwards as $fwdsid){
                $wpdb->insert(
                $wpdb->prefix . 'devis_mails',
                array(
                    "reference_user" => $last_ref[0]->reference,
                    "id_user" => get_current_user_id(),
                    "id_forwarder" => $fwdsid,
                    "mail_content" => $contents,
                    "status_mail" => 0,
                    "mail_type" => "new_devis",
                ));
            }
            
            
            /* fin envoie mail */
            
            // old 
            /*foreach($arr_to_send_mail as $k => $v){
                $arr_inter = array_intersect(explode(',',$v), $data['categorie_service']);
                $querys_categ = "select libelle_categ from ".$wpdb->prefix."category_service where id in ( ".implode(",",$arr_inter)." )";
                $lib_categ = $wpdb->get_results($querys_categ, OBJECT);
                $arr_categs = array();
                foreach($lib_categ as $libcat){
                    array_push($arr_categs, $libcat->libelle_categ);
                }
                if(!empty($arr_inter)){
                    $title   = 'Nouvelle transaction';
                    $user_info = get_userdata($k);
                    $headers = array('Content-type: text/html');
                    $contents = "Bonjour , <br><br>";
                    $contents .= "Un client a démander un dévis sur les services suivantes :  <b> ".implode(',',$arr_categs)."</b> <br><br>";
                    $contents .= "Merci de répondre l'offre en utilisant ce réference  : <b> ".$last_ref[0]->reference." </b><br><br>";
                    wp_mail($user_info->user_email, $title, $contents, $headers);
                }
            }*/
            echo  '<div class="alert alert-info">
                    '.__("<!--:fr--><strong>Succès </strong> Nouvelle transaction effectué avec succès.<!--:--><!--:en--><strong> Success </ strong> New transaction completed successfully.<!--:-->").'
                </div>';
        }else{
           echo '<div class="alert alert-danger">
                    '.__("<!--:fr--><strong>Erreur</strong> réferences déjà enregistré.<!--:--><!--:en--><Strong> Error </ strong> references already registered.<!--:-->").'
                </div>';
       }
    }
}

function save_data_forwarder(){
    global $wpdb;
    $querys = "SELECT distinct id,reference from ".$wpdb->prefix."services order by id desc";
    $allRef = $wpdb->get_results($querys, OBJECT);
    $arr_ref = array();
    foreach($allRef as $refs){
        array_push($arr_ref, $refs->reference);
    }
    
    if(isset($_POST["reference_client_forward"]) && in_array($_POST["reference_client_forward"], $arr_ref)){
        
        //verification
        $querys_verif = "select distinct reference_user from ".$wpdb->prefix."devis_forwarder where id_forwarder =".get_current_user_id();
        $user_idverif = $wpdb->get_results($querys_verif, OBJECT);
        $arr_verif = array();
        foreach($user_idverif as $refs){
            array_push($arr_verif, $refs->reference_user);
        }
        
        if(in_array($_POST["reference_client_forward"], $arr_verif))
        {
            echo '<div class="alert alert-danger">
                    '.__("<!--:fr--><strong> Erreur </strong> Réference déjà enregistrer.<!--:--><!--:en--><strong> Error </ strong> Reference already save<!--:-->").'
                 </div>';
        }else{
            $querys2 = "SELECT distinct id_client from ".$wpdb->prefix."services where reference = '".$_POST["reference_client_forward"]."' ";
            $user_idref = $wpdb->get_results($querys2, OBJECT);
            $user_idrefs = $user_idref[0]->id_client;
            $wpdb->insert(
                $wpdb->prefix .'devis_forwarder',
                array(
                    'reference_user'        => $_POST["reference_client_forward"],
                    'reference_forwarder'   => randomCaractersHi(9),
                    'prix_devis'            => $_POST['prix_devis'],
                    'id_user'               => $user_idrefs,
                    'id_forwarder'          => get_current_user_id(),
                    'status'                => 1,
                    'date_creation'         => date('Y-m-d H:i:s'),
                    //'note_forwarder'         => $_POST['note_forward'],
                )
            );
            $lastidf = $wpdb->insert_id;
            $querys4f = "SELECT distinct reference_forwarder,id_forwarder from ".$wpdb->prefix."devis_forwarder where id = ".$lastidf;
            $last_reff = $wpdb->get_results($querys4f, OBJECT);
            
            $wpdb->update( 
                $wpdb->prefix.'services',
                array( 
                    'status'  => 1,
                ), 
                array( 'reference' => $_POST["reference_client_forward"] )
            );
            
            $titler   = 'Nouvelle offre';
            $user_infor = get_userdata($user_idrefs);
            $headersr = array('Content-type: text/html');
            $contentsr = "Bonjour , <br><br>";
            $contentsr .= "On vous a proposer une offre sur votre dévis <br><br>";
            $contentsr .= "Montant de l'offre  : <b>".$_POST['prix_devis']." Euros</b> <br><br>";
            //$contentsr .= "Note  : <b>".wpautop($_POST['note_forward'])." </b> <br><br>";
            $contentsr .= "Votre réference  : <b>".$_POST["reference_client_forward"]."</b>  <br><br>";
            $contentsr .= "Réference du forwarder  : <b>".$last_reff[0]->reference_forwarder."</b>  <br><br>";
            $contentsr .= "Merci";
            wp_mail($user_infor->user_email, $titler, $contentsr, $headersr);
            
            // mail_type value : new_devis, proposition_devis, confirmation_devis
            $wpdb->insert(
                $wpdb->prefix . 'devis_mails',
                array(
                    "reference_user" => $_POST["reference_client_forward"],
                    "reference_forwarder" => $last_reff[0]->reference_forwarder,
                    "id_user" => $user_infor->ID,
                    "id_forwarder" => $last_reff[0]->id_forwarder,
                    "mail_content" => $contentsr,
                    "status_mail" => 0,
                    "mail_type" => "proposition_devis",
                ));
            
            echo '<div class="alert alert-info">
                '.__("<!--:fr--><strong>Succès</strong> Nouvelle dévis effectué avec succès.<!--:--><!--:en--><strong> Success </ strong> New quote completed successfully.<!--:-->").'
                
            </div>';
        }
    }
    
    if(isset($_POST["reference_client_forward"]) && !in_array($_POST["reference_client_forward"], $arr_ref)){
        echo '<div class="alert alert-danger">
            '.__("<!--:fr--><strong>Erreur</strong> Votre réference ne figure pas dans nos base de données.<!--:--><!--:en--><strong> Error </ strong> Your reference is not in our database.<!--:-->").'
        </div>';
    }
    
    
    if(isset($_POST['select_forwarder'])){
        $demande_id = explode("___",$_POST['select_forwarder']);
        $querys23 = "SELECT * from ".$wpdb->prefix."services where reference = '".$demande_id[0]."'";
        $datas = $wpdb->get_results($querys23, OBJECT);
    
        foreach($datas as $dat){
            $email_admin = get_option('admin_email');
            $headere = array('Content-type: text/html');
            $infos_forwarder = get_userdata($demande_id[1]);
            $infos_user = get_userdata($dat->id_client);
            /*************** Envoi information au forwarder  ************/
            $content = " Bonjour, <br><br> ";
            $content  .= " Vous avez remporter un dévis :<br><br> ";
            $content  .= " Voici l'information concernant le client <br><br> ";
            $content  .= " Réference :<b>".$dat->reference."</b><br><br> ";
            $tel = get_user_meta($dat->id_client, "billing_phone", true);
            $content  .= " Nom  : <b>".$infos_user->first_name ."</b><br><br> ";
            $content  .= " Prénom  : <b>".$infos_user->last_name."</b><br><br> ";
            $content  .= " Télephone  : <b>".$tel."</b><br><br> ";
            $content  .= " Email : <b>".$infos_user->user_email."</b><br><br> ";
            
            wp_mail($infos_forwarder->user_email, "Confirmation demande de dévis client", $content, $headere);
            // mail_type value : new_devis, proposition_devis, confirmation_devis
            $wpdb->insert(
                $wpdb->prefix . 'devis_mails',
                array(
                    "reference_user" => $dat->reference,
                    "id_user" => $infos_user->ID,
                    "id_forwarder" => $infos_forwarder->ID,
                    "mail_content" => $content,
                    "status_mail" => 0,
                    "mail_type" => "confirmation_devis",
                ));
            /****************** fin  Envoi information à l' admin ***************/
            
            /************ Envoi information forwarder au client **********/
            
            $telf = get_user_meta($demande_id[1], "billing_phone", true);
            $contentClient = "Bonjour, <br><br> ";
            $contentClient .= "Vous avez valider une dévis <br><br> ";
            $contentClient .= "Voici les information concernant le forwarder<br><br> ";
            $contentClient .= "Nom : <b>".$infos_forwarder->first_name."</b><br><br> ";
            $contentClient .= "Prénom : <b>".$infos_forwarder->last_name."</b><br><br> ";
            $contentClient .= "Télephone : <b>".$telf."</b><br><br> ";
            $contentClient .= "Email : <b>".$infos_forwarder->user_email."</b><br><br> ";
            wp_mail($infos_user->user_email, "Information Forwarder", $contentClient, $headere);
            
            $wpdb->insert(
                $wpdb->prefix . 'devis_mails',
                array(
                    "reference_user" => $dat->reference,
                    "id_user" => $infos_user->ID,
                    "id_forwarder" => $infos_forwarder->ID,
                    "mail_content" => $contentClient,
                    "status_mail" => 0,
                    "mail_type" => "confirmation_devis_client",
                ));
            /*********** fin Envoi information forwarder au client ************/

            $wpdb->update( 
                $wpdb->prefix.'services',
                array( 
                    'status'  => 2,
                    'forwarder_attrib'  => sanitize_text_field($demande_id[1]),
                ), 
                array( 'id' => $dat->id )
            );
            $wpdb->update( 
                $wpdb->prefix.'devis_forwarder',
                array( 
                    'status'  => 2,
                ), 
                array( 'reference_user' => $dat->reference )
            );
        }
    }
}

add_action('wp_ajax_get_info_by_reference', 'get_info_by_reference');
add_action('wp_ajax_nopriv_get_info_by_reference', 'get_info_by_reference');
function get_info_by_reference() {
    global $wpdb;
    $refcl      = $_GET['ref_client'];
    $quersRfCl = "SELECT * from ".$wpdb->prefix."devis_forwarder where 	reference_user = '".$refcl."'";
    $datasRfCl = $wpdb->get_results($quersRfCl, OBJECT);
    foreach($datasRfCl as $rfcl){
        echo "<option value=''>--- ".__("<!--:fr-->Choisir<!--:--><!--:en-->Choose<!--:-->")." ---</option>";
        echo "<option value='".$rfcl->reference_user."___".$rfcl->id_forwarder."'>".$rfcl->reference_forwarder."</option>";
    }
}

/*add_action('wp_ajax_envoie_information_admin', 'envoie_information_admin');
add_action('wp_ajax_nopriv_envoie_information_admin', 'envoie_information_admin');
function envoie_information_admin() {
    global $wpdb;
    //$demande_id = $_POST['id_demande'];
    //$demande_id = $_POST['select_forwarder'];
    $demande_id = explode("___",$_POST['select_forwarder']);
    
$querys23 = "SELECT * from ".$wpdb->prefix."services where reference = '".$demande_id[0]."'";
    //var_dump($querys23);die();
    $datas = $wpdb->get_results($querys23, OBJECT);
    
    foreach($datas as $dat){
        $email_admin = get_option('admin_email');
        $headere = array('Content-type: text/html');
        $content = "Bonjour, <br><br> ";
        $content  .= " Un client a validé une dévis voici son information :<br><br> ";
        $content  .= " Réference :<b>".$dat->reference."</b><br><br> ";
        $infos_user = get_userdata($dat->id_client);
        $tel = get_user_meta($dat->id_client, "billing_phone", true);
        $content  .= " Nom  : <b>".$infos_user->first_name ."</b><br><br> ";
        $content  .= " Prénom  : <b>".$infos_user->last_name."</b><br><br> ";
        $content  .= " Télephone  : <b>".$tel."</b><br><br> ";
        $content  .= " Email : <b>".$infos_user->user_email."</b><br><br> ";
        wp_mail($email_admin, "Confirmation demande de dévis client", $content, $headere);
        wp_mail($infos_user->user_email, "Confirmation demande de dévis", $content, $headere);
        $wpdb->update( 
                $wpdb->prefix.'services',
                array( 
                    'status'  => 2,
                    'status'  => $demande_id[1],
                ), 
                array( 'id' => $demande_id )
            );
        $wpdb->update( 
                $wpdb->prefix.'devis_forwarder',
                array( 
                    'status'  => 2,
                ), 
                array( 'reference_user' => $dat->reference )
            );
    }
    
}*/