<?php 
        global $wpdb;
        
        $pagenames = get_query_var("pagename");
        if($type_user == "client"){
?>
<!--div style="display: flex;"-->
<div class="row" >
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-body style_paddin_pannels_custom">
               <ul class="navs_custom">
                    <li>
                        <a class="<?php echo ($pagenames == "ancienne-transaction") ? "active":""; ?>" href="<?php echo get_site_url()."/ancienne-transaction"; ?>">
                          <?php 
                              echo __("<!--:fr-->Ancienne transaction<!--:--><!--:en-->Old transaction<!--:-->");
                          ?>
                        </a>
                    </li>
                    <li>
                        <a class="<?php echo ($pagenames == "services-10") ? "active":""; ?>" href="<?php echo get_site_url()."/services-10"; ?>">
                          <?php 
                              echo __("<!--:fr-->Nouvelle transaction<!--:--><!--:en-->New transaction<!--:-->");
                          ?>
                        </a>
                    </li>
                    <li>
                        <a class="<?php echo ($pagenames == "demande-enregistrer") ? "active":""; ?>" href="<?php echo get_site_url()."/demande-enregistrer"; ?>">

                          <?php 
                              echo __("<!--:fr-->Demande enregistrée<!--:--><!--:en-->Registered application<!--:-->");
                          ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php if($pagenames == "services-10"){ ?>
        <div class="col-md-8">
            
            <?php 
          
            if( !empty($_POST['categorie_service'])):
            ?>
                <!--div class="alert alert-info">
                    <?php 
                       // echo __("<!--:fr--><strong>Succès </strong> Nouvelle transaction effectué avec succès.<!--:--><!--:en--><strong> Success </ strong> New transaction completed successfully.<!--:-->");
                    ?>
                </div-->
            <?php 
                endif;
            ?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                  <?php  echo __("<!--:fr-->Nouvelle transaction<!--:--><!--:en-->New transaction<!--:-->");  ?>
                </div>
                <div class="panel-body">
                    <!--div class="w3-card-4"-->
                    <div class="">
                        <!--form class="w3-container" method="post" style="margin-bottom: 12px;"-->
                        <div class="alert alert-danger">
                            
                            <?php 
                                echo __("<!--:fr-->Vous êtes tenu comme seul responsable sur les informations que vous remplissez dans le formulaire.<!--:--><!--:en-->You are held solely responsible for the information you fill in the form.<!--:-->");
                            ?>
                        </div>
                        <form class="w3-container" method="post" style="margin-bottom: 12px;">
                            <p style="height:40px!important;">
                                <button class="w3-btn w3-brown btn btn-primary" style="padding: 13px;float:right!important;"><?php  echo __("<!--:fr-->Sauvegarder<!--:--><!--:en-->Save<!--:-->");  ?></button>
                                <span style="float:right!important;margin-right: 18%;margin-top: 11px;">
                                    <input type="text" name="references" value="<?php echo randomCaracters(9) ; ?>" readonly="readonly" style="background-color:#ffffff!important;border: none;">
                                </span>
                            </p>
                            <!--p>      
                                <label class="w3-text-brown"><b><?php  //echo __("<!--:fr-->Catégorie<!--:--><!--:en-->Category<!--:-->");  ?></b></label>
                                <select class="w3-select w3-border form-control"  name="categorie_service[]" multiple="multiple" required="required">
                                    <?php //foreach(getAllDataCategories() as $res): ?>
                                        <option value="<?php //echo $res->id; ?>"><?php //echo $res->libelle_categ; ?></option>
                                    <?php //endforeach; ?>
                                </select>
                            </p-->
                            <p>    
                                <label class="radio-inline">
                                    <input type="radio" name="type_soc" value='societe' required="required"><?php echo __("<!--:fr-->société<!--:--><!--:en-->company<!--:-->"); ?>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="type_soc" value='particulier' required="required"><?php echo __("<!--:fr-->particulier<!--:--><!--:en-->particular<!--:-->"); ?>
                                </label>
                            </p>
                            <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Expresse<!--:--><!--:en-->Express<!--:-->");  ?></b></label>
                                <select class="w3-select w3-border" name="express" required="required">
                                    <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                    <option value="Oui">Oui</option>
                                    <option value="Non">Non</option>
                                </select>
                            </p>
                            <div class="row">
                                <div class="col-md-6">
                                    <u><b style="font-size: 19px;font-weight: bold;"><?php  echo __("<!--:fr-->Lieu d'enlèvement<!--:--><!--:en-->Pickup location<!--:-->");  ?></b></u>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Pays<!--:--><!--:en-->Country<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="pays_chargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Ville<!--:--><!--:en-->City<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="ville_chargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Adresse<!--:--><!--:en-->Adress<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="adresse_chargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Numéros télephone<!--:--><!--:en-->Phone number<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="num_tel_chargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Date de chargement<!--:--><!--:en-->Loading date<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control dt_picks" id="dt_picks" name="date_chargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Heure souhaité<!--:--><!--:en-->Desired time<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control time_picks" name="heure_chargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Contrainte<!--:--><!--:en-->Constraint<!--:-->");  ?> </b></label>
                                        <select class="w3-select w3-border" name="contrainte_chargement" required="required">
                                            <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                            <option value="escalier"><?php  echo __("<!--:fr-->escalier<!--:--><!--:en-->staircase<!--:-->");  ?></option>
                                            <option value="ascenseur"><?php  echo __("<!--:fr-->ascenseur<!--:--><!--:en-->elevator<!--:-->");  ?></option>
                                            <option value="quai de chargement"><?php  echo __("<!--:fr-->quai de chargement<!--:--><!--:en-->loading dock<!--:-->");  ?></option>
                                            <option value="autre"><?php  echo __("<!--:fr-->autre<!--:--><!--:en-->other<!--:-->");  ?></option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <u><b style="font-size: 19px;font-weight: bold;"><?php  echo __("<!--:fr-->Lieu de livraison<!--:--><!--:en-->Delivery place<!--:-->");  ?></b></u>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Pays<!--:--><!--:en-->Country<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="pays_dechargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Ville<!--:--><!--:en-->City<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="ville_dechargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Adresse<!--:--><!--:en-->Adress<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="adresse_dechargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Numéros télephone<!--:--><!--:en-->Phone number<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="num_tel_dechargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Date de déchargement<!--:--><!--:en-->Loading date<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control dt_picksd" id="dt_picksd" name="date_dechargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Heure souhaité<!--:--><!--:en-->Desired time<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control time_picks" name="heure_dechargement" type="text" required="required">
                                    </p>
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Contrainte<!--:--><!--:en-->Constraint<!--:-->");  ?></b></label>
                                        <select class="w3-select w3-border" name="contrainte_dechargement" required="required">
                                            <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                            <option value="escalier"><?php  echo __("<!--:fr-->escalier<!--:--><!--:en-->staircase<!--:-->");  ?></option>
                                            <option value="ascenseur"><?php  echo __("<!--:fr-->ascenseur<!--:--><!--:en-->elevator<!--:-->");  ?></option>
                                            <option value="quai de chargement"><?php  echo __("<!--:fr-->quai de chargement<!--:--><!--:en-->loading dock<!--:-->");  ?></option>
                                            <option value="autre"><?php  echo __("<!--:fr-->autre<!--:--><!--:en-->other<!--:-->");  ?></option>
                                        </select>
                                    </p>
                                </div>
                            </div>
                            <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Accepter le sous-traitant<!--:--><!--:en-->Accept the subcontractor<!--:-->");  ?> </b></label>
                                <select class="w3-select w3-border" name="accepter_sous_traitant" required="required">
                                    <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                    <option value="Oui"><?php  echo __("<!--:fr-->Oui<!--:--><!--:en-->Yes<!--:-->");  ?></option>
                                    <option value="Non"><?php  echo __("<!--:fr-->Non<!--:--><!--:en-->No<!--:-->");  ?></option>
                                </select>
                            </p>
                            <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Emballage<!--:--><!--:en-->Packaging<!--:-->");  ?> </b></label>
                                <select class="w3-select w3-border" name="emballage" required="required">
                                    <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                    <option value="enveloppe"><?php  echo __("<!--:fr-->Enveloppe<!--:--><!--:en-->Envelope<!--:-->");  ?> </option>
                                    <option value="carton"><?php  echo __("<!--:fr-->Carton<!--:--><!--:en-->Cardboard<!--:-->");  ?></option>
                                    <option value="caisse"><?php  echo __("<!--:fr-->Caisse<!--:--><!--:en-->Box<!--:-->");  ?></option>
                                    <option value="container"><?php  echo __("<!--:fr-->Récipient<!--:--><!--:en-->Container<!--:-->");  ?></option>
                                    <option value="autre"><?php  echo __("<!--:fr-->Autre<!--:--><!--:en-->Other<!--:-->");  ?></option>
                                </select>
                                <!--input class="w3-input w3-border w3-sand form-control" name="emballage" type="text" required="required"-->
                            </p>
                            <div class="row">
                                <div class="col-md-3">
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Hauteur<!--:--><!--:en-->Height<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="hauteur" required="required" type="number" placeholder="En cm(ex: 200)" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Longueur<!--:--><!--:en-->Length<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="longueur" required="required" type="number" placeholder="En cm(ex: 300)" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Largeur<!--:--><!--:en-->Width<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="largeur" required="required" type="number" placeholder="En cm(ex: 200)" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <p>      
                                        <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Poids<!--:--><!--:en-->Weight<!--:-->");  ?></b></label>
                                        <input class="w3-input w3-border w3-sand form-control" name="poids" required="required"  type="text" placeholder="En kg(ex: 2.5)" onkeypress="return isNumberKey(event,this)">
                                    </p>
                                </div>

                            </div>

                            <!--p>      
                                <label class="w3-text-brown"><b>Types de matériaux</b></label>
                                <input class="w3-input w3-border w3-sand form-control" name="type_materiaux" type="text">
                            </p-->

                            <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Matières dangereuses<!--:--><!--:en-->Hazardous Material<!--:-->");  ?></b></label>
                                <select class="w3-select w3-border" name="matiere_dangereux" required="required">
                                    <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                    <option value="Oui"><?php  echo __("<!--:fr-->Oui<!--:--><!--:en-->Yes<!--:-->");  ?></option>
                                    <option value="Non"><?php  echo __("<!--:fr-->Non<!--:--><!--:en-->No<!--:-->");  ?></option>
                                </select>
                            </p>
                            <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Fragile<!--:--><!--:en-->Fragile<!--:-->");  ?></b></label>
                                <select class="w3-select w3-border" name="fragile" required="required">
                                    <option value="">--- <?php  echo __("<!--:fr-->choisir<!--:--><!--:en-->choose<!--:-->");  ?> ---</option>
                                    <option value="Oui"><?php  echo __("<!--:fr-->Oui<!--:--><!--:en-->Yes<!--:-->");  ?></option>
                                    <option value="Non"><?php  echo __("<!--:fr-->Non<!--:--><!--:en-->No<!--:-->");  ?></option>
                                </select>
                            </p>
                            <p style="height:40px!important;">
                                <button class="w3-btn w3-brown btn btn-primary" style="padding: 13px;float:right!important;"><?php  echo __("<!--:fr-->Envoyer<!--:--><!--:en-->Send<!--:-->");  ?></button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php }elseif($pagenames == "demande-enregistrer"){ 
            $queryService = "SELECT id,status,reference,date_creation,categories from ".$wpdb->prefix."services where id_client = ".get_current_user_id()." and status in (0,1) order by id desc";
            $allServicesUsers = $wpdb->get_results($queryService, OBJECT);
        ?>
        <!--div style="margin-left: 18px;width: 75%;"-->
        <div class="col-md-9">
            <!-- Trigger the modal with a button -->
        <!--button type="button" class="btn btn-info btn-lg" data-target="#selection-devis" data-toggle="modal">Open Modal</button-->
        <div class="modal fade " id="selection-devis" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body" id="modal-body-res-valider">
                  <div class="quotation-box-1">
                    <h2><?php  echo __("<!--:fr-->Choisir le transitaire<!--:--><!--:en-->Choose forwarder<!--:-->");  ?></h2><br>
                    <div class="desc-text">

                    </div>
                    <form method="post" id="popup-qoute-form">
                        <div class="row clearfix"> 
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                              <select class="w3-select w3-border" id="select-rlocation-qoute" name="select_forwarder" required >
                                  
                              </select>
                            </div>
                            <!--Form Group-->
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
                                <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i></a> 
                                <input type="submit" class="custom-button light" id="sb_submit_qoute" value="enregistrer">
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-body" >
                <table id="liste-demande" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th><?php  echo __("<!--:fr-->Date création<!--:--><!--:en-->Creation Date<!--:-->");  ?></th>
                            <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?></th>
                            <th><?php  echo __("<!--:fr-->Status<!--:--><!--:en-->Statut<!--:-->");  ?></th>
                            <th><?php  echo __("<!--:fr-->Action<!--:--><!--:en-->Action<!--:-->");  ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($allServicesUsers as $user_detail): ?>
                            <tr>
                                <td><?php echo date('d-m-Y h:i:s', strtotime($user_detail->date_creation)); ?></td>
                                <td><?php echo $user_detail->reference; ?></td>
                                <td><?php 
                                if($user_detail->status == 0){
                                    echo "Non traité";
                                }elseif($user_detail->status == 1){
                                    echo "En cours de traitement";
                                }else{
                                    echo "Terminer";
                                }
                                ?></td>
                                <td><button class="btn btn-success" <?php echo ($user_detail->status == 0 || $user_detail->status == 2)? 'disabled="disabled"':""  ?> style="padding-top: 9px; padding-bottom: 7px;" onclick="envoyerDevisAdmin('<?php echo $user_detail->id; ?>','<?php echo $user_detail->reference; ?>')"><?php  echo __("<!--:fr-->Valider<!--:--><!--:en-->Validate<!--:-->");  ?></button></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    <?php }elseif($pagenames == "ancienne-transaction"){ 
        $queryService2 = "SELECT id,status,reference,date_creation,categories from ".$wpdb->prefix."services where id_client = ".get_current_user_id()." and status = 2  order by id desc";
            $allServicesUsers2 = $wpdb->get_results($queryService2, OBJECT);
        ?>
    <!--div style="margin-left: 18px;width: 75%;"-->
    <div class="col-md-9">
        <div class="panel panel-primary">
            <div class="panel-body">
                <table id="liste-demande-ancien" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th><?php  echo __("<!--:fr-->Date création<!--:--><!--:en-->Creation date<!--:-->");  ?></th>
                            <th><?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Rerefence<!--:-->");  ?></th>
                            <th><?php  echo __("<!--:fr-->Status<!--:--><!--:en-->Statut<!--:-->");  ?></th>
                            <!--th>Action</th-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($allServicesUsers2 as $user_detail): ?>
                            <tr>
                                <td><?php echo $user_detail->date_creation; ?></td>
                                <td><?php echo $user_detail->reference; ?></td>
                                <td><?php 
                                if($user_detail->status == 0){
                                    echo "Non traité";
                                }elseif($user_detail->status == 1){
                                    echo "En cours de traitement";
                                }else{
                                    echo "Terminer";
                                }
                                ?></td>
                                <!--td><button class="btn btn-success" <?php echo ($user_detail->status == 0 || $user_detail->status == 2)? 'disabled="disabled"':""  ?> style="padding-top: 9px; padding-bottom: 7px;" onclick="envoyerDevisAdmin('<?php echo $user_detail->id; ?>','<?php echo $user_detail->reference; ?>')">Valider</button></td-->
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php } ?>
</div>

<?php 
        }else{
?>
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-body style_paddin_pannels_custom">
                <ul class="navs_custom">
                    <li><a  href="<?php echo get_site_url()."/services-10"; ?>" class="<?php echo ($pagenames == "services-10") ? "active":""; ?>"><?php  echo __("<!--:fr-->Nouveau devis<!--:--><!--:en-->New quote<!--:-->"); ?> </a></li>
                    <li><a href="<?php echo get_site_url()."/liste-devis"; ?>" class="<?php echo ($pagenames == "liste-devis") ? "active":""; ?>"><?php  echo __("<!--:fr-->Liste dévis<!--:--><!--:en-->Quote list<!--:-->"); ?></a></li>
                  </ul>
            </div>
        </div>
        
    </div>
    
        <?php if($pagenames == "services-10"){ ?>
        <div class="col-md-8">
            <div class="w3-card-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <?php  echo __("<!--:fr-->Nouveau devis<!--:--><!--:en-->New quote<!--:-->"); ?>
                    </div>
                    <div class="panel-body">
                        <?php 
                        $cartegorie_service_forward = get_user_meta(get_current_user_id(), "categorie_service", true); 
                        $queryDevis = "SELECT id,status,reference,date_creation,categories from ".$wpdb->prefix."services order by id desc";
                        $allDevisUsers = $wpdb->get_results($queryDevis, OBJECT);
                        $arr_reference = array();
                        foreach($allDevisUsers as $allDevs){
                            $categ_forwards = (explode(",",$cartegorie_service_forward));
                            $categ_recommande_user = (explode(",",$allDevs->categories));
                            $arr_inter = array_intersect($categ_forwards, $categ_recommande_user);
                            //if(!empty($arr_inter)){
                                array_push($arr_reference, $allDevs->reference);
                            //}
                        }
                        //var_dump($arr_reference);
                        ?>
                        <form class="w3-container" method="post" style="margin-bottom: 12px;">
                             <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Réference client<!--:--><!--:en-->Customer reference<!--:-->"); ?></b></label>
                                <select class="w3-select w3-border" name="reference_client_forward" required="required">
                                    <option value="">--- <?php  echo __("<!--:fr-->Choisir<!--:--><!--:en-->Choose<!--:-->");  ?> ---</option>
                                    <?php foreach($arr_reference as $refs): ?>
                                        <option value="<?php echo $refs; ?>"><?php echo $refs; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </p>
                            <p>      
                                <label class="w3-text-brown"><b><?php  echo __("<!--:fr-->Prix de devis<!--:--><!--:en-->Quote price<!--:-->");  ?></b></label>
                                <input class="w3-input w3-border w3-sand form-control" name="prix_devis" type="number" required="required">
                            </p>
                            <!--p>      
                                <label class="w3-text-brown"><b><?php  // echo __("<!--:fr-->Note<!--:--><!--:en-->Note<!--:-->");  ?></b></label>
                                <textarea class="form-control" rows="5" id="note_forward" name="note_forward" required="required"></textarea>
                            </p-->
                            <p>
                                <button class="w3-btn w3-brown btn btn-primary" style="padding: 13px;"><?php  echo __("<!--:fr-->Enregistrer<!--:--><!--:en-->Save<!--:-->");  ?></button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php }elseif($pagenames == "liste-devis"){ 
            
            $queryServiceForward = "SELECT * from ".$wpdb->prefix."devis_forwarder where id_forwarder = ".get_current_user_id()." order by id desc";
            $allServicesForward = $wpdb->get_results($queryServiceForward, OBJECT);
            ?>
            <div class="col-md-9">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <table id="liste-devis-forward" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>
                                        <?php  echo __("<!--:fr-->Date création<!--:--><!--:en-->Creation date<!--:-->");  ?>
                                    </th>
                                    <th>
                                        <?php  echo __("<!--:fr-->Réference<!--:--><!--:en-->Reference<!--:-->");  ?>
                                    </th>
                                    <th><?php  echo __("<!--:fr-->Déscription<!--:--><!--:en-->Description<!--:-->");  ?></th>
                                    <th><?php  echo __("<!--:fr-->Status<!--:--><!--:en-->Statut<!--:-->");  ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($allServicesForward as $user_detail): ?>
                                    <tr>
                                        <td><?php echo $user_detail->date_creation; ?></td>
                                        <td><?php echo $user_detail->reference_user; ?></td>
                                        <td><?php echo $user_detail->note_forwarder; ?></td>
                                        <td><?php 
                                        if($user_detail->status == 1){
                                            echo "En cours de traitement";
                                        }else{
                                            echo "Terminé";
                                        }
                                        ?></td>

                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
    
</div>
<?php 
        }
?>
<script>
    jQuery(document).ready(function(){
        
        
        jQuery("#liste-demande-ancien").DataTable({
            "language" : {
                "lengthMenu": "<?php  echo __("<!--:fr-->Afficher<!--:--><!--:en-->Display<!--:-->");  ?> _MENU_ <?php  echo __("<!--:fr-->enregistrement par page<!--:--><!--:en-->records per page<!--:-->");  ?>",
                "zeroRecords": "<?php  echo __("<!--:fr-->Aucun enregistrement<!--:--><!--:en-->Nothing found - sorry<!--:-->");  ?>",
                "info": "<?php  echo __("<!--:fr-->Voir page<!--:--><!--:en-->Showing page<!--:-->");  ?> _PAGE_ <?php  echo __("<!--:fr-->de<!--:--><!--:en-->of<!--:-->");  ?> _PAGES_",
                "infoEmpty": "<?php  echo __("<!--:fr-->Aucun enregistrement trouvé<!--:--><!--:en-->No records available<!--:-->");  ?>",
                "infoFiltered": "<?php  echo __("<!--:fr-->Filtrer parmis<!--:--><!--:en-->(filtered from<!--:-->");  ?> _MAX_ <?php  echo __("<!--:fr-->total enregistrement<!--:--><!--:en-->total records<!--:-->");  ?>)",
                "search":         "<?php  echo __("<!--:fr-->Rechercher:<!--:--><!--:en-->Search:<!--:-->");  ?>",
                "paginate": {
                    "first":      "<?php  echo __("<!--:fr-->Premier<!--:--><!--:en-->First<!--:-->");  ?>",
                    "last":       "<?php  echo __("<!--:fr-->Dérnier<!--:--><!--:en-->Last<!--:-->");  ?>",
                    "next":       "<?php  echo __("<!--:fr-->Suivant<!--:--><!--:en-->Next<!--:-->");  ?>",
                    "previous":   "<?php  echo __("<!--:fr-->Précedent<!--:--><!--:en-->Previous<!--:-->");  ?>"
                },
            }
        });
        jQuery("#liste-demande").DataTable({
            "language" : {
                "lengthMenu": "<?php  echo __("<!--:fr-->Afficher<!--:--><!--:en-->Display<!--:-->");  ?> _MENU_ <?php  echo __("<!--:fr-->enregistrement par page<!--:--><!--:en-->records per page<!--:-->");  ?>",
                "zeroRecords": "<?php  echo __("<!--:fr-->Aucun enregistrement<!--:--><!--:en-->Nothing found - sorry<!--:-->");  ?>",
                "info": "<?php  echo __("<!--:fr-->Voir page<!--:--><!--:en-->Showing page<!--:-->");  ?> _PAGE_ <?php  echo __("<!--:fr-->de<!--:--><!--:en-->of<!--:-->");  ?> _PAGES_",
                "infoEmpty": "<?php  echo __("<!--:fr-->Aucun enregistrement trouvé<!--:--><!--:en-->No records available<!--:-->");  ?>",
                "infoFiltered": "<?php  echo __("<!--:fr-->Filtrer parmis<!--:--><!--:en-->(filtered from<!--:-->");  ?> _MAX_ <?php  echo __("<!--:fr-->total enregistrement<!--:--><!--:en-->total records<!--:-->");  ?>)",
                "search":         "<?php  echo __("<!--:fr-->Rechercher:<!--:--><!--:en-->Search:<!--:-->");  ?>",
                "paginate": {
                    "first":      "<?php  echo __("<!--:fr-->Premier<!--:--><!--:en-->First<!--:-->");  ?>",
                    "last":       "<?php  echo __("<!--:fr-->Dérnier<!--:--><!--:en-->Last<!--:-->");  ?>",
                    "next":       "<?php  echo __("<!--:fr-->Suivant<!--:--><!--:en-->Next<!--:-->");  ?>",
                    "previous":   "<?php  echo __("<!--:fr-->Précedent<!--:--><!--:en-->Previous<!--:-->");  ?>"
                },
            }
        });
        jQuery("#liste-devis-forward").DataTable({
            "language" : {
                "lengthMenu": "<?php  echo __("<!--:fr-->Afficher<!--:--><!--:en-->Display<!--:-->");  ?> _MENU_ <?php  echo __("<!--:fr-->enregistrement par page<!--:--><!--:en-->records per page<!--:-->");  ?>",
                "zeroRecords": "<?php  echo __("<!--:fr-->Aucun enregistrement<!--:--><!--:en-->Nothing found - sorry<!--:-->");  ?>",
                "info": "<?php  echo __("<!--:fr-->Voir page<!--:--><!--:en-->Showing page<!--:-->");  ?> _PAGE_ <?php  echo __("<!--:fr-->de<!--:--><!--:en-->of<!--:-->");  ?> _PAGES_",
                "infoEmpty": "<?php  echo __("<!--:fr-->Aucun enregistrement trouvé<!--:--><!--:en-->No records available<!--:-->");  ?>",
                "infoFiltered": "<?php  echo __("<!--:fr-->Filtrer parmis<!--:--><!--:en-->(filtered from<!--:-->");  ?> _MAX_ <?php  echo __("<!--:fr-->total enregistrement<!--:--><!--:en-->total records<!--:-->");  ?>)",
                "search":         "<?php  echo __("<!--:fr-->Rechercher:<!--:--><!--:en-->Search:<!--:-->");  ?>",
                "paginate": {
                    "first":      "<?php  echo __("<!--:fr-->Premier<!--:--><!--:en-->First<!--:-->");  ?>",
                    "last":       "<?php  echo __("<!--:fr-->Dérnier<!--:--><!--:en-->Last<!--:-->");  ?>",
                    "next":       "<?php  echo __("<!--:fr-->Suivant<!--:--><!--:en-->Next<!--:-->");  ?>",
                    "previous":   "<?php  echo __("<!--:fr-->Précedent<!--:--><!--:en-->Previous<!--:-->");  ?>"
                },
            }
        });
        
        var datepickersOpt = {
            dateFormat: 'dd/mm/yy'
        };
        
        jQuery("#dt_picks").datepicker(jQuery.extend({
            onSelect: function() {
                var minDate = jQuery(this).datepicker('getDate');
                minDate.setDate(minDate.getDate()+0); //add two days
                jQuery("#dt_picksd").datepicker( "option", "minDate", minDate);
            }
        },datepickersOpt));

        jQuery("#dt_picksd").datepicker(jQuery.extend({
            onSelect: function() {
                var maxDate = jQuery(this).datepicker('getDate');
                maxDate.setDate(maxDate.getDate()-0);
                jQuery("#dt_picks").datepicker( "option", "maxDate", maxDate);
            }
        },datepickersOpt));
       
        
        jQuery(".time_picks").clockpicker({
            donetext: 'Choisir'
        });
    });
    function envoyerDevisAdmin(id_demande,ref_client){
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'get_info_by_reference',id_demande : id_demande,ref_client:ref_client},
            success: function (res) {
                //var fields = res.split('____');
                jQuery('#selection-devis').find('#select-rlocation-qoute').html(res);
                //jQuery('#selection-devis').find('#popup-qoute-form').html(fields[1]);
                jQuery('#selection-devis').modal('show');
            }
        });
        
        /*if(confirm("Voulez vous envoyer ce demande à l'administrateur")){
            jQuery.ajax({
                type: "POST",
                url: '<?php //echo get_site_url(); ?>/wp-admin/admin-ajax.php',
                data: {action : 'envoie_information_admin',id_demande : id_demande},
                success: function (res) {
                    if (res) {
                        window.location.href = "<?php echo get_site_url();?>/demande-enregistrer";
                    }
                }
            });
        }*/
    }
    
        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    
</script>
<?php


