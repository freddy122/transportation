<?php

function getAllDataCategories(){
    global $wpdb;
    $categ_service = $wpdb->get_results( 
	"
            SELECT *
            FROM wp_category_service
	"
    );
    return $categ_service;
   
}

add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
function wooc_extra_register_fields() {
?>

    
    <p class="form-row form-row-wide">    
        <label class="w3-text-brown"><b>

                     <?php _e( 'Type utilisateur', 'woocommerce' ); ?> 
         <span class="required">
             *
         </span>
         </b>
        </label>
        <select class="w3-select w3-border" name="type_user" id="type_user"  required="required">
            <option value="">--- choisir ---</option>
            <option value="client">Client</option>
            <option value="forwarder">Forwarder</option>
        </select>
    </p>
    <div class="display_info" style="display: none;">
        <p class="form-row form-row-wide" id="div_categ_service" style="display:none;">
            <label for="reg_pseudo">
                <?php _e( 'Catégorie', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label><?php //var_dump(getAllDataCategories());die(); ?> 
            <select class="w3-select w3-border" name="categorie_service[]" id="categorie_service" multiple="multiple">
                <?php foreach(getAllDataCategories() as $res): ?>
                    <option value="<?php echo $res->id; ?>"><?php echo $res->libelle_categ; ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p class="form-row form-row-wide">
            <label for="user_login">
                <?php _e( 'Nom utilisateur', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="user_login" id="user_login" />
        </p>
        <p class="form-row form-row-wide">
            <label for="firstname_user">
                <?php _e( 'Nom', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="firstname_user" id="firstname_user" />
        </p>
        <p class="form-row form-row-wide">
            <label for="lastname_user">
                <?php _e( 'Prénom', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="lastname_user" id="lastname_user" />
        </p>

        <p class="form-row form-row-wide" id="nom_entreprise" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Nom de l\'entreprise', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text" name="nom_entreprise" id="nom_entrepriset" />
        </p>

        <p class="form-row form-row-wide" id="num_enreg_soc" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Numéro d\'enregistrement de la société', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text" name="num_enreg_soc" id="num_enreg_soct" />
        </p>

        <p class="form-row form-row-wide" id="num_compte" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Numéro de compte', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text" name="num_compte" id="num_comptet" />
        </p>

        <p class="form-row form-row-wide" id="transaction_nationale" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Transaction nationale', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border" name="transaction_nationale" id="transaction_nationalet" >
                <option value="oui">Oui</option>
                <option value="non">Non</option>
            </select>
        </p>

        <p class="form-row form-row-wide" id="chargement_equipement" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Chargement de l\'équipement', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border"  name="chargement_equipement[]" id="chargement_equipementt" multiple="multiple">
                <option value="a">a</option>
                <option value="b">b</option>
                <option value="c">c</option>
                <option value="d">d</option>
            </select>
        </p>

        <p class="form-row form-row-wide" id="type_vehicule" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Types de véhicules', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
             <select class="w3-select w3-border"  name="type_vehicule[]" id="type_vehiculet" multiple="multiple">
                <option value="type 1">type 1</option>
                <option value="type 2">type 2</option>
                <option value="type 3">type 3</option>
                <option value="type 4">type 4</option>
            </select>
        </p>
        <p class="form-row form-row-wide" id="matiere_danger" style="display:none;">
            <label for="lastname_user">
                <?php _e( 'Matières dangereuses', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border"  name="matiere_danger" id="matiere_dangert" >
                <option value="oui">Oui</option>
                <option value="non">Non</option>
            </select>
        </p>
        
        <p class="form-row form-row-wide">
            <label for="adresse_user">
                <?php _e( 'Adresse', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="adresse_user" id="adresse_user" />
        </p>
        <p class="form-row form-row-wide">
            <label for="ville_user">
                <?php _e( 'Ville', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="ville_user" id="ville_user" />
        </p>
        <p class="form-row form-row-wide">
            <label for="pays_user">
                <?php _e( 'Pays', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="pays_user" id="pays_user" />
        </p>
        <p class="form-row form-row-wide">
            <label for="tel_user">
                <?php _e( 'Télephone', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text" name="tel_user" id="tel_user" />
        </p>
        <p class="form-row form-row-wide" id="credit_card_user">
            <label for="credit_card_user">
                <?php _e( 'Numéro de carte de crédit', 'woocommerce' ); ?> 
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text" name="credit_card_user" id="credit_card_usert" />
        </p>
    </div>
<?php    
}

/*enregistrement*/
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );
function wooc_save_extra_register_fields( $customer_id ) {
    global $wpdb;
    
    if ( isset( $_POST['user_login'] ) ) {
        $wpdb->update(
                $wpdb->users, 
                array(
                    'user_login' => $_POST['user_login']
                ), 
                array(
                    'ID' => $customer_id
                )
        );
    }
    
    if ( isset( $_POST['type_user'] ) ) {
        update_user_meta( $customer_id, 'type_user', ( $_POST['type_user'] ) );
    }
    if ( isset( $_POST['categorie_service'] ) ) {
        update_user_meta( $customer_id, 'categorie_service', implode(',', $_POST['categorie_service'] ) );
    }
    if ( isset( $_POST['nom_entreprise'] ) ) {
        update_user_meta( $customer_id, 'nom_entreprise', sanitize_text_field( $_POST['nom_entreprise'] ) );
    }
    if ( isset( $_POST['num_enreg_soc'] ) ) {
        update_user_meta( $customer_id, 'num_enreg_soc', sanitize_text_field( $_POST['num_enreg_soc'] ) );
    }
    if ( isset( $_POST['num_compte'] ) ) {
        update_user_meta( $customer_id, 'num_compte', sanitize_text_field( $_POST['num_compte'] ) );
    }
    if ( isset( $_POST['transaction_nationale'] ) ) {
        update_user_meta( $customer_id, 'transaction_nationale', sanitize_text_field( $_POST['transaction_nationale'] ) );
    }
    if ( isset( $_POST['chargement_equipement'] ) ) {
        
        update_user_meta( $customer_id, 'chargement_equipement', implode(',', $_POST['chargement_equipement'] ) );
    }
    if ( isset( $_POST['type_vehicule'] ) ) {
        update_user_meta( $customer_id, 'type_vehicule', implode(',', $_POST['type_vehicule'] ) );
    }
    
    if ( isset( $_POST['matiere_danger'] ) ) {
        update_user_meta( $customer_id, 'matiere_danger', sanitize_text_field( $_POST['matiere_danger'] ) );
    }
    
    if ( isset( $_POST['firstname_user'] ) ) {
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['firstname_user'] ) );
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['firstname_user'] ) );
    }
    
    if ( isset( $_POST['lastname_user'] ) ) {
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['lastname_user'] ) );
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['lastname_user'] ) );
    }
    
    if ( isset( $_POST['adresse_user'] ) ) {
        update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['adresse_user'] ) );
    }
    
    if ( isset( $_POST['ville_user'] ) ) {
        update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['ville_user'] ) );
    }
    
    if ( isset( $_POST['pays_user'] ) ) {
        update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['pays_user'] ) );
    }
    
    if ( isset( $_POST['tel_user'] ) ) {
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['tel_user'] ) );
    }
    
    if ( isset( $_POST['credit_card_user'] ) ) {
        update_user_meta( $customer_id, 'credit_card_user', sanitize_text_field( $_POST['credit_card_user'] ) );
    }
    
}

function randomCaracters($l)
{
    $caracter        = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789');
    $specialCaracter = str_split('@');
    shuffle($caracter); 
    shuffle($specialCaracter);     
    $rand = '';
    $mergedCaracter = array();
    foreach (array_rand($caracter, ($l-1)) as $k) $mergedCaracter[] = $caracter[$k];
    $mergedCaracter[] = $specialCaracter[array_rand($specialCaracter, 1)];
    shuffle($mergedCaracter); 
    foreach (array_rand($mergedCaracter, $l) as $i) $rand .= $mergedCaracter[$i];
    return $rand;
}


