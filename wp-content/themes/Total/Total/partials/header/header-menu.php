<?php
/**
 * Header menu template part.
 *
 * @package Total WordPress Theme
 * @subpackage Partials
 * @version 4.6
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Menu Location
$menu_location = wpex_header_menu_location();

// Custom menu
$custom_menu = wpex_custom_menu();

// Multisite global menu
$ms_global_menu = apply_filters( 'wpex_ms_global_menu', false );


// Display menu if defined
if ( has_nav_menu( $menu_location ) || $custom_menu || $ms_global_menu ) :

	// Menu arguments
	$menu_args = apply_filters( 'wpex_header_menu_args', array(
		'theme_location' => $menu_location,
		'menu_class'     => wpex_header_menu_ul_classes(),
		'container'      => false,
		'fallback_cb'    => false,
		'link_before'    => '<span class="link-inner">',
		'link_after'     => '</span>',
		'walker'         => new WPEX_Dropdown_Walker_Nav_Menu(),
	) );

	// Check for custom menu
	if ( $custom_menu ) {
		$menu_args['menu'] = $custom_menu;
	} ?>

	<?php wpex_hook_main_menu_before(); ?>

	<div id="site-navigation-wrap" class="<?php echo wpex_header_menu_classes( 'wrapper' ); ?>">

		<nav id="site-navigation" class="<?php echo wpex_header_menu_classes( 'inner' ); ?>"<?php wpex_schema_markup( 'site_navigation' ); ?><?php wpex_aria_landmark( 'site_navigation' ); ?>>

			<?php //wpex_hook_main_menu_top(); ?>

				<?php
                                
                                $pgname = get_query_var("pagename");
				/*// Display global multisite menu
				if ( is_multisite() && $ms_global_menu ) :
					
					switch_to_blog( 1 );  
					wp_nav_menu( $menu_args );
					restore_current_blog();

				// Display this site's menu
				else :

					wp_nav_menu( $menu_args );

				endif;*/ ?>

			<?php //wpex_hook_main_menu_bottom(); ?>
                        <ul id="menu-menu-haut" class="dropdown-menu sf-menu sf-js-enabled" style="touch-action: pan-y;">
                            <li id="menu-item-25" class="<?php echo ($pgname ==""? "current-menu-item":"") ;?> menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-25">
                                <a href="<?php echo get_site_url() ;?>">
                                    <span class="link-inner">Accueil</span>
                                </a>
                            </li>
                            <li id="menu-item-24" class="<?php echo ($pgname =="contact"? "current-menu-item":"") ;?>  menu-item menu-item-type-post_type menu-item-object-page  page_item page-item-12 current_page_item menu-item-24">
                                <a href="<?php echo get_site_url() ;?>/contact/">
                                    <span class="link-inner">Contact</span></a>
                            </li>
                            <li id="menu-item-23" class="<?php echo ($pgname =="services"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-23">
                                <a href="<?php echo get_site_url() ;?>/services/"><span class="link-inner">Services</span></a>
                            </li>
                            <li id="menu-item-22" class="<?php echo ($pgname =="mails"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-22">
                                <a href="<?php echo get_site_url() ;?>/mails/"><span class="link-inner">Mails</span></a>
                            </li>
                            
                            <li id="menu-item-22" class="<?php echo ($pgname =="connexion"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-22">
                                <?php 
                                    if(is_user_logged_in()):
                                         ?>
                                    <div class="dropdown_custom <?php echo ($pgname =="connexion"? "current-menu-item":"") ;?>" >
                                        <span class="link-inner">Mon compte</span>
                                        <div class="dropdown_custom_content">
                                            <p>Bienvenue <?php 
                                                if(is_user_logged_in()){
                                                    $current_user = wp_get_current_user();
                                                    echo $current_user->display_name;
                                                    echo '<a href="'.get_site_url().'/connexion/">Mon compte</a>';
                                                    echo '<a href="'.get_site_url().'/connexion/">Vie privée</a>';
                                                    echo '<a href="'.esc_url( wc_logout_url( wc_get_page_permalink('myaccount'))).'">Déconnexion</a>';
                                                }
                                            ?></p>
                                        </div>
                                    </div>
                                <?php
                                        //echo '<a href="http://localhost/transportation/connexion/"><span class="link-inner">Mon compte</span></a>';
                                        //printf('<a href="%1$s"><span class="link-inner">Déconnexion</span></a>',esc_url( wc_logout_url( wc_get_page_permalink('myaccount'))));
                                    else:
                                ?>
                                    <a href="http://localhost/transportation/connexion/"><span class="link-inner">Connexion</span></a>
                                <?php 
                                    endif;
                                ?>
                            </li>
                            
                        </ul>
		</nav><!-- #site-navigation -->

	</div><!-- #site-navigation-wrap -->

	<?php wpex_hook_main_menu_after(); ?>

<?php endif; ?>