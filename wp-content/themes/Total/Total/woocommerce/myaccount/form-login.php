<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if(isset($_GET['q'])){
    update_user_meta($_GET['q'], "has_confirm_email", 1);
}
$pagename = get_query_var('pagename'); 
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' && $pagename == "inscription" ) : ?>

<!--div class="u-columns col1-set" id="customer_login">

	<div class="u-column1 col-1"-->

<?php endif; ?>
<?php if($pagename == "connexion"): ?>
		<!--h2><?php esc_html_e( 'Login', 'woocommerce' ); ?></h2-->

		<form class="woocommerce-form woocommerce-form-login login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<button type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
				</label>
			</p>
			<p class="woocommerce-LostPassword lost_password">
                            <a href="<?php echo get_site_url() ?>/index.php/inscription">Pas de comptesss veuillez vous inscrire</a>
			</p>
			<p class="woocommerce-LostPassword lost_password">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>
<?php endif;?>
<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' && $pagename == "inscription") : ;?>
	<!--/div-->
	<!--div class="u-column2 col-2"-->
		<!--h2><?php esc_html_e( 'Register', 'woocommerce' ); ?></h2-->
		<form method="post" class="register" >
                    
			<?php do_action( 'woocommerce_register_form_start' ); ?>
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</p>
			<?php endif; ?>
                        <div class="display_info" style="display: none;">
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                            </p>
                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
                                    </p>
                            <?php endif; ?>
                            <?php do_action( 'woocommerce_register_form' ); ?>
                            <p>
                                <input required='required' type ='checkbox' name='termecond' value='1' class='chbx_termecond' data-msg-required="(Vous dévez accepter nos condition)"> Je déclare avoir lu et accepté les <a href="/conditions-termes-utilisation-vente" target="_blank">termes et conditions d'utilisation</a>
                            </p>

                            <p class="woocommerce-FormRow form-row">
                                    <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                    <button type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
                            </p>
                            <?php do_action( 'woocommerce_register_form_end' ); ?>
                        </div>
                        
		</form>
	<!--/div>
</div-->
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
<script>
    jQuery('document').ready(function(){
        //jQuery('input[type=radio][name=type_user]').change(function() {
        jQuery('#type_user').change(function() {
            
            var val_type_user = jQuery('#type_user').val();
            
            if(val_type_user == "forwarder"){
                jQuery(".display_info").show();
                jQuery("#div_categ_service").show();
                jQuery("#nom_entreprise").show();
                jQuery("#num_enreg_soc").show();
                jQuery("#num_compte").show();
                jQuery("#transaction_nationale").show();
                jQuery("#chargement_equipement").show();
                jQuery("#type_vehicule").show();
                jQuery("#matiere_danger").show();
                jQuery("#credit_card_user").hide();
                jQuery("#credit_card_usert").prop('required',false);
                jQuery("#type_vehiculet").prop('required',true);
                jQuery("#categorie_service").prop('required',true);
                jQuery("#nom_entrepriset").prop('required',true);
                jQuery("#num_enreg_soct").prop('required',true);
                jQuery("#num_comptet").prop('required',true);
                jQuery("#transaction_nationalet").prop('required',true);
                jQuery("#chargement_equipementt").prop('required',true);
                jQuery("#matiere_dangert").prop('required',true);
            }else if(val_type_user == "client"){
                jQuery(".display_info").show();
                jQuery("#div_categ_service").hide();
                jQuery("#nom_entreprise").hide();
                jQuery("#num_enreg_soc").hide();
                jQuery("#num_compte").hide();
                jQuery("#transaction_nationale").hide();
                jQuery("#chargement_equipement").hide();
                jQuery("#type_vehicule").hide();
                jQuery("#matiere_danger").hide();
                jQuery("#matiere_dangert").prop('required',false);
                jQuery("#type_vehiculet").prop('required',false);
                jQuery("#chargement_equipementt").prop('required',false);
                jQuery("#transaction_nationalet").prop('required',false);
                jQuery("#num_enreg_soct").prop('required',false);
                jQuery("#nom_entrepriset").prop('required',false);
                jQuery("#categorie_service").prop('required',false);
                jQuery("#num_comptet").prop('required',false);
                jQuery("#categorie_service").val('');
                jQuery("#credit_card_user").show();
                jQuery("#credit_card_usert").prop('required',true);
            }else{
                /*jQuery("#info_presta").hide();
                jQuery("#btn_validation_freelance").hide();*/
                jQuery(".display_info").hide();
            }
        });
    })
</script>
