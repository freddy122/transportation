<?php
/**
 * The template for displaying 404 pages (not found).
 */
get_header(); ?>
<?php global $sb_themes; ?>
<?php echo logistic_pro_theme_the_breadcrumb(); ?>
<section id="error-page" class="section-padding-70">
  <div class="container"> 
    <!-- Row -->
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="error-text">
          <?php esc_html_e( '404', 'logistic-pro' ); ?>
        </div>
        <div class="error-title"><?php echo esc_html( $sb_themes['page-404-title'] ); ?></div>
        <p><?php echo esc_html( $sb_themes['page-404-desc'] ); ?></p>
      </div>
      <div class="col-md-6 col-sm-12 col-xs-12">
        <?php if( $sb_themes['site-404-img']['url']){ ?>
        <img alt="<?php echo esc_attr( '404', 'logistic-pro' ); ?>" class="img-responsive" src="<?php echo esc_url( $sb_themes['site-404-img']['url'] ); ?>">
        <?php }else{?>
        <img alt="<?php echo esc_attr( '404', 'logistic-pro' ); ?>" class="img-responsive" src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/404.png">
        <?php } ?>
      </div>
    </div>
    <!-- Row End --> 
  </div>
  <!-- end container --> 
</section>
<?php get_footer(); ?>