<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Depilex Salon - Barber - Spa - Hairdresser - Yoga Template
 */
//47_2016-10-11 09:10:35|83_2016-10-11 09:10:48|85_2016-10-11 09:11:38
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<?php global $sb_themes; ?>
<span>&nbsp;</span>
<?php if ( have_comments() ) { ?>
<!-- blog-comment section -->
<div class="blog-section">
    <div class="custom-heading"> 
    	<h2><?php echo esc_html__('Comments: ', 'logistic-pro'); ?> (<?php echo esc_html( get_comments_number() ); ?>)</h2>  
     </div>
    <ul class="comments-list"> 
        <?php wp_list_comments(array( 'callback' => 'logistic_pro_themes_comment', 'style' => 'ul', 'type' => 'comment' ) ); ?>
    </ul>
</div>
<!-- blog-comment section end-->
<?php }?>
<?php if( ( $sb_themes['page-comments-show'] != 2 && is_page() ) || ($sb_themes['blog-comments-show'] != 2 && is_single() )  ){ ?>
<!-- blog-comment feedback -->
<div class="blog-section">
	<?php if ( !comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) { ?>
		<div class="custom-heading">
            <h2>
	            <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'logistic-pro' ); ?></p>
            </h2>
         
        </div>    	   
	<?php }else{ ?>
    <div class="custom-heading">
        <h2><?php echo esc_html__('Post Your Comments: ', 'logistic-pro'); ?></h2>
    </div>
            
		<?php
            
            $req = '*';
            $comment_args = array(
            'class_submit' => 'btn btn-primary',
            'title_reply' =>  esc_html__( '', 'logistic-pro' ),
            'cancel_reply_link' =>  esc_html__( 'Cancel Reply', 'logistic-pro' ),
            'fields' => apply_filters( 'comment_form_default_fields', array(
                    /* Name Field Setting Goes Here*/
                   
				    'author' => '<div class="col-sm-6"><div class="form-group"><label for="author">'
                        .esc_html__( 'Name', 'logistic-pro' ).( $req ? '<span class="required">*</span>' : '' ).'</label>' . 
                            '<input type="text" required placeholder="'.esc_html__( 'Your Good Name', 'logistic-pro' ).'" id="author" class="form-control" name="author" size="30"/></div></div> <!-- End col-sm-6 -->', 
                    
                    /* Email Field Setting Goes Here*/
                    'email' => '<div class="col-sm-6"><div class="form-group"><label for="email">'
                        .esc_html__( 'Email', 'logistic-pro' ).( $req ? '<span class="required">*</span>' : '' ).'</label>' . 
                            '<input type="email" required placeholder="'.esc_html__( 'Your Email Please', 'logistic-pro' ).'" id="email" class="form-control" name="email" size="30" /></div></div> <!-- End col-sm-6 -->', 
                
                    /* URL Field Setting Goes Here */
                    'url' => '<div class="col-sm-12"><div class="form-group"><label for="url">'
                        .esc_html__( 'Website', 'logistic-pro' ) . '</label>' . 
                            '<input type="text" required placeholder="'.esc_html__( 'Your URL Please', 'logistic-pro' ).'" id="url" class="form-control" name="url" size="30"' . "" . ' /></div></div> <!-- End col-sm-6 -->', 
                    
             ) ),
                    /* Comment Textarea Setting Goes Here*/
                    'comment_field' => '<div class="col-sm-12"><div class="form-group"><label for="url">'
                        . esc_html__( 'Commets:', 'logistic-pro' ) .( $req ? '<span class="required">*</span>' : '' ).'</label></div></div>' . 
                        '<div class="col-sm-12"><div class="form-group"><textarea class="form-control" id="comment" name="comment" required cols="45" rows="7" aria-required="true" ></textarea></div></div> <!-- End col-sm-6 -->', 	
                'comment_notes_after' => '',
            
            );
        comment_form($comment_args); 
        ?>
    	
	<?php } ?>

</div>
<!-- blog-comment feedback end -->
<?php }else{?>
<div class="custom-heading">
            <h2>
	            <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'logistic-pro' ); ?></p>
            </h2>
         
        </div>
<?php } ?>


<?php $r1=1; if($r1==2){?>

	<?php if ( have_comments() ) { ?>
        <div class="heading-side-bar">
          <h2 class="comments-title">
                <?php
                    printf( // WPCS: XSS OK.
                        esc_html( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'logistic-pro' ) ),
                        number_format_i18n( get_comments_number() ),
                        '<span>' . get_the_title() . '</span>'
                    );
                ?>
            </h2>
        </div>
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) { // Are there comments to navigate through? ?>
		<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'logistic-pro' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'logistic-pro' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'logistic-pro' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-above -->
		<?php } // Check for comment navigation. ?>
        <!-- Comments list -->
        <div class="comments-list"> 
			<?php
				//wp_list_comments( 'style=div&callback=logistic_pro_themes_comment' );
				wp_list_comments( 'style=div&callback=logistic_pro_themes_comment' );
			?>
        </div>
        <!-- end comments-list --> 
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) { // Are there comments to navigate through? ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h2 class="screen-reader-text"><?php esc_html_e( 'Comment navigation', 'logistic-pro' ); ?></h2>
			<div class="nav-links">

				<div class="nav-previous"><?php previous_comments_link( esc_html__( 'Older Comments', 'logistic-pro' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments', 'logistic-pro' ) ); ?></div>

			</div><!-- .nav-links -->
		</nav><!-- #comment-nav-below -->
		<?php } // Check for comment navigation. ?>

	<?php } // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) {
	?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'logistic-pro' ); ?></p>
	<?php } ?>

	
<?php } ?>