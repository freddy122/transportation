<?php

function getAllDataCategories(){
    global $wpdb;
    $categ_service = $wpdb->get_results( 
	"
            SELECT *
            FROM ".$wpdb->prefix."category_service
	"
    );
    return $categ_service;
   
}

add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );
function wooc_extra_register_fields() {
?>

    
    <p class="form-row form-row-wide">    
        <label class="w3-text-brown"><b>
            <?php  echo __("<!--:fr-->Type utilisateur<!--:--><!--:en-->User type<!--:-->");  ?> 
         <span class="required">
             *
         </span>
         </b>
        </label>
        <select class="w3-select w3-border " name="type_user" id="type_user"  required="required">
            <option value="">--- <?php  echo __("<!--:fr-->Choisir<!--:--><!--:en-->Choose<!--:-->");  ?>  ---</option>
            <option value="client"><?php  echo __("<!--:fr-->Client<!--:--><!--:en-->Customer<!--:-->");  ?></option>
            <option value="forwarder"><?php  echo __("<!--:fr-->Commissionnaire<!--:--><!--:en-->Forwarder<!--:-->");  ?></option>
        </select>
    </p>
    <div class="display_info" style="display: none;">
        <!--p class="form-row form-row-wide" id="div_categ_service" style="display:none;">
            <label for="reg_pseudo">
                <?php  //echo __("<!--:fr-->Catégorie<!--:--><!--:en-->Category<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label><?php //var_dump(getAllDataCategories());die(); ?> 
            <select class="w3-select w3-border form-control" name="categorie_service[]" id="categorie_service" multiple="multiple">
                <?php //foreach(getAllDataCategories() as $res): ?>
                    <option value="<?php //echo $res->id; ?>"><?php //echo $res->libelle_categ; ?></option>
                <?php //endforeach; ?>
            </select>
        </p-->
        <p class="form-row form-row-wide">
            <label for="user_login">
                <?php  echo __("<!--:fr-->Nom utilisateur<!--:--><!--:en-->Username<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text form-control" name="user_login" id="user_login" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>" />
        </p>
        <p class="form-row form-row-wide">
            <label for="firstname_user">
                 <?php  echo __("<!--:fr-->Nom<!--:--><!--:en-->Firstname<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text form-control" name="firstname_user" id="firstname_user" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>" />
        </p>
        <p class="form-row form-row-wide">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Prénom<!--:--><!--:en-->Lastname<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text form-control" name="lastname_user" id="lastname_user" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>" />
        </p>

        <p class="form-row form-row-wide" id="nom_entreprise" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Nom de l'entreprise<!--:--><!--:en-->Company name<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text form-control" name="nom_entreprise" id="nom_entrepriset" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>" />
        </p>

        <p class="form-row form-row-wide" id="num_enreg_soc" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Numéro d'enregistrement de la société<!--:--><!--:en-->Company registration number<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text form-control" name="num_enreg_soc" id="num_enreg_soct" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>" />
        </p>

        <p class="form-row form-row-wide" id="num_compte" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Numéro de compte<!--:--><!--:en-->Account number<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input type="text" class="input-text form-control" name="num_compte" id="num_comptet" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>"/>
        </p>

        <p class="form-row form-row-wide" id="transaction_nationale" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Transaction nationale<!--:--><!--:en-->National transaction<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border" name="transaction_nationale" id="transaction_nationalet" >
                <option value="">--- <?php  echo __("<!--:fr-->Choisir<!--:--><!--:en-->Choose<!--:-->");  ?> ---</option>
                <option value="oui"><?php  echo __("<!--:fr-->Oui<!--:--><!--:en-->Yes<!--:-->");  ?></option>
                <option value="non"><?php  echo __("<!--:fr-->Non<!--:--><!--:en-->No<!--:-->");  ?></option>
            </select>
        </p>

        <p class="form-row form-row-wide" id="chargement_equipement" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Chargement de l'équipement<!--:--><!--:en-->Loading equipment<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border"  name="chargement_equipement[]" id="chargement_equipementt" multiple="multiple" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>">
                <option value="a">a</option>
                <option value="b">b</option>
                <option value="c">c</option>
                <option value="d">d</option>
            </select>
        </p>

        <p class="form-row form-row-wide" id="type_vehicule" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Types de véhicules<!--:--><!--:en-->Types of vehicles<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border form-control"  name="type_vehicule[]" id="type_vehiculet" multiple="multiple" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>">
                <option value="type 1">type 1</option>
                <option value="type 2">type 2</option>
                <option value="type 3">type 3</option>
                <option value="type 4">type 4</option>
            </select>
        </p>
        <p class="form-row form-row-wide" id="matiere_danger" style="display:none;">
            <label for="lastname_user">
                <?php  echo __("<!--:fr-->Matières dangereuses<!--:--><!--:en-->Hazardous Material<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <select class="w3-select w3-border"  name="matiere_danger" id="matiere_dangert" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>">
                <option value="">--- <?php  echo __("<!--:fr-->Choisir<!--:--><!--:en-->Choose<!--:-->");  ?> ---</option>
                <option value="oui"><?php  echo __("<!--:fr-->Oui<!--:--><!--:en-->Yes<!--:-->");  ?></option>
                <option value="non"><?php  echo __("<!--:fr-->Non<!--:--><!--:en-->No<!--:-->");  ?></option>
            </select>
        </p>
        
        <p class="form-row form-row-wide">
            <label for="adresse_user">
                <?php  echo __("<!--:fr-->Adresse<!--:--><!--:en-->Address<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text form-control" name="adresse_user" id="adresse_user" placeholder="saisir au moins 3 caractères de votre code postale" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>"/>
        </p>
        <p class="form-row form-row-wide">
            <label for="ville_user">
                <?php  echo __("<!--:fr-->Ville<!--:--><!--:en-->City<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" type="text" class="input-text form-control" name="ville_user" id="ville_user" placeholder="saisir au moins 3 caractères de votre ville" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>"/>
        </p>
        <p class="form-row form-row-wide">
            <label for="pays_user">
                <?php  echo __("<!--:fr-->Pays<!--:--><!--:en-->Country<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            
            <select class="w3-select w3-border"  name="pays_user" id="pays_user" required="required" title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>">
                <option value="">--- <?php  echo __("<!--:fr-->Choisir<!--:--><!--:en-->Choose<!--:-->");  ?> ---</option>
                <?php 
                $pays_source = file_get_contents(get_site_url()."/pays.json");
                $ress = json_decode($pays_source);
                foreach($ress as $resu):
                ?> 
                <option value="<?php echo $resu; ?>"><?php echo $resu; ?></option>
               <?php endforeach; ?> 
            </select>
        </p>
        <p class="form-row form-row-wide">
            <label for="tel_user">
                <?php  echo __("<!--:fr-->Télephone<!--:--><!--:en-->Phone<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input required="required" class="input-text form-control" name="tel_user" id="tel_user"  type='text' pattern='[- +()0-9]{6,20}' title='<?php  echo __("<!--:fr-->Caractères accéptés:+ ( ) 0 à 9. Minimum 6 caractères et maximum 20 caractères<!--:--><!--:en-->Characters accepted: + ( ) 0 to 9. Minimum 6 characters and maximum 20 characters<!--:-->");  ?>' />
        </p>
        <p class="form-row form-row-wide" id="credit_card_user">
            <label for="credit_card_user">
                <?php  echo __("<!--:fr-->Numéro de carte de crédit<!--:--><!--:en-->Credit Card Number<!--:-->");  ?>
                <span class="required">
                    *
                </span>
            </label>
            <input type="number" pattern=".{15,15}" required title="<?php  echo __("<!--:fr-->15 chiffres<!--:--><!--:en-->15 digits<!--:-->");  ?>" class="input-text form-control" name="credit_card_user" id="credit_card_usert" />
        </p>
    </div>
<?php    
}

/*enregistrement*/
add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );
function wooc_save_extra_register_fields( $customer_id ) {
    global $wpdb;
    
    if ( isset( $_POST['user_login'] ) ) {
        $wpdb->update(
                $wpdb->users, 
                array(
                    'user_login' => $_POST['user_login']
                ), 
                array(
                    'ID' => $customer_id
                )
        );
    }
    
    if ( isset( $_POST['type_user'] ) ) {
        update_user_meta( $customer_id, 'type_user', ( $_POST['type_user'] ) );
    }
    /*if ( isset( $_POST['categorie_service'] ) ) {
        update_user_meta( $customer_id, 'categorie_service', implode(',', $_POST['categorie_service'] ) );
    }*/
    if ( isset( $_POST['nom_entreprise'] ) ) {
        update_user_meta( $customer_id, 'nom_entreprise', sanitize_text_field( $_POST['nom_entreprise'] ) );
    }
    if ( isset( $_POST['num_enreg_soc'] ) ) {
        update_user_meta( $customer_id, 'num_enreg_soc', sanitize_text_field( $_POST['num_enreg_soc'] ) );
    }
    if ( isset( $_POST['num_compte'] ) ) {
        update_user_meta( $customer_id, 'num_compte', sanitize_text_field( $_POST['num_compte'] ) );
    }
    if ( isset( $_POST['transaction_nationale'] ) ) {
        update_user_meta( $customer_id, 'transaction_nationale', sanitize_text_field( $_POST['transaction_nationale'] ) );
    }
    if ( isset( $_POST['chargement_equipement'] ) ) {
        
        update_user_meta( $customer_id, 'chargement_equipement', implode(',', $_POST['chargement_equipement'] ) );
    }
    if ( isset( $_POST['type_vehicule'] ) ) {
        update_user_meta( $customer_id, 'type_vehicule', implode(',', $_POST['type_vehicule'] ) );
    }
    
    if ( isset( $_POST['matiere_danger'] ) ) {
        update_user_meta( $customer_id, 'matiere_danger', sanitize_text_field( $_POST['matiere_danger'] ) );
    }
    
    if ( isset( $_POST['firstname_user'] ) ) {
        update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['firstname_user'] ) );
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['firstname_user'] ) );
    }
    
    if ( isset( $_POST['lastname_user'] ) ) {
        update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['lastname_user'] ) );
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['lastname_user'] ) );
    }
    
    if ( isset( $_POST['adresse_user'] ) ) {
        update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['adresse_user'] ) );
    }
    
    if ( isset( $_POST['ville_user'] ) ) {
        update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['ville_user'] ) );
    }
    
    if ( isset( $_POST['pays_user'] ) ) {
        update_user_meta( $customer_id, 'billing_state', sanitize_text_field( $_POST['pays_user'] ) );
    }
    
    if ( isset( $_POST['tel_user'] ) ) {
        update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['tel_user'] ) );
    }
    
    if ( isset( $_POST['credit_card_user'] ) ) {
        update_user_meta( $customer_id, 'credit_card_user', sanitize_text_field( $_POST['credit_card_user'] ) );
    }
    
    $titler     = 'Confirmation inscription';
    $user_infor = get_userdata($customer_id);
    $headersr   = array('Content-type: text/html');
    $contentsr  = "Bonjour , <br><br>";
    $contentsr .= "Bienvenue sur notre plateforme <br><br>";
    $contentsr .= "Voici votre nom d'utilisateur et mot de passe <br><br>";
    $contentsr .= "Nom d'utilisateur : ".$_POST['user_login']."<br><br>";
    $contentsr .= "Mot de passe :  ".$_POST['password']." <br><br>";
    $urls = make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) )."?q=".base64_encode($customer_id) );
    $contentsr .= "Pour confirmer votre inscription veuillez suivre ce lien : ".$urls."<br><br>";
    $contentsr .= "Merci";
    wp_mail($user_infor->user_email, $titler, $contentsr, $headersr);
    
}

function randomCaracters($l)
{
    $caracter        = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789');
    $specialCaracter = str_split('@');
    shuffle($caracter); 
    shuffle($specialCaracter);     
    $rand = '';
    $mergedCaracter = array();
    foreach (array_rand($caracter, ($l-1)) as $k) $mergedCaracter[] = $caracter[$k];
    $mergedCaracter[] = $specialCaracter[array_rand($specialCaracter, 1)];
    shuffle($mergedCaracter); 
    foreach (array_rand($mergedCaracter, $l) as $i) $rand .= $mergedCaracter[$i];
    return $rand;
}

function randomCaractersHi($l)
{
    $caracter        = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789');
    $specialCaracter = str_split('@');
    shuffle($caracter); 
    shuffle($specialCaracter);     
    $rand = '';
    $mergedCaracter = array();
    foreach (array_rand($caracter, ($l-1)) as $k) $mergedCaracter[] = $caracter[$k];
    $mergedCaracter[] = $specialCaracter[array_rand($specialCaracter, 1)];
    shuffle($mergedCaracter); 
    foreach (array_rand($mergedCaracter, $l) as $i) $rand .= $mergedCaracter[$i];
    return $rand;
}

add_action('wp_logout','logout_redirect');
function logout_redirect(){
    if ( isset( $_POST['user_login'] ) ){
        wp_redirect(get_site_url()."/connexion?flag_confirm_mail=true");
    }else{
        wp_redirect(get_site_url()."/connexion/");
    }
    
    exit;
}


/*function woocommerce_registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
	global $woocommerce;
	extract( $_POST );
	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
	}
	return $reg_errors;
}
add_filter('woocommerce_registration_errors', 'woocommerce_registration_errors_validation', 10, 3);
*/
/*function woocommerce_register_form_password_repeat() {
	?>
	<p class="form-row form-row-wide">
		<label for="reg_password_2">
                    <?php  echo __("<!--:fr-->Confirmer mot de passe<!--:--><!--:en-->Retype password<!--:-->");  ?>(<?php  echo __("<!--:fr-->8 caractères minimum (Chiffre ou lettre)<!--:--><!--:en-->8 characters minimum (number or letter)<!--:-->");  ?>)
                <span class="required">*</span></label>
                <input required="required" type="password" class="input-text" name="reg_password_2" id="reg_password_2" />
	</p>
	<?php
}
add_action( 'woocommerce_register_form', 'woocommerce_register_form_password_repeat' );
*/

add_action( 'user_register', 'add_has_confirm_email_user_meta');
function add_has_confirm_email_user_meta( $user_id ) {
    update_user_meta( $user_id, 'has_confirm_email', 0 );
}

/**
 * 
 */
add_filter('woocommerce_registration_redirect', 'wcs_register_redirect');
function wcs_register_redirect() {
    wp_logout();
    $login_url = get_site_url() ."/connexion?flag_confirm_mail=true";
    wp_redirect( $login_url);
    exit;
}

/**
 * 
 */
add_action('wp_login', 'prevent_user_from_login', 10, 2);
function prevent_user_from_login($user_login, $user = null ) {
    if ( !$user ) {
        $user = get_user_by('login', $user_login);
    }
    if ( !$user ) {
        return;
    }
    $has_confirm_email = get_user_meta( $user->ID, 'has_confirm_email', true );
    if ( $has_confirm_email == '0' ) {
        wp_clear_auth_cookie();
        $login_url = get_site_url() ."/connexion/";
        $login_url = add_query_arg( 'has_confirm_email', '0', $login_url);
        
        wp_redirect( $login_url );
        throw new Exception( '<strong>' . __( 'Error:', 'woocommerce' ) . '</strong> ' . __( 'Votre addresse mail n\'est pas encore confirmé ', 'woocommerce' ) );
        exit;
    }
}

function add_theme_scripts() {
wp_enqueue_style( 'slider', get_template_directory_uri() . '/css/datatable/dataTables.bootstrap.min.css',false,'1.1','all');
wp_enqueue_style( 'datepickercss', get_template_directory_uri() . '/js/datepicker/jquery.ui.all.css',false,'1.1','all');
wp_enqueue_script( 'datatables', get_template_directory_uri() . '/js/datatable/jquery.dataTables.min.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'boot', get_template_directory_uri() . '/js/datatable/dataTables.bootstrap.min.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'uicore', get_template_directory_uri() . '/js/datepicker/jquery.ui.core.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'uicorew', get_template_directory_uri() . '/js/datepicker/jquery.ui.widget.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'uicored', get_template_directory_uri() . '/js/datepicker/jquery.ui.datepicker.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'uicoredf', get_template_directory_uri() . '/js/datepicker/jquery.ui.datepicker-fr.js', array ( 'jquery' ), 1.1, true);
 wp_enqueue_style( 'timpickercss', get_template_directory_uri() . '/js/timepicker/bootstrap-clockpicker.min.css',false,'1.1','all');
    wp_enqueue_script( 'timpickerjs', get_template_directory_uri() . '/js/timepicker/bootstrap-clockpicker.min.js', array ( 'jquery' ), 1.2, true);
    wp_enqueue_script( 'validatejquerys', get_template_directory_uri() . '/js/validate/jquery.validate.js', array ( 'jquery' ), 1.2, true);
    wp_enqueue_script( 'validatejquerysss', get_template_directory_uri() . '/js/validate/additional-methods.min.js', array ( 'jquery' ), 1.2, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

add_action('admin_enqueue_scripts','wptuts53021_load_admin_script');
function wptuts53021_load_admin_script( $hook ){
    wp_enqueue_script( 'wptuts53021_script',get_template_directory_uri().'/js/jquery-1.10.2.js',array('jquery'),1.1,true);
    wp_enqueue_style( 'slider', get_template_directory_uri() . '/css/datatable/dataTables.bootstrap.min.css',false,'1.1','all');
    wp_enqueue_style( 'datepickercss', get_template_directory_uri() . '/js/datepicker/jquery.ui.all.css',false,'1.1','all');
    wp_enqueue_script( 'datatables', get_template_directory_uri() . '/js/datatable/jquery.dataTables.min.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'boot', get_template_directory_uri() . '/js/datatable/dataTables.bootstrap.min.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'uicore', get_template_directory_uri() . '/js/datepicker/jquery.ui.core.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'uicorew', get_template_directory_uri() . '/js/datepicker/jquery.ui.widget.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'uicored', get_template_directory_uri() . '/js/datepicker/jquery.ui.datepicker.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'uicoredf', get_template_directory_uri() . '/js/datepicker/jquery.ui.datepicker-fr.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'uicoredfd', get_template_directory_uri() . '/js/scripts.js', array ( 'jquery' ), 1.1, true);
}

/*add_action( 'admin_menu', 'wpse_91693_register' );
function wpse_91693_register()
{
    add_menu_page(
        'Include Text',     // page title
        'Include Text',     // menu title
        'manage_options',   // capability
        'include-text',     // menu slug
        'wpse_91693_render' // callback function
    );
}
function wpse_91693_render()
{
    global $title;

    print '<div class="wrap">';
    print "<h1>$title</h1>";

    $file = plugin_dir_path( __FILE__ ) . "included.html";

    if ( file_exists( $file ) )
        require $file;

    print "<p class='description'>Included from <code>$file</code></p>";

    print '</div>';
}*/
add_action('wp_ajax_message_not_read', 'message_not_read');
add_action('wp_ajax_nopriv_message_not_read', 'message_not_read');
function message_not_read() {
    global $wpdb;
    $user_id = get_current_user_id();
    $type_user = get_user_meta($user_id, "type_user", true);
    if($type_user == "client"){
        $quersUserclp = "SELECT count(lecture_message)  as lst_message from ".$wpdb->prefix."devis_mails where id_user = '".$user_id."' "
                . " and lecture_message ='non_lu' "
                . " and mail_type in ('proposition_devis') ";
        $resUserclp = $wpdb->get_results($quersUserclp, OBJECT);
        $quersUserclc = "SELECT count(lecture_message)  as lst_message from ".$wpdb->prefix."devis_mails where id_user = '".$user_id."' "
                . " and lecture_message ='non_lu' "
                . " and mail_type in ('confirmation_devis_client') ";
        $resUserclc = $wpdb->get_results($quersUserclc, OBJECT);
        echo "(".$resUserclp[0]->lst_message.")___(".$resUserclc[0]->lst_message.")";
    }else{
        $quersUserfwdn = "SELECT count(lecture_message) as lst_message from ".$wpdb->prefix."devis_mails where id_forwarder = '".$user_id."' "
                . " and lecture_message ='non_lu' "
                . " and mail_type in ('new_devis') ";
        $resUserfwdn = $wpdb->get_results($quersUserfwdn, OBJECT);
        
        $quersUserfwdc = "SELECT count(lecture_message) as lst_message from ".$wpdb->prefix."devis_mails where id_forwarder = '".$user_id."' "
                . " and lecture_message ='non_lu' "
                . " and mail_type in ('confirmation_devis') ";
        $resUserfwdc = $wpdb->get_results($quersUserfwdc, OBJECT);
        echo "(".$resUserfwdn[0]->lst_message.")___(".$resUserfwdc[0]->lst_message.")";
    }
    die();
}

add_action('wp_ajax_all_message_not_read', 'all_message_not_read');
add_action('wp_ajax_nopriv_all_message_not_read', 'all_message_not_read');
function all_message_not_read() {
    global $wpdb;
    $user_id = get_current_user_id();
    $type_user = get_user_meta($user_id, "type_user", true);
    if($type_user == "client"){
        $quersUsercl = "SELECT count(lecture_message)   as lst_message from ".$wpdb->prefix."devis_mails where id_user = '".$user_id."' "
                . " and lecture_message ='non_lu'"
                . " and mail_type in ('proposition_devis','confirmation_devis_client') ";
        $resUsercl = $wpdb->get_results($quersUsercl, OBJECT);
        echo "(".$resUsercl[0]->lst_message.")";
    }else{
        $quersUserfwd = "SELECT count(lecture_message)   as lst_message from ".$wpdb->prefix."devis_mails where id_forwarder = '".$user_id."'"
                . " and lecture_message ='non_lu' "
                . "  and mail_type in ('new_devis','confirmation_devis')  ";
        $resUserfwd = $wpdb->get_results($quersUserfwd, OBJECT);
        echo "(".$resUserfwd[0]->lst_message.")";
    }
    die();
}