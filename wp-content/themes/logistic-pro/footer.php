<?php global $sb_themes; ?>

<footer class="footer-area"> 
  <!--Footer Upper-->
  <?php if( $sb_themes['top-footer-switch-on'] ){ ?>
  <div class="footer-content">
    <div class="container">
      <div class="row clearfix"> 
        
        <!--Two 4th column-->
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="row clearfix">
            <div class="col-lg-7 col-sm-6 col-xs-12 column">
              <div class="footer-widget about-widget">
                <div class="logo">
                  <?php if( $sb_themes['site-footer-logo']['url'] ){ ?>
                  <img src="<?php echo esc_url( $sb_themes['site-footer-logo']['url'] ); ?>" alt="logo">
                  <?php }else{?>
                  <img src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/logo-1.png" alt="logo">
                  <?php } ?>
                </div>
                <ul class="contact-info">
                  <?php 
    if(count( $sb_themes['top-footer-contact'] ) > 0 ){
        foreach( $sb_themes['top-footer-contact']  as $ar => $val)
        {  	
            if($ar == "address" && $val != ""){echo '<li><span class="icon fa fa-map-marker"></span> '. esc_html( $val ) .'</li>';}
            else if($ar == "phone" && $val != ""){echo '<li><span class="icon fa fa-phone"></span> '. esc_html( $val ) .'</li>';}
            else if($ar == "support" && $val != ""){echo '<li><span class="icon fa fa-envelope"></span> '. esc_html( $val ) .'</li>';}
            else if($ar == "fax" && $val != ""){echo '<li><span class="icon fa fa-fax"></span> '. esc_html( $val ) .'</li>';}

        } 
    }
    ?>
                </ul>
                <div class="social-links-two clearfix">
                  <?php 
if(count( $sb_themes['top-footer-social-media'] ) > 0 ){
foreach( $sb_themes['top-footer-social-media']  as $ar => $val)
{  	
if($ar == "facebook" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="facebook img-circle"><span class="fa fa-facebook"></span></a>';}
else if($ar == "twitter" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="twitter img-circle"><span class="fa fa-twitter"></span></a>';}
else if($ar == "rss" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-rss"></span></a>';}
else if($ar == "pinterest" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-pinterest"></span></a>';}
else if($ar == "youtube" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-youtube"></span></a>';}
else if($ar == "digg" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-digg"></span></a>';}
else if($ar == "stumbleupon" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-stumbleupon"></span></a>';}
else if($ar == "linkedin" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-linkedin"></span></a>';}
else if($ar == "google" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="linkedin img-circle"><span class="fa fa-google"></span></a>';}	
else if($ar == "google-plus" && $val != ""){echo '<a href="'. esc_attr( $val ) .'" target="_blank" class="google-plus img-circle"><span class="fa fa-google-plus"></span></a>';}																								
} 
}
?>
                </div>
              </div>
            </div>
            
            <!--Footer Column-->
            <div class="col-lg-5 col-sm-6 col-xs-12 column">
              <h2><?php echo esc_html( $sb_themes['footer-service-links-title'] );?></h2>
              <div class="footer-widget links-widget">
                <ul>
                  <?php 
    $links = $sb_themes['footer-service-links-pages'];
    if( count( $links ) > 0 && $links != "")
    {
        foreach( $links as $link ){	
          if( !get_post_type( $link ) ){ continue; }	 
          echo  '<li><a href="'. esc_url( get_the_permalink( $link ) ).' ">'. esc_html( get_the_title($link) ).'</a></li>'; 
        }
    }

?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!--Two 4th column End--> 
        
        <!--Two 4th column-->
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="row clearfix"> 
            <!--Footer Column-->
            <div class="col-lg-7 col-sm-6 col-xs-12 column">
              <div class="footer-widget news-widget">
                <h2><?php echo esc_html( $sb_themes['footer-posts-title'] ); ?></h2>
                <?php $posts_array = get_posts( array('posts_per_page'   => $sb_themes['footer-posts-content'], 'orderby'  => 'date', 'order' => 'DESC','post_type'  => 'post','post_status' => 'publish','suppress_filters' => true, 'ignore_sticky_posts' => 1) ); ?>
                <?php foreach($posts_array as $post){ ?>
                <?php $image = 	wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'logistic-pro-blog-thumb' );?>
                <!--News Post-->
                <div class="news-post <?php  if( $image[0] == "" ){ echo esc_attr( 'zero-padding' ); } ?>">
                  <div class="icon"></div>
                  <div class="news-content">
                    <?php  if( $image[0] != "" ) { ?>
                    <figure class="image-thumb"> <img alt="<?php echo esc_attr( get_the_title() );?>" class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>"> </figure>
                    <?php } ?>
                    <a href="<?php echo esc_url( get_the_permalink($post->ID)); ?>" title="<?php echo esc_attr( get_the_title($post->ID)); ?>"> <?php echo esc_html( get_the_title($post->ID)); ?> </a> </div>
                  <div class="time"><?php echo esc_html( get_the_date( get_option('date_format'), $post->ID) );?></div>
                </div>
                <?php } ?>
              </div>
            </div>
            
            <!--Footer Column-->
            <div class="col-lg-5 col-sm-6 col-xs-12 column">
              <div class="footer-widget links-widget">
                <h2><?php echo esc_html( $sb_themes['footer-quick-links-title'] );?></h2>
                <ul>
                <?php 
				  	logistic_pro_themes_pages_or_cats('footer-quick-links-pages-cats', 'footer-quick-links-pages', 'footer-quick-links-cats' ); 
				?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!--Two 4th column End--> 
        
      </div>
    </div>
  </div>
  <?php } ?>
  <!--Footer Bottom-->
  <div class="footer-copyright">
    <div class="container clearfix"> 
      <!--Copyright-->
      <div class="copyright text-center">
        <?php 
        if( $sb_themes['footer-copy-right-text'] != "")
        {
            //echo wp_kses( $sb_themes['footer-copy-right-text'], logistic_pro_theme_required_tags() );
        }
        else
        {
            //$sblink = 'https://themeforest.net/user/glixentechnologies';
    ?>
        <a href="<?php //echo esc_url( 'https://wordpress.org/' ); ?>"><?php //printf( esc_html__( 'Proudly powered by %s', 'logistic-pro' ), 'WordPress' ); ?></a> <span class="sep">  </span><?php //printf( esc_html__( 'Theme: %1$s by %2$s.', 'logistic-pro' ), 'logistic-pro', '<a href="' . esc_url($sblink) . '" rel="designer">Glixen Technologies</a>' ); ?>
        <?php } ?>
      </div>
    </div>
  </div>
</footer>
<?php if( $sb_themes['footer-js-css'] != "") { echo wp_kses( $sb_themes['footer-js-css'], logistic_pro_theme_required_tags() ); } ?>
<?php 
	  if( $sb_themes['show-hide-qoute-button'] != 1)
	  { 
		get_template_part( 'template-parts/popup/qoute', '' ); 
	  }
?>

<?php wp_footer(); ?>
</body>
</html>