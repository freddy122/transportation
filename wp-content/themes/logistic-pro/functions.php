<?php
/**
 * Depilex Salon - Barber - Spa - Hairdresser - Yoga Template functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Depilex Salon - Barber - Spa - Hairdresser - Yoga Template
 */

if ( ! function_exists( 'logistic_pro_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function logistic_pro_setup() {


	/* ------------------------------------------------ */
	/* Theme Settings */
	/* ------------------------------------------------ */
	
	require trailingslashit( get_template_directory () ) . 'inc/theme_settings.php';



	/* ------------------------------------------------ */
	/* Custom Classess */ 
	/* ------------------------------------------------ */

	require trailingslashit( get_template_directory () ) . 'inc/classess/get-countries-array.php';			
	
	/* ------------------------------------------------ */
	/* Theme Utilities */ 
	/* ------------------------------------------------ */
	
	require trailingslashit( get_template_directory () ) . 'inc/utilities.php';				

	/* ------------------------------------------------ */
	/* Theme Widgets */ 
	/* ------------------------------------------------ */
	
	require trailingslashit( get_template_directory () ) . 'inc/widgets.php';				

	/* ------------------------------------------------ */
	/* TGM */ 
	/* ------------------------------------------------ */
	
	if ( file_exists( trailingslashit( get_template_directory() ) . 'tgm/tgm-init.php' ) ) {
		require_once trailingslashit( get_template_directory() ) . 'tgm/tgm-init.php';
	}


	/* Make theme available for translation. */
	load_theme_textdomain( 'logistic-pro', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );
	add_editor_style('editor.css');
	/* Enable support for Post Thumbnails on posts and pages.  */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'logistic-pro' ),
		'onepage' => esc_html__( 'Onepage', 'logistic-pro' ),
	) );

	// Add Image Sizes
	add_image_size( 'logistic-pro-blog-1', 750, 480, true );
	add_image_size( 'logistic-pro-blog-thumb', 65, 65, true );
	add_image_size( 'logistic-pro-blog-thumb-80', 80, 80, true );
	add_image_size( 'logistic-pro-rev-slider', 1200, 537, true );
	add_image_size( 'logistic-pro-rev-slider-thumb', 75, 75, true );
	
	add_image_size( 'logistic-pro-services-thumb', 370, 300, true );	
	add_image_size( 'logistic-pro-partner-thumb', 200, 130, true );
	add_image_size( 'logistic-pro-gallery-thumb', 360, 240, true );
	add_image_size( 'logistic-pro-testimonial-thumb', 120, 120, true );
	add_image_size( 'logistic-pro-team-thumb', 480, 480, true );
	
	
	/* Switch default core markup for search form, comment form, and comments */
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption', ) );

	wp_link_pages();
	/* Enable support for Post Formats. */
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link', ) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'logistic_custom_background_args', 
	array( 'default-color' => 'ffffff', 'default-image' => '', ) ) );
}
endif; // logistic_pro_setup
add_action( 'after_setup_theme', 'logistic_pro_setup' );

/*  @global int $content_width  */

function logistic_pro_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'logistic_pro_content_width', 640 );
}
add_action( 'after_setup_theme', 'logistic_pro_content_width', 0 );

/* Register widget area */
function logistic_pro_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'logistic-pro' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'LogisticPro Sidebar.', 'logistic-pro' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'logistic_pro_widgets_init' );

/* Enqueue scripts and styles. */
function logistic_pro_scripts() {
	wp_enqueue_style( 'logistic-style', get_stylesheet_uri() );
	
	global $sb_themes;
/* Register scripts. */

wp_register_script( 'bootstrap-js', trailingslashit( get_template_directory_uri() ) . 'js/bootstrap.min.js', false, false, true );
wp_register_script( 'color-switcher', trailingslashit( get_template_directory_uri() ) . 'js/color-switcher.js', false, false, true );
	
wp_register_script( 'logistic-pro-carousel', trailingslashit( get_template_directory_uri() ) . 'js/carousel.min.js', false, false, true );
wp_register_script( 'logistic-pro-custom', trailingslashit( get_template_directory_uri() ) . 'js/custom.js', false, false, true );
wp_register_script( 'logistic-pro-easing', trailingslashit( get_template_directory_uri() ) . 'js/easing.js', false, false, true );
wp_register_script( 'jquery-appear', trailingslashit( get_template_directory_uri() ) . 'js/jquery.appear.min.js', false, false, true );
wp_register_script( 'jquery-countTo', trailingslashit( get_template_directory_uri() ) . 'js/jquery.countTo.js', false, false, true );
wp_register_script( 'jquery-shuffle', trailingslashit( get_template_directory_uri() ) . 'js/jquery.shuffle.min.js', false, false, true );

wp_register_script( 'jquery-stellar', trailingslashit( get_template_directory_uri() ) . 'js/jquery.stellar.min.js', false, false, true );
wp_register_script( 'jquery-waypoints', trailingslashit( get_template_directory_uri() ) . 'js/jquery.waypoints.js', false, false, true );
wp_register_script( 'modernizr', trailingslashit( get_template_directory_uri() ) . 'js/modernizr.js', false, false, true );
wp_register_script( 'theia-sticky', trailingslashit( get_template_directory_uri() ) . 'js/theia-sticky-sidebar.js', false, false, true );
wp_register_script( 'logistic-pro-skip-link-focus-fix', trailingslashit( get_template_directory_uri() ) . 'js/skip-link-focus-fix.js', false, false, true );
wp_register_script( 'magnific-popup', trailingslashit( get_template_directory_uri() ) . 'js/jquery.magnific-popup.min.js', false, false, true );
wp_register_script( 'bootstrap-dropdownhover', trailingslashit( get_template_directory_uri() ) . 'js/bootstrap-dropdownhover.min.js', false, false, true );
wp_register_script( 'logistic-pro-validator', trailingslashit( get_template_directory_uri() ) . 'js/validator.min.js', false, false, true );
wp_register_script( 'isotope', trailingslashit( get_template_directory_uri() ) . 'js/isotope.min.js', false, false, true );
wp_register_script( 'imagesloaded', trailingslashit( get_template_directory_uri() ) . 'js/imagesloaded.js', false, false, true );


/* Load the custom scripts.*/
//
wp_register_script( 'html5shiv', trailingslashit( get_template_directory_uri() ) . 'js/html5shiv.min.js', false, false, true );
wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
wp_register_script( 'respond', trailingslashit( get_template_directory_uri() ) . 'js/respond.min.js', false, false, true );wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );
	
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	wp_enqueue_script( 'comment-reply' );
}
	
wp_enqueue_script( 'bootstrap-js', trailingslashit( get_template_directory_uri() ) . 'js/bootstrap.min.js', false, false, true );
wp_enqueue_script( 'logistic-pro-carousel' );
wp_enqueue_script( 'bootstrap-dropdownhover' );
wp_enqueue_script( 'logistic-pro-easing' );
wp_enqueue_script( 'jquery-appear' );
wp_enqueue_script( 'jquery-countTo' );
wp_enqueue_script( 'jquery-shuffle' );
wp_enqueue_script( 'jquery-stellar' );
wp_enqueue_script( 'jquery-waypoints' );
wp_enqueue_script( 'modernizr' );
wp_enqueue_script( 'theia-sticky' );
wp_enqueue_script( 'magnific-popup' );
wp_enqueue_script( 'imagesloaded' );
wp_enqueue_script( 'isotope' );


wp_enqueue_script( 'logistic-pro-custom' );
/* CSS Starts*/

function sb_logistic_merriweather_fonts() {

    $font_url_dosis = ''; 

    if ( 'off' !== 'on' ) {
        $font_url_dosis = add_query_arg( 'family', urlencode( 'Merriweather:400,300,300italic,400italic,700,700italic' ) , "//fonts.googleapis.com/css" );
    }
    return urldecode( $font_url_dosis );
}	

function sb_logistic_source_sans_pro_fonts() {

    $font_url_open_sans_css = '';
    if ( 'off' !== 'on' ) {
        $font_url_open_sans_css = add_query_arg('family', urlencode( 'Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic') , "//fonts.googleapis.com/css" );

    }
    return urldecode( $font_url_open_sans_css );
}



wp_enqueue_style( 'bootstrap-css', trailingslashit( get_template_directory_uri () )  . 'css/bootstrap.css' , array(), null );
wp_enqueue_style( 'et-line-fonts', trailingslashit( get_template_directory_uri () )  . 'css/et-line-fonts.css' , array(), null );
wp_enqueue_style( 'flaticon', trailingslashit( get_template_directory_uri () )  . 'css/flaticon.css' , array(), null );
wp_enqueue_style( 'font-awesome', trailingslashit( get_template_directory_uri () )  . 'css/font-awesome.css' , array(), null );
wp_enqueue_style( 'magnific-popup', trailingslashit( get_template_directory_uri () )  . 'css/magnific-popup.css' , array(), null );
wp_enqueue_style( 'owl-carousel', trailingslashit( get_template_directory_uri () )  . 'css/owl.carousel.css' , array(), null );
wp_enqueue_style( 'owl-style', trailingslashit( get_template_directory_uri () )  . 'css/owl.style.css' , array(), null );
wp_enqueue_style( 'bootstrap-dropdownhover', trailingslashit( get_template_directory_uri () )  . 'css/bootstrap-dropdownhover.min.css' , array(), null );
wp_enqueue_style( 'logistic-pro-animate', trailingslashit( get_template_directory_uri () )  . 'css/animate.min.css' , array(), null );
wp_enqueue_style( 'logistic-pro-style', trailingslashit( get_template_directory_uri () )  . 'css/style.css' , array(), null );
wp_enqueue_style( 'logistic-pro-custom', trailingslashit( get_template_directory_uri () )  . 'css/custom.css' , array(), null );
wp_enqueue_style( 'sb-themes-static', trailingslashit( get_template_directory_uri () )  . 'css/static.css' , array(), null );




$color_name = $sb_themes['site-color-change'];
if($color_name == ""){$color_name = 'defualt';}
wp_enqueue_style( 'logistic-pro-colors', trailingslashit( get_template_directory_uri () )  . 'css/colors/'.$color_name.'.css' , array(), null );	

if( $sb_themes['site-color-settings'] != 0 ){ wp_enqueue_style( 'theme-color', '#' , array(), null );	 }
	
wp_enqueue_style( 'fonts-dosis', sb_logistic_source_sans_pro_fonts(), array(), null );
wp_enqueue_style( 'fonts-raleway', sb_logistic_merriweather_fonts(),  array(), null );


if($sb_themes['website-type'] == '1')
{	
	if( ( ( is_home() || is_front_page() ) && $sb_themes['onepage-slides-settings'] == 0)  || $sb_themes['onepage-slides-settings'] == 1){	
	
		wp_register_script( 'logistic-pro-supersized-js', trailingslashit( get_template_directory_uri () ) . 'js/supersized.3.2.7.min.js', false, false, false );
		wp_register_script( 'logistic-pro-morphext-js', trailingslashit( get_template_directory_uri () ) . 'js/morphext.min.js', false, false, false );
		wp_enqueue_script( 'logistic-pro-supersized-js' );
		wp_enqueue_script( 'logistic-pro-morphext-js' );
		
		wp_enqueue_style( 'logistic-pro-morphext', trailingslashit( get_template_directory_uri () ) . 'css/morphext.css' , array(), null );
		wp_enqueue_style( 'logistic-pro-supersized', trailingslashit( get_template_directory_uri () )  . 'css/supersized.css' , array(), null );

	}
}


// enquee css for footer background
if($sb_themes['site-footer-bg-img']['url'] != ""){		
$data = '.footer-area {     background-color: #323232;
    background-image: url("'. $sb_themes["site-footer-bg-img"]["url"] .'");
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    color: #c9c9c9;
    font-family: "Source Sans Pro",sans-serif;
    position: relative; }';		
wp_add_inline_style( 'sb-themes-static', $data );
//
}

// 
if($sb_themes['breadcrumb-bg-img']['url'] != ""){		
$data = '.breadcrumbs-area {   background: rgba(0, 0, 0, 0) url("'.$sb_themes["breadcrumb-bg-img"]["url"].'") no-repeat scroll center center;
    padding: 72px 0;
    position: relative;
    width: 100%; }';		
wp_add_inline_style( 'sb-themes-static', $data );
//
}


}
add_action( 'wp_enqueue_scripts', 'logistic_pro_scripts' );



function logistic_pro_footer_content()
{
	global $sb_themes;

// Only For Transparent Header
if( $sb_themes['header-styles'] == 2)
{
	echo '<script type="text/javascript">
			 $(window).scroll(function() {
				if($(this).scrollTop() > 50)
				{
					$(".transparent-header .navbar-default").addClass("opaque");
					$(".transparent-header a.navbar-brand img").attr("src", "'.esc_url( $sb_themes["site-logo-transparent"]["url"] ).'");
				} else {
					$(".transparent-header .navbar-default").removeClass("opaque");
					 $(".transparent-header a.navbar-brand img").attr("src", "'.esc_url( $sb_themes["site-logo"]["url"] ).'");
				}
			});</script>';
	
}	
// Add js ony when page is contact us

	if( $sb_themes['site-color-settings'] != 0 )
	{ 
	echo '<script type="text/javascript">
            (function($) {
                "use strict";
                      $("#defualt" ).on(\'click\',function(){
                          $("#theme-color-css" ).attr("href", "'.  trailingslashit( get_template_directory_uri () ) .'css/colors/defualt.css");
                          return false;
                      });
                      $("#red" ).on(\'click\',function(){
                          $("#theme-color-css" ).attr("href", "'.  trailingslashit( get_template_directory_uri () ) .'css/colors/red.css");
                          return false;
                      });
                       $("#green" ).on(\'click\',function(){
                        
                          $("#theme-color-css" ).attr("href", "'.  trailingslashit( get_template_directory_uri () ) .'css/colors/green.css");
                          return false;
                      });
                      $("#purple" ).on(\'click\',function(){
                          $("#theme-color-css" ).attr("href", "'.  trailingslashit( get_template_directory_uri () ) .'css/colors/purple.css");
                          return false;
                      });
                      $("#yellow" ).on(\'click\',function(){
                          $("#theme-color-css" ).attr("href", "'.  trailingslashit( get_template_directory_uri () ) .'css/colors/yellow.css");
                          return false;
                      });                      
                      $("#orange" ).on(\'click\',function(){
                          $("#theme-color-css" ).attr("href", "'.  trailingslashit( get_template_directory_uri () ) .'css/colors/orange.css");
                          return false;
                      });
                      $(".picker_close").click(function(){
                            $("#choose_color").toggleClass("position");
                       });                      
            })(jQuery);            
        </script> ';     
	}
	
	if($sb_themes['website-type'] == '1')
	{
	
	if( ( ( is_home() || is_front_page() ) && $sb_themes['onepage-slides-settings'] == 0)  || $sb_themes['onepage-slides-settings'] == 1){
			
			
		if( isset( $sb_themes['opt-multi-media-slides'] ))
		{
			$thePostIdArray = explode(',', $sb_themes['opt-multi-media-slides'] );		
			if( count( $thePostIdArray ) > 0 )
			{
				$slides = '';
				foreach( $thePostIdArray as $arr )
				{					
					$img =  wp_get_attachment_image_src( $arr, 'full');
					if( $img[0] != "" )
					{
						$slides .= '{image : "'.$img[0].'", title : "", thumb : "", url : ""},';
					}
				}
			}
		}
			
			
			echo '<script type="text/javascript">
	 jQuery.noConflict();
	(function($){
		$(document).ready(function() {
				$.supersized({
					autoplay: 1,
					slide_interval: 3000,
					transition: 1,
					transition_speed: 3000,				   										   				
					slide_links: "blank",
					thumb_links: 0,
					slides:  	[ '.$slides.'],
				});
		 });	
	
	
			"use strict";
		$("#js-rotating").Morphext({
			animation: "bounceIn",
			separator: ",",
			speed: 3000,
			complete: function () {
			}
		});
	 })(jQuery);
	</script>';
	
		}
	}
	
}
add_action('wp_footer', 'logistic_pro_footer_content');

function logistic_pro_admin_scripts()
{
	wp_enqueue_style( 'logistic-pro-static', trailingslashit( get_template_directory_uri () )  . 'css/admin-style.css' );
	wp_enqueue_style( 'flaticon', trailingslashit( get_template_directory_uri () )  . 'css/flaticon.css' );
}
add_action( 'admin_enqueue_scripts', 'logistic_pro_admin_scripts' );
require WP_CONTENT_DIR . '/themes/logistic-pro/custom_functions.php';
