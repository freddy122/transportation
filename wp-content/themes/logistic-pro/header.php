<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Depilex Salon - Barber - Spa - Hairdresser - Yoga Template
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php global $sb_themes; 

$pgname = get_query_var("pagename");?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {	
	if( $sb_themes['favicon']['url'] != "" ){	
 ?>
	<link rel="Shortcut Icon" href="<?php echo esc_url($sb_themes['favicon']['url']);?>">
<?php }
	else
	{
?>
	<link rel="Shortcut Icon" href="<?php echo trailingslashit( get_template_directory_uri() ). 'images/favicon.ico';?>">
<?php		
	}
} 

?>
<!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if( $sb_themes['header-js-css'] != "") { echo wp_kses( $sb_themes['header-js-css'], logistic_pro_theme_required_tags() ); } ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php if($sb_themes['webite-loader'] ){ ?>
 <div class="preloader"><span class="preloader-gif"></span></div>
<?php } ?>
<?php if( $sb_themes['show-hide-qoute-button'] != 1){ ?>
<!--div data-target="#request-quote" data-toggle="modal" class="quote-button hidden-xs">
		  <a class="btn btn-primary" href="javascript:void(0)"><i class="fa fa-envelope"></i></a>
	</div-->
 <?php } ?>
<?php if( $sb_themes['site-color-settings'] != 0 ){?>
<!-- =-=-=-=-=-=-= Color Switcher =-=-=-=-=-=-= -->
<div class="color-switcher" id="choose_color"> <a href="javascript:void(0);" class="picker_close"><i class="fa fa-gear"></i></a>
    <h5><?php echo __('STYLE SWITCHER', 'logistic-pro' ); ?></h5>
    <div class="theme-colours">
        <p><?php echo __('Choose Colour style ', 'logistic-pro' ); ?></p>
        <ul>
            <li>
                <a href="#" class="defualt" id="defualt"></a>
            </li>
            <li>
                <a href="#" class="red" id="red"></a>
            </li>
            <li>
                <a href="#" class="green" id="green"></a>
            </li>
            <li>
                <a href="#" class="orange" id="orange"></a>
            </li>
            <li>
                <a href="#" class="purple" id="purple"></a>
            </li>
            <li>
                <a href="#" class="yellow" id="yellow"></a>
            </li>
            
        </ul>
    </div>
    <div class="clr"> </div>
</div>
 <?php } ?>
<!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
<?php 	 
	 if($sb_themes['website-type'] != 1 ){ 
		if( $sb_themes['top-header-switch-on'] ){
			
			$colored_class = '';
			if($sb_themes['top-header-color-scheme'])
			{
				$colored_class = 'color-scheme';
			}
?>
		<section class="top-bar <?php echo esc_attr( $colored_class );?>">
		  <div class="container">
			<!--div class="left-text pull-left">
			  <p><?php echo esc_html( $sb_themes['top-header-text'] ); ?></p>
			</div-->
			<!-- /.left-text -->
			
            
              <?php if(isset($sb_themes['top-header-type']) && $sb_themes['top-header-type'] == 'social' ){  ?>
              <div class="social-icons pull-right">
			  	<ul>
		<?php 
			foreach( $sb_themes['top-header-social-media']  as $ar => $val)
			{  	
				if($ar == "facebook" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-facebook"></i></a></li>';}
				else if($ar == "twitter" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-twitter"></i></a></li>';}
				else if($ar == "rss" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-rss"></i></a></li>';}
				else if($ar == "pinterest" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-pinterest"></i></a></li>';}
				else if($ar == "youtube" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-youtube"></i></a></li>';}
				else if($ar == "digg" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-digg"></i></a></li>';}
				else if($ar == "stumbleupon" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-stumbleupon"></i></a></li>';}
				else if($ar == "linkedin" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-linkedin"></i></a></li>';}
				else if($ar == "google" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-google"></i></a></li>';}	
				else if($ar == "google-plus" && $val != ""){echo ' <li><a href="'. esc_attr( $val ) .'"><i class="fa fa-google-plus"></i></a></li>';}																								
			} 
		?>
			  </ul>
              </div>
              <?php 
			  		}
			  		else
			  		{
				  
						global $current_user;
						wp_get_current_user();
			  ?>
        
         
        <!--ul class="nav-right pull-right list-unstyled">
             <?php if( !is_user_logged_in() ){ ?>
             <li> <a href="<?php if( isset( $sb_themes['top-header-login-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-login-page'] ) ); } ?>">
             		<i class="fa fa-lock"></i> <?php echo __("Login", "logistic-pro");?>
						
                   </a>
             </li>
             <li> <a href="<?php if( isset( $sb_themes['top-header-register-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-register-page'] ) ); } ?>"><i class="fa fa-user"></i> <?php echo __("Sign Up", "logistic-pro");?></a></li>
             
             <?php }else{ ?>
             
             <li class="dropdown nav-profile"> 
             
           
             
             
                <a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">
                 <?php echo get_avatar( get_current_user_id(), 32, null, null, array( 'class' => 'img-circle resize' ) ); ?>
                 <span class="hidden-xs small-padding"> 
                 		<span><?php echo esc_html( $current_user->user_login ); ?></span> 
                        <i class="fa fa-caret-down"></i>
                  </span>
                </a>
                <ul class="dropdown-menu with-arrow pull-right dropdownhover-bottom" style="">
                  <li> <a href="<?php if( isset( $sb_themes['top-header-profile-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-profile-page'] ) ); } ?>"> <i class="fa fa-user"></i> <span><?php echo __("My Profile", "logistic-pro");?></span> </a> </li>
   
                     <li> <a href="<?php if( isset( $sb_themes['top-header-order-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-order-page'] ) ); } ?>"> <i class="fa fa-pencil"></i> <span><?php echo __("Place Order", "logistic-pro");?></span> </a> </li>
               
                  <li> <a href="<?php if( isset( $sb_themes['top-header-history-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-history-page'] ) ); } ?>"> <i class="fa fa-check"></i> <span><?php echo __("Tracking History", "logistic-pro");?></span> </a> </li>
      
                  <li> <a href="<?php echo esc_url( wp_logout_url() ); ?>"> <i class="fa fa-sign-out"></i> <span><?php echo __("Log Out", "logistic-pro");?></span> </a> </li>
                </ul>
            </li>
            
            <?php } ?>
        </ul-->
        
        	
            <?php } ?>
              
			
			
		  </div>
		</section>
<?php      
		} 
		  if( $sb_themes['header-styles'] == 1)
		  { 
			get_template_part( 'template-parts/header/header', '2' ); 
		  }
		  else
		  {
			  get_template_part( 'template-parts/header/header', '1' ); 
		  }
	 }
	 else
	 {
		  get_template_part( 'template-parts/header/header', 'onepage' );
	 }
	  
?>