<?php
/* ------------------------------------------------ */
/* Limit Excerpt Length */
/* ------------------------------------------------ */
function new_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

the_post_thumbnail();
/* ------------------------------------------------ */
/* Customize Menu */
/* ------------------------------------------------ */
function logistic_pro_web_custom_menu($theme_location, $ul_parent, $ul_child)
{
	global $sb_themes;	
    if (($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location])) {
        $menu_list  = '';
        $menu       = get_term($locations[$theme_location], 'nav_menu');
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        $menu_list .= '<ul class="'. $ul_parent.' ">' . "\n";
		
        $count   = 0;
        $submenu = false;
        foreach ($menu_items as $menu_item) {
            $link  = $menu_item->url;
			$onepageClass = '';
			//For Onepage only
			if( strpos($link, '#') !== false && $sb_themes['website-type'] == '1' && !is_home())
			{
				$link = home_url('/') . $link;
					
			}
			if($sb_themes['website-type'] == '1')
			{
				$onepageClass = 'page-scroll';	
			}			
			
            $title = $menu_item->title;
            if (!$menu_item->menu_item_parent) {
                $parent_id = $menu_item->ID;
                global $wpdb;

				$has_childs = $wpdb->get_var( $wpdb->prepare( 
					"SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_menu_item_menu_item_parent' AND meta_value = '%s'", 
					$menu_item->ID
				) );				
				$myactiveclass = '';
				if( logistic_pro_get_current_url() == $link )
				{
					
					$myactiveclass = ''; 
				}				

                $class       = "";
                $a_class     = '';
                $caret       = '';
                if ($has_childs > 0) {
                    $class   = ' class="dropdown'. ' ' . $myactiveclass .'"';	
                    $caret   = ' <span class="fa fa-angle-down"></span>';
                    $a_class = '  class="dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp"';
                }
				else
				{
					 $class       =  ' class="'.$myactiveclass.'"';
					 $a_class   = ' class="'. $onepageClass.'"';	
				}

                $menu_list .= '<li' . $class . '>' . "\n";
                $menu_list .= '<a href="' . esc_url( $link ) . '"' . $a_class . '>' . esc_html( $title ). $caret . '</a>' . "\n";
            }
            if ($parent_id == $menu_item->menu_item_parent) {
                
                if (!$submenu) {
                    $submenu = true;
                    $menu_list .= '<ul class="'.$ul_child.'">' . "\n";
                }
                $menu_list .= '<li>' . "\n";
                $menu_list .= '<a href="' . esc_url( $link ). '" class="'.$onepageClass .'">' . esc_html( $title ). '</a>' . "\n";
                $menu_list .= '</li>' . "\n";
                if (isset($menu_items[$count + 1]->menu_item_parent) && ($menu_items[$count + 1]->menu_item_parent != $parent_id) && $submenu) {
                    $menu_list .= '</ul>' . "\n";
                    $submenu = false;
                }
				
            }
            $temp = 0;
            if (isset($menu_items[$count + 1]->menu_item_parent)) {
                $temp = $menu_items[$count + 1]->menu_item_parent;
            }
           
                if (isset($menu_items[$count + 1]->menu_item_parent) && $menu_items[$count + 1]->menu_item_parent != $parent_id) {
                    $menu_list .= '</li>' . "\n";
                    $submenu = false;
                }
           
            $count++;
        }
       
	    $menu_list .= '</ul>' . "\n";
		
    } else {
        $menu_list  = '';
		$menu_list .= '<ul class="nav navbar-nav">' . "\n";
		$menu_list .= '<li><a href="'. esc_url( home_url( '/' ) ) .'">'. esc_html__('Home', 'logistic-pro') .'</a></li>' . "\n";
		$menu_list .= '<li><a href="'. esc_url( get_category_link(1) ) .'">'. esc_html( get_cat_name(1) ) .'</a></li>' . "\n";
		$menu_list .= '</ul">' . "\n";
    }
	
	
		echo wp_kses( $menu_list, logistic_pro_theme_required_tags() );
}


/* ------------------------------------------------ */
/* get current page url */
/* ------------------------------------------------ */

function logistic_pro_get_current_url()
{
 return $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
}
/* ------------------------------------------------ */
/* SB Theme The Breadcrumb */
/* ------------------------------------------------ */


function logistic_pro_theme_the_breadcrumb($shortcod_title = '', $shortcod_tagline = '') {
		
		global $sb_themes; 
		$string_li = '';
        if (!is_home()) {
                $string_li .=  '<li><a href="'. esc_url( home_url('/') ) .'">'. __('Home', 'logistic-pro'). '</a></li>';
				
                if (is_category() || is_single()) 
				{
					$title = __('Blog Archive', 'logistic-pro');
					$tagline = '';
					if( logistic_pro_theme_one_category() != "")
					{ 
						$string_li .=  '<li>'. logistic_pro_theme_one_category() .'</li>';
					}					
					if (is_single())
					{
						$postType = get_post_type();
						if( $postType  == 'shipment' )
						{
							
							$title = __('Shipment Details', 'logistic-pro');
							$tagline =  esc_html( get_the_title() );          										
							$string_li .= '<li>'. __("Shipment Details", "logistic-pro") . '</li>';
							
						}
						else if( $postType  == 'services' )
						{
							
							$title = __('Our Services', 'logistic-pro');
							$tagline =  esc_html( get_the_title() );          										
							$string_li .= '<li>'. __("Services Details", "logistic-pro") . '</li>';
							
						}
						else
						{
							$title = __('Single Blog', 'logistic-pro');
							$tagline = __('Blog Description', 'logistic-pro');//get_the_title();
							$string_li .=  "<li>". logistic_pro_theme_limit_text( $tagline , 3) .'</li>';
						}
					}
				
                } 
				elseif (is_page()) 
				{
						$title = __('Single Page', 'logistic-pro');
						$tagline = get_the_title();                
					
						if($shortcod_title != "" )
						{
							$title = $shortcod_title;
						}
						if($shortcod_title != "" )
						{
							$tagline = $shortcod_tagline;
						}
						
				        $string_li .=  '<li>' . $tagline . '</li>';
                }
				elseif (is_tag())
				{
					
						$title = __('Tags:', 'logistic-pro');
						$tagline = single_tag_title("", false);                					
					    $string_li .= '<li>'.$tagline.'</li>';
				}
				elseif (is_day())
				{
						$title = __('Archive Result(s)', 'logistic-pro');
						$tagline =  get_the_time('F jS, Y');             					
						$string_li .= '<li>'.$tagline.'</li>';					
				}
				elseif (is_month()) 
				{
						$title = __('Archive Result(s)', 'logistic-pro');
						$tagline =  get_the_time('F, Y');             					
						$string_li .= '<li>'.$tagline.'</li>';										
				}
				elseif (is_year()) 
				{
						$title = __('Archive Result(s)', 'logistic-pro');
						$tagline =  get_the_time('Y');             					
						$string_li .= '<li>'.$tagline.'</li>';										
				}
				elseif (is_author()) 
				{
						$title = __('Author(s) Archive', 'logistic-pro');
						$tagline =  get_the_author_meta(  'display_name', false );
						$string_li .= '<li>'.$tagline.'</li>';										
				}
				
				elseif (isset($_GET['paged']) && !empty($_GET['paged'])) 
				{
						$title = __('Blog Archive', 'logistic-pro');
						$tagline =  __('All archive results', 'logistic-pro');            										
						$string_li .=  '<li>'. $tagline .'</li>';
				}
				elseif (is_search())
				{
						$title = __('Search Results', 'logistic-pro');
						$tagline =  esc_html( $_GET['s'] );            										
					    $string_li .= '<li>'. $tagline . '</li>';
				}
				elseif (is_404())
				{
						$title = __('Page Not Found', 'logistic-pro');
						$tagline =  '404';            										
					    $string_li .= '<li>'. $tagline . '</li>';
				}
				elseif( is_tax( 'type' ) )
				{
						$title = __('Services We Provide', 'logistic-pro');
						$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
						$tagline =  $term->name;            										
					    $string_li .= '<li>'. $tagline . '</li>';
				}
				elseif( post_type_exists( 'services' ))
				{
						$title = __('All Services', 'logistic-pro');
						$tagline =  __("Services", "logistic-pro");          										
					    $string_li .= '<li>'. __("Services", "logistic-pro") . '</li>';
				}

				

        }
		else
		{
			$title = __('Homepage', 'logistic-pro');
			if (!is_front_page())
			{
				$title = __('Blog', 'logistic-pro');
				$tagline =  'Latest Posts';     
			}
			
			if( logistic_pro_theme_one_category() != "")
			{ 
				$string_li .=  '<li><a href="'. esc_url( home_url('/') ) .'">'. __('Home', 'logistic-pro'). '</a></li>';	
			}
			
		}
		$string = '';
		
		$custom_class = '';
		if( $sb_themes['header-styles'] == 2){ $custom_class = 'customs'; }
		
		$string .= '<!--section class="breadcrumbs-area parallex">
						<div class="container">
							<div class="row">
								<div class="page-title '.$custom_class.'">
									<div class="col-sm-12 col-md-6 page-heading text-left">
										<h3>' . esc_html( $title ) .' </h3>
										<h2 style="margin-top:23px!important;">' . esc_html( @strip_tags( $tagline )) . '</h2>
									</div>
									<div class="col-sm-12 col-md-6 text-right">
										<ul class="breadcrumbs">'. $string_li .'
										</ul>
									</div>
								</div>
							</div>
						</div>
					</section-->';
		return $string;
}


function logistic_pro_theme_services_bar($type)
{
	
		$taxonomies = array($type);
		$args = array(
			'orderby'                => 'name',
			'order'                  => 'desc',
			'hide_empty'             => false,
			'pad_counts'             => false,
		); 
		
		$terms = get_terms($taxonomies, $args);
		$ar_type = array();
		foreach( $terms as $term )
		{
			$count = 'count';		 
			$ar_type[$term->term_id] = $term->name;
		}		
		
		$li = '<div class="col-md-3 col-sm-12 col-xs-12" id="side-bar"><div class="theiaStickySidebar"><div class="side-bar-services"><ul class="side-bar-list">';
		$ccc = 1;
		$class_active = '';
        foreach( $ar_type as $ar_type => $val ){ 
		$class_active = '';
		if($ccc == 1){$class_active = 'class="active"';}
		$li .= '<li><a '.$class_active .' href="'. esc_url (get_term_link( $ar_type )).'" title="'. esc_attr( $val ). '">'. esc_html( $val ). '</a></li>'; $ccc++; }
	$li .= '</ul></div></div></div>';
	return $li;
}
/* ------------------------------------------------ */
/* Get Only One Category */
/* ------------------------------------------------ */

function logistic_pro_theme_one_category(){
  $cat = get_the_category();
  $link ='';
  if(count( $cat) > 0){
  $link = '<a href="' . get_category_link($cat[0]->term_id) . '">'.$cat[0]->cat_name.'</a>';
  }
  return $link;
}
/* ------------------------------------------------ */
/* limit_text */
/* ------------------------------------------------ */
function logistic_pro_theme_limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }

/* ------------------------------------------------ */
/* Customize Comments */
/* ------------------------------------------------ */
function logistic_pro_themes_comment($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment; ?>

<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
  <div class="comment-info">
    <?php if ($comment->comment_approved == '0') { ?>
    <em><?php echo esc_html__('Your comment is awaiting moderation.', 'logistic-pro') ?></em> <br />
    <?php } ?>
    <img src="<?php echo get_avatar_url( get_the_ID() ); ?>" alt="<?php echo esc_attr( get_comment_author( get_comment_ID() )); ?>" class="pull-left hidden-xs">
    <div class="author-desc">
      <div class="author-title"> <strong><?php echo get_comment_author( get_comment_ID() ); ?></strong>
        <ul class="list-inline pull-right">
          <li><?php echo get_comment_date(); ?></li>
          <li>
            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </p>
          </li>
        </ul>
      </div>
      <?php comment_text() ?>
    </div>
  </div>
  <?php if( $args['has_children'] == "" )
   { echo '</li>'; }?>
  <!-- end media-body -->
  <?php
 }
 
//for the comment wrapping functions - ensures HTML does not break.
$comment_open_div = 0;

/**
 * Creates an opening div for a bootstrap row.
 * @global int $comment_open_div
 */
function _lp_before_comment_fields(){
    global $comment_open_div;
    $comment_open_div = 1;
    echo '<div class="row">';
}
/**
 * Creates a closing div for a bootstrap row.
 * @global int $comment_open_div
 * @return type
 */
function _lp_after_comment_fields(){
    global $comment_open_div;
    if($comment_open_div == 0)
        return;
    echo '</div>';
}

add_action('comment_form_before', '_lp_before_comment_fields');
add_action('comment_form_after', '_lp_after_comment_fields');
/* ------------------------------------------------ */
/* Pagination */
/* ------------------------------------------------ */
function logistic_pro_themes_pagination()
{
    if (is_singular())
        return;
   
    global $wp_query;
    /** Stop execution if there's only 1 page */
    
    if ($wp_query->max_num_pages <= 1)
        return;
    
    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max   = intval($wp_query->max_num_pages);
    
    /**    Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;
    
    /**    Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
    
    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
    
    echo '<ul class="pagination">' . "\n";
    
    if (get_previous_posts_link())
        printf('<li>%s</li>' . "\n", get_previous_posts_link( '<span class="fa fa-angle-double-left"></span>' ));
    
    /**    Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';
        
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');
        
        if (!in_array(2, $links))
            echo '<li><a href="javascript:void(0);">...</a></li>';
    }
    
    /**    Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }
    
    /**    Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li><a href="javascript:void(0);">...</a></li>' . "\n";
        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }
    
    if (get_next_posts_link())
        printf('<li>%s</li>' . "\n", get_next_posts_link( '<span class="fa fa-angle-double-right"></span>' ));
    echo '</ul>' . "\n";
    
}
/* ------------------------------------------------ */
/* Enquee Custom Scripts */
/* ------------------------------------------------ */
function logistic_pro_masonry_inline_script() {
  if ( wp_script_is( 'masonry-layout-script', 'done' ) ) {
	  wp_enqueue_script( 'imagesloaded' );
	  wp_enqueue_script( 'isotope' );

?>
  <script type="text/javascript">(function($) {"use strict";  })(jQuery); </script>
  <?php
  }
}
/* add_action( 'wp_footer', 'logistic_pro_masonry_inline_script' ); */

// ------------------------------------------------ //
// Blog Settings and Layouts //
// ------------------------------------------------ //
function logistic_pro_themes_blogLayout($blog_layout = 2, $sidebar = 1, $masonry = 'masonry'){

  if($blog_layout == 2)
  {
	    //Sidebar
		$cols = 8;
		$cols_inner = 6;
		$blog_layout = $blog_layout;
		$prefix_after = 2;
		$is_sidebar   = $sidebar;
  }
  else 
  {	  
		$cols = 12;
		$cols_inner = 4;
		$blog_layout = $blog_layout;
		$prefix_after = 3;
		$is_sidebar   = 0;

  }
  
 
 	$arr = array
	(
		'blog-layout' => $blog_layout,
		'blog-layout-cols' => $cols,
		'blog-layout-cols-inner' => $cols_inner,
		'clearfix' 			=> $prefix_after,
		'is-sidebar'			=> $is_sidebar,
		'masonry' => $masonry,
	);
	
 	return $arr;
}

// ------------------------------------------------ //
// Get and Set Post Views //
// ------------------------------------------------ //
 function logistic_pro_themes_getPostViews($postID){
	 $postID	=	esc_html( $postID );
  $count_key = 'sb_themes_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
   delete_post_meta($postID, $count_key);
   add_post_meta($postID, $count_key, '0');
   return "0";
  }
  return $count;
 }
 function logistic_pro_themes_setPostViews($postID) {
	 $postID 	=	esc_html( $postID );
  $count_key = 'sb_themes_post_views_count';
  $count = get_post_meta($postID, $count_key, true);
  if($count==''){
   $count = 0;
   delete_post_meta($postID, $count_key);
   add_post_meta($postID, $count_key, '0');
  }else{
   $count++;
   update_post_meta($postID, $count_key, $count);
  }
 }


/* ------------------------------------------------ */
/* Get Footer Links */
/* ------------------------------------------------ */
function logistic_pro_themes_pages_or_cats($main, $start_pages, $start_cats)
{
	global $sb_themes;
	$footer_link = '';
	if( $sb_themes[$main] )
	{
		$links = $sb_themes[$start_pages];
		if( count( $links ) > 0 && $links != "" )
		{
			foreach( $links as $link ){	
			  if( !get_post_type( $link ) ){ continue; }	 
			  $footer_link .= '<li><a href="'. esc_url( get_the_permalink( $link ) ).' ">'. esc_html( get_the_title($link) ).'</a></li>'; 
			}
		}
	}
	else
	{
		$links = $sb_themes[$start_cats];	
		if( count( $links ) > 0 )
		{
			foreach( $links as $link ){	 
			  $footer_link .= '<li><a href="'. esc_url( get_category_link( $link ) ).' ">'. esc_html( get_cat_name($link) ).'</a></li>'; 
			}
		}
	}
	
		echo wp_kses( $footer_link, logistic_pro_theme_required_tags() );
	
}


/* ------------------------------------------------ */
/* Get Image Data */
/* ------------------------------------------------ */

function logistic_pro_theme_get_attachment( $attachment_id, $img_size ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, $img_size, true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}

/* ------------------------------------------------ */
/* Add to your init function */
/* ------------------------------------------------ */

add_filter('get_search_form', 'logistic_pro_themes_search_form');
function logistic_pro_themes_search_form($text)
{
    
    $text = str_replace('<label>', '<div class="search">', $text);
    $text = str_replace('</label>', '<button class="sea-icon"><i class="fa fa-search"></i></button></div>', $text);
    $text = str_replace('<span class="screen-reader-text">Search for:</span>', '', $text);
    $text = str_replace('class="search-field"', 'class="form-control" id="serch"', $text);
    $text = str_replace('type="submit"', 'type="hidden"', $text);
    return $text;
}

/* ------------------------------------------------ */
/* Allow HTML TAGS  wp_kses( $string , logistic_pro_theme_required_tags() ) */
/* ------------------------------------------------ */

function logistic_pro_theme_words_count($contect = '', $limit = 0)
{
	$contents     = strip_tags($contect);
	$removeSpaces = str_replace(" ", "", $contents);
	if (strlen($removeSpaces) > $limit) {
		return substr($contents, 0, $limit) . '..';
	} else {
		return $contents;
	}

}

/* ------------------------------------------------ */
/* Flaticons array */
/* ------------------------------------------------ */

function logistic_pro_theme_flaticons()
{
		
	
	 return array(
	'flaticon-24-hours-delivery',
	'flaticon-24-hours-phone-service',
	'flaticon-24-hours-symbol',
	'flaticon-air-transport',
	'flaticon-airplane-around-earth',
	'flaticon-airplane-frontal-view',
	'flaticon-airplane-in-vertical-ascending-position',
	'flaticon-barscode-with-a-magnifier-business-symbol',
	'flaticon-black-delivery-small-truck-side-view',
	'flaticon-black-opened-umbrella-symbol-with-rain-drops-falling-on-it',
	'flaticon-boat-from-front-view',
	'flaticon-box-of-packing-for-delivery',
	'flaticon-boxes-piles-stored-inside-a-garage-for-delivery',
	'flaticon-boxes-storage-for-delivery-inside-a-truck-box-from-back-view',
	'flaticon-call-center-service-for-information', 
	'flaticon-call-center-worker-with-headset',
	'flaticon-chronometer', 
	'flaticon-clipboard-verification-symbol',
	'flaticon-commercial-delivery-symbol-of-a-list-on-clipboard-on-a-box-package',
	'flaticon-container-hanging-of-a-crane', 
	'flaticon-container-on-a-crane', 
	'flaticon-containers-on-oceanic-ship',
	'flaticon-crane',
	'flaticon-crane-truck',
	'flaticon-delivered-box-verification-symbol',
	'flaticon-delivery-box',
	'flaticon-delivery-box-and-timer',
	'flaticon-delivery-box-on-a-hand', 
	'flaticon-delivery-box-package-opened-with-up-arrow', 
	'flaticon-delivery-cube-box-package-with-four-arrows-in-different-directions',
	'flaticon-delivery-of-a-box',
	'flaticon-delivery-pack-security-symbol-with-a-shield',
	'flaticon-delivery-package',
	'flaticon-delivery-package-box-with-fragile-content-symbol-of-broken-glass',
	'flaticon-delivery-package-opened',
	'flaticon-delivery-package-with-umbrella-symbol',
	'flaticon-delivery-packages-on-a-trolley', 
	'flaticon-delivery-packaging-box', 
	'flaticon-delivery-packing',
	'flaticon-delivery-scale-with-a-box', 
	'flaticon-delivery-time-symbol', 
	'flaticon-delivery-time-tool', 
	'flaticon-delivery-transportation-machine',
	'flaticon-delivery-truck', 
	'flaticon-delivery-truck-1', 
	'flaticon-delivery-truck-2', 
	'flaticon-delivery-truck-with-circular-clock', 
	'flaticon-delivery-truck-with-packages-behind', 
	'flaticon-delivery-worker-giving-a-box-to-a-receiver', 
	'flaticon-fragile-broken-glass-symbol-for-delivery-boxes', 
	'flaticon-free-delivery-truck',
	'flaticon-frontal-truck',
	'flaticon-identification-for-delivery-with-bars', 
	'flaticon-international-calling-service-symbol',
	'flaticon-international-delivery', 
	'flaticon-international-delivery-business-symbol-of-world-grid-with-an-arrow-around',
	'flaticon-international-delivery-symbol',
	'flaticon-international-logistics-delivery-truck-symbol-with-world-grid-behind',
	'flaticon-localization-orientation-tool-of-compass-with-cardinal-points',
	'flaticon-locked-package',
	'flaticon-logistics-delivery-truck-and-clock', 
	'flaticon-logistics-delivery-truck-in-movement',
	'flaticon-logistics-package',
	'flaticon-logistics-transport', 
	'flaticon-logistics-truck', 
	'flaticon-logistics-weight-scale',
	'flaticon-man-standing-with-delivery-box',
	'flaticon-ocean-transportation',
	'flaticon-package-cube-box-for-delivery',
	'flaticon-package-delivery',
	'flaticon-package-delivery-in-hand',
	'flaticon-package-for-delivery',
	'flaticon-package-on-rolling-transport',
	'flaticon-package-transport-for-delivery', 
	'flaticon-package-transportation-on-a-trolley', 
	'flaticon-packages-storage-for-delivery', 
	'flaticon-packages-transportation-on-a-truck',
	'flaticon-person-standing-beside-a-delivery-box', 
	'flaticon-phone-auricular-and-a-clock',
	'flaticon-phone-auricular-and-clock-delivery-symbol',
	'flaticon-placeholder-on-map-paper-in-perspective',
	'flaticon-protection-symbol-of-opened-umbrella-silhouette-under-raindrops',
	'flaticon-sea-ship', 
	'flaticon-sea-ship-with-containers', 
	'flaticon-search-delivery-service-tool',
	'flaticon-storage',
	'flaticon-talking-by-phone-auricular-symbol-with-speech-bubble',
	'flaticon-telephone',
	'flaticon-telephone-1',
	'flaticon-three-stored-boxes-for-delivery',
	'flaticon-train-front',
	'flaticon-triangular-arrows-sign-for-recycle',
	'flaticon-up-arrows-couple-sign-for-packaging',
	'flaticon-verification-of-delivery-list-clipboard-symbol',
	'flaticon-view-symbol-on-delivery-opened-box', 
	'flaticon-weight-of-delivery-package-on-a-scale', 
	'flaticon-weight-tool',
	'flaticon-woman-with-headset',
	'flaticon-wood-package-box-of-square-shape-for-delivery',
	'flaticon-world-grid-with-placeholder',
	);
}

/* ------------------------------------------------ */
/* Get param value of VC */
/* ------------------------------------------------ */

function logistic_pro_theme_get_param_vc($break, $string)
{
 $arr = explode($break, $string);
 $res = explode(' ', $arr[1] );
 $r =  explode('"', $res[0] );
 return $r[1];
}

/* ------------------------------------------------ */
/* Allow HTML TAGS  wp_kses( $string , logistic_pro_theme_required_tags() ) */
/* ------------------------------------------------ */

function logistic_pro_theme_required_attributes()
{

return $default_attribs = array(
	'id' => array(),
	'class' => array(),
	'value' => array(),
	'title' => array(),
	'type' => array(),
	'style' => array(),
	'data' => array(),
	'role' => array(),
	'aria-haspopup' => array(),
	'aria-expanded' => array(),
	'data-toggle' => array(),
	'data-mce-id' => array(),
	'data-mce-style' => array(),
	'data-mce-bogus' => array(),
	'data-href' => array(),
	'data-tabs' => array(),
	'data-small-header' => array(),
	'data-adapt-container-width' => array(),
	'data-height' => array(),
	'data-hide-cover' => array(),
	'data-show-facepile' => array(),
	'data-hover' => array(),
	'data-toggle' => array(),
	'data-animations' => array(),
);
}


function logistic_pro_cargo_types()
{

	$types = array(
		esc_html__('Select Cargo Type', 'logistic-pro'),
		esc_html__('Courier Services', 'logistic-pro'),
		esc_html__('Ocean Cargo', 'logistic-pro'),
		esc_html__('Ground', 'logistic-pro'),
		esc_html__('Worldwide', 'logistic-pro'),
		esc_html__('Cargo Air', 'logistic-pro'),
		esc_html__('Fly Anywhere', 'logistic-pro'),
		esc_html__('Door To Door', 'logistic-pro'),
	
	);
	return $types;
}

function logistic_pro_theme_required_tags()
{
return $allowed_tags = array(
	'div'           => logistic_pro_theme_required_attributes(),
	'span'          => logistic_pro_theme_required_attributes(),
	'p'             => logistic_pro_theme_required_attributes(),
	'a'             => array_merge( logistic_pro_theme_required_attributes(), array(
		'href' => array(),
		'target' => array('_blank', '_top'),
	) ),
	'u'             =>  logistic_pro_theme_required_attributes(),
	'i'             =>  logistic_pro_theme_required_attributes(),
	'q'             =>  logistic_pro_theme_required_attributes(),
	'b'             =>  logistic_pro_theme_required_attributes(),
	'ul'            => logistic_pro_theme_required_attributes(),
	'ol'            => logistic_pro_theme_required_attributes(),
	'li'            => logistic_pro_theme_required_attributes(),
	'br'            => logistic_pro_theme_required_attributes(),
	'hr'            => logistic_pro_theme_required_attributes(),
	'strong'        => logistic_pro_theme_required_attributes(),
	'blockquote'    => logistic_pro_theme_required_attributes(),
	'del'           => logistic_pro_theme_required_attributes(),
	'strike'        => logistic_pro_theme_required_attributes(),
	'em'            => logistic_pro_theme_required_attributes(),
	'code'          => logistic_pro_theme_required_attributes(),
	'style'          => logistic_pro_theme_required_attributes(),
	'select'          => logistic_pro_theme_required_attributes(),
	'option'          => logistic_pro_theme_required_attributes(),
	'script'          => logistic_pro_theme_required_attributes(),
);


}


