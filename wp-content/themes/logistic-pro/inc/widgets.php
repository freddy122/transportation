<?php
add_action( 'widgets_init', function(){
     register_widget( 'SBThemeWidgets_recent_posts' );
});
class SBThemeWidgets_recent_posts extends WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'Logistic Pro Recent Posts' );
	}
	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
	
		
		global $post;
		if($instance['sb_no_of_posts'] == "" )
		{
			$instance['sb_no_of_posts']	=	5;	
		}
		$args = array( 'post_type' => 'post', 'posts_per_page' => $instance['sb_no_of_posts'], 'orderby' => 'date', 'order' => 'DESC');
		$recent_posts = get_posts( $args );
	?>

<div class="widget"> 
  <!--Recent Posts heading-->
  <div class="latest-news">
    <h2><?php echo esc_html( $instance['title'] ); ?></h2>
    
    <!--end Recent Posts--> 
    <!--recent section-->
    <?php
foreach ( $recent_posts as $recent_post )
{ 
	$feat_image = wp_get_attachment_image_src( get_post_thumbnail_id($recent_post->ID), 'logistic-pro-blog-thumb-80' );
	$src	=	$feat_image[0];
	$class_name = 'post';
	if($src == "")
	{
		$class_name = 'post no-thumb';
	}
	//
?>
    <div class="<?php echo esc_attr( $class_name ); ?>">
      <?php 
	$class_title	= 'class=recent-title-2';
    if( $src != "" )
    {
		$class_title	= 'class=recent-title';
    ?>
      <div class="recent-img">
        <figure class="post-thumb"> <a href="<?php echo esc_url( get_the_permalink( $recent_post->ID ) ); ?>"> <img src="<?php echo esc_url( $src ); ?>" width="100" height="75" alt="<?php echo esc_attr( get_the_title( $recent_post->ID ) ); ?>"> </a> </figure>
      </div>
      <?php
    }
    ?>
      <div <?php echo esc_attr( $class_title ); ?>>
        <h4> <a href="<?php echo esc_url( get_the_permalink( $recent_post->ID ) ); ?>"> <?php echo esc_html( get_the_title( $recent_post->ID ) ); ?> </a> </h4>
        <div class="post-info"><i class="fa fa-calendar"></i> <?php echo get_the_date(get_option( 'date_format' ), $recent_post->ID );  ?> </div>
      </div>
    </div>
    <?php 
}
?>
  </div>
</div>
<?php
	}
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = esc_html__( 'Recent Posts', 'logistic-pro' );
		}
		if ( isset( $instance[ 'sb_no_of_posts' ] ) ) {
			$sb_no_of_posts = $instance[ 'sb_no_of_posts' ];
		}
		else {
			$sb_no_of_posts = 5;
		}
		?>
<p>
  <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" > <?php echo esc_html__( 'Title:', 'logistic-pro' ); ?> </label>
  <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
</p>
<p>
  <label for="<?php echo esc_attr( $this->get_field_id( 'sb_no_of_posts' ) ); ?>">
    <?php esc_html__( 'How many posts to diplay:', 'logistic-pro' ); ?>
  </label>
  <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'sb_no_of_posts' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'sb_no_of_posts' ) ); ?>" type="text" value="<?php echo esc_attr( $sb_no_of_posts ); ?>">
</p>
<?php 
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['sb_no_of_posts'] = ( ! empty( $new_instance['sb_no_of_posts'] ) ) ? strip_tags( $new_instance['sb_no_of_posts'] ) : '';
		return $instance;
	}
} // Recent Posts