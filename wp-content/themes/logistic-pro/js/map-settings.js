// JavaScript Document
function initialize() {
	var marker =	 document.getElementById('sb_marker').value;
  	var mapCanvas = document.getElementById('map');
var get_lat	=	document.getElementById( 'sb_lat' ).value;
var get_long	=	document.getElementById( 'sb_long' ).value;
    var mapOptions = {
      center: new google.maps.LatLng(get_lat, get_long),
      disableDefaultUI: true,
      scrollwheel: false,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    //Create map
    var map = new google.maps.Map(mapCanvas, mapOptions);

    //Create marker
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(get_lat, get_long),
      map: map,
      icon: marker
 	});

    //Map marker info
    var contentString = '';

    //Add info to marker 
	var infowindow = new google.maps.InfoWindow({
	  content: contentString
	});

    //Keep map centered
    google.maps.event.addDomListener(window, 'resize', function() {
    	var center = map.getCenter();
    	google.maps.event.trigger(map, "resize");
    	map.setCenter(center); 
	});
  }
  google.maps.event.addDomListener(window, 'load', initialize);