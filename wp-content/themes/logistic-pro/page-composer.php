<?php /* Template Name: Visual Composer */
get_header(); ?>
<?php global $sb_themes; ?>
<section>
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content();?>
			<?php endwhile; ?>
             <div class="clearfix"></div>		
		<?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>
		<?php endif; ?>
</section>
<?php get_footer(); ?>
