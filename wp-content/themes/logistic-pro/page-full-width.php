<?php
/**
 * Template Name: Full Width Page
 * The template for displaying all pages.
 *
 */
$pgname = get_query_var("pagename");
if(!is_user_logged_in() && ($pgname != "connexion") && $pgname != "inscription" && $pgname != "mention-legales" && ($pgname != "contact")){
    wp_redirect(get_site_url());
    exit;
}
get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if($sb_themes['page-settings-breadcrumb'] != 0) { echo logistic_pro_theme_the_breadcrumb();  } 
?>
<!--section class="section-padding-70 style_bg_images_customs"-->
<section class="section-padding-70">
  <div class="container"> 
    <!-- Row -->
    <div class="row" style="margin-top: 25px;">
      <div class="col-sm-12 col-xs-12 col-md-12"> 
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				
<?php 
	$image =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'logistic-pro-blog-1' );
?>
  <!-- page image -->
  <div class="news-thumb">
    <?php if ( $image[0] != "" ){ ?>
    <div class="news-thumb <?php if ( $image[0] == "" ){ esc_attr( 'no-img-1' );} ?>">
    
      <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() );?>" > <img alt="<?php echo esc_attr( get_the_title() );?>" class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>"> </a>
     
    </div>
     <?php } ?>
    <!-- page image end --> 
      <?php
            /*the_content( sprintf(
                /* translators: %s: Name of current post. */
            /*wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'logistic-pro' ), array( 'span' => array( 'class' => array()))),
                the_title( '<span class="screen-reader-text">"', '"</span>', false )
            ) );*/
            ?>
    
                   <?php
                   
                   if($pgname == "services-10" || $pgname=="ancienne-transaction" ||$pgname =="demande-enregistrer" || $pgname=="liste-devis"){
                       echo do_shortcode("[service_page]");
                   }elseif($pgname =="connexion"){
                       echo do_shortcode("[woocommerce_my_account]");
                   }elseif($pgname == "contact"){
                       echo do_shortcode('[contacts_page]');
                       //echo "page contact";
                   }elseif($pgname == "mails" || $pgname == "devis-remporter" || $pgname == "information-de-lentreprise" || $pgname == "devis-transitaire" || $pgname == "devis-confirmer"){
                       echo do_shortcode("[mails_page]");
                   }elseif($pgname=="test-page"){
                       echo do_shortcode("[liste_contact]");
                   }
                   
                    /*$args = array (
                        'before'            => '<div class="page-links-foodpro text-center">',
                        'after'             => '</div>',
                        'link_before'       => '<span class="btn btn-primary">',
                        'link_after'        => '</span>',
                        'next_or_number'    => 'next',
                        'separator'         => '   ',
                        'nextpagelink'      => esc_html__( 'Next >>', 'logistic-pro' ),
                        'previouspagelink'  => esc_html__( '<< Prev', 'logistic-pro' ),
                    );*/
                     
                     wp_link_pages($args); 
                ?>            
   
    <!-- page detail end --> 
    
  </div>
  <!-- page-grid end --> 
                
                
				
				<?php
                // If comments are open or we have at least one comment, load up the comment template.
				if($sb_themes['page-comments-show'] != 1 ){
                if ( comments_open() || get_comments_number() ) 
                {
					comments_template();
                }
                else
                {
                ?>
                    <!--div class="custom-heading">
                    <h2>
                    <p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'logistic-pro' ); ?></p>
                    </h2>
                    
                    </div-->
                <?php	
                }}
                ?>

			<?php endwhile; ?>
             <div class="clearfix"></div>		
		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
        
		</div> <!-- right column -->
    </div>
    <!-- End row --> 
      </div>
    </div>    
    <!-- Row End --> 
  </div>
  <!-- end container --> 
</section>
<!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= --> 


<?php get_footer(); ?>
