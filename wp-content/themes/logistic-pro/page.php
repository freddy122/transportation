<?php
/**
 * The template for displaying all pages.
 *
 */
get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if($sb_themes['page-settings-breadcrumb'] != 0) { echo logistic_pro_theme_the_breadcrumb();  } 
?>
<section id="blog" class="custom-padding">
  <div class="container"> 
    <!-- Row -->
    <div class="row">
      <div class="col-sm-12 col-xs-12 col-md-8"> 
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					get_template_part( 'template-parts/content', 'page' );
				?>
				<?php
                // If comments are open or we have at least one comment, load up the comment template.
				if($sb_themes['page-comments-show'] != 1 ){
                if ( comments_open() || get_comments_number() ) 
                {
					comments_template();
                }
                else
                {
                ?>
                    <div class="custom-heading">
                    <h2>
                    <a  href="#" class="no-comments"><?php esc_html_e( 'Comments are closed.', 'logistic-pro' ); ?></a>
                    </h2>
                    
                    </div>
                <?php	
                }
				}
                ?>

			<?php endwhile; ?>
             <div class="clearfix"></div>		
		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
        
		</div> <!-- right column -->
       
	  <?php get_sidebar();  ?>
      <!-- right column end--> 
      <!-- /.col-md-4 --> 
    </div>
    <!-- End row --> 
      </div>
    </div>    
    <!-- Row End --> 
  </div>
  <!-- end container --> 
</section>
<!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= --> 


<?php get_footer(); ?>
