<?php /* The template for displaying all single posts. */
get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if($sb_themes['services-settings-breadcrumb'] != 0) { echo logistic_pro_theme_the_breadcrumb();  }
    $is_layout    = $sb_themes['services-sidebar-show'];
	$col = 9;
	$show_sidebar = 2;
	$clearfix = 2;
	if($is_layout == 0)
	{
		$show_sidebar = 0;
		$col = 12;
		$clearfix = 3;
	}
	else if($is_layout == 2)
	{
		$show_sidebar = 1;
	}
	$count = 1;
	
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


    <!-- =-=-=-=-=-=-= Blog & News =-=-=-=-=-=-= -->
    <section class="section-padding-70 services-2">
        <div class="container">
            <!-- Row -->
            <div class="row">
				<?php if($show_sidebar == 1) { echo logistic_pro_theme_services_bar('type'); } ?>
                <div class="col-md-<?php echo esc_attr( $col ); ?> col-sm-12 col-xs-12">
                <?php while ( have_posts() ) { the_post(); 
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'logistic-pro-blog-1' );
                ?>
                        <div class="news-thumb <?php if ( $image[0] == "" ){ esc_attr( 'no-img-1' );} ?>">
          <?php if ( $image[0] != "" ){ ?>
          <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() );?>" > <img alt="<?php echo esc_attr( get_the_title() );?>" class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>"> </a>
          <?php } ?>
        </div>
                        <div class="custom-heading margin-top-20">    
          <h2 class="h2-title"><a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ) ?>">
            <?php the_title() ?>
            </a></h2>
            </div>
                <?php the_content( sprintf( wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'logistic-pro' ), array( 'span' => array( 'class' => array()))), the_title( '<span class="screen-reader-text">"', '"</span>', false ) ) );  ?>
                <?php } ?>
                 </div>
                <!-- right column -->
                <?php if($show_sidebar == 2) { echo logistic_pro_theme_services_bar('type'); } ?>
            </div>
            <!-- Row End -->
        </div>
        <!-- end container -->
    </section>
    
    <!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= -->
</article>    
<?php get_footer(); ?>
