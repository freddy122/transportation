<?php
/**
 * The template for displaying all single posts.
 */
get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if( $sb_themes['blog-single-settings-breadcrumb'] != 0 ) { echo logistic_pro_theme_the_breadcrumb();  } 
?>
<?php 
   $is_layout    = $sb_themes['blog-settings-look'];
   $is_sidebar   = $sb_themes['blog-settings-sidebar'];
   $is_masonry   = $sb_themes['blog-settings-type'];

	$lSet 		 = logistic_pro_themes_blogLayout($is_layout, $is_sidebar, $is_masonry);
	$blog_layout      		= $lSet['blog-layout'];
	$blog_layout_cols 		= $lSet['blog-layout-cols'];
	$blog_layout_cols_inner = $lSet['blog-layout-cols-inner'];
	$clearfix				= $lSet['clearfix'];
	$is_sidebar				= $lSet['is-sidebar'];
	$masonry				= $lSet['masonry'];

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
  <section id="blog" class="custom-padding logisticpro-single-blog">
    <div class="container"> 
      <div class="row"> 
        <?php if($is_sidebar == 2){ get_sidebar(); }   ?>
        <div class="col-sm-12 col-xs-12 col-md-<?php echo esc_attr( $blog_layout_cols ); ?>">
          <?php while ( have_posts() ) : the_post(); ?>
          <?php get_template_part( 'template-parts/content', 'single' ); ?>
          <div class="clearfix"></div>
          <?php
			if($sb_themes['blog-comments-show'] != 1 )
			{
				if ( comments_open() || get_comments_number() ) 
				{
					 comments_template();
				}
				else
				{
					?>
					  <div class="custom-heading">
						<h2>
						  <a href="#" class="no-comments">
							<?php esc_html_e( 'Comments are closed.', 'logistic-pro' ); ?>
						  </a>
						</h2>
					  </div>
  			<?php	
				}
			}
			?>
          <?php endwhile; // End of the loop. ?>
        </div>
        <?php if($is_sidebar == 1){ get_sidebar(); }   ?>
      </div>
    </div>
  </section>
</article>
<?php get_footer(); ?>
