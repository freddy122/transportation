<?php /* The main template file. */
get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if($sb_themes['services-settings-breadcrumb'] != 0) { echo logistic_pro_theme_the_breadcrumb();  } 
?>
<?php 
    $is_layout    = $sb_themes['services-sidebar-show'];
	$col = 6;
	$show_sidebar = 2;
	$clearfix = 2;
	if($is_layout == 0)
	{
		$show_sidebar = 0;
		$col = 4;
		$clearfix = 3;
	}
	else if($is_layout == 2)
	{
		$show_sidebar = 1;
	}
	$count = 1;
?>
<section class="custom-padding white" id="about-section-3">
  <div class="container">
    <div class="main-boxes">
      <div class="row">
        <?php if($show_sidebar == 1) { echo logistic_pro_theme_services_bar('type'); } ?>
        <div class="col-md-9 col-sm-12 col-xs-12">
          <?php 
		$category = get_queried_object();
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args=array('post_type' => 'services', 'post_status' => 'publish', 'paged' => $paged, 'tax_query' => array(
        array(
			'taxonomy' => $category->taxonomy,
			'field' => 'slug',
			'terms' => array($category->slug))
		));
        $services = null;
        $services = new WP_Query($args);	  
	  ?>
          <?php if ( $services->have_posts() ) { ?>
          <?php while ( $services->have_posts() ) { 
					$services->the_post(); 			
					$is_image	= wp_get_attachment_image_src( get_post_thumbnail_id(), 'logistic-pro-services-thumb' );
		?>
          <div class="col-sm-12 col-md-<?php echo esc_attr( $col ); ?> col-xs-12">
            <div class="services-grid-1">
              <?php if($is_image[0] != ""){?>
              <div class="service-image"> <a href="<?php echo esc_html( get_the_permalink() ); ?>"> <img alt="<?php echo esc_attr( get_the_title() ); ?>" src="<?php echo esc_url( $is_image[0] ); ?>"> </a> </div>
              <?php } ?>
              <div class="services-text">
                <h4> <a href="<?php echo esc_url( get_the_permalink() ); ?>" title="<?php echo esc_attr( get_the_title() ); ?>"> <?php echo esc_html( get_the_title() ); ?> </a> </h4>
                <p><?php echo logistic_pro_theme_words_count( esc_html( get_the_content() ), 150); ?></p>
              </div>
              <div class="more-about"> <a class="btn btn-primary btn-lg" href="<?php echo esc_url( get_the_permalink() ); ?>"> <?php echo esc_html__( 'Read More', 'logistic-pro' ); ?> <i class="fa fa-chevron-circle-right"></i> </a> </div>
            </div>
            <!-- end services-grid-1 --> 
          </div>
          <?php		
				if( $count % $clearfix == 0 ){  echo '<div class="clearfix"></div>'; };
				$count++;
            ?>
          <?php } ?>
          <div class="clearfix"></div>
          <span class="separator"></span>
          <div class="custom-pagination text-center">
            <?php logistic_pro_themes_pagination(); ?>
          </div>
          <?php }else{ ?>
          <?php get_template_part( 'template-parts/content', 'none' ); ?>
          <?php } ?>
        </div>
        <?php if($show_sidebar == 2) { echo logistic_pro_theme_services_bar('type'); } ?>
      </div>
    </div>
    <!-- End row --> 
  </div>
</section>
<?php get_footer(); ?>