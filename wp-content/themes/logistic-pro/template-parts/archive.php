<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 */

get_header(); ?>
<?php global $sb_themes; ?>
<?php  
	// Breadcrumb settings
	if($sb_themes['blog-single-settings-breadcrumb'] != 0) { echo logistic_pro_theme_the_breadcrumb();  } 
?>
<?php 

   $is_layout    = $sb_themes['blog-settings-look'];
   $is_sidebar   = $sb_themes['blog-settings-sidebar'];
   $is_masonry   = $sb_themes['blog-settings-type'];

	$lSet 		 = logistic_pro_themes_blogLayout($is_layout, $is_sidebar, $is_masonry);
	$blog_layout      		= $lSet['blog-layout'];
	$blog_layout_cols 		= $lSet['blog-layout-cols'];
	$blog_layout_cols_inner = $lSet['blog-layout-cols-inner'];
	$clearfix				= $lSet['clearfix'];
	$is_sidebar				= $lSet['is-sidebar'];
	$masonry				= $lSet['masonry'];
	$count = 1;
?>
<section id="blog" class="custom-padding" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/los_angeles.jpg)!important;">
  <div class="container"> 
    <!-- Row -->
    <div class="row">
    	
      <?php if($is_sidebar == 2){ get_sidebar(); }   ?>
      <!--div class="col-sm-12 col-xs-12 col-md-<?php echo esc_attr( $blog_layout_cols ); ?> nopadding"--> 
      <div class="col-sm-12 col-xs-12 col-md-12 nopadding"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                          <div class="panel-heading"><?php  echo __("<!--:fr-->A propos<!--:--><!--:en-->About<!--:-->");  ?></div>
                          <div class="panel-body custom_pannel_body">
                              <div>
                                  <img src="<?php echo get_template_directory_uri(); ?>/images/avion.jpg">
                              </div>
                              <?php  echo __("<!--:fr-->Text à propos<!--:--><!--:en-->Text about<!--:-->");  ?>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><?php  echo __("<!--:fr-->Notre services<!--:--><!--:en-->Our services<!--:-->");  ?></div>
                            <div class="panel-body custom_pannel_body">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/service.jpg">
                                </div>
                              <?php  echo __("<!--:fr-->Text notre services<!--:--><!--:en-->Text our services<!--:-->");  ?>
                            </div>
                        </div>
                    </div>
                </div> 
          
		<?php if($masonry == 'masonry'){echo '<div id="posts-masonry" class="posts-masonry">';}?>
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					//get_template_part( 'template-parts/blog-layout/blog', '' );
				?>
			<?php 
				// Add clraefix for only layout
                    if( $count % $clearfix == 0 ){  echo '<div class="clearfix"></div>'; };
					$count++;
            ?>
			<?php endwhile; ?>
		
		<?php else : ?>

			<?php //get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>
      	 <?php if($masonry == 'masonry'){echo '</div>';}?>
        
             <div class="clearfix"></div>
            <span class="separator"></span>
			<!--div class="custom-pagination text-center"> <?php logistic_pro_themes_pagination(); ?> </div-->
        
        
		</div> <!-- right column -->
       
	  <?php if($is_sidebar == 1){ get_sidebar(); }   ?>
      <!-- right column end--> 
      <!-- /.col-md-4 --> 
    </div>
    <!-- End row --> 
      </div>
</section>
<!-- =-=-=-=-=-=-= Blog & News end =-=-=-=-=-=-= --> 


<?php get_footer(); ?>
