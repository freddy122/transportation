<?php global $sb_themes; ?>
<?php 	
	$image 			= 	wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'logistic-pro-blog-1' );
	$categories 	=   get_the_category(); 
	
   $is_layout    = $sb_themes['blog-settings-look'];
   $is_sidebar   = $sb_themes['blog-settings-sidebar'];
   $is_masonry   = $sb_themes['blog-settings-type'];
	
	$lSet 		 = logistic_pro_themes_blogLayout($is_layout, $is_sidebar, $is_masonry);

	$blog_layout_cols_inner = $lSet['blog-layout-cols-inner'];
?>
<div class="col-md-<?php echo esc_attr($blog_layout_cols_inner); ?> col-sm-12 col-xs-12">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?> > 
        <div class="news-box">
            <div class="news-thumb <?php if ( $image[0] == "" && $is_masonry != "masonry" ){ esc_attr( 'no-img-1' );} ?>">
            <?php if ( $image[0] != "" ){ ?>
                <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() );?>" >
                     <img alt="<?php echo esc_attr( get_the_title() );?>" class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>">
                </a>
             <?php } ?>   
                <div class="date"> 
                <strong><?php echo esc_html( get_the_date('d') ); ?></strong>
                <span><?php echo esc_html( get_the_date('M') ); ?></span> </div>
            </div>
            <div class="news-detail">
                <h2>
                    <a href="<?php the_permalink() ?>" title="<?php esc_attr( get_the_title() ); ?>">
                        <?php the_title() ?>
                    </a>
                </h2>    
                <?php
            the_excerpt( sprintf(
                /* translators: %s: Name of current post. */
            wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'logistic-pro' ), array( 'span' => array( 'class' => array()))),
                the_title( '<span class="screen-reader-text">"', '"</span>', false )
            ) );
			
            ?>

              <?php
                    $args = array (
                        'before'            => '<div class="page-links-foodpro text-center">',
                        'after'             => '</div>',
                        'link_before'       => '<span class="btn cmnt_sbt">',
                        'link_after'        => '</span>',
                        'next_or_number'    => 'next',
                        'separator'         => '   ',
                        'nextpagelink'      => esc_html__( 'Next >>', 'logistic-pro' ),
                        'previouspagelink'  => esc_html__( '<< Prev', 'logistic-pro' ),
                    );
                     
                     wp_link_pages($args); 
                ?>            	
                <div class="entry-footer">
                <?php if($sb_themes['blog-settings-admin-meta'] || $sb_themes['blog-settings-admin-meta'] == ''){?>
                    <div class="post-meta">
                        <div class="post-like"> <i class="icon-calendar"></i> 
                         <a class="meta-link" href="#"> 
						<?php echo esc_html( get_the_date( get_option('date_format'), $post->ID ) );?>
                        </a>
                        </div>
                        <div class="post-comment"> <i class="icon-chat"></i> 
                            <a class="meta-link" href="<?php the_permalink(); ?>#comments"><?php echo get_comments_number( ); ?></a> </div>
                    </div>
                <?php } ?>
                </div>
                
            </div>
        </div>
    </div>
</div>