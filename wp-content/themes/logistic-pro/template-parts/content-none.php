<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Depilex Salon - Barber - Spa - Hairdresser - Yoga Template
 */

?>

<section class="no-results not-found">
	<div class="custom-heading">   
        	<h2><?php esc_html_e( 'Nothing Found', 'logistic-pro' ); ?></h2>
        </div><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ){ ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'logistic-pro' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php }elseif ( is_search() ){ ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'logistic-pro' ); ?></p>
					<div class="side-bar">
						<aside class="widget">
                        	<h2><?php esc_html_e( 'Search Here', 'logistic-pro' ); ?></h2>
                    		<?php get_search_form(); ?>
                         </aside>
                    </div>

		<?php }else { ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'logistic-pro' ); ?></p>
			<div class="side-bar">
						<aside class="widget">
                        	<h2><?php esc_html_e( 'Search Here', 'logistic-pro' ); ?></h2>
                    		<?php get_search_form(); ?>
                         </aside>
                    </div>

		<?php } ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
