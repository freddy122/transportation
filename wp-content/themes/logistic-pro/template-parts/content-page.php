<?php global $sb_themes; ?>
<?php 
	$image =  wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'logistic-pro-blog-1' );
?>
  <!-- page image -->
  <div class="news-thumb">
    <?php if ( $image[0] != "" ){ ?>
    <div class="news-thumb <?php if ( $image[0] == "" ){ esc_attr( 'no-img-1' );} ?>">
    
      <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() );?>" > <img alt="<?php echo esc_attr( get_the_title() );?>" class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>"> </a>
     
    </div>
     <?php } ?>
    <!-- page image end --> 
    <!-- page detail -->
    <div class="news-detail single">
    <?php if( $sb_themes['top-header-show-title'] ){ ?>
    <div class="custom-heading">            
      <h2><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>">
        <?php the_title(); ?>
        </a>
      </h2>
      </div>  
     <?php } ?>
      <?php
            the_content( sprintf(
                /* translators: %s: Name of current post. */
            wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'logistic-pro' ), array( 'span' => array( 'class' => array()))),
                the_title( '<span class="screen-reader-text">"', '"</span>', false )
            ) );
            ?>
            
                   <?php
                    $args = array (
                        'before'            => '<div class="page-links-foodpro text-center">',
                        'after'             => '</div>',
                        'link_before'       => '<span class="btn btn-primary">',
                        'link_after'        => '</span>',
                        'next_or_number'    => 'next',
                        'separator'         => '   ',
                        'nextpagelink'      => esc_html__( 'Next >>', 'logistic-pro' ),
                        'previouspagelink'  => esc_html__( '<< Prev', 'logistic-pro' ),
                    );
                     
                     wp_link_pages($args); 
                ?>            
    </div>
    <!-- page detail end --> 
    
  </div>
  <!-- page-grid end --> 
