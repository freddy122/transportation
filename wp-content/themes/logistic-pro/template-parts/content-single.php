<?php global $sb_themes; ?>
<?php 
	$image 			= 	wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'logistic-pro-blog-1' );
	$categories 	=   get_the_category(); 
	logistic_pro_themes_setPostViews( get_the_ID() );
?>
<!-- blog-grid -->

<div class="news-box no-space"> 
  <!-- post image -->
  <div class="news-thumb">
    <div class="news-thumb <?php if ( $image[0] == "" ){ esc_attr( 'no-img-1' );} ?>">
      <?php if ( $image[0] != "" ){ ?>
      <a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() );?>" > <img alt="<?php echo esc_attr( get_the_title() );?>" class="img-responsive" src="<?php echo esc_url( $image[0] ); ?>"> </a>
      <?php } ?>
      <div class="date"> <strong><?php echo esc_html( get_the_date('d') ); ?></strong> <span><?php echo esc_html( get_the_date('M') ); ?></span> </div>
    </div>
    <!-- post image end --> 
    
    <!-- blog detail -->
    <div class="news-detail single">
      <div class="custom-heading">
        <h2 class="h2-title"><a href="<?php the_permalink() ?>" title="<?php echo esc_attr( get_the_title() ) ?>">
          <?php the_title() ?>
          </a></h2>
      </div>
      <?php if($sb_themes['blog-settings-admin-meta'] || $sb_themes['blog-settings-admin-meta'] == ""){ ?>
      <div class="post-admin post-admin-data">
        <div class="post-admin"> <i class="icon-profile-male"></i>
          <?php the_author_posts_link(); ?>
        </div>
        <div class="post-comment"> <i class="icon-chat"></i> <a class="meta-link" href="<?php the_permalink(); ?>#comments">
          <?php comments_number('0'); ?>
          </a> </div>
        <div class="post-like"> <i class="icon-calendar"></i> <?php echo esc_html( get_the_date( get_option('date_format'), $post->ID ) );?> </div>
        <div class="post-like"> <i class="icon-grid"></i>
          <?php $separator = ', '; $output = ''; $get_the_cat = ''; if ( ! empty( $categories ) ) { foreach( $categories as $category ) { $get_the_cat .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'logistic-pro' ), esc_html( $category->name ) ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator; } echo rtrim( $get_the_cat, $separator ); } else{echo esc_html__('No Category', 'logistic-pro');} ?>
        </div>
      </div>
      <?php } ?>
      <div class="post-desc">
      <?php
            the_content( sprintf(
                /* translators: %s: Name of current post. */
            wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'logistic-pro' ), array( 'span' => array( 'class' => array()))),
                the_title( '<span class="screen-reader-text">"', '"</span>', false )
            ) );
            ?>
      <?php
                    $args = array (
                        'before'            => '<div class="page-links-foodpro text-center">',
                        'after'             => '</div>',
                        'link_before'       => '<span class="btn btn-primary">',
                        'link_after'        => '</span>',
                        'next_or_number'    => 'next',
                        'separator'         => '   ',
                        'nextpagelink'      => esc_html__( 'Next >>', 'logistic-pro' ),
                        'previouspagelink'  => esc_html__( '<< Prev', 'logistic-pro' ),
                    );
                     
                     wp_link_pages($args); 
                ?>
      <?php if($sb_themes['blog-settings-admin-meta'] || $sb_themes['blog-settings-admin-meta'] == ""){ ?>
      <div class="post-bottom clearfix">
        <div class="tag_cloud">
          <?php 
		  		$posttags = get_the_tags();
				$count=0;
				$tags	=	'';
				if ($posttags)
				{
				foreach($posttags as $tag)
				{
				   $count++;
				   if (0 < $count)
				   {
					 $tags .=  '<a href="'. esc_url( get_tag_link($tag->term_id) ).'" title="'. esc_attr( $tag->name ).'">'. esc_html( $tag->name ). '</a>';
				   }
				}
				  echo wp_kses( $tags, logistic_pro_theme_required_tags() ); 
				} 
				
				?>
                </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
