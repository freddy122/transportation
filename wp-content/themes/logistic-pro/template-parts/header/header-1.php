<?php global $sb_themes; 
$pgname = get_query_var("pagename");
?>
<header class="header-area"> 
  <!-- Logo Bar -->
  <div class="logo-bar">
    <div class="container clearfix"> 
      <!-- Logo -->
      <div class="logo"> <a href="<?php echo esc_url( home_url() );?>">
        <?php if( $sb_themes['site-logo']['url'] ){ ?>
        	<img src="<?php echo esc_url( $sb_themes['site-logo']['url'] ); ?>" alt="logo">
        <?php }else{?>
        	<img src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/logo.png" alt="logo">
        <?php } ?>
        </a>
      </div>
      <!--Info Outer-->
      <?php if( $sb_themes['header-switch-on'] ){ ?>
      <div class="information-content"> 
        <!--Info Box-->
        <?php if( $sb_themes['header-email-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-envelope"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-email-name'] ); ?></div>
          <a href="mailto:<?php echo esc_attr( $sb_themes['header-email-value'] ); ?>"> <?php echo esc_html( $sb_themes['header-email-value'] ); ?> </a> </div>
        <?php } ?>
        <!--Info Box-->
        <?php if( $sb_themes['header-phone-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-phone"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-phone-name'] ); ?></div>
          <a href="#"> <?php echo esc_html( $sb_themes['header-phone-value'] ); ?> </a> </div>
        <?php } ?>
        <!--Info Box-->
        <?php if( $sb_themes['header-contact-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-map"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-contact-name'] ); ?></div>
          <a href="#"> <?php echo esc_html( $sb_themes['header-contact-value'] ); ?> </a> </div>
        <?php } ?>
      </div>
      <?php } ?>
      
      <div style="float: right;">
      <?php echo do_shortcode("[do_widget id=qtranslate-2]"); ?>
      </div>
    </div>
  </div>
  <!-- Header Top End --> 
  
  <!-- Menu Section -->
  <div class="navigation-2"> 
    <!-- navigation-start -->
    <nav class="navbar navbar-default ">
      <div class="container"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false"> <span class="sr-only"><?php echo esc_html__('Toggle navigation', 'logistic-pro'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        
        <div class="collapse navbar-collapse" id="main-navigation">
                <ul class="nav navbar-nav ">
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>" class="<?php echo ($pgname == "" ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Accueil<!--:--><!--:en-->Home<!--:-->");
                            ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/contact/" class="<?php echo ($pgname == "contact" ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Contact<!--:--><!--:en-->Contact<!--:-->");
                            ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/services-10/" class="<?php echo (($pgname == "services-10" || $pgname == "demande-enregistrer"|| $pgname == "ancienne-transaction") ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Services<!--:--><!--:en-->Services<!--:-->");
                            ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/mails/" class="<?php echo ($pgname == "mails" ? "background_hover_menu":"") ;?>">
                            <?php 
                            
                                global $wpdb;
                                $user_id = get_current_user_id();
                                $type_user = get_user_meta($user_id, "type_user", true);
                                if($type_user == "client"){
                                    $quersUsercl = "SELECT count(lecture_message)   as lst_message from ".$wpdb->prefix."devis_mails where id_user = '".$user_id."' "
                                            . " and lecture_message ='non_lu'"
                                            . " and mail_type in ('proposition_devis','confirmation_devis_client') ";
                                    $resUsercl = $wpdb->get_results($quersUsercl, OBJECT);
                                    
                                    echo __("<!--:fr-->Messagerie<!--:--><!--:en-->Messaging<!--:-->");echo is_user_logged_in()?"<span class='dernierMessage'>(".$resUsercl[0]->lst_message.")</span>":"";
                                }else{
                                    $quersUserfwd = "SELECT count(lecture_message)   as lst_message from ".$wpdb->prefix."devis_mails where id_forwarder = '".$user_id."'"
                                            . " and lecture_message ='non_lu' "
                                            . "  and mail_type in ('new_devis','confirmation_devis')  ";
                                    $resUserfwd = $wpdb->get_results($quersUserfwd, OBJECT);
                                    
                                    echo __("<!--:fr-->Messagerie<!--:--><!--:en-->Messaging<!--:-->");echo is_user_logged_in()?"<span class='dernierMessage'>(".$resUserfwd[0]->lst_message.")</span>":"";
                                }
                                
                            ?>
                        </a>
                    </li>
                    <?php 
                        if(is_user_logged_in()):
                    ?>
                    <li class="dropdown">
                        <a href="<?php echo get_site_url() ;?>/connexion/" class="<?php echo (($pgname == "connexion" || $pgname == "inscription") ? "background_hover_menu":"") ;?> dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">
                            <?php 
                                echo __("<!--:fr-->Mon compte<!--:--><!--:en-->My account<!--:-->");
                            ?>
                            <span class="fa fa-angle-down"></span></a>
                        <ul class="dropdown-menu dropdownhover-bottom">
                            <li>
                                 <?php 
                                                if(is_user_logged_in()){
                                                    $current_user = wp_get_current_user();
                                                    //echo $current_user->display_name;
                                                    echo '<a href="'.get_site_url().'/connexion/edit-account/">'.__("<!--:fr-->Mon compte<!--:--><!--:en-->My account<!--:-->").'</a>';
                                                    echo '<a href="'.get_site_url().'/mention-legales">'.__("<!--:fr-->Vie privée<!--:--><!--:en-->Privacy<!--:-->").'</a>';
                                                    echo '<a href="'.esc_url( wc_logout_url( wc_get_page_permalink('myaccount'))).'">'.__("<!--:fr-->Déconnexion<!--:--><!--:en-->Logout<!--:-->").'</a>';
                                                }
                                            ?>
                            </li>
                        </ul>
                    </li>
                    
                    <?php else: ?>
                    
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/connexion/" class="<?php echo (($pgname == "connexion" || $pgname == "inscription") ? "background_hover_menu":"") ;?>">
                        <?php 
                            echo __("<!--:fr-->connexion<!--:--><!--:en-->connection<!--:-->");
                        ?>
                        </a>
                    </li>
                    <?php endif; ?>
                    
                </ul>
            <?php 
                if(is_user_logged_in()):
            ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a style="text-transform: none!important;"> <?php echo __("<!--:fr-->Bienvenue<!--:--><!--:en-->Welcome<!--:-->")." ".$current_user->user_login; ?></a></li>
                </ul>
            <?php 
                endif;
            ?>
          <?php //logistic_pro_web_custom_menu('primary', 'nav navbar-nav', 'dropdown-menu'); ?>
          <?php if( isset( $sb_themes['show-hide-place-order-button'] ) && $sb_themes['show-hide-place-order-button'] != 1){ ?>
          	<!--a class="btn btn-primary pull-right" data-toggle="modal" href="<?php if( isset( $sb_themes['top-header-order-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-order-page'] ) ); } ?>">
            
            <?php 
				if( isset( $sb_themes['show-hide-place-order-button-text'] ) )
				{ 
					echo esc_html( $sb_themes['show-hide-place-order-button-text'] );
				}
				else
				{ 
					echo esc_html__('Booking', 'logistic-pro'); 
				} ?>
             </a-->
          <?php } ?>   
        </div>
        <!-- /.navbar-collapse --> 
        
      </div>
      <!-- /.container-end --> 
    </nav>
  </div>
  <!-- /.navigation-end --> 
  <!-- Menu  End --> 
</header>
<?php if(is_user_logged_in()): ?>
<script>
    jQuery(document).ready(function(){
        alert_message();
    });
    
    function alert_message(){
        var PULL_INTERVAL   = 20000; // 30 secondes
        var pullMessageNotReadUrl = "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php";
        var pullAllMessageNotReadUrl = "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php";
        if (typeof pullMessageNotReadUrl !== 'undefined' && typeof pullAllMessageNotReadUrl !== 'undefined') {
            var TChat = {
                pullMessageNotReadUrl : pullMessageNotReadUrl,
                pullAllMessageNotReadUrl: pullAllMessageNotReadUrl,
                pullMethod : 'GET',
                pullMessageNotRead : function () { 
                    jQuery.ajax({
                        url : TChat.pullMessageNotReadUrl,
                        method: TChat.pullMethod,
                        data: {action : 'message_not_read'},
                        success: function (response) {
                            var resus = response.split('___');
                            jQuery('.class_proposition_devis').html(resus[0]) ;
                            jQuery('.class_accepted_devis').html(resus[1]) ;
                        }
                    });
                },
                pullAllMessageNotRead: function () { 
                    jQuery.ajax({
                        url : TChat.pullAllMessageNotReadUrl,
                        method: TChat.pullMethod,
                        data: {action : 'all_message_not_read'},
                        success: function (response) {
                            jQuery('.dernierMessage').html(response) ;
                        }
                    });
                }
            } ;
            
            // Mise à jour automatique nombre messages non lu.
            setInterval(function () {
                TChat.pullMessageNotRead()
            }, PULL_INTERVAL ) ;

            // Mise à jour automatique des listes des messages.
            setInterval(function () {
                TChat.pullAllMessageNotRead()
            }, PULL_INTERVAL) ;
        }
    }
    
</script>
<?php endif;

