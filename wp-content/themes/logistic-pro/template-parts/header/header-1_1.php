<?php global $sb_themes; 
$pgname = get_query_var("pagename");
?>
<header class="header-area"> 
  <!-- Logo Bar -->
  <div class="logo-bar">
    <div class="container clearfix"> 
      <!-- Logo -->
      <div class="logo"> <a href="<?php echo esc_url( home_url() );?>">
        <?php if( $sb_themes['site-logo']['url'] ){ ?>
        	<img src="<?php echo esc_url( $sb_themes['site-logo']['url'] ); ?>" alt="logo">
        <?php }else{?>
        	<img src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/logo.png" alt="logo">
        <?php } ?>
        </a>
      </div>
      <!--Info Outer-->
      <?php if( $sb_themes['header-switch-on'] ){ ?>
      <div class="information-content"> 
        <!--Info Box-->
        <?php if( $sb_themes['header-email-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-envelope"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-email-name'] ); ?></div>
          <a href="mailto:<?php echo esc_attr( $sb_themes['header-email-value'] ); ?>"> <?php echo esc_html( $sb_themes['header-email-value'] ); ?> </a> </div>
        <?php } ?>
        <!--Info Box-->
        <?php if( $sb_themes['header-phone-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-phone"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-phone-name'] ); ?></div>
          <a href="#"> <?php echo esc_html( $sb_themes['header-phone-value'] ); ?> </a> </div>
        <?php } ?>
        <!--Info Box-->
        <?php if( $sb_themes['header-contact-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-map"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-contact-name'] ); ?></div>
          <a href="#"> <?php echo esc_html( $sb_themes['header-contact-value'] ); ?> </a> </div>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- Header Top End --> 
  
  <!-- Menu Section -->
  <div class="navigation-2"> 
    <!-- navigation-start -->
    <nav class="navbar navbar-default ">
      <div class="container"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false"> <span class="sr-only"><?php echo esc_html__('Toggle navigation', 'logistic-pro'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        
        
        
        
       
        <div class="collapse navbar-collapse" id="main-navigation">
            <div class="collapse navbar-collapse" id="main-navigation">
                <ul class="nav navbar-nav ">
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>" class="<?php echo ($pgname == "" ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Accueil<!--:--><!--:en-->Home<!--:-->");
                            ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/contact/" class="<?php echo ($pgname == "contact" ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Contact<!--:--><!--:en-->Contact<!--:-->");
                            ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/services-10/" class="<?php echo (($pgname == "services-10" || $pgname == "demande-enregistrer"|| $pgname == "ancienne-transaction") ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Services<!--:--><!--:en-->Services<!--:-->");
                            ?>
                        </a>
                    </li>
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/mails/" class="<?php echo ($pgname == "mails" ? "background_hover_menu":"") ;?>">
                            <?php 
                                echo __("<!--:fr-->Mails<!--:--><!--:en-->Email<!--:-->");
                            ?>
                        </a>
                    </li>
                    <?php 
                        if(is_user_logged_in()):
                    ?>
                    <li class="dropdown">
                        <a href="<?php echo get_site_url() ;?>/connexion/" class="<?php echo (($pgname == "connexion" || $pgname == "inscription") ? "background_hover_menu":"") ;?> dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">
                            <?php 
                                echo __("<!--:fr-->Mon compte<!--:--><!--:en-->My account<!--:-->");
                            ?>
                            <span class="fa fa-angle-down"></span></a>
                        <ul class="dropdown-menu dropdownhover-bottom">
                            <li>
                                 <?php 
                                                if(is_user_logged_in()){
                                                    $current_user = wp_get_current_user();
                                                    //echo $current_user->display_name;
                                                    echo '<a href="'.get_site_url().'/connexion/">'.__("<!--:fr-->Mon compte<!--:--><!--:en-->My account<!--:-->").'</a>';
                                                    echo '<a href="'.get_site_url().'/mention-legales">'.__("<!--:fr-->Vie privée<!--:--><!--:en-->Privacy<!--:-->").'</a>';
                                                    echo '<a href="'.esc_url( wc_logout_url( wc_get_page_permalink('myaccount'))).'">'.__("<!--:fr-->Déconnexion<!--:--><!--:en-->Logout<!--:-->").'</a>';
                                                }
                                            ?>
                            </li>
                        </ul>
                    </li>
                    
                    <?php else: ?>
                    
                    <li class="">
                        <a href="<?php echo get_site_url() ;?>/connexion/" class="<?php echo (($pgname == "connexion" || $pgname == "inscription") ? "background_hover_menu":"") ;?>">
                        <?php 
                            echo __("<!--:fr-->connexion<!--:--><!--:en-->connection<!--:-->");
                        ?>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php //echo do_shortcode("[do_widget id=qtranslate-3]"); ?>
                </ul>
            </div>
          <?php //logistic_pro_web_custom_menu('primary', 'nav navbar-nav', 'dropdown-menu'); ?>
          <?php if( isset( $sb_themes['show-hide-place-order-button'] ) && $sb_themes['show-hide-place-order-button'] != 1){ ?>
          	<!--a class="btn btn-primary pull-right" data-toggle="modal" href="<?php if( isset( $sb_themes['top-header-order-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-order-page'] ) ); } ?>">
            
            <?php 
				if( isset( $sb_themes['show-hide-place-order-button-text'] ) )
				{ 
					echo esc_html( $sb_themes['show-hide-place-order-button-text'] );
				}
				else
				{ 
					echo esc_html__('Booking', 'logistic-pro'); 
				} ?>
             </a-->
          <?php } ?>   
        </div>
        <!-- /.navbar-collapse --> 
        
      </div>
      <!-- /.container-end --> 
    </nav>
  </div>
  <!-- /.navigation-end --> 
  <!-- Menu  End --> 
</header>

