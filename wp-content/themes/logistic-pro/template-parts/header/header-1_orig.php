<?php global $sb_themes; ?>
<header class="header-area"> 
  <!-- Logo Bar -->
  <div class="logo-bar">
    <div class="container clearfix"> 
      <!-- Logo -->
      <div class="logo"> <a href="<?php echo esc_url( home_url() );?>">
        <?php if( $sb_themes['site-logo']['url'] ){ ?>
        	<img src="<?php echo esc_url( $sb_themes['site-logo']['url'] ); ?>" alt="logo">
        <?php }else{?>
        	<img src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/logo.png" alt="logo">
        <?php } ?>
        </a>
      </div>
      <!--Info Outer-->
      <?php if( $sb_themes['header-switch-on'] ){ ?>
      <div class="information-content"> 
        <!--Info Box-->
        <?php if( $sb_themes['header-email-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-envelope"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-email-name'] ); ?></div>
          <a href="mailto:<?php echo esc_attr( $sb_themes['header-email-value'] ); ?>"> <?php echo esc_html( $sb_themes['header-email-value'] ); ?> </a> </div>
        <?php } ?>
        <!--Info Box-->
        <?php if( $sb_themes['header-phone-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-phone"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-phone-name'] ); ?></div>
          <a href="#"> <?php echo esc_html( $sb_themes['header-phone-value'] ); ?> </a> </div>
        <?php } ?>
        <!--Info Box-->
        <?php if( $sb_themes['header-contact-name'] ){ ?>
        <div class="info-box  hidden-sm">
          <div class="icon"><span class="icon-map"></span></div>
          <div class="text"><?php echo esc_html( $sb_themes['header-contact-name'] ); ?></div>
          <a href="#"> <?php echo esc_html( $sb_themes['header-contact-value'] ); ?> </a> </div>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- Header Top End --> 
  
  <!-- Menu Section -->
  <div class="navigation-2"> 
    <!-- navigation-start -->
    <nav class="navbar navbar-default ">
      <div class="container"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false"> <span class="sr-only"><?php echo esc_html__('Toggle navigation', 'logistic-pro'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        
        <div class="collapse navbar-collapse" id="main-navigation">
          <?php logistic_pro_web_custom_menu('primary', 'nav navbar-nav', 'dropdown-menu'); ?>
          <?php if( isset( $sb_themes['show-hide-place-order-button'] ) && $sb_themes['show-hide-place-order-button'] != 1){ ?>
          	<a class="btn btn-primary pull-right" data-toggle="modal" href="<?php if( isset( $sb_themes['top-header-order-page'] ) ){ echo esc_url( get_the_permalink( $sb_themes['top-header-order-page'] ) ); } ?>">
            
            <?php 
				if( isset( $sb_themes['show-hide-place-order-button-text'] ) )
				{ 
					echo esc_html( $sb_themes['show-hide-place-order-button-text'] );
				}
				else
				{ 
					echo esc_html__('Booking', 'logistic-pro'); 
				} ?>
             </a>
          <?php } ?>   
        </div>
        <!-- /.navbar-collapse --> 
        
      </div>
      <!-- /.container-end --> 
    </nav>
  </div>
  <!-- /.navigation-end --> 
  <!-- Menu  End --> 
</header>

