<?php global $sb_themes; ?>
<header class="header-area">
  <div class="navigation"> 
    <!-- navigation-start -->
    <nav class="navbar navbar-default">
      <div class="container"> 
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false"> <span class="sr-only"><?php echo esc_html__('Toggle navigation', 'logistic-pro'); ?></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          
          <a href="<?php echo esc_url( home_url() );?>" class="navbar-brand">
        <?php if( $sb_themes['site-logo']['url'] ){ ?>
        	<img src="<?php echo esc_url( $sb_themes['site-logo']['url'] ); ?>" alt="logo" class="img-responsive" >
        <?php }else{?>
        	<img src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/logo.png" alt="logo" class="img-responsive" >
        <?php } ?>
        </a>

</div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="main-navigation">
          <?php logistic_pro_web_custom_menu('primary', 'nav navbar-nav navbar-right custom-nav', 'dropdown-menu'); ?>
        </div>
        <!-- /.navbar-collapse --> 
      </div>
      <!-- /.container-end --> 
    </nav>
  </div>
</header>
