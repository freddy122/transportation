<?php global $sb_themes; ?>
<?php 

if($sb_themes['website-type'] == '1')
{	
	if( ( ( is_home() || is_front_page() ) && $sb_themes['onepage-slides-settings'] == 0)  || $sb_themes['onepage-slides-settings'] == 1){
		
		
		 
?>
<div id="main-top" class="main-top">
<div class="main-content">
<div class="container content">
    <div class="row text-center">
       <h2> 
			<?php 
                foreach( $sb_themes['onepage-slides-text'] as $ar => $val )
                {
					if($ar == "fixed_text")
					{
						echo esc_html( $val );
					}
					
					if($ar == "rotating_text")
					{
						echo ' <span id="js-rotating"> '. esc_html( $val ). ' </span> ';
					}
					if($sb_themes['onepage-slides-line-break'] == 1){ echo '<br />';}
                }
            
            ?>
           </h2>		       	
    </div>
    <div class="row text-center">
    <?php 
			$moveId = '#one-page';
			if($sb_themes['onepage-move-to'] != "")
			{ 
				$moveId = $sb_themes['onepage-move-to'];
			}
	?>
        <a href="<?php echo esc_url( $moveId ); ?>" class="scroll-down page-scroll"></a>
    </div>
</div>
</div>
</div>
<?php }} ?>

<div class="clearfix"></div>
<!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
<div id="one-page">
<header class="header-area transparent">
<div class="navigation">
    <!-- navigation-start -->
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button aria-expanded="false" data-target="#main-navigation" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                 <span class="sr-only"><?php echo esc_html__('Toggle navigation', 'logistic-pro'); ?></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                </button>
                <a href="<?php echo esc_url( home_url() );?>" class="navbar-brand">
        <?php if( $sb_themes['site-logo']['url'] ){ ?>
        	<img src="<?php echo esc_url( $sb_themes['site-logo']['url'] ); ?>" alt="logo" class="img-responsive" >
        <?php }else{?>
        	<img src="<?php echo trailingslashit( get_template_directory_uri () ); ?>images/logo.png" alt="logo" class="img-responsive" >
        <?php } ?>
        </a>
            </div>
            <div class="collapse navbar-collapse" id="main-navigation">
                <?php logistic_pro_web_custom_menu('onepage', 'nav navbar-nav navbar-right custom-nav', 'dropdown-menu'); ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-end -->
    </nav>
</div>
</header>
</div>
<!-- =-=-=-=-=-=-= HEADER END =-=-=-=-=-=-= -->