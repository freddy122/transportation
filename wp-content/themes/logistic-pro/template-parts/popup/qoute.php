<?php global $sb_themes; ?>
<!-- =-=-=-=-=-=-= Quote Modal =-=-=-=-=-=-= -->
<div class="modal fade " id="request-quote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="quotation-box-1">
          <h2>
		  	<?php if( $sb_themes['qoute-title-text'] != "")
			{
					echo esc_html( $sb_themes['qoute-title-text'] );
			}
			else
			{
				echo esc_html__('REQUEST A QUOTE', 'logistic-pro');	
			}; 
			?></h2>
          <div class="desc-text">
            <p>
		  	<?php if( $sb_themes['qoute-tagline-text'] != "")
			{
					echo esc_html( $sb_themes['qoute-tagline-text'] );
			}
			else
			{
				echo esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'logistic-pro');	
			}; 
			?>            
            </p>
          </div>
          <form method="post" id="popup-qoute-form">
            <div class="row clearfix"> 
              	<div class="popup-msg-box form-group col-md-12 col-sm-12 col-xs-12"></div>
              <!--Form Group-->
              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" type="text" placeholder="<?php echo esc_attr__('Your Name', 'logistic-pro'); ?>" value="" id="your-name-qoute" name="your_name" required >
              </div>
              <!--Form Group-->
              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <input class="form-control" type="email" placeholder="<?php echo esc_attr__('Email Address', 'logistic-pro'); ?>" value="" id="your-email-qoute" name="your_email" required >
              </div>
              <!--Form Group-->
              <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <input class="form-control" type="text" placeholder="<?php echo esc_attr__('Subject', 'logistic-pro'); ?>" value="" id="your-subject-qoute" name="your_subject" required >
              </div>
              <div class="form-group col-md-12 col-sm-12 col-xs-12">
              
              <?php 
			  
					$typecargo = logistic_pro_cargo_types();
					$typrArrs =  wp_kses( $typecargo, logistic_pro_theme_required_tags() );
					$optionsTypes = '';
					if( count( $typrArrs ) > 0 )
					{
						foreach( $typrArrs as $typrArr)
						{
							$optionsTypes .= '<option value="'.$typrArr.'">'.$typrArr.'</option>';	
						}
					}
			  ?>
              
                <select class="form-control" id="select-cargo-type-qoute" name="select_cargo_type" required >
                   <?php echo $optionsTypes;?>
                   </select>
              </div>
              <!--Form Group-->
              <div class="form-group col-md-6 col-sm-12 col-xs-12">
                <select class="form-control" id="select-location-qoute" name="select_location" required >
                  <option value=""><?php echo esc_attr__('Select Your Location', 'logistic-pro'); ?></option>
                    <?php 
						$countries = CountriesArray::get2d(null, array( 'name' ));
						foreach( $countries as $country)
						{		
							echo '<option value="'. esc_attr( $country['name'] ) .'">'. esc_html( $country['name'] ) .'</option>';				
						}
						
						?>
                </select>
              </div>
              <div class="form-group col-md-6 col-sm-12 col-xs-12">
                <select class="form-control" id="select-rlocation-qoute" name="select_rlocation" required >
                  <option value=""><?php echo esc_attr__('Select Destination', 'logistic-pro'); ?></option>
                    <?php 
						$countries = CountriesArray::get2d(null, array( 'name' ));
						foreach( $countries as $country)
						{		
							echo '<option value="'. esc_attr( $country['name'] ) .'">'. esc_html( $country['name'] ) .'</option>';				
						}
						
						?>
                </select>
              </div>
              <!--Form Group-->
              <div class="form-group col-md-12 col-sm-12 col-xs-12">
                <textarea class="form-control" rows="7" cols="20" placeholder="<?php echo esc_attr__('Your Message', 'logistic-pro'); ?>" id="your-message-qoute" name="your_message" required ></textarea>
              </div>
              
              <!--Form Group-->
              <div class="form-group col-md-12 col-sm-12 col-xs-12 text-right">
              <a href="javascript:void(0)" class="requesting custom-button light hidden-thing"><i class="fa fa-spinner" aria-hidden="true"></i>
 <?php echo esc_html__('Requesting...', 'logistic-pro'); ?></a> 
              	<a class="custom-button light" id="sb_submit_qoute">
					<?php if( $sb_themes['qoute-button-text'] != "")
                    {
                            echo esc_html( $sb_themes['qoute-button-text'] );
                    }
                    else
                    {
                        echo esc_html__('Submit Request', 'logistic-pro');	
                    }; 
                    ?> 			  		
                </a> 
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- =-=-=-=-=-=-= Quote Modal End =-=-=-=-=-=-= --> 
