<style>
    .cutom_div_left{
        border: 1px solid #d3ced2!important;
        padding: 20px!important;
        margin: 2em 0!important;
        text-align: left!important;
        border-radius: 5px!important;
    }
</style>
<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if(isset($_GET['q'])){ ?>
    <ul class="woocommerce-error" role="alert">
    <li>
        <strong>
            <?php echo __("<!--:fr-->Succès<!--:--><!--:en-->Success<!--:-->"); ?>&nbsp;&nbsp;&nbsp;
        </strong>
        <?php echo __("<!--:fr-->Votre compte est maintenant actif; veuillez vous connecter.<!--:--><!--:en-->Your account is now active; please login.<!--:-->"); ?>
    </li>
</ul>
<?php
    update_user_meta(base64_decode($_GET['q']), "has_confirm_email", 1);
}
$pagename = get_query_var('pagename'); 
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if(isset($_GET['flag_confirm_mail'])): ?>
<ul class="woocommerce-error" role="alert">
    <li>
        <strong>
            <?php echo __("<!--:fr-->Succès<!--:--><!--:en-->Success<!--:-->"); ?>&nbsp;&nbsp;&nbsp;
        </strong>
        <?php echo __("<!--:fr-->Votre compte a été crée veuillez consulter votre mail et suivre les instructions pour le confirmer.<!--:--><!--:en-->Your account has been created please check your email and follow the instructions to confirm it.<!--:-->"); ?>
    </li>
</ul>
<?php endif; ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' && $pagename == "inscription" ) : ?>

<!--div class="u-columns col1-set" id="customer_login">

	<div class="u-column1 col-1"-->

<?php endif; ?>
<?php if($pagename == "connexion"): ?>
		<!--h2><?php esc_html_e( 'Login', 'woocommerce' ); ?></h2-->
            <div class="row">
                <div class="col-md-6">
                    
                    <form class="woocommerce-form woocommerce-form-login login" method="post">
                        <h2>
                            <?php  echo __("<!--:fr-->Vous avez dejà un compte ?<!--:--><!--:en-->Do you already have an account ?<!--:-->");  ?>
                        </h2>
                        <?php do_action( 'woocommerce_login_form_start' ); ?>
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="username">
                                    <?php  echo __("<!--:fr--> Identifiant/adresse email<!--:--><!--:en-->Login / email address<!--:-->"); ?>
                                    <?php //esc_html_e( 'Username or email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                                <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                        </p>
                        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                <label for="password"><?php  echo __("<!--:fr-->Mot de passe<!--:--><!--:en-->Password<!--:-->");  ?><?php //esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                <input class="woocommerce-Input woocommerce-Input--text input-text form-control" type="password" name="password" id="password" />
                        </p>
                        <?php do_action( 'woocommerce_login_form' ); ?>

                        <p class="form-row">
                                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
                                        <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span>
                                           <?php  echo __("<!--:fr-->Mémoriser mon identifiant<!--:--><!--:en-->Remember me<!--:-->");  ?>
                                           <?php //esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
                                </label>
                                <br>
                                <button type="submit" class="woocommerce-Button button btn btn-primary" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
                        </p>
                        <p class="woocommerce-LostPassword lost_password">
                            <!--a href="<?php echo get_site_url() ?>/index.php/inscription">Pas de compte veuillez vous inscrire</a-->
                        </p>
                        <p class="woocommerce-LostPassword lost_password">
                                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>">
                                    <?php  echo __("<!--:fr-->J’ai oublié mon mot de passe<!--:--><!--:en-->I forgot my password<!--:-->");  ?>
                                    <?php //esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
                        </p>
                        <?php do_action( 'woocommerce_login_form_end' ); ?>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="cutom_div_left">
                        <h2>
                            <?php  echo __("<!--:fr-->Vous n’avez pas de compte ?<!--:--><!--:en-->Do not have an account?<!--:-->");  ?>
                        </h2>
                        <span class="toogle_class" style="cursor:pointer;color: #016db6 !important;">
                           <?php  echo __("<!--:fr-->Créer mon compte<!--:--><!--:en-->Create Account<!--:-->");  ?> 
                        </span>
                        <form method="post" class="custom_toogle" style="display:none;" id="register_user">
                            <?php do_action( 'woocommerce_register_form_start' ); ?>
                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                        <label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                    </p>
                            <?php endif; ?>
                            <div class="display_info" style="display: none;">
                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                                    <input type="email"  required="required" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                </p>
                                <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>(<?php  echo __("<!--:fr-->8 caractères minimum (Chiffre ou lettre)<!--:--><!--:en-->8 characters minimum (number or letter)<!--:-->");  ?>) <span class="required">*</span></label>
                                            <input type="password" pattern=".{8,}"   required title="<?php  echo __("<!--:fr-->8 caractères minimum (Chiffre ou lettre)<!--:--><!--:en-->8 characters minimum (number or letter)<!--:-->");  ?>" required="required" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="password" id="reg_password" />
                                    </p>
                                    <p class="form-row form-row-wide">
                                        <label for="reg_billing_first_name">
                                            <?php  echo __("<!--:fr-->Répéter mot de passe<!--:--><!--:en-->Repeat password<!--:-->");  ?>
                                            
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <input required="required" type="password" class="input-text  form-control" name="repeat_password" id="repeat_password" title="<?php  echo __("<!--:fr-->Veuillez fournir la même valeur.<!--:--><!--:en-->Please provide the same value<!--:-->");  ?>" />
                                    </p>
                                <?php endif; ?>
                                <?php do_action( 'woocommerce_register_form' ); ?>
                                <p>
                                    <input required='required' type ='checkbox' name='termecond' value='1' class='chbx_termecond' title="<?php  echo __("<!--:fr-->Ce champ est obligatoire.<!--:--><!--:en-->This field is required.<!--:-->");  ?>"> 
                                    <?php  echo __("<!--:fr-->Je déclare avoir lu et accepté les<!--:--><!--:en-->I declare to have read and accepted<!--:-->");  ?>
                                    <a href="<?php echo get_site_url(); ?>/mention-legales" target="_blank">
                                        <?php  echo __("<!--:fr-->termes et conditions d'utilisation<!--:--><!--:en-->the terms and conditions of use<!--:-->");  ?>
                                        </a>
                                </p>
                                <p class="woocommerce-FormRow form-row">
                                        <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                        <button type="submit" class="woocommerce-Button button btn btn-primary" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
                                </p>
                                <?php do_action( 'woocommerce_register_form_end' ); ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
<?php endif;?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' && $pagename == "inscription") : ;?>
	<!--/div-->
	<!--div class="u-column2 col-2"-->
		<!--h2><?php esc_html_e( 'Register', 'woocommerce' ); ?></h2-->
		<form method="post" class="register" >
                    
			<?php do_action( 'woocommerce_register_form_start' ); ?>
			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
				</p>
			<?php endif; ?>
                        <div class="display_info" style="display: none;">
                            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="reg_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?> <span class="required">*</span></label>
                                    <input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                            </p>
                            <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                                    <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                            <label for="reg_password"><?php esc_html_e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
                                            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="password" id="reg_password" />
                                    </p>
                            <?php endif; ?>
                            <?php do_action( 'woocommerce_register_form' ); ?>
                            <p>
                                <input required='required' type ='checkbox' name='termecond' value='1' class='chbx_termecond' data-msg-required="(Vous dévez accepter nos condition)"> Je déclare avoir lu et accepté les <a href="<?php echo get_site_url(); ?>/mention-legales" target="_blank">termes et conditions d'utilisation</a>
                            </p>

                            <p class="woocommerce-FormRow form-row">
                                    <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                                    <button type="submit" class="woocommerce-Button button btn btn-primary" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
                            </p>
                            <?php do_action( 'woocommerce_register_form_end' ); ?>
                        </div>
                        
		</form>
	<!--/div>
</div-->
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
<script src="<?php echo  get_bloginfo("template_url")."/js/validate/jquery.validate.js"; ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSYzE5NAIFz3hovepBqQ91NluAS7DqgRA&libraries=places&callback=initMap" async defer></script>
<script>
    function initMap() {
        var input = (document.getElementById('ville_user'));
        var input2 = (document.getElementById('adresse_user'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {});
        var autocomplete2 = new google.maps.places.Autocomplete(input2);
        autocomplete2.addListener('place_changed', function() {});
    }
    jQuery('document').ready(function(){
        jQuery('.toogle_class').click(function(){
            jQuery('.custom_toogle').toggle("slow")
        });
        //jQuery('input[type=radio][name=type_user]').change(function() {
        jQuery('#type_user').change(function() {
            
            var val_type_user = jQuery('#type_user').val();
            
            if(val_type_user == "forwarder"){
                jQuery(".display_info").show();
                jQuery("#div_categ_service").show();
                jQuery("#nom_entreprise").show();
                jQuery("#num_enreg_soc").show();
                jQuery("#num_compte").show();
                jQuery("#transaction_nationale").show();
                jQuery("#chargement_equipement").show();
                jQuery("#type_vehicule").show();
                jQuery("#matiere_danger").show();
                jQuery("#credit_card_user").hide();
                jQuery("#credit_card_usert").prop('required',false);
                jQuery("#type_vehiculet").prop('required',true);
                jQuery("#categorie_service").prop('required',true);
                jQuery("#nom_entrepriset").prop('required',true);
                jQuery("#num_enreg_soct").prop('required',true);
                jQuery("#num_comptet").prop('required',true);
                jQuery("#transaction_nationalet").prop('required',true);
                jQuery("#chargement_equipementt").prop('required',true);
                jQuery("#matiere_dangert").prop('required',true);
            }else if(val_type_user == "client"){
                jQuery(".display_info").show();
                jQuery("#div_categ_service").hide();
                jQuery("#nom_entreprise").hide();
                jQuery("#num_enreg_soc").hide();
                jQuery("#num_compte").hide();
                jQuery("#transaction_nationale").hide();
                jQuery("#chargement_equipement").hide();
                jQuery("#type_vehicule").hide();
                jQuery("#matiere_danger").hide();
                jQuery("#matiere_dangert").prop('required',false);
                jQuery("#type_vehiculet").prop('required',false);
                jQuery("#chargement_equipementt").prop('required',false);
                jQuery("#transaction_nationalet").prop('required',false);
                jQuery("#num_enreg_soct").prop('required',false);
                jQuery("#nom_entrepriset").prop('required',false);
                jQuery("#categorie_service").prop('required',false);
                jQuery("#num_comptet").prop('required',false);
                jQuery("#categorie_service").val('');
                jQuery("#credit_card_user").show();
                jQuery("#credit_card_usert").prop('required',true);
            }else{
                /*jQuery("#info_presta").hide();
                jQuery("#btn_validation_freelance").hide();*/
                jQuery(".display_info").hide();
            }
        });
        jQuery( "#register_user" ).validate({
            rules: {
              reg_password: "required",
              repeat_password: {
                equalTo: "#reg_password"
              },
              
              credit_card_usert: {
                    required: true,
                    number: true,
                    rangelength:[15,15]
              }
            }
          });
    })
</script>
