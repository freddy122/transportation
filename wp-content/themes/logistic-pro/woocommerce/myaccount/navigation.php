<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-body style_paddin_pannels_custom ">
                <ul class="navs_custom">
                    <!--li>
                        <a class="<?php //echo ($pagenames == "ancienne-transaction") ? "active":""; ?>" href="<?php //echo get_site_url()."/ancienne-transaction"; ?>">
                          <?php 
                              //echo __("<!--:fr-->Ancienne transaction<!--:--><!--:en-->Old transaction<!--:-->");
                          ?>
                        </a>
                    </li-->
                    <li>
                        <a class="<?php echo (is_wc_endpoint_url( 'edit-account' )) ? "active":""; ?>" href="<?php echo get_site_url()."/connexion/edit-account/"; ?>">
                          <?php  
                              echo __("<!--:fr-->Détails du compte<!--:--><!--:en-->Account details<!--:-->");
                          ?>
                        </a>
                    </li>
                    <li>
                        <a class="<?php echo ($pagenames == "demande-enregistrer") ? "active":""; ?>" href="<?php echo esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) ); ?>">
                          <?php 
                              echo __("<!--:fr-->Déconnexion<!--:--><!--:en-->Logout<!--:-->");
                          ?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
<!--nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav-->

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
