<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
//current-menu-item
$pgname = get_query_var("pagename");

if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) || is_active_sidebar( 'sidebar-1' )  ) : ?>
	<div id="secondary" class="secondary">

		<?php if ( has_nav_menu( 'primary' ) ) : ?>
			
                        <nav id="site-navigation" class="main-navigation" role="navigation">
                            <div class="menu-menu-haut-container">
                                <ul id="menu-menu-haut" class="nav-menu">
                                    <li id="menu-item-25" class="<?php echo ($pgname ==""? "current-menu-item":"") ;?> menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-25"><a href="http://localhost/transportation/">Accueil</a></li>
                                    <li id="menu-item-24" class="<?php echo ($pgname =="contact"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-24"><a href="http://localhost/transportation/contact/">Contact</a></li>
                                    <li id="menu-item-23" class="<?php echo ($pgname =="services"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-23"><a href="http://localhost/transportation/services/">Services</a></li>
                                    <li id="menu-item-22" class="<?php echo ($pgname =="mails"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-22"><a href="http://localhost/transportation/mails/">Mails</a></li>
                                    <li id="menu-item-26" class="<?php echo ($pgname =="connexion"? "current-menu-item":"") ;?> menu-item menu-item-type-post_type menu-item-object-page menu-item-26"><a href="http://localhost/transportation/connexion/">Connexion</a></li>
                                </ul>
                            </div>			
                        </nav>
		<?php endif; ?>

		<?php if ( has_nav_menu( 'social' ) ) : ?>
			<nav id="social-navigation" class="social-navigation" role="navigation">
				<?php
					// Social links navigation menu.
					wp_nav_menu( array(
						'theme_location' => 'social',
						'depth'          => 1,
						'link_before'    => '<span class="screen-reader-text">',
						'link_after'     => '</span>',
					) );
				?>
			</nav><!-- .social-navigation -->
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<div id="widget-area" class="widget-area" role="complementary">
				<?php //dynamic_sidebar( 'sidebar-1' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>

	</div><!-- .secondary -->

<?php endif; ?>
